@extends("layouts.base")

@section("content")

  <div class="content">
    <div class="container">
      <div class="row">

        <!--        Overview of news Nyhedsoverblik-->
        <div class="col-md-8">
          @yield("content-column")
        </div>

        <!-- Seneste nyt-->
        <div class="col-md-4 col-news-latest">
          @section("news-column")
            <div class="news-latest">
              <div class="head">Latest news</div>
              <ul class="nav nav-justified gray font-proxima-b">
                <li class="active" ><a href="#">Database</a> </li>
                <li><a href="#">Courses</a></li>
                <li><a href="#">Jobs</a></li>
              </ul>
            </div>
            <div class="news-text font-merriweather">
              @foreach($feedNews as $article)
                <a href="{{route('db.content', ["id" => $article->id])}}">
                  <div class="news-type font-proxima">
                    @if($icon = $article->getIconType())
                      <img src="{{$icon}}" class="show-icon16" alt="icon" />
                    @endif
                    {{$article->getTypeName()}}</div>
                  <div class="summary-short">{{$article->summary_short}}</div>
                  <div class="gray">{{date("d-m-Y", strtotime($article->date_publication)) . ", ".$article->tab->name}}</div>
                </a>
              @endforeach
              {{--<a href="#">
                <div class="news-type font-proxima"><img src="/plugins/template/images/twitter.png" class="show-icon16" alt="icon" />Dagbladsrtikel</div>
                <div class="summary-short">Ej krav pa erstatning mv. i familiesammenforingssag</div>
                <div class="gray">26-08-2014, Jyllandsposten</div>
              </a>
              <a href="#">
                  <div class="news-type font-proxima"><img src="/plugins/template/images/twitter.png" class="show-icon16" alt="icon" />DOM</div>
                <div class="summary-short">Lejebetaling ved bankoverforsel</div>
                <div class="gray">26-08-2014, Ufr. 2014.201 H</div>
              </a>
              <a href="#">
                <div class="news-type font-proxima"><img src="/plugins/template/images/twitter.png" class="show-icon16" alt="icon" />Dagbladsrtikel</div>
                <div class="summary-short">Ej krav pa erstatning mv. i familiesammenforingssag</div>
                <div class="gray">26-08-2014, Jyllandsposten</div>
              </a>
               <a href="#">
                <div class="news-type font-proxima"><img src="/plugins/template/images/twitter.png" class="show-icon16" alt="icon" />DOM</div>
                <div class="summary-short">Lejebetaling ved bankoverforsel</div>
                <div class="gray">26-08-2014, Ufr. 2014.201 H</div>
              </a>
              <a href="#">
                <div class="news-type font-proxima"><img src="/plugins/template/images/twitter.png" class="show-icon16" alt="icon" />Dagbladsrtikel</div>
                <div class="summary-short">Ej krav pa erstatning mv. i familiesammenforingssag</div>
                <div class="gray">26-08-2014, Jyllandsposten</div>
              </a>  --}}
            </div>
            <a href="#" class="see-more font-merriweather"> See more<img class="pull-right" src="/img/arrow-right.png" alt="see-more"/> </a>
          @show
        </div>

      </div>
    </div>
  </div>

@stop
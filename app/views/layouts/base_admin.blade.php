<!DOCTYPE html>
<html>
  <head>
    <title>{{$_siteTitle}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">
    @section("styles")
    <!-- Bootstrap -->
    <link href="/plugins/template/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="/plugins/template/css/bootstrap-glyphicons.css" rel="stylesheet" media="screen">
    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="/plugins/template/favicon.ico">
      <link href="/plugins/template/css/style.css" rel="stylesheet">
      <link rel="stylesheet" href="/plugins/template/css/icon/font-awesome.css">
      <link rel="stylesheet" href="/styles/back-style.css">

      <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
      <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
      <script src="/plugins/template/js/respond.min.js"></script>
      <![endif]-->
      <script language="JavaScript">
        Firefox = navigator.userAgent.indexOf("Firefox") >= 0;
        if(Firefox) document.write("<link rel='stylesheet' href='/css/moz.css' type='text/css'>");
      </script>
    @show
  </head>
  <body>
  
  @yield("content")


  @section("scripts")
    {{--<script src="/plugins/template/js/jquery.min.js"></script>--}}
    <script src="/js/jquery-2.1.1.min.js" type="text/javascript"></script>
    {{--<script src="/plugins/template/js/jquery-ui.min.js"></script>--}}

    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="/plugins/template/js/bootstrap.min.js"></script>
    {{--<script src="/plugins/template/js/google-code-prettify/prettify.js"></script>

    <script src="/plugins/template/js/jquery.flot.js"></script>
    <script src="/plugins/template/js/jquery.flot.pie.js"></script>
    <script src="/plugins/template/js/jquery.flot.orderBars.js"></script>
    <script src="/plugins/template/js/jquery.flot.resize.js"></script>
    <script src="/plugins/template/js/jquery.flot.categories.js"></script>
    <script src="/plugins/template/js/graphtable.js"></script>
    <script src="/plugins/template/js/fullcalendar.min.js"></script>
    <script src="/plugins/template/js/chosen.jquery.min.js"></script>
    <script src="/plugins/template/js/autoresize.jquery.min.js"></script>
    <script src="/plugins/template/js/jquery.autotab.js"></script>
    <script src="/plugins/template/js/jquery.jgrowl_minimized.js"></script>
    <script src="/plugins/template/js/jquery.dataTables.min.js"></script>
    <script src="/plugins/template/js/jquery.stepy.min.js"></script>
    <script src="/plugins/template/js/jquery.validate.min.js"></script>
    <script src="/plugins/template/js/raphael.2.1.0.min.js"></script>
    <script src="/plugins/template/js/justgage.1.0.1.min.js"></script>
    <script src="/plugins/template/js/glisse.js"></script>
    <script src="/plugins/template/js/styleswitcher.js"></script>--}}
    <script src="/plugins/template/js/moderniz.js"></script>
    {{--<script src="/plugins/template/js/jquery.sparkline.min.js"></script>
    <script src="/plugins/template/js/slidernav-min.js"></script>
    <script type="text/javascript" src="/plugins/template/js/jquery.fancybox.js?v=2.1.4"></script>--}}

    <script>
      Modernizr.load([
        {
          load: [
            '/plugins/template/js/main.js'
          ],
          complete: function()
          {
            $.fn.ready(function()
            {
              window.App.init();
            });
          }
        }
      ]);
    </script>
    {{--<script src="/plugins/template/js/application.js"></script>--}}
    @show
  </body>
</html>     


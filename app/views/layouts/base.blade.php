<!DOCTYPE html>
<html>
  <head>
    <title>{{$_siteTitle}}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    @section("styles")
    <!-- Bootstrap -->
    <link href="/plugins/bootstrap-3.3.4/css/bootstrap.min.css" rel="stylesheet"  type="text/css" media="screen">
    <link href="/styles/front-style.css" rel="stylesheet"  type="text/css" >
    <!-- Le fav and touch icons -->
    {{-- <link rel="shortcut icon" href="/plugins/template/favicon.ico"> --}}
    @show
  </head>
  <body>
  @section("header")
    @include("partials.front.header")
  @show

  @section("messages")
    {{ Alert::renderAll() }}
  @show

  @yield("content")

  @section("footer")
    @include("partials.front.footer")
  @show  

  @section("scripts")
    <script src="/js/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="/plugins/bootstrap-3.3.4/js/bootstrap.min.js" type="text/javascript"></script>

    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-58718869-2', 'auto');
      ga('send', 'pageview');

    </script>

  @show
  </body>
</html>     


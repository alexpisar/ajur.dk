<!DOCTYPE html>
<html lang="en-US">
<head>
  <meta charset="utf-8">
</head>
<body>
<h2>Message from <a href="{{ URL::to("/") }}">Ajur.dk</a></h2>

<div>
  <h4>Support inquiry: </h4>
    <br />Name : {{ $name }}<br>
    E-mail : {{ $email }}<br>
    Phone : {{ $phone }}<br>
    Inquiry : {{ $inquiry }}
</div>

</body>
</html>
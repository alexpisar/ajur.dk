<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Welcome to <a href="{{ URL::to("/") }}">Ajur.dk</a></h2>

<div>
    Your account has been created on <a href="{{ URL::to("/") }}">Ajur.dk</a><br>
    Login : {{ $login }}<br>
    Password : {{ $password }}<br>
</div>

</body>
</html>
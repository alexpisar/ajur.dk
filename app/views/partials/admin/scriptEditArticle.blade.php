<script type="text/javascript">
    //start plugin
    $("document").ready(function(){
        $('#tree').jstree({
            "core" : {
                "check_callback" : true,
                "data" : {
                    "url" : "{{route("getTree")}}",
                    "data" : function (node) {
                        return { "id" : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "icon-folder-open"
                },
                "root" : {
                    "icon" : "icon-sitemap"
                },
                "topic" : {
                    "icon" : "icon-book"
                },
                "subtopic" : {
                    "icon" : "icon-folder-open"
                },
                "tab" : {
                    "icon" : "icon-table"
                },
                "article" : {
                    "icon" : "icon-file"
                }
            },
            "plugins" : ["sort", "types" ] //"dnd",
            //"contextmenu" : {"items": customMenu}
        });

    });
    var selectedText = "";
    var selectedType = "";
    var selectedID = "";

    $("#show-tree").on('click', function( event ) {


    });

    $("#select-tab").on('click', function( event ) {
        if(!selectedType || selectedType != "tab") {
            event.preventDefault();
            return;
        }
       // console.log(selectedText);
        //var text = $("#tree").jstree(true).get_selected()[0].node;
        $("#selected_tab").val(selectedText);
        $("#sel-tab_id").val(selectedID);
    });


    $('#tree').on("select_node.jstree", function (e, obj) {
        //e.preventDefault();
        var inst = $.jstree.reference(obj.node);

       // console.log(obj.node);
       // byID = inst.get_node("t1");
        selectedType = obj.node.type;

        if(selectedType == "tab") {
            var parents = obj.node.parents.slice();
            parents.reverse();
            var text = "";

            $.each(parents, function (index, value) {
                if(index == 0 || index == 1)
                    return true;

                text = inst.get_node(value).text;
                if(inst.get_node(value).type == "subtopic")
                    text = text.substring(text.indexOf(')') + 2);

                if(index == 2) {
                    selectedText = text;
                    return true;
                }
                selectedText += " - " + text;

            });
            selectedText += " - " + obj.node.text;
            selectedID = obj.node.id;
        }
       // console.log(selectedText);
    });

</script>
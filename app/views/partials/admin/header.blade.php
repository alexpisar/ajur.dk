    <!--BEGIN HEADER-->
    <div id="header" role="banner">
       <a id="menu-link" class="head-button-link menu-hide" href="#menu"><span>Menu</span></a>
       <!--Logo--><a href="{{route('dashboard')}}" class="logo"></a><!--Logo END-->
       <h1 class="logo-text">AJUR.DK</h1>
       <div class="right">
       {{--
       <!--message box-->
         <div class="dropdown left">
          
          <a class="dropdown-toggle head-button-link" data-toggle="dropdown" href="#"><span class="notice-new">1</span></a> 
          
          <div class="dropdown-menu pull-right messages-list">
          <div class="triangle"></div>
          
            <div class="notice-title">You Have 1 Massages</div>
            <!-- message #1 -->
            <div class="notice-message">
            <a href="#" class="box">
              <div class="avatar"><img src="/plugins/template/images/avatar-2.png" alt=""></div>
              <div class="info">
                <div class="author">Csaba Gyulai</div>
                <div class="date">Jan 9, 2013</div>
                <div class="clearfix"></div>
                <div class="text">This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit.</div>
              </div>
              <div class="clearfix"></div>
            </a>
            </div>
            <!--message #2-->
 
            <!--message #3-->
         
            <a href="#" class="notice-more">View All Messages</a>
          </div>
        </div>
       <!--message box end-->
       --}}

       <!--profile box-->
         <div class="dropdown left profile">
          <a class="dropdown-toggle" data-toggle="dropdown" href="#">
            <span class="double-spacer"></span>
            <div class="profile-avatar"><img src="/plugins/template/images/avatar.png" alt=""></div>
            <div class="profile-username"><span>Welcome, </span>{{Auth::user()->name}}</div>
            <div class="profile-caret"> <span class="caret"></span></div>
            <span class="double-spacer"></span>
          </a>
          <div class="dropdown-menu pull-right profile-box">
          <div class="triangle-3"></div>
          
            <ul class="profile-navigation">
              <li><a href="#"><i class="icon-user"></i> My Profile</a></li>
              <li><a href="#"><i class="icon-cog"></i> Settings</a></li>
              <li><a href="#"><i class="icon-info-sign"></i> Help</a></li>
              <li><a href="{{route('login.logout')}}"><i class="icon-off"></i> Logout</a></li>
            </ul>
          </div>
        </div>
        <div class="clearfix"></div>
       <!--profile box end-->
       
       </div>
       
      
    </div>
    <!--END HEADER-->
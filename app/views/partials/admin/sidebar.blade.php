<!--BEGIN SIDEBAR-->
<div id="menu" role="navigation">

  <ul class="main-menu">
     <li class="{{$_activeMenu == 'dashboard' ? 'active':''}}"><a href="{{route('dashboard')}}"><i class="general"></i> Dashboard</a></li>
     <li class="{{$_activeMenu == 'database' ? 'active':''}}"><a href="{{route('database')}}"><i class="forms"></i> Database</a></li>
      <li class="{{$_activeMenu == 'content' ? 'active':''}}"><a href="{{route('content.manage')}}"><i class="forms"></i>Content</a></li>
     <li class="{{$_activeMenu == 'subscriptions' ? 'active':''}}"><a href="{{route('subscriptions')}}"><i class="components"></i> Subscriptions</a></li> {{--route('subscriptions')--}}
     <li class="{{$_activeMenu == 'pricing' ? 'active':''}}"><a href="{{route('pricing')}}"><i class="components"></i> Pricing</a></li>
     <li class="{{$_activeMenu == 'advertisements' ? 'active':''}}"><a href="{{route('advertisements')}}"><i class="components"></i> Advertisements</a></li>
     <li><a href="#"><i class="components"></i> Optional</a></li>
     <li class="{{$_activeMenu == 'statistics' ? 'active':''}}"><a href="{{route('statistics')}}"><i class="statistics"></i> Statistics</a></li>
     <li class="{{$_activeMenu == 'staff' ? 'active':''}}"><a href="{{route("users.staff")}}"><i class="components"></i> Staff</a></li>
     <li class="{{$_activeMenu == 'settings' ? 'active':''}}"><a href="{{route("settings")}}"><i class="components"></i> Settings</a></li>
     <li class="{{$_activeMenu == 'static' ? 'active':''}}"><a href="{{route("static.list")}}"><i class="components"></i> Static pages</a></li>
  </ul>
 
  <div class="clearfix"></div>
</div>
  @section("messages")
    {{ Alert::renderAll() }}
  @show
<!--SIDEBAR END-->


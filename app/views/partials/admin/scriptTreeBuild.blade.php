<script type="text/javascript">
    //styles
    var deleteIconStyle = "red-icon icon-remove-sign",
            createIconStyle = "green-icon icon-plus-sign",
            editIconStyle = "blue-icon icon-pencil",
            moveIconStyle = "blue-icon icon-move",
            cutIconStyle = "blue-icon icon-cut",
            pasteIconStyle = "blue-icon icon-paste",
            copyIconStyle = "blue-icon icon-copy",
            uploadIconStyle = "green-icon icon-upload",
            defaultIconStyle = "icon-folder-open",
            topicIconStyle = "icon-book",
            subtopicIconStyle = "icon-folder-open",
            tabIconStyle = "icon-table",
            articleIconStyle = "icon-file",
            rootIconStyle = "icon-sitemap";

    //start plugin
    $("document").ready(function(){
        $('#tree').jstree({
            "core" : {
                "check_callback" : true,
                "data" : {
                    "url" : "{{route("getTree")}}",//,"//localhost/treejson/?lazy"
                    "data" : function (node) {
                        return { "id" : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                  "icon" : defaultIconStyle
                },
                "root" : {
                    "icon" : rootIconStyle
                },
                "topic" : {
                    "icon" : topicIconStyle
                },
                "subtopic" : {
                    "icon" : subtopicIconStyle
                },
                "tab" : {
                    "icon" : tabIconStyle
                },
                "article" : {
                    "icon" : articleIconStyle
                }
            },
            "plugins" : ["contextmenu", "sort", "types" ], //"dnd",
            "contextmenu" : {"items": customMenu}
           /* "sort" : function (a, b) {
                //if(! weight !== undefined)
                return this.get_node(a).original.weight > this.get_node(b).original.weight ? 1 : -1;
            }*/
        });

    });


    //different menus
    function customMenu(node) {
        // The default set of all items
        //console.log(node.type);
        switch (node.type){
            case "root":
                return {
                    "create": {
                        "icon"  : createIconStyle,
                        "label" : "New topic",
                        "action": function (data){
                            createTreeObject(data, "topic");
                        }
                    },
                    "paste" : {
                        "icon"              : pasteIconStyle,
                        "_disabled"         : function (data) {
                            var inst = $.jstree.reference(data.reference);
                            //console.log(inst.get_buffer().node[0]);
                            return !($.jstree.reference(data.reference).can_paste()
                            && inst.get_buffer().node[0].type == "subtopic");
                        },
                        "label"             : "Paste",
                        "action"            : pasteObjectAction
                    }
                };
                break;

            case "topic"    :
            case "subtopic" :
                return {
                    "new": { //submenu
                        "icon"       : createIconStyle,
                        "label"      : "New ...",
                        "action"     : false,
                        "submenu" : {
                            "create1" : {
                                "label"				: "New topic",
                                "icon"              : createIconStyle,
                                "action"			: function (data) {
                                    createTreeObject(data, "subtopic");
                                }
                            },
                            "create2": {
                                "label"				: "New tab",
                                "icon"              : createIconStyle,
                                "action"			: function (data) {
                                    createTreeObject(data, "tab");
                                }
                            }
                        }
                    },
                    "rename":{
                        "label"		: "Rename",
                        "icon"      : editIconStyle,
                        "action"    : renameTreeObject
                    },
                    "weight":{
                        "label"		: "Set weight",
                        "icon"      : editIconStyle,
                        "action"    : setWeight
                    },
                    "ccp" : {
                        "icon"              : moveIconStyle,
                        "label"             : "Copy/Move",
                        "action"            : false,
                         "submenu" : {
                            "copy" : {
                                "icon"              : copyIconStyle,
                                "label"             : "Copy",
                                "action"            : copyObjectAction
                            },
                            "cut" : {
                                "icon"              : cutIconStyle,
                                "label"             : "Cut",
                                /*"_disabled"         : function (data) {
                                    var inst = $.jstree.reference(data.reference);
                                    return inst.get_node(data.reference).type != "subtopic"; //all folders are subtopic
                                },*/
                                "action"            : cutObjectAction
                            },
                            "paste" : {
                                "icon"              : pasteIconStyle,
                                "_disabled"         : function (data) {
                                    var inst = $.jstree.reference(data.reference);
                                    //console.log(inst.get_buffer().node[0]);
                                    return !($.jstree.reference(data.reference).can_paste()
                                            && inst.get_buffer().node[0].type != "article");
                                },
                                "label"             : "Paste",
                                "action"            : pasteObjectAction
                            }
                        }
                    },
                    "remove":{
                        "label"    : "Delete",
                        "icon"      : deleteIconStyle,
                        "action"    : removeTreeObject
                    }
                };
                break;

            case "tab":
                return{
                    "create":{
                        "icon"  : createIconStyle,
                        "label" : "New content",
                        "action": function (data){
                            var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                            window.location.href = "{{route('content.article.new')}}" + "/?tab_id=" + obj.id;
                           // createTreeObject(data, "article");
                        }
                    },
                    "upload" :{
                        "icon"  : uploadIconStyle,
                        "label" : "Show/Upload icon",
                        "action": uploadIcon
                    },
                    "rename" :{
                        "icon"  : editIconStyle,
                        "label" : "Rename",
                        "action": renameTreeObject
                    },
                    "ccp" : {
                        "icon"              : moveIconStyle,
                        "label"             : "Copy/Move",
                        "action"            : false,
                        "submenu" : {
                            "copy" : {
                                "icon"              : copyIconStyle,
                                "label"             : "Copy",
                                "action"            : copyObjectAction
                            },
                            "cut" : {
                                "icon"              : cutIconStyle,
                                "label"             : "Cut",
                                "action"            : cutObjectAction //function
                            },
                            "paste" : {
                                "icon"              : pasteIconStyle,
                                "_disabled"         : function (data) {
                                    var inst = $.jstree.reference(data.reference);
                                    //console.log(inst.get_buffer().node[0]);
                                    return !($.jstree.reference(data.reference).can_paste()
                                    && inst.get_buffer().node[0].type == "article");
                                },
                                "label"             : "Paste",
                                "action"            : pasteObjectAction
                            }
                        }
                    },
                    "remove" :{
                        "icon"   : deleteIconStyle,
                        "label"  : "Delete",
                        "action" : removeTreeObject
                    }
                };
                break;

            case "article":
                return {
                    "rename":{
                        "icon"  : editIconStyle,
                        "label" : "Edit",
                        "action": function (data){
                            var inst = $.jstree.reference(data.reference),
                                    obj = inst.get_node(data.reference);
                            window.location.href = "{{route('content.article.edit')}}" + "/?id=" + obj.id;
                        }
                    },
                    "ccp" : {
                        "icon"              : moveIconStyle,
                        "label"             : "Copy/Move",
                        "action"            : false,
                        "submenu" : {
                            "copy" : {
                                "icon"              : copyIconStyle,
                                "label"             : "Copy",
                                "action"            : copyObjectAction
                            },
                            "cut" : {
                                "icon"              : cutIconStyle,
                                "label"             : "Cut",
                                "action"            : cutObjectAction
                            }

                        }
                    },
                    "remove" :{
                        "icon"  : deleteIconStyle,
                        "label" : "Delete",
                        "action": removeTreeObject
                    }
                };
            break;
        }
    }


    /*
    =====================================================
     */

    //menu actions
    function createTreeObject(data, type){
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        inst.create_node(obj, {}, "last", function (new_node) {
            new_node.type = type;
            switch(type){
                case "topic":
//                    new_node.icon = topicIconStyle;
//                    break;
                case "subtopic":
                    new_node.icon = subtopicIconStyle;
                    break;
                case "tab":
                    new_node.icon = tabIconStyle;
                    break;
            }
            setTimeout(function () {
                inst.edit(new_node);
            }, 0);
        });
    }

    function removeTreeObject (data) {
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        if(inst.is_selected(obj)) {
            inst.delete_node(inst.get_selected());
        }
        else {
            inst.delete_node(obj);
        }
    }
        //rename
    function renameTreeObject (data) {
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        if(obj.type == "subtopic"){
            obj.text = obj.text.substring(obj.text.indexOf(')') + 2);
            //var txt = obj.text;
            //console.log(txt.substri);
        }
        inst.edit(obj);
    }

    var tempNode = null; //for upload icon

    function uploadIcon(data){
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        tempNode = obj.original;
       // console.log(obj.original);
        $("#tab_id").attr("value", obj.original.id);
        //clear input file
       // $("#inputFile").replaceWith($("#inputFile").val('').clone(true));
        $("#inputFile").filestyle('clear');
        $('#modalUploads').modal('show');
        if(obj.original.iconName){//not null
           // console.log("icon name not null");
            $("#no-icon").hide();
            $("#show-icon img").attr("src", obj.original.iconName);
            $("#show-icon").show();
        }
        else{
            //console.log(obj.original);
            $("#show-icon").hide();
            $("#no-icon").show();
        }
    }

    function setWeight(data){
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        tempNode = obj.original; //for quick find parent
        //$("#topic_id").attr("value", obj.original.id);
        $("#topic_id").val(obj.original.id);
        $("#inputWeight").val(obj.original.weight);
        $('#modalWeight').modal('show');

    }

    function copyObjectAction(data){
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        if(inst.is_selected(obj)) {
            inst.copy(inst.get_selected());
        }
        else {
            inst.copy(obj);
        }
    }

    function cutObjectAction(data){
        var inst = $.jstree.reference(data.reference),
                obj = inst.get_node(data.reference);
        if(inst.is_selected(obj)) {
            inst.cut(inst.get_selected());
            // console.log(inst.get_selected());
        }
        else {
            inst.cut(obj);
            // console.log(obj);
        }
    }

    function pasteObjectAction(data){
        var inst = $.jstree.reference(data.reference),
        obj = inst.get_node(data.reference);
        inst.paste(obj);
    }
    /* ======================================
    //event handlers
    */

    //create  (& rename)
    $('#tree').on("rename_node.jstree", function (e, obj) {
       // console.log(obj);
        e.preventDefault();
        var inst = $.jstree.reference(obj.node);
        var parentNode = inst.get_parent(obj.node);



        $.ajax({
           "url"            : "{{route("tree.create")}}",
            "type"          : "POST",
            "datatype"      : "JSON",
            "contentType"   : "application/json charset=utf-8",
            "data"          : JSON.stringify(obj.node),
            "success"       : function (data){
                if(data.success){
                    console.log("success rename");
                }
                else{
                    alert('Something went to wrong.Please Try again later...');
                }
               // console.log(data);
                inst.refresh_node(parentNode);
            },
            "error"         : function (xhr, textStatus, thrownError){
                alert('Something went to wrong.Please Try again later...');
                inst.refresh_node(parentNode);
                console.log(thrownError);
            }
        });
    });


    $('#tree').on("delete_node.jstree", function (e, obj) {
        // console.log(obj);
        e.preventDefault();
        var inst = $.jstree.reference(obj.node);
        var parentNode = inst.get_parent(obj.node);
        $.ajax({
            "url"            : "{{route("tree.delete")}}" + "/?id=" + obj.node.id,
            "type"          : "GET",
            "success"       : function (data){
                if(data.success){
                    console.log("success delete");
                }
                else{
                    alert('Something went to wrong.Please Try again later...');
                    inst.refresh_node(parentNode);
                }
                // console.log(data);

            },
            "error"         : function (xhr, textStatus, thrownError){
                alert('Something went to wrong.Please Try again later...');
                inst.refresh_node(parentNode);
                console.log(thrownError);
            }
        });
    });

    $('#tree').on("move_node.jstree", function (e, obj) {
        e.preventDefault();

        if(obj.parent == obj.old_parent)
            return;
        var inst = $.jstree.reference(obj.node);
        //var parentNode = inst.get_parent(obj.node);
       // obj.node["old_parent"] = obj.old_parent;//couldn't serialize obj. It's need if would be Many To Many articles
       // console.log(obj.node);
        $.ajax({
            "url"            : "{{route("tree.move")}}",
            "type"          : "POST",
            "datatype"      : "JSON",
            "contentType"   : "application/json charset=utf-8",
            "data"          : JSON.stringify(obj.node),
            "success"       : function (data){
                if(data.success){
                    console.log("success move");
                }
                else{
                    alert('Something went to wrong.Please Try again later...');
                    inst.refresh();
                }
                // console.log(data);
                //inst.refresh_node(parentNode);
            },
            "error"         : function (xhr, textStatus, thrownError){
                alert('Something went to wrong.Please Try again later...');
                inst.refresh();
                console.log(thrownError);
            }
        });
    });

    $('#tree').on("copy_node.jstree", function (e, obj) {
        e.preventDefault();
        //console.log(obj);
        //if(obj.original.parent == obj.node.parent)
         //   return;
        var inst = $.jstree.reference(obj.node);
        var parentNode = inst.get_parent(obj.node);
        obj.node["original_id"] = obj.original.id;
       // console.log(obj.node);
        $.ajax({
            "url"            : "{{route("tree.copy")}}",
            "type"          : "POST",
            "datatype"      : "JSON",
            "contentType"   : "application/json charset=utf-8",
            "data"          : JSON.stringify(obj.node),
            "success"       : function (data){
                if(data.success){
                    console.log("success copy");
                    inst.refresh_node(parentNode);
                }
                else{
                    alert('Something went to wrong.Please Try again later...');
                    inst.refresh();
                }
            },
            "error"         : function (xhr, textStatus, thrownError){
                alert('Something went to wrong.Please Try again later...');
                inst.refresh();
                console.log(thrownError);
            }
        });

    });

    $('#tree').on("dblclick.jstree", function (e) {
        var instance = $('#tree').jstree(true);
        var node = instance.get_selected(true)[0];
        //console.log(node);
        if(node !== undefined && node.type == "article")
            window.location.href = "{{route('content.article.edit')}}" + "/?id=" + node.id;
    });


    // ============ Modal dialog
    //Tab Icon
    $("#formUploadIcon").submit(function( event ) {
        //console.log("clickForm");
        var inst = $.jstree.reference(tempNode);
        var parentNode = inst.get_parent(tempNode);

        //console.log(tempNode);
        var $form = $(this),
               // data = $form.serialize(),
                url = $form.attr("action");

        $.ajax( {
            url: url,
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.success) {
                    console.log("success upload");
                    inst.refresh_node(parentNode);
                    tempNode = null;
                }
                else {
                    alert('Something went to wrong.Please Try again later...');
                }
                $('#modalUploads').modal('hide');
            }

        } );

        event.preventDefault();
    });

    //Set Weight
    $("#formWeight").submit(function( event ) {
        //console.log("clickForm");
        var inst = $.jstree.reference(tempNode);
        var parentNode = inst.get_parent(tempNode);

        //console.log(tempNode);
        var $form = $(this),
        // data = $form.serialize(),
                url = $form.attr("action");

        $.ajax( {
            url: url,
            type: 'POST',
            data: new FormData( this ),
            processData: false,
            contentType: false,
            success: function (data) {
                if (data.success) {
                    console.log("success set weight");
                    inst.refresh_node(parentNode);
                    tempNode = null;
                    $('#modalWeight').modal('hide');
                }
                else {
                    alert('Something went to wrong.Please Try again later...');
                }

            }
        });
        event.preventDefault();
    });

</script>
       {{-- <div id="support" class="font-proxima"><button type="button" class="btn btn-default" data-container="body" data-toggle="popover" data-placement="bottom" data-content="Vivamus sagittis lacus vel augue laoreet rutrum faucibus."> 
  SUPPORT
</button></div>--}}     
      <div id="support" class="dropdown"><a href="#" data-toggle="dropdown" class="font-proxima-b" role="button">SUPPORT</a>
             <div class="dropdown-menu left gray font-merriweather">
                {{--<div class="arrow"></div> --}}
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{Form::open(['route' => 'support', "id" => "support-form"])}}
                {{--Form::hidden("support_is_open", false, ["id" => "support_is_open"])--}}
                <h4 class="text-center">Support</h4>
                  {{Form::text("name", null, ["class" => "form-control", "placeholder"=> "Name", "maxlength"=> "255", "required" => "required"])}}
                  <div class="spacer-1em"></div>
                  {{Form::email("email", null, ["class"=> "form-control", "placeholder"=> "E-mail", "maxlength"=> "255", "required" => "required"])}}
                  <div class="spacer-1em"></div>
                  {{Form::text("phone", null, ["class"=> "form-control", "placeholder"=> "Telephone number", "maxlength"=> "255", "required" => "required"])}}
                  <div class="spacer-1em"></div>
                  {{Form::textarea("inquiry", null, ["class"=>"form-control", "required" => "required", "rows" => "5", "placeholder" => "Inquiry", "required" => "required"])}}
                  <div class="spacer"></div>
                  {{Form::submit("Send", ["class" => "btn btn-default bold font-proxima pull-right", "id" => "send_support"])}}
                 {{Form::close()}}
            </div>       

      </div>
@section("scripts")
 @parent
    <script type="text/javascript">
        $(document).ready(function() {
           {{-- submit captcha ajax --}}
          $("#support-form").submit(function( event ) {
              //console.log("clickForm");
              event.preventDefault();
              var form = $(this),
               formData = form.serializeArray(),
              
                 url = form.attr("action");
              $.post(url, formData, 
                function(data){
                  if(data.success){
                      //clear form
                      $("#support-form input[type=text]").val('');
                      $("#support-form input[type=email]").val('');
                      $("#support-form textarea").val('');
                      //hide window
                       $("#support>a").dropdown("toggle");
                  }
                  alert(data.msg);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    alert("{{t('globals.support_error')}}");
                        });
          });
          
          
          {{--centering login window --}}
           $('#login-dropdown').on('shown.bs.dropdown', function () {
              //console.log($(this).find('.dropdown-menu').outerWidth());
              var left1, left2;
              var boxItem = document.getElementById('login-dropdown').getBoundingClientRect();
              //length from x menu item to right border screen
              var x = window.innerWidth -  boxItem.left;
              //center of menu item
              var c = boxItem.width / 2;
              //coordinate of window login
              var w2 = $("#login-window").outerWidth() / 2;
              if(x - c - w2 < 15){ // 15 - right margin
                left1 = 15 + w2 * 2 - x;
              }else{ //if dosn't need move, put to center
                left1 = w2 - c;
              }
              //coordinate of triangle window
              var left2 = Math.round(left1 + c); //15 center triangle
              left1 = -Math.round(left1);
              //window.innerWidth
                $('#login-window').css('left', left1 + 'px');
                $('#triangle').css('left', left2 + 'px');
            });
        });
    </script>
@stop
<div class="gray-line"></div>
<!-- Header - banner-->
  <div class="header">
      <div class="container">
          <div class="row">
              <div class="col-sm-1 logo-head">
                    <a href="/" class="logo"></a>
              </div>
              <div class="col-sm-7">
                  <ul class="nav navbar-nav">
                    @foreach($topMenu as $item)
                      <li> <a href="{{$item["url"]}}">{{$item["title"]}}</a></li>
                    @endforeach  
                  </ul>
               <div class="clearfix"></div>      
                    <ul class="nav navbar-nav main-menu">
                      <li class="active"> <a href="{{route('db')}}">{{t("globals.menu.database")}}</a></li>
                      <li> <a href="#">{{t("globals.menu.courses")}}</a></li>
                      <li> <a href="#">{{t("globals.menu.jobs")}}</a></li>
                  </ul>                 
              </div>
              <div class="col-sm-4">
                  <ul class="nav navbar-nav pull-right login">
                      <li> <a href="{{route("profile")}}">{{t("globals.menu.profile")}}</a></li>
                      <li class="dropdown" id="login-dropdown"> 
                      @if(Auth::check())
                         <a class="gray" href="{{route('login.logout')}}">{{t("globals.menu.sign_out")}} </a>
                      @else
                        <a class="gray" data-toggle="dropdown" href="#"> {{t("globals.menu.sign_in")}}</a>
                      <div id="login-window" class="dropdown-menu bottom gray font-merriweather">
                          <div id="triangle" class="arrow"></div>
                          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            {{Form::open(["route" => "login.user"])}}
                               <a href="/admin/login">(Login to backend)</a>
                               <div class="clearfix spacer-1em"></div>
                                {{Form::email("email", null, ["class" => "form-control", "title" => ucfirst(t('validation.attributes.email')),
                                          "placeholder" => ucfirst(t('validation.attributes.email')), "required"=>"required" ])}}
                                {{Form::password("password", ["class" => "form-control spacer-1em", "title" => ucfirst(t('validation.attributes.password')), "required"=>"",
                                          "placeholder"=> ucfirst(t('validation.attributes.password'))])}} <br />
                                <p class="remember checkbox">
                                    <label>
                                    {{Form::checkbox('remember_me', 1, false)}}
                                    <span>{{ucfirst(t("globals.remember_me"))}}</span></label>
                                </p>
                                <p class="login-btn">
                                    {{--<a class="btn btn-default font-proxima pull-right" href="#" role="button">SIGN IN</a> --}}
                                    {{Form::submit(strtoupper(t("globals.menu.sign_in")), ["class" => "btn btn-default font-proxima pull-right"])}}
                                </p>
                             {{Form::close()}}
                          <div class="clearfix"></div>
                          <div class="spacer"></div>
                          <a class="btn sign-up" href="{{route("teaser")}}" role="button">{{t("globals.sign_up")}} </a>
                          <a class="btn forgot" href="{{route("password.lost")}}" role="button"> {{t('globals.forgot_password')}}</a>
                          <div class="clearfix"></div>
                      </div>
                    @endif
                      </li>
                  </ul> 
                  <div class="clearfix"></div>
                    <div id="search" class="form-group has-feedback">
                      {{Form::open(["route" => "search.simple", "method" => "GET"])}}
                        <input type="search" class="form-control" name="query">
                        <span class="glyphicon glyphicon-search form-control-feedback gray"></span>
                      {{Form::close()}}
                  </div>
              </div>
          </div>
      </div>
  </div>
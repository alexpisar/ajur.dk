  <div class="advertisement">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12">
                <!--<img src="../public/uploads/advertisements/test-advert.png" class="footer-advert" alt=""/>-->
                <img class="footer-advert" src="/uploads/advertisements/advertisement.png" alt="advertisement"/>
            </div>
        </div>
    </div>
  </div>
<div class="footer">
    <div class="container">
        <div class="row company">
            <div class="col-md-8 compaign">
                <div class="head26 font-playfair-b">
                    En forsikrings- og erstatningsretling informationsportal
                </div>
                <p class="font-merriweather">Sed ut dui lobortis lacus porta euismod. Integer eget auctor enim. Praesent nee eros molestie,
                    malesuada lectus et, condimentum odio. Maecenas egestas commodo preti.Nulla eu congue massa, 
                    quis fermen odio. Nunc viverra aliquam tellus, vitae accunisl vehicula id.</p>
            </div>
            <div class="col-xs-6 col-md-2">
                <div class="head28 font-playfair-b">
<!--                    Bliv medlem-->
{{--<span class="nowrap">Become a </span> <br />member--}}
					Become a member
                </div>
                <a href="#" class="btn-pricing text-center font-playfair-b"> Pricing</a>
                <a class="pull-left left-icon" href="#"><img src="/img/icons/android.png" alt="android"/></a>
                <a class="pull-left left-icon" href="#"><img src="/img/icons/apple.png" alt="apple"/></a>
            </div>
            <div class="col-xs-6 col-md-2">
                <img src="/img/icons/ipad.png" alt="mobile"/>
            </div>
        </div>

    </div>
    <div class="footer menu">
        <div class="container">
            <div class="row font-merriweather gray">
                <div class="col-sm-8">
                     <ul class="nav navbar-nav">
                        @foreach($bottomMenu as $item)
                          <li> <a href="{{$item["url"]}}">{{$item["title"]}}</a></li>
                        @endforeach 
                     </ul>
                    <div class="clearfix"></div>
                    <ul class="nav navbar-nav white">
                      <li class="active"> <a href="{{route('db')}}">{{t("globals.menu.database")}}</a></li>
                      <li> <a href="#">{{t("globals.menu.courses")}}</a></li>
                      <li> <a href="#">{{t("globals.menu.jobs")}}</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 text-right">
                    <b>Ajur Agade 17,3. th.  &nbsp;&nbsp; 9000 Aalborg</b>
                    <p>Admnistration (+45) 30 25 03 86  &nbsp;&nbsp; info@ajur.dk</p>
                </div>
            </div>
        </div>
    </div>
</div>
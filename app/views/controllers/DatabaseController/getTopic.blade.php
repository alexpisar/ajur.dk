@extends("layouts.front_page")

@section("content-column")

  <div class="head">{{t("globals.menu.database")}}</div>
  <ul class="breadcrumbs font-merriweather-b gray">
    <li><a href="{{route("db")}}"><img src="/img/arr-left.png" alt="left"/></a></li>
    @for($i=0; $i < $breadcrumbs->count() - 1; $i++)
      <li class="slash"><a href="{{route("db.topic", ["id" => $breadcrumbs[$i]->id])}}">{{$breadcrumbs[$i]->text}}</a></li>
    @endfor
    <li>{{ $breadcrumbs->last()->text }}</li>
  </ul>
  @if($topics->count() == 0 && $tabs->count() == 0)
  <div class="no-content"> {{t("database.no_content")}}</div>
  @else
  <ul class="topics font-merriweather black">
    @foreach($topics as $topic)
    <li><a href="{{route("db.topic", ["id"=>$topic->id])}}">{{$topic->text}}</a></li>
    @endforeach
  </ul>
  <div id="tabs" >
    <ul class="tabs spacer-1em black">
      @foreach($tabs as $tab)
         @if($icon = $tab->getIconView())
          <li><a class="btn" href="{{route("ajax.tab", ["id"=>$tab->id])}}" role="button">
              <img class="show-icon24" src="{{$icon}}" alt="icon"/> 
             {{$tab->name}}</a></li>
         @else
           <li><a class="btn no-icon" href="{{route("ajax.tab", ["id"=>$tab->id])}}" role="button">
             {{$tab->name}}</a></li>        
         @endif
      @endforeach   
    </ul>
  </div>
  @endif
  <div class="clearfix spacer"></div>
  <div class="clearfix spacer"></div>
@stop
@section("styles")
  @parent
  {{--<!--<link href="/plugins/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet">-->
<!--  <link href="/plugins/jquery-ui-1.11.4/jquery-ui.theme.min.css" rel="stylesheet">-->--}}
    <!--    Icon fonts -->
    <link href="/fonts/icon-fonts/styles.css" rel="stylesheet" type="text/css"/>
@stop
@section("scripts")
@parent
 <script src="/plugins/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript">
		$(document).ready(function() {
      $('#tabs').on('click', '.summary-short a',function(e){
        e.preventDefault();
        var parent = $(this).parent();
        var full = parent.siblings('.summary-full');
        parent.toggle(100);
        full.toggle(200);
      });
      $('#tabs').on('click', '.summary-full a', function(e){
         e.preventDefault();
         var parent = $(this).parent();
         var short = parent.siblings('.summary-short');
         parent.toggle(200);
         short.toggle(200);       
      });
     // set up tabs
       $( "#tabs" ).tabs({
          beforeLoad: function( event, ui ) {
            ui.jqXHR.fail(function() {
              ui.panel.html(
                      "{{ t('database.error_load_tab')}}");
            });
          }
         // heightStyle: "fill"
         // hide: { effect: "explode", duration: 300 }
       });
    });
    //pagination in tab
    $('#tabs').on('click', '.pagination a', function(e){
       e.preventDefault();
       var url = $(this).attr("href");
       //var container = $(this).parents(".ui-tabs-panel");
       var container = $(this).parent().parent().parent().parent();
       $.ajax({
         url: url
       }).done(function(data){
         container.html(data);
         //location.hash = page;
       });
    });
</script>
@stop
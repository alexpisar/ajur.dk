@extends("layouts.front_page")

@section("content-column")
  <div class="head">{{t("globals.menu.database")}}</div>
  <ul class="db spacer-05em font-proxima-b">
    @foreach($topics as $topic)
    <li><a class="btn" href="{{route("db.topic", ["id"=>$topic->id])}}" role="button">{{$topic->text}}</a></li>
    @endforeach
  </ul>
  <div class="clearfix spacer"></div>
  <div class="clearfix spacer"></div>
@stop

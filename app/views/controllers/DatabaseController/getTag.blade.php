@extends("layouts.front_page")

@section("content-column")

  <div class="head">{{t("globals.menu.database")}}</div>
  <ul class="breadcrumbs font-merriweather-b gray">
    <li><a href="{{route("db")}}"><img src="/img/arr-left.png" alt="left"/></a></li>
    <li>{{ $tag->name }}</li>
  </ul>
  <div class="tag spacer-2em">
    <ul>
      @foreach($contents as $articles)
        <li>
          <p>
           @if($icon = $articles[0]->getIconType())
             <img src="{{$icon}}" class="show-icon16" alt="icon" />
           @endif
           {{$articles[0]->getTypeName()}}
          </p>
          @foreach($articles as $article)
            <div>
              <a href="#" >{{$article->title}}</a>
              {{$article->summary_short}} 
              ({{date("Y", strtotime($article->year))}})
            </div>
          @endforeach
        </li>
      @endforeach
    </ul>
    {{$links}}
  </div>
  <div class="clearfix spacer"></div>
  <div class="clearfix spacer"></div>
@stop

@extends("layouts.front_page")

@section("content-column")

  <div class="head">{{t("globals.menu.database")}}</div>
  <ul class="breadcrumbs font-merriweather-b gray">
    <li><a href="{{route("db")}}"><img src="/img/arr-left.png" alt="left"/></a></li>
    <li>{{ $article->title }}</li>
  </ul>
  
  <div class="ui-tabs-panel spacer-2em">
    <div class="articles">
     <div class="head">{{$article->title}}</div>
     <div class="titles font-merriweather">
       @if($article->authors()->count() > 0)
         {{t("database.authors")}}: 
         <?php 
           $authors = [];
           foreach($article->authors as $author)
             $authors[] = "<a href='#'>" . $author->name . "</a>";
         ?>
         {{implode(", ", $authors)}}
       @endif
       @if($article->publishers()->count() > 0)
         <span class="icon-dot gray"></span>
         <?php 
           $publishers = [];
           foreach($article->publishers as $p)
             $publishers[] = "<a href='#'>" . $p->name . "</a>";
         ?>
         {{implode(", ", $publishers)}}
       @endif
         <span class="icon-dot gray"></span>
       {{date("d-m-Y", strtotime($article->date_publication))}}
       @if($article->isbn)
         <span class="icon-dot gray"></span>
         ISBN: {{$article->isbn}}
       @endif
     </div>
     <div class="summary-short">
       {{$article->summary_short}}
       <a href="#" class="font-merriweather black">[+]</a>
     </div>
     <div class="summary-full" hidden>
       {{$article->summary_full}}
       <a href="#" class="font-merriweather black">[-]</a>
     </div>
     <div class="tags font-proxima-b">
       @foreach($article->tags as $tag)
       <div><a href="{{route("db.tag", ["id"=>$tag->id])}}">{{$tag->name }}</a></div>
       @endforeach
     </div>
     <div class="feed-type font-proxima">
         @if($icon = $article->getIconType())
           <img src="{{$icon}}" class="show-icon24" alt="icon" />
         @endif
         <span>({{$article->getTypeName()}})</span>
         @if((bool)$article->print_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-print.png" alt="print" ></a>
         @endif
         @if((bool)$article->send_as_mail_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-mail.png" alt="mail" ></a>
         @endif
         @if((bool)$article->borrow_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-library.png" alt="library" ></a>
         @endif
         @if((bool)$article->buy_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-buy.png" alt="buy" ></a>
         @endif
         @if((bool)$article->reviews_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-content.png" alt="content" ></a>
         @endif
         @if((bool)$article->table_contents_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-table.png" alt="table" ></a>
         @endif
         @if((bool)$article->view_case_num_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-ecli.png" alt="ecli" ></a>
         @endif
         @if((bool)$article->view_cover_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-eye.png" alt="eye" ></a>
         @endif
         @if((bool)$article->save_button)
           <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-floppy.png" alt="floppy" ></a>
         @endif
         @if((bool)$article->content_link_button) {{-- && $article->content_link) --}}
           <a class="pull-right right-icon" href="{{$article->content_link}}"><img src="/img/icons/icon-link.png" alt="link" ></a>
         @endif
         <div class="clearfix spacer-1em"></div>
     </div>
    </div>
      <div class="clearfix spacer"></div>
      <div class="clearfix spacer"></div>
      <div class="clearfix spacer"></div>
      <div class="clearfix spacer"></div>
  </div>
  <div class="clearfix spacer"></div>
  <div class="clearfix spacer"></div>
@stop

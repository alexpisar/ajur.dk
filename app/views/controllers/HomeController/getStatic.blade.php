@extends("layouts.front_page")

@section("content-column")
  {{$page->trans->content}}
  <div class="clearfix spacer"></div>
  <div class="clearfix spacer"></div>
@stop

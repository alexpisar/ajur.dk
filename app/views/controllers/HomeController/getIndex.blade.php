@extends("layouts.front_page")

@section("content-column")

  <!--        Overview of news Nyhedsoverblik-->
  <div class="row header">
      <div class="col-xs-9 head-news">
          <div class="">{{t("home.header_feed_news")}}</div>
      </div>
      <div class="col-xs-3 date-news font-proxima gray">
          <p id="time">{{date("H.i")}}</p> <p>{{t("globals.week." . date("l"))}}</p><p> {{date("d-m-Y")}}</p>
      </div>
  </div>
  <ul class="news-feed">
    @foreach($frontNews as $article)
      <li>
        <a href="{{route('db.content', ["id" => $article->id])}}"><div class="head">{{$article->title}}</div>
        <div class="gray font-proxima bold feed-date">
          @if($article->publishers->count() > 0)
            {{$article->publishers->first()->name}},
          @endif
          {{date("d-m-Y", strtotime($article->date_publication))}} </div>
        <div class="font-merriweather">{{ $article->truncateSummary(220) }}</div>
        </a>
        <div class="feed-type font-proxima bold">
          @if($icon = $article->getIconType())
              <img src="{{$icon}}" class="show-icon24" alt="icon" />
            @endif
            <span class="blue">{{$article->getTypeName()}} </span><span class="gray"> ({{$article->onlineEditionPresenter()}})</span>
            @if($article->save_button)
              <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-floppy.png" alt="floppy" ></a>
            @endif
            @if($article->content_link_button && $article->content_link)
              <a class="pull-right right-icon" href="{{$article->content_link}}"><img src="/img/icons/icon-link.png" alt="link" ></a>
            @endif
        </div>
     </li>
    @endforeach
      {{--<li>
          <a href=""><div class="head">Amagerbanken-aktionar taber erstatningssag</div></a>
          <div class="gray font-proxima bold feed-date">Jyllandsposten, 26-08-2014 </div>
          <div class="font-merriweather">Proin blandit facilisis tellus, non commodo nisi faucibus
              at. Nullam sed mi vestibulum, pulvinar nisi nee, consequat dolor. Donee interdum lorem
              ut fermentum semper. Lorem ipsum dolor sit arnet, consectetur adipiscing...</div>
          <div class="feed-type font-proxima bold">
              <img src="/plugins/template/images/twitter.png" class="show-icon24" alt="icon" />
              <span class="blue">Dagsbladartikel </span><span class="gray"> (Trykt udgave)</span>
              <a class="pull-right right-icon" href="#"><img src="/img/icons/floppy16.png" alt="floppy" ></a>
              <a class="pull-right right-icon" href="#"><img src="/img/icons/icon-link.png" alt="link" ></a></div>
     </li>--}}
  </ul>

@stop

@section("scripts")
@parent
<script>
  $(document).ready(function() {
    var today = new Date();
    var h = today.getHours();
    var m = today.getMinutes();
    if(h < 10)
      h = "0" + h;
    if(m < 10)
      m = "0" + m;
    $("#time").text(h+"."+m);
    
  });
</script>
@stop
@extends("layouts.base")

@section("content")
  <div id="profile" class="container font-merriweather">
    <h4 class="spacer-1em">
      <b>{{ucfirst(t("profile.change_password"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.change_password_text")}}
      </div>
   </div>

   {{Form::open()}}
    <div class="form-group row spacer">
      <div class="col-sm-4">
          <label>{{t("profile.type_your_password")}}</label>
        {{--Form::password("old_password", ["class"=>"form-control", 
          "maxlength"=>"255", "required"=>"required"])--}}
  <input type="password" class="form-control"  required="required" name="old_password"
        maxlength="255" value="{{Input::old('old_password','')}}" />
      </div>
    </div>
    <div class="form-group row spacer-1em">
      <div class="col-sm-4">
          <label>{{t("profile.type_new_password")}}</label>
        <input type="password" class="form-control"  required="required" name="password"
        maxlength="255" value="{{Input::old('password','')}}" />          
      </div>
    </div>
    <div class="form-group row spacer-1em">
      <div class="col-sm-4">
          <label>{{t("profile.type_new_password_again")}}</label>
        <input type="password" class="form-control"  required="required"
               name="password_confirmation" maxlength="255" 
               value="{{Input::old('password_confirmation','')}}" /> 
      </div>
    </div>
    <div class="form-group row spacer-2em">
      <div class="col-sm-4">
          {{Form::submit(strtoupper(t("profile.change_password_button")), 
            ["class" => "btn btn-default font-proxima"])}}
      </div>
    </div>

   {{Form::close()}}
    
    
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop

@extends("layouts.base")

@section("content")
  <div class="container font-merriweather">
    <h4 class="spacer-1em">
      <b>{{ucfirst(t("profile.my_payments"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.my_payments_text")}}
      </div>
   </div>

   <div class="row spacer-2em info-list">
     <div class="col-md-12 col-lg-8"> <!--table-responsive-->
      <table class="table font-proxima"  id="payments">
        <thead>
          <tr><th class="slash">{{t("profile.invoice_number")}} </th>
              <th class="slash">{{t("profile.billing_date")}} </th>
              <th class="slash">{{t("profile.payment_due")}} </th>
              <th class="slash">{{t("profile.total_amount")}} </th>
              <th>{{t("profile.status")}} </th>
              <th></th>
              <th></th>
          </tr>
        </thead>
        <tbody>
         @foreach($payments as $payment)
           <tr><td>{{$payment->invoice_number}}</td>
              <td>{{date("d/m-Y", strtotime($payment->billing_date))}}</td>
              <td>{{date("d/m-Y", strtotime($payment->payment_due))}}</td>
              <td>{{$payment->total_amount_due . " ".t("profile.currency")}}</td>
              <td>{{strtoupper($payment->getStatus())}}</td>
              <td><a href="#" class="btn btn-default font-proxima width-100" role="button">
                   {{strtoupper(t("profile.receipt"))}}</a></td>
              <td><a href="#" class="btn btn-default font-proxima width-100" role="button">
                   {{strtoupper(t("profile.invoice"))}}</a></td>
           </tr>
          @endforeach
        </tbody>
      </table>
     </div>
    </div>
    <div class="row spacer-1em">
    </div> 
    
    
    
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
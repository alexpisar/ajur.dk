@extends("layouts.base")

@section("content")
  <div id="profile" class="container font-merriweather">
    <h4 class="spacer-1em">
      <b>{{ucfirst(t("globals.menu.profile"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.paying_profile_text")}}
      </div>
   </div>
    <div class="row spacer-1em">
        <div class="col-sm-12">
            <a href="{{route("profile.subscriptions")}}" class="btn btn-default font-proxima" role="button">
                {{strtoupper(t("profile.my_subscriptions"))}}</a>
            <a href="{{route("profile.password")}}" class="btn btn-default font-proxima" role="button">
                {{strtoupper(t("profile.change_password"))}}</a>
            <a href="#" class="btn btn-default font-proxima" role="button">
                {{strtoupper(t("profile.my_saved_data"))}}</a>
          @if($user->subscriber_info->persons > 1)
            <a href="{{route("profile.add_users")}}" class="btn btn-default font-proxima" role="button">
                {{strtoupper(t("profile.add_delete_users"))}}</a>
          @else
            <a href="#" class="btn btn-default font-proxima disabled" role="button">
                {{strtoupper(t("profile.add_delete_users"))}}</a>
          @endif
        </div>
    </div>
    <div class="row">
      <div class="col-sm-8"><div class="footer-line spacer-2em"></div> </div>
    </div>
    <div class="row spacer-1em info-list">
      <div class="col-sm-3 col-md-2">
          {{t("receipt.name_subscription")}} <br />
          {{t("receipt.license_number")}}
      </div>
        <div class="col-sm-3 col-md-3 text-right">
          @if($user->subscriber_info)
            {{ $user->subscriber_info->name }} <br />
            {{ $user->subscriber_info->licens_number }}
          @endif
        </div>
    </div>
    <div class="row spacer-1em">
      <div class="col-sm-8"><div class="footer-line"></div> </div>
    </div> 
    <div class="row spacer-2em"></div>
        @if($user->subscriber_info)
          {{Form::model($user->subscriber_info, ["id"=>"your-info"])}}
          {{Form::hidden("id", $user->subscriber_info->id)}}
        @else
          {{Form::open(["id"=>"your-info"])}}
        @endif
          <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("name", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.company_name')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
           <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("vat_number", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.vat_number')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("street_name", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.street_name')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("post_box", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.post_box')),
                "maxlength"=>"255"])}}
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4">
             <div class="row inline-input">
                 <div class="col-sm-3">
              {{Form::text("house_nr", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.house_nr')) . "*",
                "maxlength"=>"255"])}}
                </div>
                 <div class="col-sm-3">
              {{Form::text("letter", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.letter')),
                "maxlength"=>"255"])}}
                </div>
                 <div class="col-sm-3">
              {{Form::text("floor", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.floor')),
                "maxlength"=>"255"])}}
                </div>
                 <div class="col-sm-3">
              {{Form::text("side", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.side')),
                "maxlength"=>"255"])}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4">
             <div class="row inline-input">
                 <div class="col-sm-3">
              {{Form::text("post_number", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.post_number')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
                </div>
                 <div class="col-sm-9">
              {{Form::text("city", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.city')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
                </div>
              </div>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("country", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.country')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
         <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("nameAdmin", $user->name, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.nameAdmin')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
         <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("phone", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.phone')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
         <div class="form-group row">
            <div class="col-sm-4">
              {{Form::email("emailAdmin", $user->email, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.emailAdmin')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-4">
              {{Form::text("occupation", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.occupation')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            </div>
          </div>                
          <div class="spacer clearfix"></div>
          <div class="form-group row">
            <div class="col-sm-4">
              <button class="btn btn-default font-proxima" type="submit">
                <span>{{strtoupper(t("profile.save_changes"))}}</span></button>
           </div>
          </div>
           <div class="spacer clearfix"></div>

				{{Form::close()}}
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
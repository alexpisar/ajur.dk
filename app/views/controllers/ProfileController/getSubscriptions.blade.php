@extends("layouts.base")

@section("content")
  <div id="profile" class="container font-merriweather">
    <h4 class="spacer-1em">
      <b>{{ucfirst(t("profile.my_subscriptions"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.my_subscriptions_text")}}
      </div>
   </div>
    <div class="row">
      <div class="col-sm-8"><div class="footer-line spacer-2em"></div> </div>
    </div>

    <div class="row spacer-2em info-list">
     <div class="col-sm-8">
       <div class="row">
           <div class="col-xs-7 col-sm-4 text-center">
                <div class="width-auto text-center">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <span class="panel-title">{{$user->subscriber_info->subscription_name}}</span> 
                  </div>
                  <div class="panel-body">
                    <p>{{$user->subscriber_info->persons}} persones adgang</p>
                     <div class="spacer-1em clearfix"></div>
                    <p>{{$user->subscriber_info->price}} kr./md eksl. moms</p>
                    <div class="spacer clearfix"></div>
                  </div>
                </div>
                </div>
            </div>
          <div class="col-xs-6 col-sm-4 nowrap">
            {{ucfirst(t('validation.attributes.company_name'))}} <br />
            {{t("receipt.name_subscription")}} <br />
            {{t("receipt.license_number")}} <br />
            {{t("profile.number_users")}} <br />
            {{t("profile.next_payment")}} <br />
            {{t("profile.type_of_payment")}} <br />
            {{t("profile.card_number")}} <br />
          </div>
          <div class="col-xs-6 col-sm-4 text-right nowrap">
            @if($user->subscriber_info)
              {{ $user->subscriber_info->name }} <br />
              {{ $user->subscriber_info->subscription_name }} <br />
              {{ $user->subscriber_info->licens_number }} <br />
              {{ $user->subscriber_info->persons }} <br />
              @if($lastPayment)
                {{ date("d/m-y", strtotime($lastPayment->payment_due)) }} <br />
                {{ $lastPayment->getMethodPayment() }} <br />
                {{ $lastPayment->formatedCardNumber() }}
              @endif
            @endif
          </div>
       </div>
     </div>
    </div>
    <div class="row spacer-1em">
      <div class="col-sm-8"><div class="footer-line"></div> </div>
    </div> 
    <div class="row spacer-1em wrap" id="panel-btns">
      <div class="col-sm-8">
        <div class="row">
          <div class="col-xs-4">
            <a href="#" class="btn btn-default font-proxima wrap width-100" role="button">
                {{strtoupper(t("profile.change_payment_method"))}}</a>
          </div>
          <div class="col-xs-4">
            <a href="{{route("profile.subscriptions.change")}}" class="btn btn-default font-proxima wrap width-100" role="button">
                {{strtoupper(t("profile.change_type_subscription"))}}</a>
          </div>
          <div class="col-xs-4">
            <a href="{{route("profile.payments")}}" class="btn btn-default font-proxima wrap width-100" role="button">
                {{strtoupper(t("profile.see_payments"))}}</a>
          </div>
        </div>
        <div class="row spacer-1em">
          <div class="col-xs-4">
            @if($user->subscriber_info->persons > 1)
              <a href="{{route("profile.add_users")}}" class="btn btn-default font-proxima wrap width-100" role="button">
                {{strtoupper(t("profile.add_del_users"))}}</a>
            @else
              <a href="#" class="btn btn-default font-proxima wrap width-100 disabled" role="button">
                {{strtoupper(t("profile.add_del_users"))}}</a>
            @endif
          </div>
          <div class="col-xs-4">
            <a href="#" class="btn btn-default font-proxima wrap width-100" role="button">
                {{strtoupper(t("profile.cancel_subscription"))}}</a>
          </div>
          <div class="col-xs-4">
            <a href="{{route("profile.owner")}}" class="btn btn-default font-proxima wrap width-100" role="button">
                {{strtoupper(t("profile.go_to_profile"))}}</a>
          </div>
        </div>
      </div>
    </div>
    
    
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
@extends("layouts.base")

@section("content")
  <div id="profile" class="container font-merriweather">
    <h4 class="spacer-1em">
        <b>{{ucfirst(t("globals.menu.profile"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.colleague_profile_text")}}
      </div>
   </div>
    <div class="row spacer-1em">
      <div class="col-sm-8">
        <a href="{{route("profile.password")}}" class="btn btn-default font-proxima" role="button">
            {{strtoupper(t("profile.change_password"))}}</a>
        <a href="#" class="btn btn-default font-proxima" role="button">
            {{strtoupper(t("profile.my_saved_data"))}}</a>
        <div class="clearfix spacer-2em"></div>
        <div class="footer-line"></div>
      </div>
    </div>
    <div class="row spacer-1em info-list">
      <div class="col-sm-3 col-md-2">
          {{t("receipt.name_subscription")}} <br />
          {{t("receipt.license_number")}}
      </div>
        <div class="col-sm-3 col-md-3 text-right">
          @if($user->subscriber_info)
          {{ $user->subscriber_info->name }} <br />
          {{ $user->subscriber_info->licens_number }}
          @endif
        </div>
    </div>
    <div class="row spacer-1em">
      <div class="col-sm-4 form-group">
          <p>{{t("profile.my_information")}}</p>
        {{Form::text("name", $user->name, ["class"=>"form-control", "maxlength" => "255",
                "readonly"=>"readonly"])}}
        {{Form::text("email", $user->email, ["class"=>"form-control spacer-1em", "maxlength" => "255",
                "readonly"=>"readonly"])}}
      </div>
    </div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
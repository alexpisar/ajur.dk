@extends("layouts.base")

@section("content")
  <div id="profile" class="container font-merriweather">
    <h4 class="spacer-1em">
      <b>{{ucfirst(t("profile.add_del_users"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.add_del_users_text")}}
      </div>
   </div>
    <div class="row spacer info-list">
     <div class="col-sm-8">
       <div class="row">
           <div class="col-xs-7 col-sm-4 text-center">
              <div class="width-auto text-center">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <span class="panel-title">{{$subscriber->subscription_name}}</span> 
                  </div>
                  <div class="panel-body">
                    <p>{{$subscriber->persons}} persones adgang</p>
                     <div class="spacer-1em clearfix"></div>
                    <p>{{$subscriber->price}} kr./md eksl. moms</p>
                    <div class="spacer clearfix"></div>
                  </div>
                </div>
              </div>
            </div>
          <div class="col-xs-6 col-sm-4 nowrap">
            {{ucfirst(t('validation.attributes.company_name'))}} <br />
            {{t("receipt.name_subscription")}} <br />
            {{t("receipt.license_number")}} <br />
            {{t("profile.number_users")}} <br />
            {{t("profile.next_payment")}} <br />
            {{t("profile.type_of_payment")}} <br />
            {{t("profile.card_number")}} <br />
          </div>
          <div class="col-xs-6 col-sm-4 text-right nowrap">
            @if($subscriber)
              {{ $subscriber->name }} <br />
              {{ $subscriber->subscription_name }} <br />
              {{ $subscriber->licens_number }} <br />
              {{ $subscriber->persons }} <br />
              @if($lastPayment)
                {{ date("d/m-y", strtotime($lastPayment->payment_due)) }} <br />
                {{ $lastPayment->getMethodPayment() }} <br />
                {{ $lastPayment->formatedCardNumber() }}
              @endif
            @endif
          </div>
       </div>
     </div>
    </div>
      
    <div class="row spacer">
      <div class="col-md-8">
        <p> <b>{{t("profile.overview")}}</b></p>
        <table id="users" class="table info-list">
          <tbody>
           <tr><td> {{--Administrator--}}
              <div class="row">
                <div class="col-sm-2">
                  {{t("profile.user") . " 1"}} <br />
                  {{ucfirst(t("validation.attributes.name"))}} <br />
                  {{ucfirst(t("validation.attributes.email"))}}
                </div>
                <div class="col-sm-6">
                  {{ucfirst("(".t("profile.administrator").")")}} <br />
                  {{$subscriber->owner->name}} <br />
                  {{$subscriber->owner->email}} <br />
                </div>
              </div>
            </td></tr>
           <?php $i = 1; ?>
            @foreach($subscriber->customers as $user)
              <tr><td> {{--Users--}}
               <div class="row">
                 <div class="col-sm-2">
                   {{t("profile.user")." ".(++$i) }} <br />
                   {{ucfirst(t("validation.attributes.name"))}} <br />
                   {{ucfirst(t("validation.attributes.email"))}}
                 </div>
                 <div class="col-sm-6">
                   <br />
                   {{$user->name}} <br />
                   {{$user->email}} <br />
                 </div>
                 <div class="col-sm-3">
                   <br />
                   <a class="btn btn-default font-proxima width-100" href="{{route("profile.del_user", ["id" => $user->id])}}" 
                      role="button">{{strtoupper(t("profile.delete_user"))}}
                     <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                       data-container="body" data-toggle="popover" data-placement="right" 
                       data-content="{{t('profile_hint.delete_user')}}"></span>
                   </a>
                 </div>
               </div>
             </td></tr>
            @endforeach
           <tr><td>  {{--Add user--}}
            @if($subscriber->persons - 1 > $subscriber->customers->count())
             {{Form::open()}}
              <div class="row">
                <div class="col-sm-8">
                  {{t("profile.add_user")." ".(++$i)}} <br />
                  <i class="nowrap">{{ucfirst(t("profile.fill_text_to_add_user"))}} </i><br />
                   <div class="form-group row spacer-1em">
                      <div class="col-sm-9">
                        {{Form::text("name", null, ["class"=>"form-control", 
                          "placeholder"=>ucfirst(t('profile.type_name')),
                          "maxlength"=>"255"])}}
                          <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                              data-container="body" data-toggle="popover" data-placement="right" 
                              data-content="{{t('profile_hint.type_name')}}"></span>
                      </div>
                    </div> 
                   <div class="form-group row spacer-1em">
                      <div class="col-sm-9">
                        {{Form::email("email", null, ["class"=>"form-control", 
                          "placeholder"=>ucfirst(t('profile.type_email')),
                          "maxlength"=>"255"])}}
                          <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                              data-container="body" data-toggle="popover" data-placement="right" 
                              data-content="{{t('profile_hint.type_email')}}"></span>
                      </div>
                    </div> 
                </div>
                <div class="col-sm-3">
                  <div class="spacer-2em clearfix"></div>
                  <div class="spacer-2em clearfix"></div>
                  <button class="btn btn-default font-proxima width-100 spacer" 
                     type="submit">{{strtoupper(t("profile.send_link"))}}
                  <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('profile_hint.send_link')}}"></span></button>
                </div>
              </div>
             {{Form::close()}}
             @else
              <div class="row">
                <div class="col-sm-8">
                  <p class="spacer-1em"><b>{{t("profile.maximum_users")}}</b></p>
                </div>
              </div>
             @endif
            </td></tr>
          </tbody>
        </table>
      </div>
    </div>
    <div class="row spacer">
      <div class="col-sm-8">
        <p> <b> &nbsp; {{t("profile.more_users")}}</b></p>
           <a class="btn btn-default font-proxima" href="{{route("profile.subscriptions.change")}}" 
              role="button">{{strtoupper(t("profile.change_subscription"))}}</a>
      </div>
    </div>
    
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
@section("scripts")
@parent
{{--<script src="/plugins/data-table/js/jquery.dataTables.min.js" type="text/javascript"></script>--}}
<script type="text/javascript">
   // var tableUsers = null;
		$(document).ready(function() {
      $('.glyphicon').popover({
        "delay" : { "show": 200, "hide": 100 },
        "trigger": "hover"
      });
    });
</script>
@stop
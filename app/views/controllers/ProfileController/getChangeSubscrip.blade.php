@extends("layouts.base")

@section("content")
  <div class="container font-merriweather steps">
    <h4 class="spacer-1em">
      <b>{{ucfirst(t("profile.my_subscriptions"))}}</b>
    </h4>
    <div class="row">
      <div class="col-sm-8 line-2">
        {{t("profile.my_subscriptions_text")}}
      </div>
   </div>
    <div class="row spacer-2em">
      <div class="col-sm-8"><p><b>{{t("profile.current_subscription")}}</b></p></div>
    </div>
    <div class="row info-list spacer-05em">
     <div class="col-sm-8">
       <div class="row">
           <div class="col-xs-7 col-sm-4 text-center">
                <div class="width-auto text-center cross-line">
                <div class="panel panel-default">
                  <div class="panel-heading">
                    <span class="panel-title">{{$user->subscriber_info->subscription_name}}</span> 
                  </div>
                  <div class="panel-body">
                    <p>{{$user->subscriber_info->persons}} persones adgang</p>
                     <div class="spacer-1em clearfix"></div>
                    <p>{{$user->subscriber_info->price}} kr./md eksl. moms</p>
                    <div class="spacer clearfix"></div>
                  </div>
                </div>
                </div>
            </div>
          <div class="col-xs-6 col-sm-4 nowrap">
            {{ucfirst(t('validation.attributes.company_name'))}} <br />
            {{t("receipt.name_subscription")}} <br />
            {{t("receipt.license_number")}} <br />
            {{t("profile.number_users")}} <br />
            {{t("profile.next_payment")}} <br />
            {{t("profile.type_of_payment")}} <br />
            {{t("profile.card_number")}} <br />
          </div>
          <div class="col-xs-6 col-sm-4 text-right nowrap">
            @if($user->subscriber_info)
              {{ $user->subscriber_info->name }} <br />
              {{ $user->subscriber_info->subscription_name }} <br />
              {{ $user->subscriber_info->licens_number }} <br />
              {{ $user->subscriber_info->persons }} <br />
              @if($lastPayment)
                {{ date("d/m-y", strtotime($lastPayment->payment_due)) }} <br />
                {{ $lastPayment->getMethodPayment() }} <br />
                {{ $lastPayment->formatedCardNumber() }}
              @endif
            @endif
          </div>
       </div>
     </div>
    </div>
    <div class="row spacer-2em">
      <div class="col-sm-8"><b>{{t("profile.choose_new_subscription")}}</b> </div>
    </div> 
    <div class="row wrap spacer-05em" id="panel-btns">
      <div class="col-sm-8">
			   {{Form::open(["route"=>"choose.payment", "id"=>"chooseform", "method"=>"GET"])}}				
          @for($i=0, $count = $pricing->count(); $i < $count; )
				<div class="row">
            @for($j=0; $j<3 && $i<$count; $i++, $j++)
              <div class="col-sm-4">
                @if($pricing[$i]->id == $user->subscriber_info->subscriptionType->id)
                <div class="radio disabled">
                @else
                <div class="radio">
                @endif
                <label class="text-center">
                <div class="panel panel-default" id="{{'p' . $pricing[$i]->id}}">
                  <div class="panel-heading">
                    <span class="panel-title">{{$pricing[$i]->name}}</span> 
                  </div>
                  <div class="panel-body">
                    <p>{{$pricing[$i]->persons}} persones adgang</p>
                     <div class="spacer-1em clearfix"></div>
                    <p>{{$pricing[$i]->price}} kr./md eksl. moms</p>
                    <p class="spacer-1em text-center clearfix disabled">
                      @if($pricing[$i]->id == $user->subscriber_info->subscriptionType->id)
                        &nbsp;
                      @else
                        {{Form::radio("subscription_type_id", $pricing[$i]->id, false)}}
                      @endif
                    </p>
                    <div class="spacer clearfix"></div>
                  </div>
                    @if($pricing[$i]->id == $user->subscriber_info->subscriptionType->id)
                   <div class="cross"></div>
                   @endif
                </div>
                </label>
                </div>			
              </div>
            @endfor
				</div>
				  @endfor
				<div class="spacer clearfix"></div>				
				<button id="next" class="btn btn-default font-proxima pull-right" type="submit">
						<span>{{strtoupper(t("pagination.next"))}}</span></button>
				{{Form::close()}}
      </div>
    </div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
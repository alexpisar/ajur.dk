@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
   		  <div class="clearfix"></div>
          <div class="grid">
	        <div class="grid-title">
	           <div class="pull-left">
	              <span class="table-title">{{$_siteTitle}}</span> 
	              <div class="clearfix"></div>
	           </div>
	         </div>
	         <div class="grid-content overflow">
	          <table class="table table-bordered table-mod-2 table-responsive" id="datatable_3">
	            <thead>
	              <tr>
                  <th> Title </th>
                  <th> Show</th>
                  <th> Page type</th>
                  <th> Show in top menu</th>
                  <th> Show in bottom menu</th>
                  <th> Delete</th>
                  <th> Edit</th>
	              </tr>
	            </thead>
	            <tbody>
				    @foreach($pages as $page)
				      <tr>
				        <td>{{ $page->title }}</td>
				        <td>
                  @if(!$page->is_system)
                    {{ $page->show ? "yes" : "no" }}
                  @else
                    n/a
                  @endif
                </td>
                <td>{{$page->is_system?"System page":"Static page"}}</td>
                <td>
                  @if(!$page->is_system)
                    {{ $page->show_in_top ? "yes" : "no" }}
                  @else
                    n/a
                  @endif
                </td>
                <td>
                  @if(!$page->is_system)
                    {{ $page->show_in_bottom ? "yes" : "no" }}
                  @else
                    n/a
                  @endif
                </td>
						  <td class="action-table"> 
                @if(!$page->is_system)
                  <a href="{{route("static.delete", ["alias"=>$page->alias]) }}"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a> 
                @else
                  n/a
                @endif
              </td>
						  <td class="action-table"> <a href="{{route("static.edit", ["alias"=>$page->alias]) }}"><img src="/plugins/template/images/icon/table_view.png" alt="Edit"></a> </td>
				      </tr>
				    @endforeach	            	
	            </tbody>
	          </table>           
	         </div>
          </div>
        <a id="btn-staff" href="{{route("static.add")}}" class="btn btn-success btn-block pull-right" role="button">Add new</a>
   		  <div class="clearfix"></div>           
   		  {{-- @include("partials.admin.footer")
   		  <div class="clearfix"></div> --}}
   		  </div>
   		</div>
</div>

@stop

@section("scripts")
@parent
{{-- <script src="/plugins/template/js/jquery.dataTables.min.js"></script> --}}
<script src="/plugins/data-table/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('#datatable_3').dataTable( {
			"sPaginationType": "full_numbers",
			"sPageButton": "btn btn-default",
      "aoColumns": [
          null,
          null,
          null,
          null,
          null,
          { "bSortable": false },
          { "bSortable": false }
          ],
			"oLanguage": { //
				"sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
				'<option value="5">5</option>'+
				'<option value="10">10</option>'+
				'<option value="50">50</option>'+
				'<option value="100">100</option>'+
				'<option value="200">200</option>'+
				'</select>entries </div> <div class="clearfix"></div>'

			},
			"fnInitComplete":function (){
				$(".dataTables_filter input").addClass("form-data-table");
				$(".dataTables_filter label").addClass("form-table-label");
			}
		} );
	} );
</script>
@stop

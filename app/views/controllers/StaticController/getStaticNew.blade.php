@extends("layouts.base_admin")

@section("styles")
    @parent
    <link href="/plugins/bootsrtap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
@stop

@section("content")

@include("partials.admin.header")

<div id="wrap">
    @include("partials.admin.sidebar")
    <div id="main" role="main">
        <div class="block">
            <div class="clearfix"></div>
            <div class="grid">
                <div class="grid-title">
                    <div class="pull-left">
                        <span class="table-title">{{$_siteTitle}}</span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="grid-content overflow">
                    {{Form::open(['route'=> 'static.edit','class'=>'form-horizontal', 'role' => 'form'])}}
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Select language</label>
                        <div class="col-sm-4">
                            {{Form::select("locale", $languages, Config::get("app.fallback_locale"), 
                                      ["class" => "form-control selectpicker", "data-live-search" => "true", 'id'=>'selectpicker'])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Title</label>
                        <div class="col-sm-9">
                            {{Form::text("title", null, ["class"=>"form-control","required" =>"required", "maxlength" => "255"])}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Content</label>
                        <div class="col-sm-9">
                            {{Form::textarea("content", null, ["id" => "summary_full","class"=>"form-control", "required" => "required", "rows" => "10", "cols" => "80"])}}
                        </div>
                    </div> 
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Page alias</label>
                          <div class="col-sm-9">
                              {{Form::text("alias", null, ["class"=>"form-control","required" =>"required", "maxlength" => "255"])}}
                          </div>
                      </div>                  
                      <div class="form-group">
                          <label class="col-sm-2 control-label">Weight</label>
                          <div class="col-sm-4">
                              {{Form::text("weight", null, ["class"=>"form-control", "maxlength" => "8"])}}
                          </div>
                      </div>                   
                      <div class="form-group">
                         <div class="col-sm-offset-2 col-sm-6 checkbox">
                            <p class="spacer-1em">
                                {{Form::checkbox('show', 1, false, ["id" => "c1"])}}
                                <label for="c1"><span></span>Show</label>
                            </p>
                            <p class="spacer-1em">
                                {{Form::checkbox('show_in_top', 1, false, ["id" => "c2"])}}
                                <label for="c2"><span></span>Show in top menu</label>
                            </p>
                            <p class="spacer-1em">
                                {{Form::checkbox('show_in_bottom', 1, false, ["id" => "c2"])}}
                                <label for="c2"><span></span>Show in bottom menu</label>
                            </p>
                         </div>
                      </div> 
                    <div class="form-group spacer">
                      <div class="col-sm-offset-7 col-sm-2">
                          <a href="{{route("static.list")}}" class="btn btn-default width-100" role="button">Cancel</a>
                      </div>
                      <div class="col-sm-2">
                          {{Form::submit("Save", ["class" => "btn btn-success width-100"])}}
                      </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

@stop




@section("scripts")
    @parent
    <script src="/plugins/bootsrtap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script type="text/javascript">
      $(document).ready(function() {
        CKEDITOR.replace('summary_full');
        $('.selectpicker').selectpicker();
      });
    </script>
@stop

@extends("layouts.base")

@section("content")
  <div class="container">
    <div class="row spacer-1em">
      <img src="/img/ajur-denied.png" class="img-responsive center-block" alt=""/>
   </div>
    <div class="row spacer-2em">
      <div class="col-sm-offset-2 col-sm-8 font-merriweather text-center line-2">
        {{t("registration.access_denied_text")}}
      </div>
   </div>
    <div class="row spacer-2em">
      <div id="denied" class="col-sm-offset-2 col-sm-8 col-md-offset-3 col-md-6">
         <div id="login" class="gray font-merriweather">
              {{Form::open(["route" => "login.user"])}}
                  {{Form::email("email", null, ["class" => "form-control", "title" => "Enter your email",
                            "placeholder" => ucfirst(t('validation.attributes.email')), "required"=>"required" ])}}
                  {{Form::password("password", ["class" => "form-control spacer-2em", "title" => "Enter your password", "required"=>"",
                            "placeholder"=> ucfirst(t('validation.attributes.password'))])}} <br />
                   <div class="spacer-1em"></div>
                  <p class="remember checkbox">
                      <label>
                      {{Form::checkbox('remember_me', 1, false)}}
                      <span>{{ucfirst(t("globals.remember_me"))}}</span></label>
                  </p>
                  <p class="login-btn">
                      {{--<a class="btn btn-default font-proxima pull-right" href="#" role="button">SIGN IN</a> --}}
                      {{Form::submit(strtoupper(t("globals.menu.sign_in")), ["class" => "btn btn-default font-proxima pull-right"])}}
                  </p>
            <div class="clearfix"></div>
               {{Form::close()}}
            <div class="spacer"></div>
            <a class="btn sign-up" href="{{route("teaser")}}" role="button">{{t('globals.sign_up')}}</a>
            <a class="btn forgot" href="{{route("password.lost")}}" role="button"> {{t('globals.forgot_password')}}</a>
            <div class="clearfix"></div>
        </div>
        <div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
      </div>
   </div>
    
  </div>
@stop
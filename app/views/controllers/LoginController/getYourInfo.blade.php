@extends("layouts.base")

@section("content")
  <div class="content">
      <div class="container steps">
		<div class="row progress-steps">
				<div class="col-xs-12"> 
					<div class="spacer"></div>					
					<div class="step">
						1. {{t("registration.choose_subscription")}}
					</div>
					<div class="step active">
						2. {{t("registration.your_information")}}
					</div>
					<div class="step">
						3. {{t("registration.payment")}}
					</div>		
					<div class="step">
						4. {{t("registration.receipt")}}
					</div>		
				</div>

		</div>
          <div class="row spacer">
              <div class="col-md-8 font-merriweather">
				<h4> Lorem ipsum dolor sit amet</h4>
				<p>{{t("registration.your_information_text")}}</p>
				<div class="spacer-1em"></div>
			   {{Form::open(["id"=>"your-info"])}}
         {{Form::hidden("subscription_type_id", $subscription->id)}}
           {{-- <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("company", null, ["class"=>"form-control", 
                   "placeholder"=>t("validation.attributes.company"), 
                   "maxlength"=>"255", "required"=>"required"])}}
            </div>
           </div>--}}
          <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("company_name", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.company_name')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
                <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.company_name')}}"></span>
            </div>
          </div>
           <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("vat_number", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.vat_number')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.vat_number')}}"></span>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("street_name", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.street_name')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.street_name')}}"></span>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("post_box", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.post_box')),
                "maxlength"=>"255"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.post_box')}}"></span>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
             <div class="row inline-input">
                 <div class="col-sm-3">
              {{Form::text("house_nr", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.house_nr')) . "*",
                "maxlength"=>"255"])}}
                </div>
                 <div class="col-sm-3">
              {{Form::text("letter", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.letter')),
                "maxlength"=>"255"])}}
                </div>
                 <div class="col-sm-3">
              {{Form::text("floor", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.floor')),
                "maxlength"=>"255"])}}
                </div>
                 <div class="col-sm-3">
              {{Form::text("side", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.side')),
                "maxlength"=>"255"])}}
                </div>
              </div>
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.house_nr')}}"></span> 
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
             <div class="row inline-input">
                 <div class="col-sm-3">
              {{Form::text("post_number", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.post_number')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
                </div>
                 <div class="col-sm-9">
              {{Form::text("city", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.city')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
                </div>
              </div>
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.city')}}"></span> 
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("country", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.country')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.country')}}"></span>
            </div>
          </div>
           <div class="clearfix spacer-2em"></div>
                <p>{{ucfirst(t("validation.attributes.nameAdmin"))}}</p>           
         <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("nameAdmin", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.nameAdmin')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.nameAdmin')}}"></span> 
            </div>
          </div>
         <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("phone", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.phone')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
            <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.phone')}}"></span>     
            </div>
          </div>
         <div class="form-group row">
            <div class="col-sm-6">
              {{Form::email("emailAdmin", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.emailAdmin')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
               <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.emailAdmin')}}"></span> 
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              {{Form::text("occupation", null, ["class"=>"form-control", 
                "placeholder"=>ucfirst(t('validation.attributes.occupation')) . "*",
                "maxlength"=>"255", "required"=>"required"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.occupation')}}"></span>
            </div>
          </div>                
         <div class="form-group row">
            <div class="col-sm-6">
              {{--Form::password("password", ["class"=>"form-control", 
                "placeholder" => t('registration.password') . "*",
                "required"=>"required", "maxlength"=>"255"])--}}
               <input type="password" name="password" class="form-control" 
                      placeholder="{{ t('registration.password') . '*'}}" required="required"
                      maxlength="255" value="{{Input::old('password','')}}"/>
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.password')}}"></span> 
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6">
              {{--Form::password("password_confirmation", ["class"=>"form-control", 
                "placeholder"=>t('registration.password_confirmation') . "*",
                "maxlength"=>"255", "required"=>"required"])--}}
                <input type="password" name="password_confirmation" class="form-control" 
                      placeholder="{{ t('registration.password_confirmation') . '*'}}" required="required"
                      maxlength="255" value="{{Input::old('password_confirmation','')}}"/>
                <p><i class="info">{{t('registration.OBS_amdinistrator_profile')}}</i></p>
                <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('subscriber_hint.password_confirmation')}}"></span> 
            </div>
          </div>
           <div class="spacer clearfix"></div>
          <div class="form-group row">
            <div class="col-sm-6 checkbox">
             <label>
               {{Form::checkbox("accepted_terms_conditions", 1, false)}}
               {{--<span> <a href="#"> Link </a>{{t("registration.accepted_terms_conditions")}}</span>--}}
                <span>{{$msgAccept}}</span>
             </label>
           </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-6 checkbox">
             <label>
               {{Form::checkbox("to_recive_letters", 1, false)}}
               <span> {{t("registration.to_recive_letters")}} </span>
             </label>
           </div>
          </div>
          <div class="form-group row spacer">
            <div class="col-sm-6">
              <a id="btn-go-back" class="btn btn-default font-proxima" href="{{route("choose.subscriptions", ["subscription_type_id" => $subscription->id])}}" role="button">
                <span>{{strtoupper(t("pagination.go_back"))}}</span></a>
              <button id="next" class="btn btn-default font-proxima pull-right" type="submit">
                <span>{{strtoupper(t("pagination.next"))}}</span></button>
           </div>
          </div>
           
           <div class="spacer clearfix"></div>

				{{Form::close()}}
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
              </div> {{--end left col--}}
              <div class="col-md-4 text-center">
                  <div id="order" class="gray font-proxima">
                      <b>{{t("registration.your_order")}}</b>
                  </div>
                  <div class="spacer-2em"></div>
                  <div class="radio">
                    <label class="width-auto text-center font-merriweather">
                    <div class="panel panel-default checked">
                      <div class="panel-heading">
                        <span class="panel-title">{{$subscription->name}}</span> 
                      </div>
                      <div class="panel-body">
                        <p>{{$subscription->persons}} persones adgang</p>
                         <div class="spacer-1em clearfix"></div>
                        <p>{{$subscription->price}} kr./md eksl. moms</p>
                        <p class="spacer-1em text-center clearfix">
                          {{Form::radio("subscription_type_id", $subscription->id, 
                                true)}} {{--["class"=>"text-center"]--}}
                        </p>
                        <div class="spacer clearfix"></div>
                      </div>
                    </div>
                    </label>
                  </div>	
              </div>
          </div>
      </div>
  </div>	
@stop
@section("scripts")
@parent
<script type="text/javascript">
		$(document).ready(function() {
      $('.glyphicon').popover({
        "delay" : { "show": 200, "hide": 100 },
        "trigger": "hover"
      });
      
      $("#your-info").submit(function(e){
        if(!$("input[name='accepted_terms_conditions']").prop('checked')){
          e.preventDefault();
          alert("{{t('registration.please_accepted_conditions')}}");
        }
      });
	});
</script>
@stop
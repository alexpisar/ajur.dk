@extends("controllers.LoginController.baseLogin")


@section("subcontent")
    {{ Form::open(array('class'=>'form-login', "role"=>"form")) }}
    {{--Form::hidden("token", $token)--}}
        <div class="content-login">
            <div class="header">Account Login</div>
              @section("messages")
                {{ Alert::renderAll() }}
              @show
            <div class="form-group inputs">
                {{Form::email("email", null, ["required" => "required", "class" => "form-control", "id" => "inputUsername", "placeholder" => "Email"])}}
            </div>
            <div class="form-group inputs">
                {{Form::password("password", ["class" => "form-control", "id" => "inputPassword1", "placeholder" => "Password"])}}
            </div>
            <div class="form-group inputs">
                {{Form::password("password_confirmation", ["class" => "form-control", "id" => "inputPassword1", "placeholder" => "Password confirmation"])}}
            </div>
            {{--<div class="link-1">{{link_to_route('login.register', 'Create New Account')}}</div>--}}
            <div class="link-2">{{link_to_route('login', 'Sign In')}}</div>
            <div class="clear"></div>
            <div class="button-login">{{ Form::submit('Send', array('class' => 'btn btn-primary btn-lg btn-block', 'type' => 'button', 'id' => 'login-btn')) }}</div>
        </div><!-- .content-login -->

         <div class="footer-login">
        </div>
    {{Form::close()}}
 @stop

    
    
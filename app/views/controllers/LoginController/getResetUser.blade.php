@extends("layouts.base")

@section("content")
  <div class="container">
    <h3 class="spacer-1em font-proxima">
        <b>{{t("reminders.lost_password")}}</b>
    </h3>
    <div class="row">
      <div class="col-sm-8 font-merriweather line-2">
        {{t("reminders.restore_password_text")}}
      </div>
   </div>
    {{Form::open(["id"=>"reminder"])}}
     <div class="row spacer-1em">
      <div class="form-group">
         <div class="col-sm-8">
          <label class="font-proxima">{{t("reminders.type_in_new_password")}}</label>
         </div>
     </div>
    </div>
    <div class="row">
      <div class="col-sm-4 form-group">
        <div class="form-group">
          {{Form::email("email", null, ["class"=>"form-control", "maxlength" => "255",
                  "placeholder"=>ucfirst(t('validation.attributes.email')), "required"=>""])}}
        </div>
        <div class="form-group spacer-1em">
          <input type="password" name="password" class="form-control" 
                      placeholder="{{ t('registration.password')}}" required="required"
                      maxlength="255" value="{{Input::old('password','')}}"/>
        </div>
        <div class="form-group spacer-1em">
          <input type="password" name="password_confirmation" class="form-control" 
                      placeholder="{{ t('registration.password_confirmation') }}" required="required"
                      maxlength="255" value="{{Input::old('password_confirmation','')}}"/>
        </div>
      </div>
    </div>
        {{Form::submit(strtoupper(t("reminders.send")), 
            ["class" => "btn btn-default font-proxima"])}}
     {{Form::close()}}
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
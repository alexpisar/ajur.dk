@extends("layouts.base")

@section("content")
  <div class="content">
      <div class="container steps">
		<div class="row progress-steps">
				<div class="col-xs-12"> 
					<div class="spacer"></div>					
					<div class="step">
						1. {{t("registration.choose_subscription")}}
					</div>
					<div class="step">
						2. {{t("registration.your_information")}}
					</div>
					<div class="step active">
						3. {{t("registration.payment")}}
					</div>		
					<div class="step">
						4. {{t("registration.receipt")}}
					</div>		
				</div>

		</div>
          <div class="row spacer">
              <div class="col-md-8">
				<h4 class="font-merriweather-b"> Lorem ipsum dolor sit amet</h4>
				<p class="font-merriweather">{{t("registration.choose_payment_text")}}</p>
				<div class="spacer-2em clearfix"></div>
				<div class="spacer-2em"></div>
			   {{Form::open(["id"=>"payment-method", "route"=>"choose.paymentInfo", "class" => "font-proxima gray", "method" => "GET"])}}
          {{--Form::hidden("subscription_type_id", $subscription->id)--}}
          <div class="form-group row">
            <div class="col-sm-10 radio checked">
              <label>
                  <b>{{t("registration.credit_card")}}</b>
                  {{Form::radio("payment_method", Payment::METHOD_CREDITCARD, true, ["hidden" => "hidden"])}}
                  <img src="/img/americ-express.png" alt="American express"/>
                  <img src="/img/card-red-blue.png" alt="Credit card"/>
                  <img src="/img/jcb.png" alt="JCB"/>
                  <img src="/img/v-pay.png" alt="V-pay"/>
                  <img src="/img/visa2.png" alt="Visa"/>
                  <img src="/img/visa1.png" alt="Visa"/>
                  <img src="/img/maestro.png" alt="Maestro"/>
                  <img src="/img/MKbank.png" alt="mastercard"/>
                  <img src="/img/DKbank.png" alt="DK bank"/>
                <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.credit_card')}}"></span>
              </label>
            </div>
          </div>
          <div class="form-group row">
            <div class="col-sm-10 radio">
              <label>
                  <b>{{t("registration.online_banking")}}</b>
                  {{--Form::radio("payment_method", Payment::METHOD_ONLINE_BANKING, false, ["hidden" => "hidden"])--}}
                  <img src="/img/online3.png" alt="online"/>
                  <img src="/img/online2.png" alt="online"/>
                  <img src="/img/online1.png" alt="online"/>
                <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.credit_card')}}"></span>
              </label>
            </div>
          </div>   
          <div class="form-group row">
            <div class="col-sm-10 radio">
              <label>
                  <b>{{t("registration.payment_slip")}}</b>
                  {{--Form::radio("payment_method", Payment::METHOD_PAYMENT_SLIP, false, ["hidden" => "hidden"])--}}
                  <img src="/img/e-pay.png" alt="e"/>
                <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.payment_slip')}}"></span>
              </label>
            </div>
          </div>     
          <div class="form-group row">
            <div class="col-sm-10 radio">
              <label>
                  <b>{{t("registration.ean_payment")}}</b>
                  {{--Form::radio("payment_method", Payment::METHOD_EAN, false, ["hidden" => "hidden"])--}}
                  <img src="/img/EAN.png" alt="EAN"/>
                <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.ean_payment')}}"></span>
              </label>
            </div>
          </div>    
           <div class="spacer clearfix"></div>
          <div class="form-group row spacer">
            <div class="col-sm-10">
              <a id="btn-go-back" class="btn btn-default font-proxima" href="{{route("login.yourinfo", ["subscription_type_id" => $subscription->id])}}" role="button">
                <span>{{strtoupper(t("pagination.go_back"))}}</span></a>
              <button id="next" class="btn btn-default font-proxima pull-right" type="submit">
                <span>{{strtoupper(t("pagination.next"))}}</span></button>
           </div>
          </div>
           
           <div class="spacer clearfix"></div>

				{{Form::close()}}
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
              </div> {{--end left col--}}
              
              
              <div class="col-md-4 text-center">
                  <div id="order" class="gray font-proxima">
                      <b>{{t("registration.your_order")}}</b>
                  </div>
                  <div class="spacer-2em"></div>
                  <div class="radio">
                    <label class="width-auto text-center font-merriweather">
                    <div class="panel panel-default checked">
                      <div class="panel-heading">
                        <span class="panel-title">{{$subscription->name}}</span> 
                      </div>
                      <div class="panel-body">
                        <p>{{$subscription->persons}} persones adgang</p>
                         <div class="spacer-1em clearfix"></div>
                        <p>{{$subscription->price}} kr./md eksl. moms</p>
                        <p class="spacer-1em text-center clearfix">
                          {{Form::radio("subscription_type_id", $subscription->id, true)}}
                        </p>
                        <div class="spacer clearfix"></div>
                      </div>
                    </div>
                    </label>
                  </div>	
              </div>
          </div>
      </div>
  </div>	
@stop
@section("scripts")
@parent
<script type="text/javascript">
		$(document).ready(function() {
      $('.glyphicon').popover({
        "delay" : { "show": 200, "hide": 100 },
        "trigger": "hover"
      });
      
      $('input[type=radio]').change(function() {
        $(".radio").removeClass("checked");
        $(this).parent().parent().addClass("checked");
			});
	});
</script>
@stop
@extends("layouts.base")

@section("content")
  <div class="content">
      <div class="container steps">
		<div class="row progress-steps">
				<div class="col-xs-12"> 
					<div class="spacer"></div>					
					<div class="step active">
						1. {{t("registration.choose_subscription")}}
					</div>
					<div class="step">
						2. {{t("registration.your_information")}}
					</div>
					<div class="step">
						3. {{t("registration.payment")}}
					</div>		
					<div class="step">
						4. {{t("registration.receipt")}}
					</div>		
				</div>

		</div>
          <div class="row spacer">
              <div class="col-md-8 font-merriweather">
				<h4> Lorem ipsum dolor sit amet</h4>
				<p>{{t("registration.choose_subscription_text")}}</p>
				<div class="spacer"></div>
			   {{Form::open(["route"=>"login.yourinfo", "id"=>"chooseform", "method"=>"GET"])}}				
					{{--@foreach($pricing as $i => $item)--}}
          @for($i=0, $count = $pricing->count(); $i < $count; )
				<div class="row">
            @for($j=0; $j<3 && $i<$count; $i++, $j++)
              <div class="col-sm-4">
                <div class="radio">
                <label class="text-center">
                <div class="panel panel-default" id="{{'p' . $pricing[$i]->id}}">
                  <div class="panel-heading">
                    <span class="panel-title">{{$pricing[$i]->name}}</span> 
                  </div>
                  <div class="panel-body">
                    <p>{{$pricing[$i]->persons}} {{t("registration.persons_access")}}</p>
                     <div class="spacer-1em clearfix"></div>
                    <p>{{$pricing[$i]->price}} kr./md eksl. moms</p>
                    <p class="spacer-1em text-center clearfix">
                      {{Form::radio("subscription_type_id", $pricing[$i]->id, $pricing[$i]->id == $subscription_type_id)}}
                    </p>
                    <div class="spacer clearfix"></div>
                  </div>
                </div>
                </label>
                </div>			
              </div>
            @endfor
				</div>
				  @endfor
				<div class="spacer clearfix"></div>				
				<a id="btn-go-back" class="btn btn-default font-proxima" href="{{route("teaser")}}" role="button">
						<span>{{strtoupper(t("pagination.go_back"))}}</span></a>
				{{--Form::submit(t("pagination.next"), ["id"=>"next", "class"=>"btn btn-default font-proxima pull-right"])--}}
				<button id="next" class="btn btn-default font-proxima pull-right" type="submit">
						<span>{{strtoupper(t("pagination.next"))}}</span></button>
				{{Form::close()}}
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
              </div>
          </div>
      </div>
  </div>	
@stop
@section("scripts")
@parent
<script type="text/javascript">
		$(document).ready(function() {
			$('input[type=radio]').change(function() {
					if (this.value) {
							//alert(this.value);
							$(".panel").removeClass("checked");
							$("#p" + this.value).addClass("checked");
							//$(".panel:has(input[type='radio']:checked)").addClass("checked");
					}
			});
			//set border on checked
			$(".panel:has(input[type='radio']:checked)").addClass("checked");
      //add subscription_type_id to GET.
      $("#chooseform").submit(function(e){
        e.preventDefault();
        var id = $("input[name='subscription_type_id']:checked").val();
        if(typeof id === 'undefined'){
          //e.preventDefault();
          alert("Please, choose subscription.");
        }
        else{
          var url = $(this).attr("action") + "/?subscription_type_id=" + id;
          window.location.href = url;
        }
      });
	});
</script>
@stop
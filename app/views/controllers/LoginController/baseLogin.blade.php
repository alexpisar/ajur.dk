@extends("layouts.base_admin")

@section("styles")
    @parent
    <link href="/plugins/template/css/login.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="/plugins/template/js/html5shiv.js"></script>
    <![endif]-->
@stop

@section("content")
    <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4">
        <div id="wrapper">
        @yield("subcontent")
        </div> <!-- /#wrapper -->
    </div><!-- /.col -->
@stop

@section("scripts")
    <script src="/plugins/template/js/jquery.min.js"></script>
    <script src="/plugins/template/js/bootstrap.min.js"></script>
    <!-- Enable responsive features in IE8 with Respond.js (https://github.com/scottjehl/Respond) -->
    <script src="/plugins/template/js/respond.min.js"></script>
    <!--    <script src="/plugins/templatejs/holder.js"></script> is exist?-->
@stop


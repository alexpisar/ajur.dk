@extends("layouts.base")

@section("content")
  <div class="content">
      <div class="container steps">
		<div class="row progress-steps">
				<div class="col-xs-12"> 
					<div class="spacer"></div>					
					<div class="step">
						1. {{t("registration.choose_subscription")}}
					</div>
					<div class="step">
						2. {{t("registration.your_information")}}
					</div>
					<div class="step">
						3. {{t("registration.payment")}}
					</div>		
					<div class="step active">
						4. {{t("registration.receipt")}}
					</div>		
				</div>

		</div>
          <div class="row spacer font-merriweather">
              <div id="receipt" class="col-md-8">
				<h4> Lorem ipsum dolor sit amet</h4>
				<p class="head-note">{{t("registration.receipt_text")}}</p>
				<div class="spacer-2em clearfix"></div>
          <div class="row">
            <div class="col-sm-12">
              <h4><b>{{t("receipt.receipt_title")}}</b> </h4>
              <b>{{t("receipt.sign_up")}}</b>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5">
              {{t("receipt.date_sign_up")}}<br />
              {{t("receipt.order_number")}}<br />
              {{t("receipt.license_number")}}<br />
              {{t("receipt.name_subscription")}}<br />
              {{t("receipt.description_title")}}<br />
            </div>
            <div class="col-sm-6">
              {{$subscriber->owner->created_at->format("d/m-Y")}}<br />
              {{str_pad($subscriber->id, 9, '0', STR_PAD_LEFT)}}<br />
              {{str_pad($subscriber->licens_number, 9, '0', STR_PAD_LEFT)}}<br />
              {{$subscriber->subscription_name}}<br />
            </div>
          </div>
          <div class="row footer-line">
            <div class="col-sm-10 spacer-1em">
                {{str_replace(":data", $subscriber->persons, t("receipt.description"))}}
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
                <div class="spacer-1em"></div>
              <b>{{t("registration.payment")}}</b>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5">
              {{t("receipt.payment_method")}}<br />
              {{t("receipt.credit_card_number")}}<br />
              {{t("receipt.amount_ex_vat")}}<br />
              {{t("receipt.amount_incl_vat")}}<br />
              {{t("receipt.amount_due")}}<br />
            </div>
            <div class="col-sm-6">
              {{$payment->getMethodPayment()}}<br />
              {{$payment->formatedCardNumber()}}<br />
              {{$payment->amount_vat}}<br />
              {{$payment->amount_vat}}<br />
              {{$payment->total_amount_due}}<br />
            </div>
          </div>
          <div class="row footer-line">
            <div class="col-sm-10 spacer-1em">
              {{t("receipt.OBS_subscription_will_run")}}
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
                <div class="spacer-1em"></div>
              <b>{{t("receipt.customer_info")}}</b>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5">
              {{$subscriber->name}}<br />
              {{"CVR: " . $subscriber->vat_number}}<br />
              {{$subscriber->street_name . ", " . $subscriber->house_nr}}<br />
              {{$subscriber->post_number . " " . $subscriber->city}}<br />
              {{$subscriber->owner->name}}<br />
              {{$subscriber->phone}}<br />
              {{$subscriber->owner->email}}<br />
            </div>
          </div>
          <div class="row footer-line">
          </div>
          <div class="row">
            <div class="col-sm-12">
                <div class="spacer-1em"></div>
              <b>{{t("receipt.contact_info")}}</b>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-5">
              {{t("contact.company_name")}}<br />
              {{t("contact.street") . ", " . t("contact.house")}}<br />
              {{t("contact.post_number") . " " . t("contact.city")}}<br />
              {{t("contact.country")}}<br />
              <br />
              {{"CVR: " . t("contact.vat-number")}}<br />
              <br />
              {{t("contact.email")}}<br />
              {{t("contact.phone")}}
            </div>
          </div>
          <div class="row">
            <div class="col-sm-7 spacer-1em footnote font-proxima">
                <i>{{t("receipt.send_message_to_admin")}}</i>
            </div>
          </div>
        
          <div class="row spacer">
            <div class="col-sm-10">
                <a id="next" href="{{route("index")}}" class="btn btn-default font-proxima" role="button">
                <span>{{strtoupper(t("receipt.continue_front_page"))}}</span></a>
           </div>
          </div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
              </div> {{--end left col--}}
              
              
              <div class="col-md-4 text-center">
                  <div id="order" class="gray font-proxima">
                      <b>{{t("registration.your_order")}}</b>
                  </div>
                  <div class="spacer-2em"></div>
                  <div class="radio">
                    <label class="width-auto text-center">
                    <div class="panel panel-default checked">
                      <div class="panel-heading">
                        <span class="panel-title">{{$subscriber->subscription_name}}</span> 
                      </div>
                      <div class="panel-body">
                        <p>{{$subscriber->persons}} persones adgang</p>
                         <div class="spacer-1em clearfix"></div>
                        <p>{{$subscriber->price}} kr./md eksl. moms</p>
                        <p class="spacer-1em text-center clearfix">
                          {{Form::radio("subscription_type_id", 0, true)}}
                        </p>
                        <div class="spacer clearfix"></div>
                      </div>
                    </div>
                    </label>
                  </div>	
              </div>
          </div>
      </div>
  </div>	
@stop
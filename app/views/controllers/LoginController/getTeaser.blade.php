@extends("layouts.base")

@section("content")
  <div class="content">
      <div class="container teaser">
		<div class="row">
				<div class="col-md-8"> <h1 class="font-playfair-b">Overskrift om oprettelse</h1> </div>
		</div>
          <div class="row">
              <div class="col-md-8 font-merriweather">
				<p>{{t("registration.teaser_page_top_text")}}</p>
					<ul class="teaser-list clearfix">
							 <li>Fuld digital adgang  </li>
							<li>	Fuld digital adgang  </li>
							<li>	Ingen binding  </li>
							<li>	Ingen binding </li>
							 <li>Fuld digital adgang  </li>
							<li>	Fuld digital adgang  </li>
							<li>	Ingen binding  </li>
							<li>	Ingen binding </li>
					</ul>
				<p>{{t("registration.teaser_page_bottom_text")}}</p>
				<div class="spacer-1em"></div>
				<a class="btn btn-default font-proxima" href="{{route("choose.subscriptions")}}" role="button">{{strtoupper(t("globals.sign_up"))}}</a>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
              </div>
							
              <div class="col-md-4">
				<ul class="font-proxima teaser-right">
						<li><b>Dybegaende temaer.</b> hvor du far det fulde overblik over aktuelle begivenheder.</li>
						<li><b>Personlig aktiv.</b> hvor du far det fulde overblik over aktuelle begivenheder.</li>
				</ul>

              </div>
          </div>
      </div>
  </div>	
@stop
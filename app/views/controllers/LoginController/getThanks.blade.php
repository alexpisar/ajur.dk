@extends("layouts.base")

@section("content")
  <div class="container">
    <h3 class="spacer-1em font-proxima">
        <b>{{t("reminders.lost_password")}}</b>
    </h3>
    <div class="row">
      <div class="col-sm-8 font-merriweather line-2">
       {{ t("reminders.thank_you_text") }}
      </div>
   </div>
    <div class="row spacer-1em">
      <div class="col-sm-8 font-proxima">
          <b> {{t("reminders.thank_you_message")}} </b>
      </div>
    </div>
      <div class="spacer-1em"></div>
      <a id="next" href="{{route("index")}}" class="btn btn-default font-proxima" role="button">
                <span>{{strtoupper(t("receipt.continue_front_page"))}}</span></a>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    <div class="spacer clearfix"></div>
    
  </div>
@stop
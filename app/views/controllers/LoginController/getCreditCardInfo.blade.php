@extends("layouts.base")

@section("content")
  <div class="content">
      <div class="container steps">
		<div class="row progress-steps">
				<div class="col-xs-12"> 
					<div class="spacer"></div>					
					<div class="step">
						1. {{t("registration.choose_subscription")}}
					</div>
					<div class="step">
						2. {{t("registration.your_information")}}
					</div>
					<div class="step active">
						3. {{t("registration.payment")}}
					</div>		
					<div class="step">
						4. {{t("registration.receipt")}}
					</div>		
				</div>

		</div>
          <div class="row spacer">
              <div class="col-md-8">
				<h4 class="font-merriweather-b"> Lorem ipsum dolor sit amet</h4>
				<p class="font-merriweather">{{t("registration.credit_card_info_text")}}</p>
				<div class="spacer clearfix"></div>
			   {{Form::open(["id"=>"payment-info", "class" => "form-horizontal font-proxima gray"])}}
          <div class="form-group row">
            <label class="col-sm-3 control-label"><b>{{t("registration.credit_card")}}</b> </label>
            <div class="col-sm-7 col-md-8 col-lg-7 cards">
              <img class="active" src="/img/DKbank.png" alt="DK bank"/>
              <img src="/img/MKbank.png" alt="mastercard"/>
              <img src="/img/maestro.png" alt="Maestro"/>
              <img src="/img/visa1.png" alt="Visa"/>
              <img src="/img/visa2.png" alt="Visa"/>
              <img src="/img/v-pay.png" alt="V-pay"/>
              <img src="/img/jcb.png" alt="JCB"/>
              <img src="/img/card-red-blue.png" alt="Credit card"/>
              <img src="/img/americ-express.png" alt="American express"/>
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.credit_card_info')}}"></span>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label"><b>{{t("registration.card_number")}}</b> </label>
            <div class="col-sm-5 col-md-6 col-lg-5">
              {{Form::text("card_number", null, ["class" => "form-control", "required" => "required", "maxlength" => "16"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.credit_card_info')}}"></span>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label"><b>{{t("registration.date_expiration")}}</b> </label>
            <div class="col-sm-5 col-md-6 col-lg-5">
              {{Form::text("date_expiration1", null, ["class" => "form-control pull-left width-45", "required" => "required", "maxlength" => "2"])}}
              <label class="text-center width-10">/</label>
              {{Form::text("date_expiration2", null, ["class" => "form-control pull-right width-45", "required" => "required", "maxlength" => "2"])}}
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.date_expiration')}}"></span>
            </div>
          </div>
          <div class="form-group row">
            <label class="col-sm-3 control-label"><b>{{t("registration.validation_number")}}</b> </label>
            <div class="col-sm-5 col-md-6 col-lg-5">
              {{Form::text("validation_number", null, ["class" => "form-control pull-left width-45", "required" => "required", "maxlength" => "3"])}}
              <label class="width-10 pull-left">&nbsp;</label>
              <img src="/img/valid-num.png" class="pull-left" alt="valid-num"/>
              <span class="glyphicon glyphicon-info-sign"  aria-hidden="true" 
                      data-container="body" data-toggle="popover" data-placement="right" 
                      data-content="{{t('payment_hint.validation_number')}}"></span>
            </div>
          </div>
          <div class="form-group row spacer">
            <div class="col-sm-10">
                    <button id="next" class="btn btn-default font-proxima" type="submit">
                <span>{{strtoupper(t("registration.finish_payment"))}}</span></button>
           </div>
          </div>
           
           <div class="spacer-1em clearfix"></div>
              <p class="font-playfair footnote"><img src="/img/lock.png" class="img-responsive" alt="lock"/><i>
                    Ajur.dk er krypteret med en 128-bits SSIIkrypteringfor hoj sikkerhed. 
                    Betalingen gennemfores i somorbejde medNets.dkogQukkpay.dk. 
                    Ajur.dkerenforsikrings-ogerstatningsretliginformationsportal,
                    hvisformUier ai holde de bedste jurister opdaieret pd fagreievente 
                    informotioner.
                  </i></p>
                  <p><a href="#"><img src="/img/nets.png" alt="nets"/></a>
                  <a href="#"><img src="/img/quickpay.png" alt="quickpay"/> </a> </p>
              
              
				{{Form::close()}}
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
				<div class="spacer clearfix"></div>
              </div> {{--end left col--}}
              
              
              <div class="col-md-4 text-center">
                  <div id="order" class="gray font-proxima">
                      <b>{{t("registration.your_order")}}</b>
                  </div>
                  <div class="spacer-2em"></div>
                  <div class="radio">
                    <label class="width-auto text-center font-merriweather">
                    <div class="panel panel-default checked">
                      <div class="panel-heading">
                        <span class="panel-title">{{$subscription->name}}</span> 
                      </div>
                      <div class="panel-body">
                        <p>{{$subscription->persons}} persones adgang</p>
                         <div class="spacer-1em clearfix"></div>
                        <p>{{$subscription->price}} kr./md eksl. moms</p>
                        <p class="spacer-1em text-center clearfix">
                          {{Form::radio("subscription_type_id", $subscription->id, true)}}
                        </p>
                        <div class="spacer clearfix"></div>
                      </div>
                    </div>
                    </label>
                  </div>	
              </div>
          </div>
      </div>
  </div>	
@stop
@section("scripts")
@parent
<script type="text/javascript">
		$(document).ready(function() {
      $('.glyphicon').popover({
        "delay" : { "show": 200, "hide": 100 },
        "trigger": "hover"
      });
	});
</script>
@stop
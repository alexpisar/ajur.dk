@extends("layouts.front_page")

@section("content-column")
  <div class="sp-advanced-search">
    <label>{{t("search.advanced_search_title")}}</label>
    <div>
      {{Form::open(["route" => "search.extended"])}}
        @if(isset($typeFilterData) && !empty($typeFilterData))
          @foreach($typeFilterData as $i=>$row)
            <div class="sp-long-filter-row">
              {{Form::select("filters[type_filter][type][]", $queryTypes, $row["type"])}}
              {{Form::select("filters[type_filter][condition][]", $wordConditions, $row["condition"])}}
              {{Form::text("filters[type_filter][text][]", $row["text"], ["placeholder" => t("search.search")])}}
              <a href="#" class="sp-long-filter-add-new {{ (count($typeFilterData)===$i+1)?"":"hidden"}}">{{t("search.add_new")}}</a>
              <a href="#" class="sp-long-filter-remove {{ (count($typeFilterData)===$i+1)?"hidden":""}}">{{t("search.remove_filter")}}</a>
            </div>
          @endforeach
        @else
          <div class="sp-long-filter-row">
            {{Form::select("filters[type_filter][type][]", $queryTypes, 1)}}
            {{Form::select("filters[type_filter][condition][]", $wordConditions, 1)}}
            {{Form::text("filters[type_filter][text][]", null, ["placeholder" => t("search.search")])}}
            <a href="#" class="sp-long-filter-add-new">{{t("search.add_new")}}</a>
            <a href="#" class="sp-long-filter-remove hidden">{{t("search.remove_filter")}}</a>
          </div>
        @endif


        <div class="sp-filter-row">
          <label>{{t("search.publication_date")}}</label>
          {{Form::select("filters[publication_date]", $availableYears)}}
        </div>

        <div class="sp-filter-row">
          <label>{{t("search.content_type")}}</label>
          {{Form::select("filters[content_type]", $contentTypes)}}
        </div>

        <div class="sp-filter-row">
          <label>{{t("search.locale")}}</label>
          {{Form::select("filters[locale]", $languages)}}
        </div>

        <div class="sp-filter-row">
          <label>{{t("search.start_date")}}</label>
          <div class="sp-filter-date-row">
            {{Form::selectRange("filters[start_date][day]", 1, 31 )}}
            {{Form::select("filters[start_date][month]", $monthList, null, ["class" => "sp-filter-date-month"])}}
            {{Form::text("filters[start_date][year]")}}
          </div>
        </div>

        <div class="sp-filter-row">
          <label>{{t("search.end_date")}}</label>
          <div class="sp-filter-date-row">
            {{Form::selectRange("filters[end_date][day]", 1, 31 )}}
            {{Form::select("filters[end_date][month]", $monthList, null, ["class" => "sp-filter-date-month"])}}
            {{Form::text("filters[end_date][year]")}}
          </div>
        </div>

        <div>
          {{Form::submit(t("search.search"), ["class" => "btn btn-default font-proxima"])}}
          {{Form::reset(t("search.reset_form"), ["class" => "btn btn-default  reset-spform-button"])}}
        </div>
      {{Form::close()}}
    </div>
    <br>
    <br>
  </div>

  @if(isset($searchResults))
    <div>
      @forelse($searchResults as $row)
        <div class="response-holder">
          {{ $row->model->id }}
          <br><br><br>
          {{$row->model->title}}
          <br><br><br>
          {{$row->model->summary_full}}
          <br><br><br>
          <hr>
        </div>
      @empty
        <div class="response-holder">
          <h4>{{t("search.no_results")}}</h4>
        </div>
      @endforelse

      {{ $searchResults->links() }}
    </div>
  @endif
@stop

@section("scripts")
  @parent
  <script type="text/javascript" src="/js/searchController.js"></script>
@stop
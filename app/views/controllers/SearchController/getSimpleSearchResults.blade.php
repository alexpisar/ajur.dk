@extends("layouts.front_page")

@section("content-column")
  <div>
    @forelse($searchResults as $row)
      <div class="response-holder">
        {{ $row->model->id }}
        <br><br><br>
        {{$row->model->title}}
        <br><br><br>
        {{$row->model->summary_full}}
        <br><br><br>
        <hr>
      </div>
    @empty
      <div class="response-holder">
        <h4>{{t("search.no_results")}}</h4>
      </div>
    @endforelse

    {{ $searchResults->links() }}
  </div>
@stop

@section("scripts")
  @parent
  <script type="text/javascript" src="/js/searchController.js"></script>
@stop
@extends("layouts.base_admin")

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
   
    <div id="main" role="main">
        <div class="block">
            <div class="clearfix"></div>
            <!--page title-->
              <div class="pagetitle">
                  <h1>PRICING</h1>
              </div>
               <div class="clearfix spacer-2em"></div>
              {{Form::open()}}
              <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3">
                  <div class="grid">
                      <div class="grid-title">
                          <div class="pull-left">
                            <span class="table-title" id="title">{{Input::old("name")}}</span> 
                            <div class="clearfix"></div>
                         </div>
                      </div>
                      <div class="grid-content">
                          <div class="control-group clearfix">
                              <label class="control-label label-text" for="appendedInput">Navn</label>
                              <div class="controls">
                                  {{Form::text("name", null, ["class" => "form-control input-name", "maxlength" => "255", "required" => "required", "data-target" => "title"])}}
                              </div>
                          </div>
                          <div class="control-group clearfix">
                              <label class="control-label label-text" for="appendedInput">Personers adgang</label>
                              <div class="controls">
                                  {{Form::text("persons", null,["class" => "form-control width-60", "maxlength" => "6", "required" => "required"])}}
                              </div>
                          </div>
                           <div class="control-group clearfix">
                              <label class="control-label label-text" for="appendedInput">kr./md eksl. moms</label>
                              <div class="controls">
                                  {{Form::text("price", null,["class" => "form-control width-60", "maxlength" => "20", "required" => "required"])}}
                              </div>
                          </div>
                           <p class="spacer-1em">
                                {{Form::checkbox('institutional_access', 1, false,["id" => "ca"])}}
                                <label for="{{"ca"}}"><span></span>Institutional access</label>
                            </p>
                      </div>
                    </div>
                  </div>
              </div>
              <div class="clearfix"></div>
                <div class="row form-group spacer">
                    <div class="col-xs-3 col-md-2">
                        {{--URL::previous()--}}
                        <a href="{{route("pricing")}}" class="btn btn-default width-100" role="button">Cancel</a>
                    </div>
                    <div class="col-xs-3 col-md-2">
                        {{Form::submit("Save", ["class" => "btn btn-success width-100"])}}
                    </div>
                </div>
              </div>
              {{Form::close()}}
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section("scripts")
    @parent
    <script type="text/javascript">
    $(document).ready(function() {
                //Autofill title by name
        $(".input-name").blur(function(e){
          var id = $(this).data("target");
          $("#" + id).text($(this).val());
        });
    });
    </script>
@stop
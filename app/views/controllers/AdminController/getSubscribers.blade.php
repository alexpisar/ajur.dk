@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
   		  <div class="clearfix"></div>
   		   <div class="pagetitle">
                    <h1>SUBSCRIBERS (OVERVIEW)</h1>
            </div>
          <div class="grid">
	          <div class="grid-title">
	           <div class="pull-left">
	              <span class="table-title">Subscribers</span> 
	              <div class="clearfix"></div>
	           </div>
	         </div>
	         <div class="grid-content overflow">
	          <table class="table table-bordered table-mod-2 table-responsive" id="datatable_3">
	            <thead>
	              <tr>
	                <th>Company name</th>
	                <th>Joined</th>
	                <th>Delete</th>
	                <th>Edit</th>
	              </tr>
	            </thead>
	            <tbody>
				   {{--  @foreach($items as $item)
				      <tr>
				        <td>{{ $item->name }}</td>
				        <td>{{ $item->created_at->format("d.m.Y") }}</td>
						<td class="action-table"> <a href="{{route("subscribers.delete", ["id" => $item->id]) }}"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a> </td>
						<td class="action-table"> <a href="{{route("subscribers.edit", ["id" => $item->id]) }}"><img src="/plugins/template/images/icon/table_view.png" alt="Delete"></a> </td>	
				     </tr>
				    @endforeach	 --}}            	
	            </tbody>
	          </table>           
	         </div>
          </div>
   		  <div class="clearfix"></div>           
   		  </div>
   		</div>

</div>

@stop

@section("scripts")
@parent
<script src="/plugins/data-table/js/jquery.dataTables.min.js"></script>
<script>
	$(document).ready(function() {
		$('#datatable_3').dataTable( {
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url" : "{{route("subscribers.table")}}"
            },
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) { //sData - text content of <td>
                        var url = "{{route("subscribers.delete")}}" + "?id=" + sData;
                        $(nTd).html('<a href="'+ url +'"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a>').addClass("action-table");
                    }
                },
                { "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var url = "{{route("subscribers.edit")}}" + '?id=' + sData;
                        $(nTd).html('<a href="'+ url +'"><img src="/plugins/template/images/icon/table_view.png" alt="Edit"></a>').addClass("action-table");
                    }
                }
            ],
			"oLanguage": { //
				"sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
				'<option value="5">5</option>'+
				'<option value="10">10</option>'+
				'<option value="50">50</option>'+
				'<option value="100">100</option>'+
				'<option value="200">200</option>'+
				'</select>entries </div> <div class="clearfix"></div>'

			},
			"fnInitComplete":function (){
				$(".dataTables_filter input").addClass("form-data-table");
				$(".dataTables_filter label").addClass("form-table-label");
			}
		} );
	} );
</script>
@stop
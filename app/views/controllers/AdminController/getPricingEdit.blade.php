@extends("layouts.base_admin")

@section("content")

@include("partials.admin.header")
    {{-- <div class="alert alert-warning alert-dismissable" hidden="hidden">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Attention!</strong> If you delete a pricing, all data about subscribed users will be deleted automatically.
    </div> --}} 
<div id="wrap">
	@include("partials.admin.sidebar")
    <div class="alert alert-warning alert-dismissable" hidden="hidden">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <strong>Attention!</strong> If you delete a pricing, all data about subscribed users will be deleted automatically.
    </div>    
    <div id="main" role="main">
        <div class="block">
            <div class="clearfix"></div>
            <!--page title-->
              <div class="pagetitle">
                  <h1>PRICING</h1>
              </div>
              <div class="clearfix"></div>
              <div class="container">
                <div class="row">
                  <div class="col-xs-3 col-md-3">
                    <a href="{{route("pricing.new")}}" class="btn btn-success btn-block width-100"   role="button">Add new pricing</a>
                  </div>
                </div>
              </div>
               <div class="clearfix spacer-2em"></div>
              {{Form::open()}}
              {{Form::hidden("count", $pricing->count())}}
              <div class="container">
              <div class="row">
              
              @for($i=0, $count = $pricing->count(); $i < $count; )
            {{-- <div class="row">   --}}
                {{-- @foreach($pricing as $price) --}}
                @for($j=0; $j<3 && $i<$count; $i++, $j++ )
                  <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 is-delete">
                  {{Form::hidden("id" . ($i), $pricing[$i]->id)}}
                    <div class="grid">
                        <div class="grid-title">
                            <div class="pull-left">
                              <span class="table-title" id={{"title" . $i}}>{{$pricing[$i]->name}}</span> 
                              <div class="clearfix"></div>
                           </div>
                        </div>
                        <div class="grid-content">
                            <div class="control-group clearfix">
                                <label class="control-label label-text" for="appendedInput">Navn</label>
                                <div class="controls">
                                    {{Form::text("name" . $i, $pricing[$i]->name, ["class" => "form-control input-name", "maxlength" => "255", "required" => "required", "data-target" => "title" . $i])}}
                                </div>
                            </div>
                            <div class="control-group clearfix">
                                <label class="control-label label-text" for="appendedInput">Personers adgang</label>
                                <div class="controls">
                                    {{Form::text("persons" . $i, $pricing[$i]->persons,["class" => "form-control width-60", "maxlength" => "6", "required" => "required"])}}
                                </div>
                            </div>
                             <div class="control-group clearfix">
                                <label class="control-label label-text" for="appendedInput">kr./md eksl. moms</label>
                                <div class="controls">
                                    {{Form::text("price" . $i, $pricing[$i]->price,["class" => "form-control width-60", "maxlength" => "20", "required" => "required"])}}
                                </div>
                            </div>
                             <p class="spacer-1em">
                                {{Form::checkbox('institutional_access' . $i, 1, $pricing[$i]->institutional_access,["id" => "ca" . $i])}}
                                <label for="{{"ca" . $i}}"><span></span>Institutional access</label>
                            </p>
                            <p class="spacer-1em text-center delete">
                                {{Form::checkbox('isDelete' . $i, 1, false,["id" => "c" . $i])}}
                                <label for="{{"c" . $i}}"><span></span>Delete</label>
                            </p>
                        </div>
                    </div>
                  </div>
                {{-- @endforeach --}}
                @endfor
              @endfor
              </div>
              <div class="clearfix"></div>
                <div class="form-group spacer">
                    <div class="col-xs-offset-6 col-md-offset-8 col-xs-3 col-md-2">
                        {{--URL::previous()--}}
                        <a href="{{route("dashboard")}}" class="btn btn-default width-100" role="button">Cancel</a>
                    </div>
                    <div class="col-xs-3 col-md-2">
                        {{Form::submit("Save changes", ["class" => "btn btn-success width-100"])}}
                    </div>
                </div>
              </div>
              {{Form::close()}}
            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop

@section("scripts")
    @parent
    <script type="text/javascript">
    $(document).ready(function() {
        $(".delete input[type='checkbox']").change(function(){
            if($(this).is(":checked")) {
              // var returnVal = confirm("Attention! If you delete this pricing, all data about subscribed users will be deleted automatically.");
               // $(this).attr("checked", returnVal);
               $(".alert").show();
                $(this).parents(".is-delete").addClass("deleted");//.css( "opacity", ".4" );
            }
            else{
                $(this).parents(".is-delete").removeClass("deleted");
            }
        });
                //Autofill title by name
        $(".input-name").blur(function(e){
          var id = $(this).data("target");
          $("#" + id).text($(this).val());
        });
    });
    </script>
@stop
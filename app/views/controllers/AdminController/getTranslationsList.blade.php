@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>Enabled languages</h1>
                </div>
                      <div class="grid">
                        <div class="grid-title">
                            <div class="pull-left">
                                <span class="table-title">Enabled languages</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="grid-content overflow">
                            <table  id="datatable_3" class="table table-bordered table-mod-2 table-responsive">
                              <thead>
                                <tr>
                                  <th> Name </th>
                                  <th> Short </th>
                                  <th> Delete </th>
                                  <th> Edit </th>
                                </tr>
                              </thead>
                              <tbody>
                                @foreach($languages as $lang)
                                  <tr>
                                    <td> {{$lang->name}} </td>
                                    <td> {{$lang->locale}} </td>
                                    <td class="action-table"> 
                                        @if(strtolower($lang->locale) == strtolower(Config::get("app.fallback_locale")))
                                            Can't delete default language
                                        @else
                                            <a href='{{route("settings.translations.delete", ["locale" => $lang->locale])}}'><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a> 
                                        @endif
                                    </td>
                                    <td class="action-table"> <a href='{{route("settings.translations.edit", ["locale" => $lang->locale])}}'><img src="/plugins/template/images/icon/table_view.png" alt="Edit"></a> </td>
                                  </tr>
                                @endforeach
                              </tbody>
                            </table>
                        </div>
                    </div>
                <a id="btn-staff" href="{{route('settings.translations.edit', ['locale' => 'null'])}}" class="btn btn-success btn-block pull-right" role="button">Add new </a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop



@section("scripts")
    @parent
    <script src="/plugins/data-table/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#datatable_3').dataTable( {
            "pagingType": "full_numbers",
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": true },
                { "bSortable": false},
                { "bSortable": false}
            ],
            "oLanguage": { //
                "sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<option value="50">50</option>'+
                '<option value="100">100</option>'+
                '<option value="200">200</option>'+
                '</select>entries </div> <div class="clearfix"></div>'

            },
            "fnInitComplete":function (){
                $(".dataTables_filter input").addClass("form-data-table");
                $(".dataTables_filter label").addClass("form-table-label");
            }
           // "order": [[ 1, "asc" ]] //default sort
        } );
    } );
    </script>
@stop

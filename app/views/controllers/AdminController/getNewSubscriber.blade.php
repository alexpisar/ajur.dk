@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
          <!--page title-->
              <div class="pagetitle">
                  <h1>ADD NEW SUBSCRIBER</h1>
              </div>
              <div class="clearfix"></div>
   		  <div class="clearfix"></div>
          {{Form::open()}}
          {{-- Company information --}}
          <div class="grid">
	        <div class="grid-title">
	           <div class="pull-left">
	              <span class="table-title">Company information</span> 
	              <div class="clearfix"></div>
	           </div>
	         </div>
	         <div class="grid-content overflow">
	          <table class="table" id="">
	            <tbody class="vertical-top">
				      <tr>
				        <td class="col-sm-3 col-md-2 col-lg-2">{{ Form::label("name", "Company name") }}</td>
				        <td class="action-table">{{ Form::text("name", null, ["required"=>"required", "class"=>"form-control", "placeholder" => "Type in company name", "maxlength"=>"255"])}} <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /> </td>
				      </tr>
              <tr>
                <td>{{ Form::label("vat_number", "VAT number") }}</td>
                <td class="action-table">{{ Form::text("vat_number", null, ["required"=>"required", "class"=>"form-control", "placeholder" => "Type in vat number", "maxlength"=>"255"])}} <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /></td>
              </tr>
               <tr>
              <td>{{ Form::label("license_number", "License number") }}</td>
                <td class="action-table">{{ Form::text("licens_number", null, ["readonly"=>"readonly" ,"required"=>"required", "class"=>"form-control", "maxlength"=>"255"])}} </td>
              </tr>
             <tr>
                <td>{{ Form::label("subscription_type", "Subscription type") }}</td>
                <td class="action-table">
                    <div class="form-group inner-row">
                      {{Form::hidden('subscription_type_id', 0, ["id" => "subscription_type_id"])}}
                      <div class="col-xs-10 col-md-5">
                        {{Form::text("name_subscription", "choose subscription (From green button)", ["class"=>"form-control", "id" => "name_subscr", "required"=>"required", "readonly"])}}
                      </div>
                      <div class="col-xs-4 col-md-2">
                          <a href="#" class="btn btn-danger width-100 metro disabled" role="button">Delete <br /> subscription</a>
                      </div>
                    <div class="col-xs-4 col-md-2">
                        <a href="#" class="btn btn-success width-100 metro" role="button" data-toggle="modal" data-target="#modalSelect">Change <br /> subscription</a>
                    </div>
                    <div class="col-xs-4 col-md-2">
                        <a href="#" class="btn btn-warning width-100 height-eq disabled" role="button">Payments{{--  <br />&nbsp --}}</a>
                    </div>
                </div>
                   <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                 </td>
              </tr>
    
              <tr>
                <td>{{ Form::label("joined", "Joined") }}</td>
                <td  class="action-table">{{ Form::text("joined", null, ["class"=>"form-control", "required"=>"required", "readonly", "placeholder"=>"Date (XX/XX/XX)"])}}
                  <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                </td>
              </tr>
              <tr>
                <td>{{ Form::label("address", "Address") }}</td>
                <td  class="action-table"><p>{{ Form::text('street_name',  null, array('class' => 'form-control', "required"=>"required", 'placeholder' => 'Street name', "maxlength"=>"255")) }}</p>
                  <p class="spacer-1em">{{ Form::text('post_number',  null, array('class'=>'form-control', "required"=>"required", 'placeholder' => 'Post number', "maxlength"=>"255")) }}</p>
                  <p class="spacer-1em">{{ Form::text('city',  null, array('class'=>'form-control', 'placeholder' => 'City/town', "required"=>"required", "maxlength"=>"255")) }}</p>
                  <p class="spacer-1em">{{ Form::text('country',  null, array('class'=>'form-control', 'placeholder' => 'Country', "required"=>"required", "maxlength"=>"255")) }}</p>
                    <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                </td>
              </tr>
              <tr>
                  <td>{{ Form::label("note", "Add note") }}</td>
                  <td class="action-table">
                     {{Form::textarea("note", null, ["id" => "note", "class"=>"form-control width-100", "rows" => "5"])}}
                     <a href="#" id="clean-note"> <img class="delete-icon" src="/plugins/template/images/icon/table_del.png" /></a>
                     <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                  </td>
              </tr>


	            </tbody>
	          </table>           
	         </div>
          </div>
          {{-- Administrator --}}
         <div class="grid">
          <div class="grid-title">
             <div class="pull-left">
                <span class="table-title">Administrator</span> 
                <div class="clearfix"></div>
             </div>
           </div>
           <div class="grid-content overflow">
            <table class="table">
              <tbody>
              <tr>
                <td class="col-sm-3 col-md-2 col-lg-2">{{ Form::label("name", "Name") }}</td>
                <td class="action-table">{{ Form::text("nameAdmin", null, ["id" => "name-admin", "required"=>"required", "class"=>"form-control admin-input", "placeholder" => "Type in name administrator", "maxlength"=>"255", "data-target" => "td-name-admin"])}}
                 <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /> </td>
              </tr>
             <tr>
                <td>{{ Form::label("email", "E-mail") }}</td>
                <td class="action-table">
                   <div class="form-group inner-row">
                      <div class="col-xs-10 col-md-8">
                        {{ Form::email("emailAdmin", null, ["required"=>"required", "class"=>"form-control admin-input", "placeholder" => "Type in administrator's email", "maxlength"=>"255", "id" => "email-admin", "data-target" => "td-email-admin"])}}
                      </div>
                    <div class="col-xs-4 col-md-3">
                        <a href="#" class="btn btn-default width-100 disabled" role="button">Recover password</a>
                    </div>
                   </div>
                   <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                 </td>
              </tr>
              <td>{{ Form::label("phone", "Phone") }}</td>
                <td class="action-table">{{ Form::text("phone", null, ["required"=>"required", "class"=>"form-control", "maxlength"=>"255", 'placeholder' => "Type in administrator's phone number"])}}
                 <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /></td>
              </tr>
              <tr>
                <td>{{ Form::label("note", "Add note") }}</td>
                <td  class="action-table">{{ Form::text("note_about_admin", null, ["class"=>"form-control", "placeholder"=>"Add short note about administrator", "maxlength"=>"255"])}}
                  <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                </td>
              </tr>
              <tr>
                <td>{{ Form::label("occupation", "Occupation") }}</td>
                <td  class="action-table">{{ Form::text('occupation',  null, array('class' => 'form-control', "required"=>"required", 'placeholder' => "Type in administrator's occupation", "maxlength"=>"255")) }}
                    <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                </td>
              </tr>
              <tr>
                  <td>{{ Form::label("optional", "Optional") }}</td>
                  <td class="action-table">
                     {{Form::text("optional", null, ["class"=>"form-control width-100"])}}
                     <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                  </td>
              </tr>
              </tbody>
            </table>           
           </div>
          </div> 
          {{-- Users --}}
          {{Form::hidden("countCustomers", null, ["id"=> "countCustomers"])}}
          <div class="grid">
          <div class="grid-title">
             <div class="pull-left">
                <span class="table-title">Users</span> 
                <div class="clearfix"></div>
             </div>
           </div>
           <div class="grid-content overflow">
            <table class="table table-bordered table-mod-2 table-responsive" id="datatable_3">
              <thead>
                <tr>
                  <th></th>
                  <th>E-mail</th>
                  <th>Name</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody id="users-table">
                <tr>
                  <td>Admin</td>
                  <td >{{Form::text("tdEmailAdmin", null, ["class"=>"form-control", "readonly", 'placeholder' => "Administrator's e-mail", "id" => "td-email-admin"])}}</td>
                  <td>{{Form::text("tdNameAdmin", null, ["class"=>"form-control", "readonly", 'placeholder' => "Administrator's name", "id" => "td-name-admin"])}}</td>
                  <td></td>
                </tr>
            <?php 
                  if(Input::old("countCustomers"))
                    $maxLength = strlen($subscTypes->max('persons')); 
                //for sorting by name User 000. 
            ?>
                @for( $i=1; $i < Input::old("countCustomers"); $i++)
               <tr>
                <td>User {{str_pad($i+1, $maxLength, '0', STR_PAD_LEFT)}}</td>
                <td>  {{-- Form::hidden("user_id[$i]", 0)--}}
                       {{ Form::text("tdEmailUser$i", null, ["class"=>"form-control", "placeholder" => "*Add new user", "maxlength"=>"255"])}}
                </td>
                <td>
                  {{ Form::text("tdNameUser$i", null, ["class"=>"form-control", "placeholder" => "*Add name of new user", "maxlength"=>"255"])}}
                </td>
              <td> <a href="#" class="action-table"><img src="/plugins/template/images/icon/table_del.png" alt="Delete" class="clear-row"></a></td>
              </tr>               
              @endfor
              </tbody>
            </table>           
           </div>
          </div>
          <table id="hidden-pages-data"></table>
          {{Form::submit("Create new subscriber",["id"=>"btn-staff", "class"=>"btn btn-success btn-block pull-right"])}}
          {{Form::close()}}
   		  <div class="clearfix"></div>           
   		  </div>
   		</div>
</div>

    <!-- Modal edit-->
    <div class="modal fade" id="modalSelect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Select subscription</h4>
                </div>
                <div class="modal-body">
                    {{Form::select("personsCount", $subscTypes->lists("persons", "id"), null, ["hidden" => "hidden", "id" => "persons-count"])}}
                    {{Form::select("subscriptions", $subscTypes->lists("name", "id"), null, ["class"=>"form-control", "id" => "nameContent"])}}
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="#" id="select-subscr" role="button" class="btn btn-success" data-dismiss="modal">Select</a>
                </div>
            </div>
        </div>
    </div>
@stop

@section("scripts")
@parent
{{-- <script src="/plugins/template/js/jquery.dataTables.min.js"></script> --}}
<script src="/plugins/data-table/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
  var tableCust = null,
      maxCountCust = 0;

  $(document).ready(function() {
                //clean note
         $("#clean-note").on("click", function(e) {
         // console.log("click clean");
           // $("#note").text('');
            $("#note").val('');
          });
      
   tableCust =  $('#datatable_3').dataTable( {
      "sPaginationType": "full_numbers",
      "sPageButton": "btn btn-default",
      "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false }
                ],
      "oLanguage": { //
        "sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
        '<option value="5">5</option>'+
        '<option value="10" selected="selected">10</option>'+
        '<option value="50">50</option>'+
        '<option value="100">100</option>'+
        '<option value="200">200</option>'+
        '</select>entries </div> <div class="clearfix"></div>'

      },
      "fnInitComplete":function (){
        $(".dataTables_filter input").addClass("form-data-table");
        $(".dataTables_filter label").addClass("form-table-label");
      }
    } );

   //==================================================================
        //Event Choose Subscriber Type
        $("#select-subscr").on("click", function(e){
           // e.preventDefault();
           var countCust = 1, //admin
               subscr_id = $("#nameContent").val(),
               subscr_name = $('#nameContent option[value="' + subscr_id + '"]').text(),
               countChange = parseInt($('#persons-count option[value="' + subscr_id + '"]').text());
            if($("#countCustomers").val())
                countCust = parseInt($("#countCustomers").val());
            if(countChange < 1)
                  countChange = 1;
            //console.log("current: " + countCust + " new: " + countChange);
            if(countCust > countChange)
              removeCustomers(countChange, countCust);
            else if(countChange > countCust)
              addCustomers(countCust, countChange);
            else if(!$("#users-table input[name='tdNameUser"+(countCust - 1)+"']").length > 0
                        && countChange > 1){ // if ==  for back->withInput()
             // console.log('addUser ==' + (countCust - 1));
              addCustomers(1, countChange);
            }

            $("#name_subscr").val(subscr_name);
            $("#subscription_type_id").val(subscr_id);
            $("#countCustomers").val(countChange);
            //console.log(countCust);
            
        });

        getMaxCountCust();

        //Table Users - event click delete icon.
        $("#users-table").on("click", '.clear-row' , function(e){
            //console.log("clearRow");
            $(this).parents("tr").find("input").val('');
        });

        //Autofill admin record in table users
        $(".admin-input").blur(function(e){ //leave focus
          var id = $(this).data("target");
          //$("#" + id).val($(this).val());
          //if row [0] is hidden
          $(tableCust.fnGetNodes(0)).find("#" + id).val($(this).val());
        });

                //click submit & get all rows table to post
         $("#btn-staff").on("click" , function(e){
            //set first page
            var countItems = $("#datatable_3_length select option:selected").val();
             var data = tableCust.$('tr');//.slice(0, countItems);           
            $("#users-table").html(data.slice(0, countItems));
            //append hidden data
            $("#hidden-pages-data").html(data.slice(countItems)); //for raplace, when many submitting 
        }); 
  });



        function addCustomers(begin, end){
          if(begin == end)
            return;
          var mData = [], row = null, formatter, num;
          // for (var i = 0; i < end.length; i++)
          //   formatter += "#";

          for (var i = begin; i < end; i++) {
            row = [];
             //num = $.parseNumber(i +1, {format:formatter, locale:"us"});
             formatter = ""; //put '0' to the begin for sorting - if redrow table
             num = (i + 1).toString();
             diff = maxCountCust.toString().length - num.length;
             for (var j = 0; j < diff; j++)
                formatter += "0";

              formatter += num;

              row.push('User '+ formatter);
              row.push('<input class="form-control" maxlength="255" placeholder="*Add new user" name="tdEmailUser'+ i + '" type="email">');
              row.push('<input class="form-control" maxlength="255" placeholder="*Add name of new user" name="tdNameUser'+ i + '" type="text">');
              row.push('<a href="#" class="action-table"><img src="/plugins/template/images/icon/table_del.png" alt="Delete" class="clear-row"></a>');
             mData.push(row);
          };
           // tableCust.fnDraw();
            //console.log(mData);
           tableCust.fnAddData(mData);
        }

        function removeCustomers(begin, end){
          if(begin == end)
            return;
          for (var i = end; i > begin; i--) {
            tableCust.fnDeleteRow(i - 1, null, false);
          };
            tableCust.fnDraw(false);
          //console.log("removeRows " + begin + ", " + end);
        }

        function getMaxCountCust () {
          $('#persons-count option').each(function(index){
            //console.log($(this).text());
            if(parseInt($(this).text()) > maxCountCust)
              maxCountCust = parseInt($(this).text());
          });
          return maxCountCust;
        }
</script>
@stop
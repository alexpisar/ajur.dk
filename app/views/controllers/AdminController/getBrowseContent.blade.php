@extends("layouts.base_admin")
@section("styles")
    @parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <div class="grid">
                    <div class="grid-title">
                        <div class="pull-left">
                            <span class="table-title">{{$_siteTitle}}</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="grid-content overflow">
                        <a id="btn-staff" href="{{route("content.add")}}" class="btn btn-success btn-block" role="button">Add new content</a>
                        <div class="clearfix spacer-2em"></div>
                        {{--<div class="form-horizontal form-group">
                            <label class="col-sm-1 control-label">Show</label>
                            <div class="col-sm-2">
                                {{Form::select("filterPublished", ["Unpublished", "Published"], 1, ["class" =>"form-data-table", "id" => "filterPublished2"])}}
                            </div>
                        </div>--}}
                        <label><div class="form-group-dt">Show{{Form::select("filterPublished", ["Unpublished", "Published"], 1, ["class" =>"form-data-table", "id" => "filterPublished"])}}</div></label>
                        <div class="clearfix spacer-1em"></div>
                          <table id="table-content" class="table table-bordered table-mod-2 table-responsive">
                            <thead>
                            <tr>
                                <th>{{Form::select("filterTypes", $filterTypes, 0, ["class" =>"form-control", "id" => "filterTypes" ])}}</th>
                                <th>{{Form::select("filterTitles", ["titles" => "Title", "authors" => "Authors", "publishers" => "Publishers"], "titles", ["class" =>"form-control", "id" => "filterTitles" ])}}</th>
                                <th>By</th>
                                <th>Date</th>
                                <th>Delete</th>
                                <th></th>
                            </tr>
                            </thead>

                        </table>
                        {{--<div class="pull-right">
                            {{$items->appends(['search' => $search, 'itemsPerPage' => $itemsPerPage])->links()}}
                        </div>--}}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop



@section("scripts")
    @parent
    <script src="/plugins/data-table/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        var tableContent = null;
        $(document).ready(function() {
           tableContent =  $('#table-content').dataTable( {
               "pagingType": "full_numbers",
               "sPageButton": "btn btn-default",//not work
                "processing": true,
                "serverSide": true,
                "ajax":{
                    "url" : "{{route("content.browse.json")}}",
                    "data":  function ( d ) {
                        d.published = $('#filterPublished').val();
                        d.filterTypes = $('#filterTypes').val();
                        d.filterTitles = $('#filterTitles').val();
                        // etc
                    }
                },
                "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) { //sData - text content of <td>  = $article->id;
                            var url = "{{route("content.delete")}}" + "/?id=" + sData;
                           // $(nTd).html('<a href="'+ url +'"><img class="delete-item" src="/plugins/template/images/icon/table_del.png" alt="Delete"></a>').addClass("action-table");
                            $(nTd).html('<a href="#" data-url="'+ url +'"  class="delete-item"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a>').addClass("action-table");
                        }
                    },
                    { "bSortable": false,
                        "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                            var url = '{{route("content.article.edit")}}' + '/?id=' + sData;
                            $(nTd).html('<a href="'+ url +'"><img src="/plugins/template/images/icon/table_view.png" alt="Edit"></a>').addClass("action-table");
                        }
                    }
                ],
                "oLanguage": { //
                    "sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="50">50</option>'+
                    '<option value="100">100</option>'+
                    '<option value="200">200</option>'+
                    '</select>entries </div> <div class="clearfix"></div>'

                },
               "fnInitComplete":function (){
                   $(".dataTables_filter input").addClass("form-data-table");
                   $(".dataTables_filter label").addClass("form-table-label");
               }

            } );

            $('#filterPublished, #filterTitles, #filterTypes').change(
                function(){
                   tableContent.fnDraw();

                });

            $("#table-content tbody").on("click", '.delete-item' , function(e){
                e.preventDefault();
                var url = $(this).data("url");
                $.ajax({
                    "url"            : url,
                    "type"          : "GET",
                    "success"       : function (data){
                        if(data.success){
                            console.log("success delete");
                            tableContent.fnDeleteRow($(this).parents('tr'));
                           // tableContent.fnRedraw();
                           // $(this).parents('tr').remove();
                        }
                        else{
                            alert('Something went to wrong.Please Try again later...');
                        }
                    },
                    "error"         : function (xhr, textStatus, thrownError){
                        alert('Something went to wrong.Please Try again later...');
                        console.log(thrownError);
                    }
                });
                });
        } );



    </script>
@stop

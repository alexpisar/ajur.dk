@extends("layouts.base_admin")

@section("styles")
    @parent
        {{--Topics-Tree Plugin--}}
    <link href="/plugins/jstree-master/dist/themes/default/style.css" rel="stylesheet" type="text/css"/>
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                    <div class="grid">
                        <div class="grid-title">
                            <div class="pull-left">
                                <div class="icon-title"><i class="icon-folder-open"></i></div>
                                <span class="table-title">Build Database</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="grid-content overflow">
                            <div id="tree"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modalUploads" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                {{Form::open(["route" => "tree.uploadIcon", "files" => true, "id"=> "formUploadIcon"])}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Upload Tab Icon</h4>
                </div>
                <div class="modal-body">
                        <div class="form-group">
                            {{Form::hidden("tabID", null, ["id"=>"tab_id"])}}
                            <label class="control-label">Push to upload icon:</label>
                            <div class="spacer-1em">
                            {{Form::file('fileIcon', ['id' => 'inputFile', "class" => "filestyle", "data-buttonBefore" => "true", "accept" => "image/jpeg,image/jpg,image/png,image/bmp"])}}
                            </div>
                            <div class="help-block" id="show-icon">
                                Current icon:
                                <img src="#" class="img-responsive img-showicon show-icon" alt="icon" />
                            </div>
                            <div class="help-block" id="no-icon">
                                No current icon
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{Form::submit('Save changes', array('class'=>'btn btn-success', 'id'=>'btnSendIcon'))}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalWeight" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                {{Form::open(["route" => "tree.weight", "id"=> "formWeight"])}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Set weight</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        {{Form::hidden("topicID", null, ["id"=>"topic_id"])}}
                        <label>Edit weight of topic:</label>
                        {{Form::text('inputWeight', null, ['id' => 'inputWeight', "class" => "form-control"])}}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{Form::submit('Save changes', array('class'=>'btn btn-success', 'id'=>'btnSetWeight'))}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@stop
@section("scripts")
    @parent
    {{--Topics-Tree plugin--}}
    <script src="/plugins/jstree-master/dist/jstree.js" type="text/javascript"></script>
    <script src="/plugins/input-file/bootstrap-filestyle.min.js" type="text/javascript"></script>
    @include("partials.admin.scriptTreeBuild")

@stop

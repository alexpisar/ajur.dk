@extends("layouts.base_admin")

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>{{strtoupper($_siteTitle)}}</h1>
                </div>
                <div class="clearfix"></div>
                <div class="spacer"></div>
                <div class="container">
                    {{Form::open(['class'=>'form-horizontal', 'role' => 'form', 'files' => true])}}
                    <div class="form-group">
                        <div class="col-sm-offset-1 col-sm-5">
                            <p>
                            {{Form::select("selectFile", $images, $activeID, ["class" => "form-control", "id" => "selectFile"])}}
                            {{Form::select("selectPath", $imagePaths, null, ["hidden" => "hidden", "id" => "selectPath"])}}
                            </p>
                            <p class="spacer">
                                {{Form::file('inputFile', ['id' => 'inputFile', "class" => "filestyle", "accept" => "image/jpeg,image/jpg,image/png,image/bmp", 'title' => 'Select file'])}}
                            </p>
                            <p>
                            <P  id="upload" class="text-right">
                                <a href="#" class="btn btn-default width-100" role="button">Upload</a>
                                {{--Form::submit('Upload',["class" => "btn btn-default width-100"])--}}
                            </P>
                            </p>
                        </div>
                        <div class="col-sm-6 show-advert">
                            <label>Current advertisement</label>
                            <img src="" id="show-image" class="img-responsive spacer-1em" alt="no image" />
                        </div>
                    </div>
                    <div class="clearfix spacer"></div>
                    <div class="form-group spacer">
                        <div class="col-sm-offset-4 col-sm-2">
                            {{--URL::previous()--}}
                            <a href="{{route("dashboard")}}" id="clear-destination" class="btn btn-default width-100" role="button">Cancel</a>
                        </div>
                        <div class="col-sm-2">
                            {{--<button type="submit" class="btn btn-default width-100" id="new-author-submit">Save to list</button>--}}
                            {{Form::submit("Save", ["class" => "btn btn-success width-100"])}}
                        </div>
                    </div>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@stop

@section("scripts")
    @parent
    <script src="/plugins/input-file/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        //$('.selectpicker').selectpicker();
        $(document).ready(function() {
            $("#upload").on("click", function(e){
                e.preventDefault();
                if($("#inputFile").val())
                    $('form').trigger('submit');
            });
            //$(":file").filestyle('input', false);
            $("#selectFile").change(refreshImage);
            refreshImage();
        });

        function refreshImage(){
            var id = $("#selectFile").val();
            if(id == 0)
                $("#show-image").attr("src", "");
            else {
               // $("#selectPath").val(id).change();
                $("#show-image").attr("src", $('#selectPath option[value="' + id + '"]').text());
               // console.log($('#selectPath option[value="' + id + '"]').text());
            }
        }
    </script>
@stop
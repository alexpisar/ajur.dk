@extends("layouts.base_admin")


@section("content")

@include("partials.admin.header")

<div id="wrap">
    @include("partials.admin.sidebar")
    <div id="main" role="main">
        <div class="block">
            <div class="clearfix"></div>
            <!--page title-->
            <div class="pagetitle">
                <h1>{{strtoupper($_siteTitle)}}</h1>
            </div>
            <div class="clearfix"></div>
            <div class="spacer"></div>
                {{Form::model($item, [ null, 'class'=>'form-horizontal', 'role' => 'form'])}}
                {{Form::hidden("key")}}
                <div class="form-group">
                    <label for="new-author" class="col-sm-2 control-label">Description</label>
                    <div class="col-sm-9">
                        {{Form::text("description", null, ["class" => "form-control", "placeholder" => "Type in description", 'required' => 'required', "maxlength" => "512"])}}
                    </div>
                </div>
                <div class="form-group">
                    <label for="new-author" class="col-sm-2 control-label">Value</label>
                    <div class="col-sm-9">
                        {{Form::text("value", null, ["class" => "form-control", "placeholder" => "Type in value", 'required' => 'required', "maxlength" => "1024"])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-9 col-sm-2 text-right">
                        {{--<button type="submit" class="btn btn-default width-100" id="new-author-submit">Save to list</button>--}}
                        {{Form::submit("Save", ["class" => "btn btn-success width-100"])}}
                    </div>
                </div>
                {{Form::close()}}
        </div>
    </div>
</div>
@stop
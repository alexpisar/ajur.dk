@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
          <!--page title-->
              <div class="pagetitle">
                  <h1>PAYMENTS</h1>
              </div>
              <div class="clearfix"></div>
   		  <div class="clearfix"></div>
          {{Form::model($subscriber, ["id" => "form-info"])}}
          {{-- Company information --}}
          {{Form::hidden("id")}}
          <div class="grid">
	        <div class="grid-title">
	           <div class="pull-left">
	              <span class="table-title">Payments</span> 
	              <div class="clearfix"></div>
	           </div>
	         </div>
	         <div class="grid-content overflow">
	          <table class="table" id="">
	            <tbody class="vertical-top">
				      <tr>
				        <td class="col-sm-3 col-md-2 col-lg-2">{{ Form::label("name", "Company") }}</td>
				        <td class="action-table">{{ Form::text("name", null, ["readonly"=>"readonly", "class"=>"form-control", "maxlength"=>"255"])}} {{-- <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /> --}} </td>
				      </tr>
               <tr>
              <td>{{ Form::label("license_number", "License number") }}</td>
                <td class="action-table">{{ Form::text("licens_number", null, ["readonly"=>"readonly", "class"=>"form-control"])}}
                </td>
              </tr>
              <tr>
              <td class="col-sm-3 col-md-2 col-lg-2">{{ Form::label("owner_name", "Administrator") }}</td>
                <td class="action-table">
                        {{ Form::text("nameAdmin", $subscriber->owner->name, ["id" => "name-admin", "readonly"=>"readonly", "class"=>"form-control"])}}
                </td>
              </tr>
              <tr>
                  <td>{{ Form::label("note", "Add note") }}</td>
                  <td class="action-table">
                     {{Form::textarea("note", null, ["id" => "note", "class"=>"form-control width-100", "rows" => "5"])}}
                     <a href="#" id="clean-note"> <img class="delete-icon" src="/plugins/template/images/icon/table_del.png" /></a>
                     <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                  </td>
              </tr>
	            </tbody>
	          </table>           
	         </div>
          </div>
          {{-- Payment method --}}
        {{-- <div class="grid">
          <div class="grid-title">
             <div class="pull-left">
                <span class="table-title">Payment method</span> 
                <div class="clearfix"></div>
             </div>
           </div>
           <div class="grid-content overflow">
              <p class="spacer-2em">
                  {{Form::radio('payment_method', 1, true,["id" => "r1"])}}
                  <label for="r1"><span></span>Credit card</label>
              </p>
              <p class="spacer">
                  {{Form::radio('payment_method', 2, false,["id" => "r2"])}}
                  <label for="r2"><span></span>Online banking</label>
              </p>    
              <p class="spacer">
                  {{Form::radio('payment_method', 3, false,["id" => "r3"])}}
                  <label for="r3"><span></span>Payment slip</label>
              </p> 
              <p class="spacer">
                  {{Form::radio('payment_method', 4, false,["id" => "r4"])}}
                  <label for="r4"><span></span>EAN</label>
              </p>
              <div class="spacer-2em"></div>
           </div>
          </div> --}}
          {{-- Billing --}}
          <div class="grid">
          <div class="grid-title">
             <div class="pull-left">
                <span class="table-title">Billing</span> 
                <div class="clearfix"></div>
             </div>
           </div>
           <div class="grid-content overflow">
            <table class="table table-bordered table-mod-2 table-responsive" id="datatable_3">
              <thead>
                <tr>
                  <th>Invoice number</th>
                  <th>Billing date</th>
                  <th>Payment due</th>
                  <th>Amount ex. VAT</th>
                  <th>Total amount due</th>
                  <th class="pdf">Status</th>
                  <th class="pdf">Reciept</th>
                  <th class="pdf">Invoice</th>
                </tr>
              </thead>
              <tbody>
                @foreach($subscriber->payments as $payment)
                  <tr>
                    <td class="action-table">
                      {{Form::text("invoice_number", $payment->invoice_number, ["class" => "form-control", "maxlength" => "255"])}} 
                      <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                    </td>
                    <td class="action-table">
                      {{Form::text("billing_date", date("d.m.y", strtotime($payment->billing_date)), ["class" => "form-control", "maxlength" => "8", "placeholder"=> "XX.XX.XX"])}} 
                      <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                    </td>
                    <td class="action-table">
                      {{Form::text("payment_due", date("d.m.y", strtotime($payment->payment_due)), ["class" => "form-control", "maxlength" => "8", "placeholder"=> "XX.XX.XX"])}} 
                      <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                    </td>
                    <td class="action-table">
                      {{Form::text("amount_vat", $payment->amount_vat, ["class" => "form-control", "maxlength" => "20"])}}
                      <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                    </td>
                    <td class="action-table">
                      {{Form::text("total_amount_due", $payment->total_amount_due, ["class" => "form-control", "maxlength" => "20"])}}
                      <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" />
                    </td>
                    <td>
                      <div class="btn-group "> {{-- < put open --}}
                          <button class="btn width-102px btn-success dropdown-toggle" data-toggle="dropdown"><span class="text-status">Paid</span> <span class="caret"></span>
                            {{Form::hidden("payment_status", $payment->payment_status)}}
                          </button>
                            <ul class="dropdown-menu">
                                 <li class="spacer-1em"><a href="#" class="btn btn-success btn-status metro" role="button" data-status="{{Payment::STATUS_PAID}}">Paid</a></li>
                                 <li class="spacer-1em"><a href="#" class="btn btn-warning btn-status metro" role="button" data-status="{{Payment::STATUS_PENDING}}">Pending </a></li>
                                 <li class="spacer-1em"><a href="#"  class="btn btn-danger btn-status metro" role="button" data-status="{{Payment::STATUS_EXCEEDED}}">Exceeded </a></li>
                                 <li class="clearfix"></li>
                            </ul>
                     </div>
                    </td>
                    <td class="action-table">
                      <label class="control-label label-text">PDF</label>
                     <a href="#"> <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /> </a>
                    </td>
                    <td class="action-table">
                      <label class="control-label label-text">PDF</label>
                      <a href="#"><img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /></a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>           
           </div>
          </div>
          <div class="clearfix spacer"></div>
          <div class="row form-group spacer">
            <div class="col-sm-offset-8 col-sm-2">
                <a href="{{route("subscribers.edit", ["id" => $subscriber->id]) }}" id="clear-destination" class="btn btn-default width-100" role="button">Cancel</a>
            </div>
            <div class="col-sm-2">
                {{Form::submit("Save", ["class" => "btn btn-success width-100"])}}
            </div>
          </div>
   		  <div class="clearfix"></div>           
          {{Form::close()}}
   		  </div>
   		</div>
</div>

    <!-- Modal select-->
    {{-- <div class="modal fade" id="modalSelect" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Select subscription</h4>
                </div>
                <div class="modal-body">
                    {{Form::select("personsCount", $subscTypes->lists("persons", "id"), null, ["hidden" => "hidden", "id" => "persons-count"])}}
                    {{Form::select("subscriptions", $subscTypes->lists("name", "id"), null, ["class"=>"form-control", "id" => "nameContent"])}}
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="#" id="select-subscr" role="button" class="btn btn-success" data-dismiss="modal">Select</a>
                </div>
            </div>
        </div>
    </div> --}}
@stop

@section("scripts")
@parent
<script src="/plugins/data-table/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
  var table = null;
  $(document).ready(function() {
        //clean note
      /* $("#clean-note").on("click", function(e) {
          //console.log("click clean");
          $("#note").val('');
      });*/
      
   table =  $('#datatable_3').dataTable( {
      "sPaginationType": "full_numbers",
      "searching": false,
       "dom": '<"top">rt<"bottom"lp><"clear">',
      "aoColumns": [
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false }
                ],
      "oLanguage": { //
        "sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
        '<option value="5" selected="selected">5</option>'+
        '<option value="10">10</option>'+
        '<option value="50">50</option>'+
        '<option value="100">100</option>'+
        '<option value="200">200</option>'+
        '</select>entries </div> <div class="clearfix"></div>'

      },
      "fnInitComplete":function (){
        $(".dataTables_filter input").addClass("form-data-table");
        $(".dataTables_filter label").addClass("form-table-label");
      }
    } );
      //function turnStatusSelect(){
        $(".btn.dropdown-toggle").each(function(i){
          var status = $(this).find("input[name='payment_status']").val();
          switchStatus($(this), status);
        });
      //}

      $(".btn-status").on("click", setStatus);
   });

      function setStatus(e){
        e.preventDefault;
        var btnStatus = $(this).parent().parent().siblings(".btn.dropdown-toggle"),
            status = $(this).data("status");
        switchStatus(btnStatus, status);
      }

      function switchStatus(btnStatus, status){
        var btnClasses = "btn-success btn-warning btn-default";
        switch(status){
          case "{{Payment::STATUS_PAID}}":
            //btnStatus.switchClass(btnClasses, "btn-success", 1000, "easeInOutQuad" ); //JqueryUI
            btnStatus.removeClass(btnClasses).addClass("btn-success");
            btnStatus.children(".text-status").text("Paid");
            break;
          case "{{Payment::STATUS_PENDING}}":
            btnStatus.removeClass(btnClasses).addClass("btn-warning");
            btnStatus.children(".text-status").text("Pending");
            break;
          case "{{Payment::STATUS_EXCEEDED}}":
            btnStatus.removeClass(btnClasses).addClass("btn-danger");
            btnStatus.children(".text-status").text("Exceeded");
            break;
        }
      }

</script>
@stop
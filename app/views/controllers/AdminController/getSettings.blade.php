@extends("layouts.base_admin")

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>Settings</h1>
                    <div class="clearfix"></div>
                    <div class="container">
                        <div class="row text-center">
                          <div class="col-xs-12 col-sm-5">
                            @foreach($settings as $item)  
                               <p> <a href="{{route("settings.edit", ["key" => $item->key])}}" class="btn btn-manage" role="button">{{strtoupper($item->description)}}</a> </p>
                            @endforeach
                          </div>
                          <div class="col-xs-12 col-sm-5">
                            <p> <a href="{{route("settings.translations")}}" class="btn btn-manage" role="button">TRANSLATIONS</a> </p>
                          </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop


@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
    <link href="/plugins/bootsrtap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>{{$_siteTitle}}</h1>
                </div>
                <div class="clearfix"></div>
                {{Form::open()}}
                {{Form::hidden("locale", $locale)}}
                <div class="form-group spacer">
                    <label class="col-sm-2 control-label">Select language</label>
                    <div class="clearfix"></div>
                    <div class="col-sm-5 spacer-1em">
                        {{Form::select("locale-select", $languages, $locale, ["class" => "form-control selectpicker", "data-live-search" => "true", ($locale == Config::get("app.fallback_locale")) ? "disabled" : ""])}}
                    </div>
                </div>
                <div class="clearfix"></div>
              <div class="grid-transparent spacer-2em">
                 <div class="accordion" id="accordion">
                   @foreach($sections as $name => $section)
                      <div class="accordion-group">
                        <div class="accordion-titleing">
                          <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href={{"#collapse" . $name}}>
                            {{$name}}
                          </a>
                        </div>
                        <div id={{"collapse" . $name}} class="accordion-body collapse">
                          <div class="accordion-inner">
                            <div class="form-group">
                              @foreach($section as $key => $text)
                                {{Form::text("translation[$name][$key]", $text, ["class"=>"form-control", "placeholder" => $key, "title"=>$key, "maxlength" => "1024", "required" => "required"])}}
                              @endforeach
                            </div>
                          </div>
                        </div>
                      </div>
                   @endforeach
                  </div>   
                </div>
                <div class="form-group spacer">
                    <div class="col-xs-offset-6 col-md-offset-8 col-xs-3 col-md-2">
                        {{--URL::previous()--}}
                        <a href="{{route("settings.translations")}}" class="btn btn-default width-100" role="button">Cancel</a>
                    </div>
                    <div class="col-xs-3 col-md-2">
                        {{Form::submit("Save changes", ["class" => "btn btn-success width-100"])}}
                    </div>
                </div>
              
              {{Form::close()}}
              <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop

@section("scripts")
    @parent
    <script src="/plugins/bootsrtap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('.selectpicker').selectpicker();
      });
    </script>
@stop
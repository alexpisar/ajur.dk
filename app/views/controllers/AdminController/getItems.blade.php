@extends("layouts.base_admin")

@section("styles")
@parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>CREATE AND EDIT {{$_siteTitle}}</h1>
                </div>
                      <div class="grid">
                        <div class="grid-title">
                            <div class="pull-left">
                                <span class="table-title">{{$_siteTitle}}</span>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="grid-content overflow">
                        {{Form::hidden("title", $_siteTitle, ["id" => "title"])}}
                            <table  id="datatable_3" class="table table-bordered table-mod-2 table-responsive">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Delete</th>
                                    <th>Edit</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                <a id="btn-staff" href="{{route($routeAdd)}}" class="btn btn-success btn-block pull-right" role="button">Add new {{substr(strtolower($_siteTitle), 0, -1)}}</a>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop



@section("scripts")
    @parent
    <script src="/plugins/data-table/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
        //select itemsPerPage - to submit
        // $('#selectItemsPage').change(
        //         function(){
        //             $(this).closest('form').trigger('submit');
                    /* or:
                     $('#formElementId').trigger('submit');
                     or:
                     $('#formElementId').submit();
                     */
             //   });
    $(document).ready(function() {
        $('#datatable_3').dataTable( {
            "pagingType": "full_numbers",
            "processing": true,
            "serverSide": true,
            "ajax":{
                "url" : "{{route("database.items.json", ["title" => $_siteTitle])}}"
                /*"data":  function ( d ) {
                        d.title = $('#title').val();
                    }*/
            },
            "aoColumns": [
                { "bSortable": true },
                { "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) { //sData - text content of <td>
                        var url = "{{route("$routeDelete")}}" + "?id=" + sData;
                        $(nTd).html('<a href="'+ url +'"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a>').addClass("action-table");
                    }
                },
                { "bSortable": false,
                    "fnCreatedCell": function (nTd, sData, oData, iRow, iCol) {
                        var url = "{{route("$routeEdit")}}" + '?id=' + sData;
                        $(nTd).html('<a href="'+ url +'"><img src="/plugins/template/images/icon/table_view.png" alt="Edit"></a>').addClass("action-table");
                    }
                }
            ],
            "oLanguage": { //
                "sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
                '<option value="5">5</option>'+
                '<option value="10">10</option>'+
                '<option value="50">50</option>'+
                '<option value="100">100</option>'+
                '<option value="200">200</option>'+
                '</select>entries </div> <div class="clearfix"></div>'

            },
            "fnInitComplete":function (){
                $(".dataTables_filter input").addClass("form-data-table");
                $(".dataTables_filter label").addClass("form-table-label");
            }
        } );
    } );
    </script>
@stop

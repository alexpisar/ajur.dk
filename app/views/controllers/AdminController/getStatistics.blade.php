@extends("layouts.base_admin")

@section("content")

  @include("partials.admin.header")

  <div id="wrap">
    @include("partials.admin.sidebar")
    <div id="main" role="main">
      <div class="block">
        <div class="clearfix"></div>
        <div class="pagetitle">
          <h1>Statistics</h1>
        </div>

        <div class="container">
          <div id="embed-api-auth-container"></div>
          <div id="view-selector-container"></div>

          <div class="row">
            <div class="col-md-4">
              <label>Select from date: <input type="text" class="datepicker from-date" value="{{date("Y-m-d", strtotime("-1 week"))}}" readonly></label>
            </div>
            <div class="col-md-4">
              <label>Select to date: <input type="text" class="datepicker to-date" value="{{date("Y-m-d")}}" readonly></label>
            </div>
          </div>
          <br>
          <div class="row">
            <div class="col-md-6">
              <div id="active-users-container"></div>
            </div>
            <div class="col-md-6">
              <div id="new-users-container"></div>
            </div>
          </div>

          <br>
          <br>

          <div class="row">
            <div class="col-md-6">
              <div id="top-countries-container"></div>
            </div>
            <div class="col-md-6">
              <div id="top-browsers-container"></div>
            </div>

          </div>


        </div>

        <div class="clearfix"></div>
      </div>
    </div>
  </div>
@stop


@section("styles")
  @parent

  <link href="/plugins/jquery-ui-1.11.4/jquery-ui.min.css" rel="stylesheet">
  <link href="/plugins/jquery-ui-1.11.4/jquery-ui.theme.min.css" rel="stylesheet">
@stop


@section("scripts")
  @parent

  <script src="/plugins/jquery-ui-1.11.4/jquery-ui.min.js"></script>
  <script src="/js/admin-statistics.js"></script>


@stop
@extends("layouts.base_admin")

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>Database</h1>
                    <div class="clearfix"></div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-xs-12 col-sm-5">
                                <a href="{{route("database.build")}}" class="btn btn-manage" role="button">BUILD DATABASE</a>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                               <p> <a href="{{route("database.authors")}}" class="btn btn-manage" role="button">AUTHORS</a> </p>
                               <p> <a href="{{route("database.publishers")}}" class="btn btn-manage" role="button">PUBLISHERS</a> </p>
                               <p><a href="{{route("database.tags")}}" class="btn btn-manage" role="button">TAGS</a> </p>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop


@extends("layouts.base_admin")

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
    <div id="main" role="main">
        <div class="block">
            <div class="clearfix"></div>
            <div class="grid">
                <div class="grid-title">
                    <div class="pull-left">
                        <div class="icon-title"><i class="icon-book"></i></div>
                        <span class="table-title">Ajur.dk / {{$news->description}}</span>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="grid-content overflow">
                    <p class="news-text">{{$news->value}}</p>
                    <div class="spacer"></div>
                 </div>
            </div>

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop


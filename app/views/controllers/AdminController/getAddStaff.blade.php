@extends("layouts.base_admin")


@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
   		  <div class="clearfix"></div>
          {{Form::open()}}
          <div class="grid">
	        <div class="grid-title">
	           <div class="pull-left">
	              <span class="table-title">Staff Management</span> 
	              <div class="clearfix"></div>
	           </div>
	         </div>
	         <div class="grid-content overflow">
	          <table class="table" id="">
	            <tbody>
				      <tr>
				        <td class="col-sm-2 col-lg-1">{{ Form::label("name", "Full Name") }}</td>
				        <td class="action-table">{{ Form::text("name", null, ["required"=>"required", "class"=>"form-control", "maxlength"=>"255"])}} <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /> </td>
				      </tr>
              <tr>
                <td>{{ Form::label("initials", "Initials") }}</td>
                <td class="action-table">{{ Form::text("initials", null, ["required"=>"required", "class"=>"form-control", "maxlength"=>"255"])}} <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /></td>
              </tr>
              <tr>
                <td>{{ Form::label("email", "Email") }}</td>
                <td class="action-table">{{ Form::email("email", null, ["required"=>"required", "class"=>"form-control"])}} <img class="edit-icon" src="/plugins/template/images/icon/table_edit.png" /></td>
              </tr> 
              <tr>
                <td>{{ Form::label("last_login", "Last login") }}</td>
                <td>{{ Form::text("last_login", null, ["class"=>"form-control", "readonly", "placeholder"=>"XX-XX-XXXX - XX:XX:XX"])}}</td>
              </tr>
              <tr>
                <td>{{ Form::label("role", "Roles") }}</td>
                <td>{{ Form::select('role',  $roles, null, array('class'=>'form-control')) }}

                    {{--<select class="form-control" name="role">
                      <option selected disabled>Select an option</option>
                      @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                      @endforeach
                    </select>--}}
                </td>
              </tr>


	            </tbody>
	          </table>           
	         </div>
          </div>
          {{Form::submit("Add staff",["id"=>"btn-staff", "class"=>"btn btn-success btn-block pull-right"])}}
          {{Form::close()}}
   		  <div class="clearfix"></div>           
   		  {{--@include("partials.admin.footer")
   		  <div class="clearfix"></div> --}}
   		  </div>
   		</div>

</div>

@stop
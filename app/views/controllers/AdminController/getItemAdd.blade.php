@extends("layouts.base_admin")

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>ADD {{strtoupper($_siteTitle)}}</h1>
                </div>
                <div class="clearfix"></div>
                <div class="spacer"></div>
                <div class="container">
                    {{--'route' => 'database.authors.add',--}}
                    {{Form::open(['class'=>'form-horizontal', 'role' => 'form'])}}
                <div class="form-group">
                    <label for="new-author" class="col-sm-2 control-label">{{$_siteTitle . ":"}}</label>
                    <div class="col-sm-9">
                        {{Form::text("name", null, ["class" => "form-control", "placeholder" => "Type in " . strtolower($_siteTitle), 'required' => 'required', "maxlength" => "255"])}}
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-9 col-sm-2 text-right">
                        {{--<button type="submit" class="btn btn-default width-100" id="new-author-submit">Save to list</button>--}}
                        {{Form::submit("Save to list", ["class" => "btn btn-success width-100"])}}
                    </div>
                </div>
                {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
@stop
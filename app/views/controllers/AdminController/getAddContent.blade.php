@extends("layouts.base_admin")
@section("styles")
    @parent
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <div class="grid">
                    <div class="grid-title">
                        <div class="pull-left">
                            <span class="table-title">Content type overview</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="grid-content overflow">
                        <a href="#" id="show-type" data-toggle="modal" data-target="#modalType" class="btn btn-success btn-manage" role="button">Add new content type</a>
                        <div class="clearfix spacer-2em"></div>
                          <table id="table-content" class="table table-bordered table-mod-2 table-responsive">
                            <thead>
                            <tr>
                                <th>Content type</th>
                                <th>Delete</th>
                                <th>Edit</th>
                                <th>Add content</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach($contentTypes as $type)
                                  <tr>
                                    <td>{{ $type->name }}</td>
                                    <td class="action-table"> <a href="{{route("content.type.delete", ["id"=>$type->id]) }}"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a> </td>
                                    <td class="action-table"><a href="#" class="edit-content" data-contentid = "{{$type->id}}" data-icon="{{ContentType::getFilePathIcon($type->icon_name)}}"><img src="/plugins/template/images/icon/table_view.png" alt="Edit"></a></td>
                                    <td class="action-table"> <a href="{{route("content.article.new", ["content_type_id"=>$type->id]) }}"><img src="/plugins/template/images/icon/table_edit.png" alt="Add"></a> </td>
                                  </tr>
                                @endforeach                 
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

      <!-- Modal add-->
    <div class="modal fade" id="modalType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                {{Form::open(["files" => true, "id"=> "formNewContentType"])}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add new content type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Name new content type</label>
                        <div  class="spacer-1em">
                            {{Form::text("name", null, ["class"=>"form-control", "required" => "required", "maxlength" => "255"])}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group spacer-1em">
                        <label class="control-label">Upload/choose icon</label>
                        <div class="spacer-1em">
                            {{Form::file('icon_name', ['id' => 'inputIcon', "class" => "filestyle",  "data-buttonBefore" => "true", "accept" => "image/jpeg,image/jpg,image/png,image/bmp", 'title' => 'Select icon'])}}
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{Form::submit('Add', array('class'=>'btn btn-success', 'id'=>'add-type'))}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>

    <!-- Modal edit-->
    <div class="modal fade" id="modalTypeEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">

            <div class="modal-content">
                {{Form::open(["route" => "content.edit", "files" => true, "id"=> "formEditContentType"])}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit content type</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label class="control-label">Name content type</label>
                        <div class="spacer-1em">
                            {{Form::text("name", null, ["class"=>"form-control ", "id" => "nameContent", "required" => "required", "maxlength" => "255"])}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="form-group spacer-1em">
                        <label class="control-label">Upload/choose icon</label>
                        <div class="spacer-1em">
                            {{Form::file('icon_name', ['id' => 'inputIcon2', "class" => "filestyle",  "data-buttonBefore" => "true", "accept" => "image/jpeg,image/jpg,image/png,image/bmp", 'title' => 'Select icon'])}}
                            <div class="help-block" id="show-icon">
                                Current icon:
                                <img src="#" class="img-responsive img-showicon show-icon" alt="icon" />
                            </div>
                            <div class="help-block" id="no-icon">
                                No current icon
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    {{Form::submit('Save', array('class'=>'btn btn-success', 'id'=>'edit-type'))}}
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>  
@stop



@section("scripts")
    @parent
    <script src="/plugins/data-table/js/jquery.dataTables.min.js"></script>
    <script src="/plugins/input-file/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        var table = null;
        $(document).ready(function() {
           table =  $('#table-content').dataTable( {
               "pagingType": "full_numbers",
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false }
                ],
                "oLanguage": { //
                    "sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
                    '<option value="5">5</option>'+
                    '<option value="10">10</option>'+
                    '<option value="50">50</option>'+
                    '<option value="100">100</option>'+
                    '<option value="200">200</option>'+
                    '</select>entries </div> <div class="clearfix"></div>'

                },
               "fnInitComplete":function (){
                   $(".dataTables_filter input").addClass("form-data-table");
                   $(".dataTables_filter label").addClass("form-table-label");
               }

            } );
            
            //click Edit content type in modal window
            $(".edit-content").on("click", function () {
                var icon = $(this).data("icon"),
                   // url = "{{route("content.edit")}}".replace("%7Bid%7D", $(this).data("contentid"));
                    url = "{{route("content.edit")}}" + "?id=" + $(this).data("contentid"),
                    name = $(this).parents("tr").find("td:first-child").text();
                $("#formEditContentType").attr("action", url);

                $("#nameContent").val(name);
                $('#modalTypeEdit').modal('show');
                if (icon) {//not null
                    // console.log("icon name not null");
                    $("#no-icon").hide();
                    $("#show-icon img").attr("src", icon);
                    $("#show-icon").show();
                }
                else {
                    //console.log(obj.original);
                    $("#show-icon").hide();
                    $("#no-icon").show();
                }
            });

        } );



    </script>
@stop
@extends("layouts.base_admin")

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <!--page title-->
                <div class="pagetitle">
                    <h1>CONTENT MANAGEMENT</h1>
                    <div class="clearfix"></div>
                    <div class="container">
                        <div class="row text-center">
                            <div class="col-xs-12 col-sm-5">
                                <a href="{{route("content.browse")}}" class="btn btn-manage" role="button">BROWSE CONTENT</a>
                            </div>
                            <div class="col-xs-12 col-sm-5">
                                <p> <a href="{{route("content.add")}}" class="btn btn-manage" role="button">ADD CONTENT</a> </p>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@stop


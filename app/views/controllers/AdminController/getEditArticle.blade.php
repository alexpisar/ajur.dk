@extends("layouts.base_admin")

@section("styles")
    @parent
    <link href="/plugins/jstree-master/dist/themes/default/style.css" rel="stylesheet" type="text/css"/>
    <link href="/plugins/bootsrtap-select/css/bootstrap-select.min.css" rel="stylesheet" type="text/css"/>
@stop

@section("content")

    @include("partials.admin.header")

    <div id="wrap">
        @include("partials.admin.sidebar")
        <div id="main" role="main">
            <div class="block">
                <div class="clearfix"></div>
                <div class="grid">
                    <div class="grid-title">
                        <div class="pull-left">
                            <span class="table-title">{{$_siteTitle}}</span>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="grid-content overflow">
                        {{Form::model($article, ['class'=>'form-horizontal', 'role' => 'form', 'files' => true])}}
                        {{Form::hidden('id')}}
                        {{Form::hidden('backUrl', URL::previous())}}
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Content type</label>
                            <div class="col-sm-9">
                                {{Form::select("content_type_id", $contentTypes, null, ["class" => "form-control selectpicker", "data-live-search" => "true"])}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Topics / Tab</label>
                            <div class="col-sm-3">
                                <a href="#" id="show-tree" data-toggle="modal" data-target="#modalTree" class="btn btn-default width-100" role="button">Select tab</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Selected Tab</label>
                            <div class="col-sm-9">
                                {{Form::hidden("tab_id", null, ["id" => "sel-tab_id"])}}
                                {{Form::text("selected_tab", null, ["class"=>"form-control", "id" => "selected_tab","readonly", "placeholder" => "No select tab"])}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Year</label>
                            <div class="col-sm-5">
                                {{Form::select("year", $years, date("Y"), ["id" => "year_temp", "class" => "form-control selectpicker", "data-live-search" => "true"])}}
                            </div>
                            <div class="col-sm-2">
                                <a href="#" id="clear-destination" class="btn btn-default width-100" role="button">Clear</a>
                            </div>
                            <div class="col-sm-2">
                                <a href="#" class="btn btn-default width-100" id="add-destination" role="button">Add</a>
                            </div>
                        </div>
                        <div id="adding-destinations">
                        {{Form::hidden("countDestinations", count($destinations), ["id" => "countDestinations"])}}
                          @if(!Input::old("countDestinations"))
                            @for($i = 0; $i < count($destinations); $i++)
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Destination {{--$i + 1--}}</label>
                                    <div class="col-sm-9">
                                        {{Form::hidden("article_ids[$i]", $destinations[$i]["id"])}}
                                        {{Form::text("destinations[$i]", $destinations[$i]["destination"], ["class"=>"form-control", "readonly"])}}
                                    </div>
                                     <div class="col-sm-1">
                                         @if($i == 0)
                                             {{-- <label class="control-label"></label> --}}
                                         @else
                                            <a href="#" role="button" class="btn btn-default width-100 delete-destination">Delete</a>
                                         @endif
                                    </div>
                                </div>
                            @endfor
                          @else
                            @foreach(Input::old("destinations") as $i => $destination)
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Destination {{--$i + 1--}}</label>
                                    <div class="col-sm-9">
                                        {{Form::hidden("article_ids[$i]", 0)}}
                                        {{Form::hidden("years[$i]", 0)}}
                                        {{Form::hidden("tab_ids[$i]", 0)}}
                                        {{Form::text("destinations[$i]", null, ["class"=>"form-control", "readonly"])}}
                                    </div>
                                     <div class="col-sm-1">
                                         @if($i == 0)
                                             {{-- <label class="control-label"></label> --}}
                                         @else
                                            <a href="#" role="button" class="btn btn-default width-100 delete-destination">Delete</a>
                                         @endif
                                    </div>
                                </div>
                              @endforeach
                          @endif
                        </div>
                        {{--<div class="form-group">
                            <label class="col-sm-2 control-label">Destination 1</label>
                            <div class="col-sm-9">
                                {{Form::text("Destination1", null, ["class"=>"form-control", "readonly"])}}
                            </div>--}}


                        <div class="form-group spacer">
                            <label class="col-sm-2 control-label">Title</label>
                            <div class="col-sm-9">
                                {{Form::text("title", null, ["class"=>"form-control","required" =>"required", "maxlength" => "255"])}}
                            </div>
                        </div>
                        <div class="spacer-2em"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Authors</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" data-live-search="true" id="author-select">
                                    <option selected disabled> </option>
                                    <?php  
                                        if(!Input::old("countAuthors"))
                                            $diffAuthors =  $authors->diff($thisAuthors);
                                        else{
                                            $diffAuthors = $authors->except(Input::old("author_ids"));
                                        }
                                    ?>
                                    @foreach($diffAuthors as $author)
                                        <option value="{{$author['id']}}">{{$author["name"]}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-9 col-sm-2">
                                <a href="#" id="add-author" class="btn btn-default width-100" role="button">Add</a>
                            </div>
                        </div>
                         {{-- || $i < Input::old("countAuthors") --}}
                        <div id="adding-authors">
                            {{Form::hidden("countAuthors", $thisAuthors->count(), ["id" => "countAuthors"])}}
                            @if(!Input::old("countAuthors"))
                                @for($i = 0; $i < $thisAuthors->count(); $i++)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Author</label>
                                        <div class="col-sm-9">
                                            {{Form::hidden("author_ids[]", $thisAuthors[$i]->id)}}
                                            {{Form::text("authors[]", $thisAuthors[$i]->name, ["class"=>"form-control", "readonly"])}}
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="#" class="btn btn-default width-100 delete-author" role="button">Delete</a>
                                        </div>
                                    </div>
                                @endfor
                             @else
                            {{-- old Input --}}
                             {{-- @for($i = 0; $i < Input::old("countAuthors"); $i++) --}}
                                 @foreach(Input::old("author_ids") as $i => $id)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Author</label>
                                        <div class="col-sm-9">
                                            {{Form::hidden("author_ids[$i]")}}
                                            {{Form::text("authors[$i]", null, ["class"=>"form-control", "readonly"])}}
                                        </div>
                                        <div class="col-sm-1">
                                            <a href="#" class="btn btn-default width-100 delete-author" role="button">Delete</a>
                                        </div>
                                    </div>
                                @endforeach
                             @endif
                            {{-- @endfor            --}}
                        </div>
                        {{--<div class="form-group">
                            <label class="col-sm-2 control-label">Author</label>
                            <div class="col-sm-9">
                                {{Form::text("Author1", null, ["class"=>"form-control", "readonly"])}}
                            </div>
                        </div>--}}

                        <div class="spacer-2em"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Publishers</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" data-live-search="true" id="publisher-select">
                                    <option selected disabled> </option>
                                    <?php  
                                        if(!Input::old("countPublishers"))
                                            $diffPublishers =  $publishers->diff($thisPublishers);
                                        else{
                                            $diffPublishers = $publishers->except(Input::old("publisher_ids"));
                                        }
                                    ?>
                                    @foreach($diffPublishers as $publisher)
                                        <option value="{{$publisher->id}}">{{$publisher->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-9 col-sm-2">
                                <a href="#" class="btn btn-default width-100" id="add-publisher" role="button">Add</a>
                            </div>
                        </div>
                        <div id="adding-publishers">
                            {{Form::hidden("countPublishers", $thisPublishers->count(), ["id" => "countPublishers"])}}
                            @if(!Input::old("countPublishers"))
                            @for($i = 0; $i < $thisPublishers->count(); $i++)
                                <div class="form-group" id={{"add-publ" . $i}}>
                                    <label class="col-sm-2 control-label">Publisher</label>
                                    <div class="col-sm-9">
                                        {{Form::hidden("publisher_ids[]", $thisPublishers[$i]->id)}}
                                        {{Form::text("publishers[]", $thisPublishers[$i]->name, ["class"=>"form-control", "readonly"])}}
                                    </div>
                                    <div class="col-sm-1">
                                        <button name="deletePublisher"  class="btn btn-default width-100 delete-publisher">Delete</button>
                                    </div>
                                </div>
                            @endfor
                            @else
                              @foreach(Input::old("publisher_ids") as $i => $id)
                                <div class="form-group" id={{"add-publ" . $i}}>
                                    <label class="col-sm-2 control-label">Publisher</label>
                                    <div class="col-sm-9">
                                        {{Form::hidden("publisher_ids[$i]")}}
                                        {{Form::text("publishers[$i]", null, ["class"=>"form-control", "readonly"])}}
                                    </div>
                                    <div class="col-sm-1">
                                        <button name="deletePublisher"  class="btn btn-default width-100 delete-publisher">Delete</button>
                                    </div>
                                </div>
                              @endforeach
                            @endif
                        </div>
                        <div class="spacer-2em"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Edition</label>
                            <div class="col-sm-3">
                                {{Form::text("date_publication", date("d-m-Y"), ["class"=>"form-control", "maxlength" => "10", "placeholder" => "XX-XX-XXXX"])}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Summary (list view)</label>
                            <div class="col-sm-9">
                                {{Form::text("summary_short", null, ["class"=>"form-control", "required" => "required", "maxlength" => "500"])}}
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Summary (Full)</label>
                            <div class="col-sm-9">
                                {{Form::textarea("summary_full", null, ["id" => "summary_full","class"=>"form-control", "required" => "required", "rows" => "10", "cols" => "80"])}}
                            </div>
                        </div>
                        <div class="spacer-2em"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tags</label>
                            <div class="col-sm-9">
                                <select class="form-control selectpicker" data-live-search="true" id="tag-select">
                                    <option selected disabled> </option>
                                    <?php  
                                        if(!Input::old("countTags"))
                                            $diffTags =  $tags->diff($thisTags);
                                        else{
                                            $diffTags = $tags->except(Input::old("tag_ids"));
                                        }
                                    ?>
                                    @foreach($diffTags as $tag)
                                        <option value="{{$tag->id}}">{{$tag->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-9 col-sm-2">
                                <a href="#" class="btn btn-default width-100" id="add-tag" role="button">Add</a>
                            </div>
                        </div>
                        <div id="adding-tags">
                            {{Form::hidden("countTags", $thisTags->count(), ["id" => "countTags"])}}
                            @if(!Input::old("countTags"))
                            @for($i = 0; $i < $thisTags->count(); $i++)
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tag</label>
                                    <div class="col-sm-9">
                                        {{Form::hidden("tag_ids[]", $thisTags[$i]->id)}}
                                        {{Form::text("tags[]", $thisTags[$i]->name, ["class"=>"form-control", "readonly"])}}
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="#" class="btn btn-default width-100 delete-tag" role="button">Delete</a>
                                    </div>
                                </div>
                            @endfor
                            @else
                              @foreach(Input::old("tag_ids") as $i => $id))
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Tag</label>
                                    <div class="col-sm-9">
                                        {{Form::hidden("tag_ids[$i]")}}
                                        {{Form::text("tags[$i]", null, ["class"=>"form-control", "readonly"])}}
                                    </div>
                                    <div class="col-sm-1">
                                        <a href="#" class="btn btn-default width-100 delete-tag" role="button">Delete</a>
                                    </div>
                                </div>
                              @endforeach
                            @endif
                        </div>
                        <div class="spacer-2em"></div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Optional buttons</label>
                            <div class="col-sm-9">
                                <p class="spacer-1em">
                                    <input type="hidden" value=0 name="print_button">
                                    {{Form::checkbox('print_button', 1, false,["id" => "c1"])}}
                                    <label for="c1"><span></span>Print</label>
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="send_as_mail_button">
                                    {{Form::checkbox('send_as_mail_button', 1, false,["id" => "c2"])}}
                                    <label for="c2"><span></span>Send as mail</label>
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="borrow_button">
                                    {{Form::checkbox('borrow_button', 1, false,["id" => "c3"])}}
                                    <label for="c3"><span></span>Borrow from library (etc.)</label>
                                </p>
                                <p class="spacer-1em">
                                    {{Form::text("borrow_link", null, ["class"=>"form-control", "placeholder" => "Borrow - Insert link (link to library etc.)",  "maxlength" => "500"])}}
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="buy_button">
                                    {{Form::checkbox('buy_button', 1, false,["id" => "c4"])}}
                                    <label for="c4"><span></span>Buy</label>
                                </p>
                                <p class="spacer-1em">
                                    {{Form::text("buy_link", null, ["class"=>"form-control", "placeholder" => "Buy - Insert link (link to webshop/affiliates etc.)",  "maxlength" => "500"])}}
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="reviews_button">
                                    {{Form::checkbox('reviews_button', 1, false,["id" => "c5"])}}
                                    <label for="c5"><span></span>View reviews</label>
                                </p>
                                <p class="spacer-1em">
                                    {{Form::text("reviews_link", null, ["class"=>"form-control", "placeholder" => "View reviews - Insert link (link to materiale)",  "maxlength" => "500"])}}
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="table_contents_button">
                                    {{Form::checkbox('table_contents_button', 1, false,["id" => "c6"])}}
                                    <label for="c6"><span></span>View table of contents</label>
                                </p>
                                <p class="spacer-1em width-60">
                                    <label for="inputFile1">Select file (table of contents):</label>
                                    {{Form::file('table_contents_file', ['id' => 'inputFile1', "class" => "filestyle", "accept" => "image/jpeg,image/jpg,image/png,image/bmp", 'title' => 'Select file (table of contents)'])}}
                                </p>
                                @if(!empty($article->table_contents_file))
                                <div id="contents-image" class="help-block">
                                    <label >Current file (table of contents):</label>
                                    <img src="{{Article::getFilePathImage($article->table_contents_file)}}" class="img-responsive show-icon" alt="contents file" />
                                </div>
                                @endif
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="publish_on_page_news">
                                    {{Form::checkbox('publish_on_page_news', 1, false,["id" => "c7"])}}
                                    <label for="c7"><span></span>Publish on front page of Ajur.dk (as front page news)</label>
                                </p>
                                <div class="formRow">
                                    <div class="distance distance-2">
                                        <p>
                                            {{Form::radio('online_edition', 1, null,["id" => "r1"])}}
                                            <label for="r1"><span></span>Online edition</label>
                        
                                        </p>
                                        <p>
                                            {{Form::radio('online_edition', 0, null,["id" => "r2"])}}
                                            <label for="r2"><span></span>Offline edition</label>
                                        </p>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="view_cover_button">
                                    {{Form::checkbox('view_cover_button', 1, false,["id" => "c8"])}}
                                    <label for="c8"><span></span>View cover</label>
                                </p>
                                <p class="spacer-1em width-60">
                                    <label for="inputFile1">Select file (cover):</label>
                                    {{Form::file('view_cover_file', ['id' => 'inputFile2', "class" => "filestyle", "accept" => "image/jpeg,image/jpg,image/png,image/bmp", 'title' => 'Select file (cover)'])}}
                                </p>
                                @if(!empty($article->view_cover_file))
                                    <div id="contents-image" class="help-block">
                                        <label >Current file (cover):</label>
                                        <img src="{{Article::getFilePathImage($article->view_cover_file)}}" class="img-responsive show-icon" alt="contents file" />
                                    </div>
                                @endif
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="view_case_num_button">
                                    {{Form::checkbox('view_case_num_button', 1, false,["id" => "c12"])}}
                                    <label for="c12"><span></span>View case number (ECLI etc.)</label>
                                </p>
                                <p class="spacer-1em">
                                    {{Form::text("case_num", null, ["class"=>"form-control", "placeholder" => "Insert case number (ECLI etc.)",  "maxlength" => "255"])}}
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="save_button">
                                    {{Form::checkbox('save_button', 1, false,["id" => "c9"])}}
                                    <label for="c9"><span></span>Show floppy disk icon (save icon)</label>
                                </p>
                                <p class="spacer-1em">
                                  <input type="hidden" value=0 name="content_link_button">
                                    {{Form::checkbox('content_link_button', 1, false,["id" => "c10"])}}
                                    <label for="c10"><span></span>Link to content (insert direct link to material on another website)</label>
                                </p>
                                <p class="spacer-1em">
                                    {{Form::text("content_link", null, ["class"=>"form-control", "placeholder" => "Insert link (link to content)",  "maxlength" => "500"])}}
                                </p>
                            </div>
                        </div>
                        <div class="form-group spacer-2em">
                            <div class="col-sm-offset-2 col-sm-9">
                                <div class="distance distance-2">
                                    <p>
                                      <input type="hidden" value=0 name="show_in_news_feed">
                                        {{Form::checkbox('show_in_news_feed', 1, false,["id" => "c11"])}}
                                        <label for="c11"><span></span>Show in news feed</label>
                                    </p>
                                    <p>
                                        {{Form::radio('published', 0, null,["id" => "r3"])}}
                                        <label for="r3"><span></span>Save as unpublished</label>
                                    </p>
                                    <p>
                                        {{Form::radio('published', 1, null,["id" => "r4"])}}
                                        <label for="r4"><span></span>Save as published</label>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="form-group spacer-2em">
                            <label class="col-sm-2 control-label">Select language</label>
                            <div class="col-sm-4">
                                 {{Form::select("locale", $languages, null, 
                                      ["class" => "form-control selectpicker", "data-live-search" => "true", 'id'=>'selectpicker'])}}
                            </div>
                        </div>
                        <div class="form-group spacer-2em">
                            <label class="col-sm-2 control-label">Key words</label>
                            <div class="col-sm-9">
                              {{Form::textarea("key_words", null, 
                                ["id" => "summary_full","class"=>"form-control", 
                                   "rows" => "5", "cols" => "80"])}}
                            </div>
                        </div>
                        <div class="form-group spacer-2em">
                            <label class="col-sm-2 control-label">ISBN</label>
                            <div class="col-sm-4">
                              {{Form::text("isbn", null, ["class"=>"form-control", "placeholder" => "ISBN",  "maxlength" => "50"])}}
                            </div>
                        </div>
                        <div class="form-group spacer-2em">
                            <label class="col-sm-2 control-label">ECLI</label>
                            <div class="col-sm-4">
                              {{Form::text("ecli", null, ["class"=>"form-control", "placeholder" => "ECLI",  "maxlength" => "50"])}}
                            </div>
                        </div>
                        <div class="form-group spacer">
                            <div class="col-sm-offset-8 col-sm-3">
                                {{Form::submit('Save', ['class' => 'btn btn-primary width-100'])}}
                            </div>
                        </div>
                        {{Form::close()}}
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalTree" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                {{Form::open(["route" => "tree.uploadIcon", "files" => true, "id"=> "formUploadIcon"])}}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Set destination</h4>
                </div>
                <div class="modal-body">
                    <div id="tree"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <a href="#" id="select-tab" role="button" class="btn btn-success" data-dismiss="modal">Select Tab</a>
                </div>
                {{Form::close()}}
            </div>
        </div>
    </div>
@stop




@section("scripts")
    @parent
    <script src="/plugins/bootsrtap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <script src="/plugins/jstree-master/dist/jstree.js" type="text/javascript"></script>
    <script src="/plugins/input-file/bootstrap-filestyle.min.js" type="text/javascript"></script>
    <script src="/js/edit-article.js" type="text/javascript"></script>
    <script type="text/javascript">
        CKEDITOR.replace('summary_full');
        $('.selectpicker').selectpicker();
    </script>
    @include("partials.admin.scriptEditArticle")

@stop

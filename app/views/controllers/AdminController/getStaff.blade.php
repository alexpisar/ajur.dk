@extends("layouts.base_admin")

@section("styles")
@parent

    <!-- <link rel="stylesheet" href="/plugins/template/css/jquery-ui-1.8.16.custom.css" media="screen"  />
    <link rel="stylesheet" href="/plugins/template/css/fullcalendar.css" media="screen"  />
    <link rel="stylesheet" href="/plugins/template/css/chosen.css" media="screen"  />
    <link rel="stylesheet" href="/plugins/template/css/glisse.css?1.css">
    <link rel="stylesheet" href="/plugins/template/css/jquery.jgrowl.css"> -->
    <link rel="stylesheet" href="/plugins/template/css/demo_table.css" >
    {{-- <link rel="stylesheet" href="/plugins/template/css/jquery.fancybox.css?v=2.1.4" media="screen" /> --}}
@stop

@section("content")

@include("partials.admin.header")

<div id="wrap">
	@include("partials.admin.sidebar")
        <!--BEGIN MAIN CONTENT-->
        <div id="main" role="main">
          <div class="block">
   		  <div class="clearfix"></div>
          <div class="grid">
	        <div class="grid-title">
	           <div class="pull-left">
	              <span class="table-title">Staff Management</span> 
	              <div class="clearfix"></div>
	           </div>
	         </div>
	         <div class="grid-content overflow">
	          <table class="table table-bordered table-mod-2 table-responsive" id="datatable_3">
	            <thead>
	              <tr>
	                <th>User name</th>
	                <th>Created</th>
					  <th>Delete</th>
	              </tr>
	            </thead>
	            <tbody>
				    @foreach($usersList as $user)
				      <tr>
				        <td>{{ $user->name }}</td>
				        {{--<td>{{ $user->getPresenter()->created_at }}</td>--}}
				        {{--<td>{{ date("d.m.Y", strtotime($user->created_at)) }}</td>--}}
				        <td>{{ $user->created_at->format("d.m.Y") }}</td>
						  <td class="action-table"> <a href="{{route("users.delete", ["id"=>$user->id]) }}"><img src="/plugins/template/images/icon/table_del.png" alt="Delete"></a> </td>
				      </tr>
				    @endforeach	            	
	            </tbody>
	          </table>           
	         </div>
          </div>
          <a id="btn-staff" href="{{route("users.addStaff")}}" class="btn btn-success btn-block pull-right" role="button">Add staff</a>
   		  <div class="clearfix"></div>           
   		  {{-- @include("partials.admin.footer")
   		  <div class="clearfix"></div> --}}
   		  </div>
   		</div>

</div>

@stop

@section("scripts")
@parent
{{-- <script src="/plugins/template/js/jquery.dataTables.min.js"></script> --}}
<script src="/plugins/data-table/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('#datatable_3').dataTable( {
			"sPaginationType": "full_numbers",
			"sPageButton": "btn btn-default",
			"oLanguage": { //
				"sLengthMenu": '<div class="form-group-dt">Show<select class="form-data-table">'+
				'<option value="5">5</option>'+
				'<option value="10">10</option>'+
				'<option value="50">50</option>'+
				'<option value="100">100</option>'+
				'<option value="200">200</option>'+
				'</select>entries </div> <div class="clearfix"></div>'

			},
			"fnInitComplete":function (){
				$(".dataTables_filter input").addClass("form-data-table");
				$(".dataTables_filter label").addClass("form-table-label");
			}
		} );
	} );
</script>
@stop
<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('hello', function()
{  
	return View::make('hello'); //link login to backend
});

 Route::get('/', array('as' => 'index', 'uses' => 'HomeController@getIndex'));
 Route::get('logout', array('as' => 'login.logout', 'uses' => 'LoginController@getLogout'));
 Route::post('/support', array('as' => 'support', 'uses' => 'HomeController@postSupport'));
 Route::get('/denied', array('as' => 'access.denied', 'uses' => 'LoginController@getAccessDenied'));
 Route::post('login', array('as' => 'login.user', 'uses' => 'LoginController@postUserLogin'));
 Route::get('static/{alias}', array('as'=>'static', 'uses' =>'HomeController@getStatic')); 

 Route::get("search-extended", ["as" => "search.extended", "uses" => "SearchController@getExtendedSearchPage"]);
 Route::post("search-extended", ["as" => "search.extended", "uses" => "SearchController@postExtendedSearch"]);
 Route::get("search-result", ["as" => "search.result", "uses" => "SearchController@getSearchResult"]);
 Route::get("search", ["as" => "search.simple", "uses" => "SearchController@getSimpleSearchResults"]);


 //is not Auth
Route::group(array('before' => 'guest'), function(){
    Route::get('/teaser', array('as' => 'teaser', 'uses' => 'LoginController@getTeaser'));
    Route::get('/subscriptions', array('as' => 'choose.subscriptions', 'uses' => 'LoginController@getSubscriptions'));
    Route::get('/information', array('as' => 'login.yourinfo', 'uses' => 'LoginController@getYourInfo'));
    Route::post('/information', array('uses' => 'LoginController@postYourInfo'));
    
    Route::get('/password/remind', array('as' => 'password.lost', 'uses' => 'LoginController@getLostPassword'));
    Route::post('/password/remind', array('uses' => 'LoginController@postLostPassword'));
    Route::get('/password/thanks', array('as' => 'password.thanks', 'uses' => 'LoginController@getThanks'));
    Route::get("/password/reset/{token}", "LoginController@getResetUser");
    Route::post("/password/reset/{token}", "LoginController@postResetUser");
    
});
//guest or owner
Route::group(array('before' => 'auth.owner_or_guest'), function(){
    Route::get('/payment/method', array('as' => 'choose.payment', 'uses' => 'LoginController@getPaymentMethod'));
    Route::get('/payment/info', array('as' => 'choose.paymentInfo', 'uses' => 'LoginController@getCreditCardInfo'));
    Route::post('/payment/info', array('uses' => 'LoginController@postCreditCardInfo'));
    Route::get('/payment/receipt', array('as' => 'receipt', 'uses' => 'LoginController@getReceipt'));
});

// Any customer
Route::group(array('before' => 'auth.any_customer'), function(){
   // Route::get('/profile', array('as' => 'profile', 'uses' => 'ProfileController@getProfile'));
    Route::get('/profile', array('as' => 'profile', 
        function(){
          Alert::keep(true);
           if(Entrust::hasRole("customer"))
             return Redirect::route("profile.customer");
           elseif(Entrust::hasRole("company_owner"))
             return Redirect::route("profile.owner");
           else
             return Redirect::route("access.denied");
    }));    
    Route::get('/profile/password', array('as' => 'profile.password', 'uses' => 'ProfileController@getChangePassword'));
    Route::post('/profile/password', array('uses' => 'ProfileController@postChangePassword'));
    
    //DataBase
    Route::get('database', array('as' => 'db', 'uses' => 'DatabaseController@getIndex'));
    Route::get('database/topic/{id}', array('as' => 'db.topic', 'uses' => 'DatabaseController@getTopic'))->where('id', "[0-9]*");
    Route::get('ajax/tab/{id}', array('as' => 'ajax.tab', 'uses' => 'DatabaseController@getAjaxTab'))->where('id', "[0-9]*");
    Route::get('database/tag/{id}', array('as' => 'db.tag', 'uses' => 'DatabaseController@getTag'))->where('id', "[0-9]*");
    Route::get('database/content/{id}', array('as' => 'db.content', 'uses' => 'DatabaseController@getArticle'))->where('id', "[0-9]*");
});

//Profile  collegue customer
Route::group(array('before' => 'auth.customer'), function(){
    Route::get('/profile/customer', array('as' => 'profile.customer', 'uses' => 'ProfileController@getCustomer'));
});

//Paying customer - owner
Route::group(array('before' => 'auth.company_owner'), function()
{
    Route::get('/profile/owner', array('as' => 'profile.owner', 'uses' => 'ProfileController@getPayingOwner'));
    Route::post('/profile/owner', array('uses' => 'ProfileController@postPayingOwner'));
    Route::get('/profile/subscriptions', array('as' => 'profile.subscriptions', 'uses' => 'ProfileController@getSubscriptions'));
    Route::get('/profile/subscriptions/change', array('as' => 'profile.subscriptions.change', 'uses' => 'ProfileController@getChangeSubscrip'));
    Route::get('/profile/payments', array('as' => 'profile.payments', 'uses' => 'ProfileController@getPayments'));
    Route::get('/profile/users/add', array('as' => 'profile.add_users', 'uses' => 'ProfileController@getAddUsers'));
    Route::post('/profile/users/add', array('uses' => 'ProfileController@postAddUser'));
    Route::get('/profile/users/delete/{id}', array('as' => 'profile.del_user', 'uses' => 'ProfileController@getDeleteUser'))->where('id', "[0-9]*");
});

//admin login
Route::group(array("prefix" => "admin", 'before' => 'auth.adminlogin'), function()
{
   // Route::get("/", "HomeController@getIndex");
    
    Route::get('login', array('as' => 'login', 'uses' => 'LoginController@getLogin'));
    Route::get('register', array('as' => 'login.register', 'uses' => 'LoginController@getRegister'));
    Route::post('login', array('uses' => 'LoginController@postLogin'));
    Route::post('register', array('uses' => 'LoginController@postStore'));
 
    Route::get("/password/remind", array('as'=> 'password.remind', 'uses' => "LoginController@getRemind"));
    Route::post("/password/remind", "LoginController@postRemind");
    Route::get("/password/reset/{token}", "LoginController@getReset");
    Route::post("/password/reset/{token}", "LoginController@postReset");
});

//admins area
Route::group(["prefix" => "admin", "before" => "auth.admin"], function (){
       //users, staff
    Route::get("users/staff", array("as"=>"users.staff", "uses" => "AdminController@getStaff"));
    Route::get("users/addstaff", array('as'=>'users.addStaff' , "uses" =>"AdminController@getAddStaff"));
    Route::post("users/addstaff", array("uses" =>"AdminController@postAddStaff"));
    Route::get("users/delete", array('as'=>'users.delete' , "uses" =>"AdminController@getDeleteStaff"));  
    
    //Pricing
    //Route::get('prising', array('as'=>'pricing', 'uses' =>'AdminController@getPricing'));
    Route::get('prising/new', array('as'=>'pricing.new', 'uses' =>'AdminController@getPricingNew'));
    Route::post('prising/new', array('uses' =>'AdminController@postPricingNew'));
    Route::get('prising/edit', array('as'=>'pricing', 'uses' =>'AdminController@getPricingEdit'));
    Route::post('prising/edit', array( 'uses' =>'AdminController@postPricingEdit'));
    
    //Subscriptions
    Route::get('subscriptions', array('as'=>'subscriptions', 'uses' =>'AdminController@getSubscriptions'));
    Route::get('subscribers/browse', array('as'=>'subscribers.browse', 'uses' =>'AdminController@getSubscribers'));
    Route::get('subscribers/table', array('as'=>'subscribers.table', 'uses' =>'AdminController@getSubscribersJson'));
    Route::get('subscribers/edit', array('as'=>'subscribers.edit', 'uses' =>'AdminController@getEditSubscriber'));
    Route::post('subscribers/edit', array('uses' =>'AdminController@postEditSubscriber'));
    Route::get('subscribers/delete', array('as'=>'subscribers.delete', 'uses' =>'AdminController@getDeleteSubscr'));
    Route::get('subscribers/add', array('as'=>'subscribers.add', 'uses' =>'AdminController@getNewSubscriber'));
    Route::post('subscribers/add', array('uses' =>'AdminController@postNewSubscriber'));
    //Remind user password on subscriber
    Route::get('subscribers/password', array('as'=>'subscriber.password', 'uses' =>'AdminController@getSubscriberRemind'));
    Route::get('subscription/delete', array('as'=>'subscription.delete', 'uses' =>'AdminController@getSubscriptionDelete'));

    //payments
    Route::get('payments', array('as'=>'payments', 'uses' =>'AdminController@getPayments'));
    Route::post('payments', array('uses' =>'AdminController@postPayments'));

    //advertisements
    Route::get('advertisements', array('as'=>'advertisements', 'uses' =>'AdminController@getAdvertisements'));
    Route::post('advertisements', array('uses' =>'AdminController@postAdvertisements'));   

    //settings
    Route::get('settings', array('as'=>'settings', 'uses' =>'AdminController@getSettings'));
    Route::get('settings/edit', array('as'=>'settings.edit', 'uses' =>'AdminController@getEditSettings'));
    Route::post('settings/edit', array('uses' =>'AdminController@postEditSettings'));
    //Languages/translations
    Route::get('settings/translations', array('as'=>'settings.translations', 'uses' =>'AdminController@getTranslationsList'));
    //Route::get('settings/translations/new', array('as'=>'settings.translations.new', 'uses' =>'AdminController@getTranslationNew'));
    Route::get('settings/translations/edit', array('as'=>'settings.translations.edit', 'uses' =>'AdminController@getTranslationEdit'));
    Route::post('settings/translations/edit', array('as'=>'settings.translations.save', 'uses' =>'AdminController@postTranslationEdit'));
    Route::get('settings/translations/delete', array('as'=>'settings.translations.delete', 'uses' =>'AdminController@getTranslationDelete'));

    Route::get("statistics", ["as" => "statistics", "uses" => "AdminController@getStatistics"]);
});


//admins area employee
Route::group(["prefix" => "employee", "before" => "auth.employee"], function (){
     Route::get("dashboard", array("as"=>"dashboard", "uses" => "AdminController@getDashboard"));

//dataBase
    Route::get("database", array("as"=>"database", "uses" =>"AdminController@getDatabase"));
    Route::get("database/build", array("as"=>"database.build", "uses" =>"AdminController@getBuildDatabase"));
    Route::get("database/articles", array("as"=>"database.articles", "uses" =>"AdminController@getArticlesDatabase"));

    Route::get('treejson/{id?}', array('as'=>'getTree' ,'uses' =>'AdminController@getTreeJson'));
    Route::post('database/create', array('as'=>'tree.create' ,'uses' =>'AdminController@postCreateTreeObj'));
    Route::post('database/move', array('as'=>'tree.move' ,'uses' =>'AdminController@postMoveTreeObj'));
    Route::post('database/copy', array('as'=>'tree.copy' ,'uses' =>'AdminController@postCopyTreeObj'));
    Route::get('database/delete', array('as'=>'tree.delete' ,'uses' =>'AdminController@getDeleteTreeObj'));
    Route::post('database/uploadicon', array('as'=>'tree.uploadIcon' ,'uses' =>'AdminController@postUploadTabIcon'));
    Route::post('database/weight', array('as'=>'tree.weight' ,'uses' =>'AdminController@postSetTopicWeight'));

    Route::get('database/authors', array('as'=>'database.authors', 'uses' =>'AdminController@getAuthors'));
    Route::get('database/add/authors', array('as'=>'database.authors.add', 'uses' =>'AdminController@getAuthorAdd'));
    Route::post('database/add/authors', array('uses' =>'AdminController@postAuthorAdd'));
    Route::get('database/edit/authors', array('as'=>'database.authors.edit', 'uses' =>'AdminController@getAuthorEdit'));
    Route::post('database/edit/authors', array( 'uses' =>'AdminController@postAuthorEdit'));
    Route::get('database/delete/authors', array('as'=>'database.authors.delete', 'uses' =>'AdminController@getAuthorsDelete'));

    Route::get('database/publishers', array('as'=>'database.publishers' ,'uses' =>'AdminController@getPublishers'));
    Route::get('database/add/publisher', array('as'=>'database.publisher.add', 'uses' =>'AdminController@getPublisherAdd'));
    Route::post('database/add/publisher', array('uses' =>'AdminController@postPublisherAdd'));
    Route::get('database/edit/publisher', array('as'=>'database.publisher.edit', 'uses' =>'AdminController@getPublisherEdit'))->where('id', "[0-9]*");
    Route::post('database/edit/publisher', array( 'uses' =>'AdminController@postPublisherEdit'))->where('id', "[0-9]*");
    Route::get('database/delete/publisher', array('as'=>'database.publisher.delete', 'uses' =>'AdminController@getPublisherDelete'))->where('id', "[0-9]*");

    Route::get('database/tags', array('as'=>'database.tags', 'uses' =>'AdminController@getTags'));
    Route::get('database/add/tag', array('as'=>'database.tag.add', 'uses' =>'AdminController@getTagAdd'));
    Route::post('database/add/tag', array('uses' =>'AdminController@postTagAdd'));
    Route::get('database/edit/tag', array('as'=>'database.tag.edit', 'uses' =>'AdminController@getTagEdit'))->where('id', "[0-9]*");
    Route::post('database/edit/tag', array( 'uses' =>'AdminController@postTagEdit'))->where('id', "[0-9]*");
    Route::get('database/delete/tag', array('as'=>'database.tag.delete', 'uses' =>'AdminController@getTagDelete'))->where('id', "[0-9]*");

    //Itmes authors, pulishers, tags - json for table
    Route::get('database/itmes/data', array('as'=>'database.items.json', 'uses' =>'AdminController@getItemsJson'));
//content
    Route::get('content/manage', array('as'=>'content.manage', 'uses' =>'AdminController@getManageContent'));
    Route::get('content/browse', array('as'=>'content.browse', 'uses' =>'AdminController@getBrowseContent'));
    Route::get('content/browse/table', array('as'=>'content.browse.json', 'uses' =>'AdminController@getContentJson'));
    Route::get('content/browse/delete', array('as'=>'content.delete', 'uses' =>'AdminController@getContentDelete'));
    Route::get('content/type/delete', array('as'=>'content.type.delete', 'uses' =>'AdminController@getContentTypeDelete'));
    Route::get('content/type/add', array('as'=>'content.add', 'uses' =>'AdminController@getAddContent'));
    Route::post('content/type/add', array('uses' =>'AdminController@postAddContent'));
  //  Route::post('content/edit/{id}', array('as'=>'content.edit', 'uses' =>'AdminController@postEditContent'));
    Route::post('content/type/edit', array('as'=>'content.edit', 'uses' =>'AdminController@postEditContent'));
    Route::get('content/new', array('as'=>'content.article.new', 'uses' =>'AdminController@getNewArticle'));
    Route::post('content/new', array('uses' =>'AdminController@postNewArticle'));
    Route::get('content/edit', array('as'=>'content.article.edit', 'uses' =>'AdminController@getEditArticle'));
    Route::post('content/edit', array('uses' =>'AdminController@postEditArticle'));
//static pages
    Route::get('static/list', array('as'=>'static.list', 'uses' =>'StaticController@getStaticList'));
    Route::get('static/delete/{alias}', array('as'=>'static.delete', 'uses' =>'StaticController@getStaticDelete'));
    Route::get('static/edit/{alias?}', array('as'=>'static.edit', 'uses' =>'StaticController@getStaticEdit'));
    Route::post('static/edit/{alias?}', array('uses' =>'StaticController@postStaticEdit'));
    Route::get('static/add', array('as'=>'static.add', 'uses' =>'StaticController@getStaticNew'));
});

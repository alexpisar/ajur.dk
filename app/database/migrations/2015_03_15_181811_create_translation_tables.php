<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranslationTables extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
  public function up()
  {

    Schema::create("translation_keys", function(Blueprint $table){
      $table->increments("id");
      $table->string("key", 512)->index();
      $table->string("group")->index();
    });

    Schema::create("translations", function(Blueprint $table){
      $table->increments("id");
      $table->integer("translation_key_id", false, true)->index();
      $table->string("locale")->index();
      $table->string("text",1024);
      $table->timestamps();

      //fk
      $table->foreign("translation_key_id")->references("id")->on("translation_keys")->onDelete("cascade");
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists("translations");
    Schema::dropIfExists("translation_keys");
  }

}

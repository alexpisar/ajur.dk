<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPagesTable extends Migration {

	public function up(){
   Schema::create("static_pages", function(Blueprint $table){
      //$table->increments("id");
      $table->string("alias")->unique();
      $table->integer("weight")->default(0);
      $table->boolean("show")->default(0);
      $table->boolean("show_in_top")->default(0);
      $table->boolean("show_in_bottom")->default(0);
      $table->boolean("is_system")->default(0);
      $table->timestamps();
    });
	}

	public function down(){
		Schema::dropIfExists("static_pages");
	}

}

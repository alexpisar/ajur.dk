<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration {

	public function up()
	{
		Schema::create('articles', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->integer('content_type_id')->unsigned()->nullable();
			$table->string('title');
			$table->datetime('date_publication');//date of publication
			$table->string('summary_short', 500);
			$table->text('summary_full');
			$table->date('year')->nullable();
			$table->boolean('print_button')->default(false);
			$table->boolean('send_as_mail_button')->default(false);
			$table->boolean('borrow_button')->default(false);
			$table->string('borrow_link', 500)->nullable();
			$table->boolean('buy_button')->default(false);
			$table->string('buy_link', 500)->nullable();
			$table->boolean('reviews_button')->default(false);
			$table->string('reviews_link', 500)->nullable();
			$table->boolean('table_contents_button')->default(false);
			$table->string('table_contents_file')->nullable();
			$table->boolean('publish_on_page_news')->default(false);
			$table->boolean('online_edition')->default(false);
			$table->boolean('view_cover_button')->default(false);
			$table->string('view_cover_file')->nullable();
			$table->boolean('view_case_num_button')->default(false);//ecli
			$table->string('case_num')->nullable();
			$table->boolean('save_button')->default(false);
			$table->boolean('content_link_button')->default(false);
			$table->string('content_link', 500)->nullable();
			$table->boolean('show_in_news_feed')->default(false);
			$table->boolean('published')->default(true);
			$table->bigInteger('duplicate_id')->unsigned()->nullable();//foreign id set null
			$table->bigInteger('tab_id')->unsigned()->nullable();
			$table->integer('user_id')->unsigned()->nullable();//written By
			$table->foreign('duplicate_id')->references('id')->on('articles')
				->onDelete('SET NULL')
				->onUpdate('CASCADE');
			$table->foreign('tab_id')->references('id')->on('tabs')
				->onDelete('SET NULL')
				->onUpdate('CASCADE');
			$table->foreign('content_type_id')->references('id')->on('content_types')
				->onDelete('set null')
				->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')
				->onDelete('set null')
				->onUpdate('cascade');
      //added columns
//          $table->string('locale');
//          $table->text("key_words")->nullable();
//          $table->string("isbn", 50)->nullable();
//          $table->string("ecli", 50)->nullable();
		});
	}

	public function down()
	{
		Schema::table('articles', function(Blueprint $table) {
			$table->dropForeign('articles_tab_id_foreign');
			//$table->dropForeign('articles_duplicate_id_foreign');
			$table->dropForeign('articles_content_type_id_foreign');
			$table->dropForeign('articles_user_id_foreign');
		});
		Schema::drop('articles');
	}

}

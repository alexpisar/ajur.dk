<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration {

	public function up(){
		Schema::create('supports', function(Blueprint $table)
		{		    //support messages
		    $table->bigIncrements('id');
		    $table->string('name');
		    $table->string('email');
		    $table->string('phone');
    		$table->text("inquiry");
    		$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists("supports");
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTopicsTable extends Migration {


	public function up()
	{
		Schema::create('topics', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('text')->nullable();
			$table->string('path', 255)->nullable()->index();
			$table->bigInteger('parent_id')->unsigned()->nullable()->index();
			$table->integer('level')->index()->default('0');
			$table->integer('weight')->default('0');
			//$table->string('type')->default('topic');//root, topic, subtopic,|dynamic: tab & article - for join tab
			$table->foreign('parent_id')->references('id')->on('topics')->onDelete('SET NULL');
		});
	}

	public function down()
	{
		Schema::table('topics', function(Blueprint $table) {
			$table->dropForeign('topics_parent_id_foreign');
		});
		Schema::drop('topics');
	}

}
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditForeignSubscriberCustormerTable extends Migration {


	public function up()
	{
		Schema::table('subscriber_customer', function(Blueprint $table) {
			$table->dropForeign('subscriber_customer_parent_user_id_foreign');
			$table->dropForeign('subscriber_customer_user_id_foreign');
			$table->dropForeign('subscriber_customer_subscriber_info_id_foreign');
			$table->foreign('parent_user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
			$table->foreign('subscriber_info_id')->references('id')->on('subscriber_infos')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		/*Schema::table('subscriber_customer', function(Blueprint $table) {
			$table->dropForeign('subscriber_customer_parent_user_id_foreign');
			$table->dropForeign('subscriber_customer_user_id_foreign');
			$table->dropForeign('subscriber_customer_subscriber_info_id_foreign');
						$table->foreign('parent_user_id')->references('id')->on('users')
						->onDelete('set null')
						->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('set null')
						->onUpdate('cascade');
			$table->foreign('subscriber_info_id')->references('id')->on('subscriber_infos')
						->onDelete('set null')
						->onUpdate('cascade');
		});*/
	}

}

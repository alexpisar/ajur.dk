<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSubscriptionTypesTable extends Migration {

    public function up()
    {
        Schema::table('subscription_types', function(Blueprint $table) {
          $table->boolean('institutional_access')->default(0);
        });
    }

    public function down()
    {
        Schema::table('subscription_types', function(Blueprint $table) {
            $table->dropColumn(['institutional_access']);
        });
    }

}

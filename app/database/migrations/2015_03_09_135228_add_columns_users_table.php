<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsUsersTable extends Migration {

    public function up()
    {
            Schema::table('users', function(Blueprint $table) {
                    $table->boolean('is_blocked')->default(0);
            });
    }

    public function down()
    {
            Schema::table('users', function(Blueprint $table) {
                    $table->dropColumn('is_blocked');
            });
    }

}

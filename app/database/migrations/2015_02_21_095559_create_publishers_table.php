<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePublishersTable extends Migration {

	public function up()
	{
		Schema::create('publishers', function(Blueprint $table) {
			$table->bigIncrements('id')->unsigned();
			$table->timestamps();
			$table->string('name');
		});

		Schema::create('article_publisher', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('publisher_id')->unsigned();
			$table->bigInteger('article_id')->unsigned();
			$table->foreign('publisher_id')->references('id')->on('publishers')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('article_id')->references('id')->on('articles')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('article_publisher', function(Blueprint $table) {
			$table->dropForeign('article_publisher_publisher_id_foreign');
			$table->dropForeign('article_publisher_article_id_foreign');
		});
		Schema::drop('publishers');
		Schema::drop('article_publisher');
	}
}

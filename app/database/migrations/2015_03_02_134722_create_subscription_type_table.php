<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionTypeTable extends Migration {

	public function up(){
		Schema::create('subscription_types', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->integer('persons');
			$table->decimal('price');
			//$table->boolean('institutional_access');
		});
	}

	public function down(){
		Schema::drop('subscription_types');
	}

}

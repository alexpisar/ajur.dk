<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditArticlesTable extends Migration {

    public function up()
    {
        Schema::table('articles', function(Blueprint $table) {
          $table->string('locale');
          $table->text("key_words")->nullable();
          $table->string("isbn", 50)->nullable();
          $table->string("ecli", 50)->nullable();
        });
    }

    public function down()
    {
        Schema::table('articles', function(Blueprint $table) {
            $table->dropColumn(['locale', 'key_words', 'isbn', 'ecli']);
        });
    }


}

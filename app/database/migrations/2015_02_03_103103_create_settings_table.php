<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	public function up()
	{
		Schema::create('settings', function(Blueprint $table) {
			$table->string('key')->unique();
			$table->string('value', 1024)->index();
			$table->string('description', 512);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('settings');
	}
}
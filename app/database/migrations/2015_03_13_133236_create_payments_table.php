<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration {

	public function up()
	{
		Schema::create('payments', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('invoice_number');
			$table->date('billing_date');
			$table->date('payment_due');
			$table->decimal('amount_vat');
			$table->decimal('total_amount_due');
			$table->enum('payment_method', ['creditcard', 'online_banking', 'payment_slip', 'ean']);
			$table->enum('payment_status', ['paid', 'pending', 'exceeded']);
			//$table->integer('user_id')->unsigned()->nullable();
			$table->integer('subscriber_info_id')->unsigned()->nullable();
			$table->foreign('subscriber_info_id')->references('id')->on('subscriber_infos')
						->onDelete('set null')
						->onUpdate('cascade');
//add column      
     // $table->string('card_number');
		});
	}

	public function down()
	{
		Schema::table('payments', function(Blueprint $table) {
			$table->dropForeign('payments_subscriber_info_id_foreign');
		});
		Schema::drop('payments');
	}
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriberInfosTable extends Migration {

	public function up()
	{
		Schema::create('subscriber_infos', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->string('vat_number');
			$table->string('licens_number');//license
			//$table->date('joined_date'); //== created_at
			$table->string('street_name');
			$table->string('post_number');
			//$table->string('zip_code');
			$table->string('city');
			$table->string('country');
			//$table->integer('country_id');
			$table->text('note')->nullable();
			$table->integer('subscription_type_id')->unsigned()->nullable();
			$table->integer('user_id')->unsigned()->nullable(); //company_owner
			$table->string('phone');
			$table->string('note_about_admin')->nullable();
			$table->string('occupation');
			$table->string('optional')->nullable();
			$table->foreign('subscription_type_id')->references('id')->on('subscription_types')
						->onDelete('set null')
						->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('set null')
						->onUpdate('cascade');
      
//        $table->string('subscription_name');
//        $table->integer('persons');
//        $table->decimal('price');
//        $table->string('post_box')->nullable();      
//        $table->string('house_nr')->nullable();      
//        $table->string('letter')->nullable();      
//        $table->string('floor')->nullable();      
//        $table->string('side')->nullable();      
//        $table->boolean('to_recive_letters')->default(0);
//        $table->boolean('is_paid')->default(0);
		});
	}

	public function down()
	{
		Schema::table('subscriber_infos', function(Blueprint $table) {
			$table->dropForeign('subscriber_infos_subscription_type_id_foreign');
			$table->dropForeign('subscriber_infos_user_id_foreign');
		});
		Schema::drop('subscriber_infos');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUsersTable extends Migration {

    public function up()
    {
        Schema::table('users', function(Blueprint $table) {
          $table->integer("subscriber_info_id")->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function(Blueprint $table) {
            $table->dropColumn(['subscriber_info_id']);
        });
    }

}

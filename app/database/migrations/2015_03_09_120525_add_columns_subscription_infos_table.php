<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsSubscriptionInfosTable extends Migration {

    public function up()
    {
            Schema::table('subscriber_infos', function(Blueprint $table) {
                    $table->string('subscription_name');
                    $table->integer('persons');
                    $table->decimal('price');
            });
    }

    public function down()
    {
            Schema::table('subscriber_infos', function(Blueprint $table) {
                    $table->dropColumn(['subscription_name', 'persons', 'price']);
            });
    }

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStaticPageTranslations extends Migration {

	public function up(){
		Schema::create("static_page_translations", function(Blueprint $table){
      $table->increments("id");
      $table->string("static_page_alias")->index();
      $table->string("title");
      $table->text("content");
      $table->string("locale");
      $table->timestamps();

      //fk
      $table->foreign("static_page_alias")->references("alias")->on("static_pages")
        ->onDelete("cascade")->onUpdate('cascade');;
    });
	}

	public function down(){
		Schema::dropIfExists("static_page_translations");
	}

}

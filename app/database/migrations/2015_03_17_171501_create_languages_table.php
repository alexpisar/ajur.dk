<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagesTable extends Migration {

	public function up()
	{
		// Creates the languages table
		Schema::create('languages', function(Blueprint $table)
		{		    
		    $table->increments('id');
		    $table->string('name');
		    $table->string('locale');
    		$table->boolean("enabled")->default(0);
    		//$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists("languages");
	}

}

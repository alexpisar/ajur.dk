<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPaymentsTable extends Migration {


    public function up()
    {
        Schema::table('payments', function(Blueprint $table) {
          $table->string('card_number')->nullable();
        });
    }

    public function down()
    {
        Schema::table('payments', function(Blueprint $table) {
            $table->dropColumn(['card_number']);
        });
    }

}

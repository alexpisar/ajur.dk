<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsSubscriberInfosTable extends Migration {

    public function up()
    {
        Schema::table('subscriber_infos', function(Blueprint $table) {
          $table->string('post_box')->nullable();      
          $table->string('house_nr')->nullable();      
          $table->string('letter')->nullable();      
          $table->string('floor')->nullable();      
          $table->string('side')->nullable();
          $table->boolean('to_recive_letters')->default(0);
          $table->boolean('is_paid')->default(0);
        });
    }

    public function down()
    {
        Schema::table('subscriber_infos', function(Blueprint $table) {
            $table->dropColumn(['post_box', 'house_nr', 'letter', 'floor', 
                          'side', 'to_recive_letters', 'is_paid']);
        });
    }

}

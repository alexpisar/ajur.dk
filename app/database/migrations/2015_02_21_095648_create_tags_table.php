<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration {

	public function up()
	{
		Schema::create('tags', function(Blueprint $table) {
			$table->bigIncrements('id')->unsigned();
			$table->timestamps();
			$table->string('name');
		});
		Schema::create('article_tag', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('tag_id')->unsigned();
			$table->bigInteger('article_id')->unsigned();
			$table->foreign('tag_id')->references('id')->on('tags')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('article_id')->references('id')->on('articles')
				->onDelete('cascade')
				->onUpdate('cascade');
		});

	}

	public function down()
	{
		Schema::table('article_tag', function(Blueprint $table) {
			$table->dropForeign('article_tag_tag_id_foreign');
			$table->dropForeign('article_tag_article_id_foreign');
		});
		Schema::drop('article_tag');
		Schema::drop('tags');
	}
}

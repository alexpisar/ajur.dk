<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContentTypesTable extends Migration {

	public function up()
	{
		Schema::create('content_types', function(Blueprint $table) {
			$table->increments('id');
			$table->timestamps();
			$table->string('name');
			$table->string('icon_name')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('content_types');
	}

}

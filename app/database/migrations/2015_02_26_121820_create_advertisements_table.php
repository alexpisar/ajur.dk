<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdvertisementsTable extends Migration {

	public function up()
	{
		Schema::create('advertisements', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('file_name')->unique();
			$table->string('original_name');
			$table->boolean('is_active')->default(false);
		});
	}

	public function down()
	{
		Schema::drop('advertisements');
	}

}

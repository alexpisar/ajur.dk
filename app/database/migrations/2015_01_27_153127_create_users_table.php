<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');//->unique();
			$table->string('initials');
			$table->string('email')->unique();
			$table->string('password', 128);
			$table->datetime('last_login');
			// $table->string('phone')->nullable();
			// $table->string('note')->nullable();
			// $table->string('occupation')->nullable();
			// $table->string('optional')->nullable();
			//$table->integer('usertype_id')->unsigned()->nullable();
            $table->rememberToken();
            $table->timestamps();
          //add field for quick access
          //$table->integer("subscriber_info_id")->nullable();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}
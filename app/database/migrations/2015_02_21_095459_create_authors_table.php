<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration {

	public function up()
	{
		Schema::create('authors', function(Blueprint $table) {
			$table->bigIncrements('id')->unsigned();
			$table->timestamps();
			$table->string('name');
		});

		Schema::create('article_author', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->bigInteger('author_id')->unsigned();
			$table->bigInteger('article_id')->unsigned();
			$table->foreign('author_id')->references('id')->on('authors')
				->onDelete('cascade')
				->onUpdate('cascade');
			$table->foreign('article_id')->references('id')->on('articles')
				->onDelete('cascade')
				->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('article_author', function(Blueprint $table) {
			$table->dropForeign('article_author_author_id_foreign');
			$table->dropForeign('article_author_article_id_foreign');
		});

		Schema::drop('article_author');
		Schema::drop('authors');
	}

}

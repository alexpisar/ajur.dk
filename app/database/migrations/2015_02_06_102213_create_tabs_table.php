<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabsTable extends Migration {

	public function up()
	{
		Schema::create('tabs', function(Blueprint $table) {
			$table->bigIncrements('id');
			$table->timestamps();
			$table->string('name', 100);
			$table->string('icon_name')->nullable();
			//$table->integer('icon_id')->unsigned()->nullable();
			$table->bigInteger('topic_id')->nullable()->unsigned();
			$table->foreign('topic_id')->references('id')->on('topics')
				->onDelete('set null')
				->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('tabs', function(Blueprint $table) {
			$table->dropForeign('tabs_topic_id_foreign');
		});
		Schema::drop('tabs');
	}
}

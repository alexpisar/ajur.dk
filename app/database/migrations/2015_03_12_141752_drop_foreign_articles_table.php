<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropForeignArticlesTable extends Migration {

	public function up()
	{
		Schema::table('articles', function(Blueprint $table) {
			$table->dropForeign('articles_duplicate_id_foreign');
		});
	}

	public function down()
	{ //was commented in create migrate
		/*Schema::table('articles', function(Blueprint $table) {
			$table->foreign('duplicate_id')->references('id')->on('articles')
				->onDelete('SET NULL')
				->onUpdate('CASCADE');
		});*/
	}

}

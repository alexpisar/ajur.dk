<?php
class TabTableSeeder extends Seeder {

    public function run(){
        Tab::truncate();
        Topic::find(3)->tabs()->save(new Tab(["name" => "Tab-text"]));
    }
}
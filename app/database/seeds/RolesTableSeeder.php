<?php

class RolesTableSeeder extends Seeder {
    
    public function run(){
        DB::table('roles')->truncate();
        DB::table('permissions')->truncate();
        DB::table('permission_role')->truncate();
        DB::table('assigned_roles')->truncate();
        
        DB::transaction(function (){
            $roleAdmin = Role::create(['name'=> 'admin']);
            $roleEmployee = Role::create(['name'=> 'employee']);
            $roleOwner = Role::create(['name'=> 'company_owner']);
            $roleCustomer = Role::create(['name'=> 'customer']);

            $permAdmin = Permission::create([
                'name'=>'AdminController@getList',
                'display_name' => 'Edit users, payments'
                ]);

            $permEmpl = Permission::create([
                'name'=>'AdminController@getList1',
                'display_name' => 'Edit content'
                ]);

            $roleAdmin->perms()->sync(array($permAdmin->id,$permEmpl->id));
            $roleEmployee->perms()->sync(array($permEmpl->id));

    //        $userAdmins = User::where('id','IN','(1, 2)')->get();
    //        foreach ($userAdmins as $user) {
    //            $user->attachRole( $roleAdmin );
    //        }

            $userEmpl = User::where('id', '=', '3')->first();
            $userEmpl->attachRole($roleEmployee);

            User::where('id','=','1')->first()->attachRole($roleAdmin);       
            User::where('id','=','2')->first()->attachRole($roleAdmin);       
            User::where('email','=','alexeydslr@gmail.com')->first()->attachRole($roleAdmin);
        });
    }
}


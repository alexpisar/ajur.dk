<?php

class PublisherTableSeeder extends Seeder {

    public function run(){
        Publisher::truncate();
        DB::table('article_publisher')->truncate();
        DB::transaction(function () {
            $faker = Faker\Factory::create();

            for($i=0; $i<10; $i++){
                Publisher::create([
                    "name"=>$faker->name
                ]);
            }
        });
    }
}
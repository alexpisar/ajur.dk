<?php
class SubscriberInfoTableSeeder extends Seeder {

    public function run(){
      SubscriberInfo::truncate();
      $owner = new User;
      $owner->name = $owner->initials = "Owner";
      $owner->email = "owner@owner.com";
      $owner->last_login = date("Y-m-d H:i:s");
  
      $customer = new User;
      $customer->name = $customer->initials = "Customer";
      $customer->email = "customer@customer.com";
      $customer->last_login = date("Y-m-d H:i:s");
      
      $subscriber = new SubscriberInfo ([
          "name" => "Company name",
          "vat_number" => "00000000",
          "licens_number" => "00000000",
          "street_name" => "Street",
          "post_number" => "Post number",
          "city" => "Town",
          "country" => "country",
          "phone" => "Phone 0-1-2-3",
          "occupation" => "Boss"
      ]);
      $subscrType = SubscriptionType::first();
      $subscriber->is_paid = 1;
      //duplicate inform
      $subscriber->subscription_name = $subscrType->name;
      $subscriber->persons = $subscrType->persons;
      $subscriber->price = $subscrType->price;      
      $subscrType->subscriberInfos()->save($subscriber);
      $subscriber->licens_number = str_pad($subscriber->id, 9, '0', STR_PAD_LEFT);
      $subscriber->save();  
      
      $owner->saveUser($subscriber, 0, true);
      $owner->password = Hash::make("123456");
      $owner->save();
      $customer->saveUser($subscriber, 0);
      $customer->password = Hash::make("123456");
      $customer->save();      
    }
}
<?php
class TopicTableSeeder extends Seeder {

    public function run(){
        Topic::truncate();
        DB::transaction(function (){
            $root       = Topic::create(["text"=>"DataBase"]);
            $root->setAsRoot();

            $child      = with(Topic::create(["text"=>"SomeTopic1"]))->setChildOf($root); // New child
            with(Topic::create(["text"=>"SomeTopic2"]))->setSiblingOf($child);
            $subChild = with(Topic::create(["text"=>"SubTopic1"]))->setChildOf($child);
            $sibling2 = with(Topic::create(["text"=>"SubTopic2"]))->setSiblingOf($subChild);
            with(Topic::create(["text"=>"Topic"]))->setChildOf($sibling2);

            $root->save();
        });
    }
}
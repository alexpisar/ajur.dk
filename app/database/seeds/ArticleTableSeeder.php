<?php
class ArticleTableSeeder extends Seeder {

    public function run(){
  //     DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        Article::truncate();
      //DB::statement('SET FOREIGN_KEY_CHECKS=1;');         
        DB::transaction(function () {
            $faker = Faker\Factory::create();
            $articles = [];

            $authors = Author::take(3)->lists('id');
            $publishers = Publisher::take(3)->lists('id');
            $tags = Tag::take(3)->lists('id');
            $userID = User::first()->id;
            $locale = Config::get("app.fetch_back");
            for($i=0; $i<10; $i++){
               $articles[] = new Article([
                    "title" => $faker->sentence(5),
                    "summary_short" => $faker->sentence(10),
                    "summary_full" => $faker->paragraph(10),
                   "date_publication" => date("Y-m-d H:i:s"),
                   "year" => date("Y-m-d"),
                   "content_type_id" => $i + 1,
                   "user_id" => $userID,
                   "publish_on_page_news" => 1,
                   "show_in_news_feed"  => 1,
                   "locale"  => Config::get("app.fallback_locale")
                ]);
               //$articles[$i]->save();
               $articles[$i]->content_type_id = $i + 1;
               //echo $articles[$i]->content_type_id;
            }

            //attaching relations
           // Tab::first()->articles()->sync($idArray);
           Tab::first()->articles()->saveMany($articles);
            foreach($articles as $i => $article){
                $article->duplicate_id = $article->id;
                
               // echo $article->duplicate_id;
                //$article->duplicate_id = $i;
                $article->authors()->sync($authors);
                $article->publishers()->sync($publishers);
                $article->tags()->sync($tags);
                $article->save();
            }
        });
     // DB::statement('SET FOREIGN_KEY_CHECKS=1;');        
    }
}
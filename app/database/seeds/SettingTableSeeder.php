<?php
class SettingTableSeeder extends Seeder {

    public function run(){
        $faker = Faker\Factory::create();

        Setting::truncate();
        Setting::create([
            "key"=>'news',
            "description" => 'News',
            "value"=> $faker->paragraph(2)
        ]);
        Setting::create([
            "key" => "support",
            "description" => "Email to support messages.",
            "value" => "admin@admin.com"
        ]);
    }
}

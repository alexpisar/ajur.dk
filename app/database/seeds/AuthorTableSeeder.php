<?php

class AuthorTableSeeder extends Seeder {

    public function run(){
        Author::truncate();
        DB::table('article_author')->truncate();
        DB::transaction(function () {
            $faker = Faker\Factory::create();

            for($i=0; $i<10; $i++){
                Author::create([
                    "name"=>$faker->name
                ]);
            }
        });
    }
}
<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();
		DB::statement('SET FOREIGN_KEY_CHECKS=0;');
		 $this->call('UserTableSeeder');
		 $this->call('RolesTableSeeder');
		 $this->call('SettingTableSeeder');
		 $this->call('TopicTableSeeder');
		 $this->call('TabTableSeeder');
		$this->call('AuthorTableSeeder');
		 $this->call('PublisherTableSeeder');
		 $this->call('TagTableSeeder');
		  $this->call('ContentTypeTableSeeder');
		 $this->call('ArticleTableSeeder');
		 $this->call('SubscriptionTypeTableSeeder');
		$this->call('SubscriberInfoTableSeeder');
		$this->call('PaymentTableSeeder');
		$this->call('TranslationTableSeeder');
		$this->call('LanguageTableSeeder');
		$this->call('StaticPageTableSeeder');
		DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}

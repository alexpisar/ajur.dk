<?php

class UserTableSeeder extends Seeder {
    
    public function run(){
       // $users = new \Illuminate\Database\Eloquent\Collection;
        //$users->add(new User);
        User::truncate();
        DB::transaction(function (){
            $faker = Faker\Factory::create();
            
                User::create([
                 "name"=>'Kasper',
                 "initials"=>'Kasper',
                 "password"=> Hash::make("123456"),
                 "email" => "ps999777@gmail.com",
                  "last_login" => date("Y/m/d H:i:s")
                    ]);       
                User::create([
                 "name"=>'Christoffer',
                 "initials"=>'Christoffer',
                 "password"=> Hash::make("123456"),
                 "email" => "admin@gmail.com",
                  "last_login" => date("Y/m/d H:i:s")
                    ]);       

                /*User::create([
                 "name"=>'Nastya',
                 "initials"=>'Christoffer',
                 "password"=> Hash::make("123456"),
                 "email" => "testsolidteks@gmail.com",
                  "last_login" => date("Y/m/d H:i:s")
                    ]); */
                
                 User::create([
                 "name"=>'employee',
                 "initials"=>$faker->name,
                 "password"=> Hash::make("123456"),
                 "email" => "empl@gmail.com",
                 "last_login" => date("Y/m/d H:i:s")
                    ]);

                User::create([
                  "name"=>'me',
                  "initials"=>$faker->name,
                  "password"=> Hash::make("admin"),
                  "email" => "alexeydslr@gmail.com",
                  "last_login" => date("Y/m/d H:i:s")
                ]);

          for($i=0; $i<10; $i++){
                User::create([
                 "name"=>$faker->userName,
                 "initials"=>$faker->name,
                 "password"=> Hash::make("123456"),
                 "email" => $faker->email,
                  "last_login" => $faker->dateTime('now')
                ]);
            }
        });
    }
}


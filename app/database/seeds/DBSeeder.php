<?php
class DBSeeder extends Seeder {

  /**@var \Faker\Generator $faker*/
  protected $faker;

  protected $maxSubtopics = 5;
  protected $subtopicsDepth = 4;
  protected $branchTopicsLimit = 100;
  protected $branchTopicsCnt = 0;
  protected $topicBranches = 5;

  protected $maxTabs = 3;

  protected $maxArticlesPerTab = 5;

  public function run(){

    echo "It will take some time .... I advice you to go to make some coffee :)\n";

    $this->faker = Faker\Factory::create();
    Eloquent::unguard();
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');

    Topic::truncate();
    Tab::truncate();
    Article::truncate();
    DB::table("article_publisher")->truncate();
    DB::table("article_author")->truncate();
    DB::table("article_tag")->truncate();

    $this->seedTopics();
    echo Topic::count()." topic(s) has been created\n";

    $tabs = $this->seedTabs();
    echo count($tabs)." tab(s) has been created\n";

    while(count($tabs) > 0){
      $this->seedArticles(
        array_splice($tabs, 0, 100)
      );
    }
    echo Article::count()." article(s) has been created\n";

    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

  }

  protected function seedTopics(){
    DB::transaction(function(){
      $root = Topic::create(["text"=>"DataBase"]);
      $root->setAsRoot();


      for($i=0; $i < $this->topicBranches; $i++){
        $this->makeSubtopic($root, 0);
        $this->branchTopicsCnt = 0;
      }

      $root->save();
    });

  }

  protected function makeSubtopic($parent, $level){
    if(
      $level > $this->subtopicsDepth ||
      $this->branchTopicsLimit <= $this->branchTopicsCnt
    ) return;

    $data = [
      "text" => $this->faker->text(50)
    ];
    $topic = Topic::create($data)->setChildOf($parent);
    $this->branchTopicsCnt++;

    $subtopicsCnt = rand(1, $this->maxSubtopics);

    for($j=0; $j<$subtopicsCnt; $j++){
      $this->makeSubtopic($topic, $level+1 );
    }
  }


  protected function seedTabs(){

    $tabs = [];

    DB::transaction(function() use(&$tabs){
      $topics = Topic::whereNotNull("parent_id")->get();
      foreach($topics as $topic){
        /**@var Topic $topic*/
        $tabsLeft = rand(0, $this->maxTabs);
        while($tabsLeft-- >0){
          $tabs[] = $topic->tabs()->create([
            "name" => $this->faker->text(50)
          ]);
        }
      }
    });

    return $tabs;
  }


  protected function seedArticles($tabs){

    $authors    = Author::take(100)->lists('id');
    $publishers = Publisher::take(100)->lists('id');
    $users      = User::take(100)->lists('id');
    $tags       = Tag::take(100)->lists('id');
    $contentTypes= ContentType::take(100)->lists('id');

    DB::transaction(function () use($tabs, $authors, $publishers, $users, $tags,$contentTypes){

      $randArraValues = function(array $data, $elements){
        $elements = (count($data) > $elements)?$elements: count($data);
        return array_intersect_key(
          $data,
          array_flip((array)array_rand($data, $elements) )
        );
      };

      $randArraValue = function(array $array){
        return $array[rand(0, count($array)-1)];
      };

      foreach($tabs as $tab){
        $articlesLimit = rand(0, $this->maxArticlesPerTab);
        $articles = [];

        /**@var Tab $tab*/
        for($i=0; $i< $articlesLimit; $i++){
          $a = Article::create([
            "title" => $this->faker->sentence(5),
            "summary_short" => $this->faker->sentence(10),
            "summary_full" => $this->faker->paragraph(10),
            "date_publication" => date("Y-m-d H:i:s"),
            "year" => date("Y-m-d"),
            "content_type_id" => $randArraValue($contentTypes),
            "user_id" => $randArraValue($users),
            "isbn" => $this->faker->numberBetween(11111111, 999999999),
            "ecli" => $this->faker->numberBetween(11111111, 999999999),
            "locale" => $this->faker->languageCode
          ]);

          $a->duplicate_id = $a->id;
          $a->authors()->sync($randArraValues($authors, 2));
          $a->publishers()->sync($randArraValues($publishers, 2));
          $a->tags()->sync($randArraValues($tags, 2));

          $articles[] = $a;
        }
        $tab->articles()->saveMany($articles);
      }

    });
  }
}
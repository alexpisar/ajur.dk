<?php

class TranslationTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * .return void
	 */
	public function run()
	{
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    Translation::truncate();
    TranslationKey::truncate();

    $locale = Config::get("app.locale");

    foreach(File::files(app_path("lang/$locale")) as $fPath){
      $group = File::name($fPath);
      $tmpGData = File::getRequire($fPath);

      if(!is_array($tmpGData)) throw new Exception("Invalid file format $fPath");
      $iterator = new RecursiveIteratorIterator(new RecursiveArrayIterator($tmpGData));

      DB::transaction(function() use($iterator, $locale, $group){
        foreach($iterator as $key => $data){
          $keys = [$group];
          foreach(range(0,$iterator->getDepth()) as $depth){
            $keys[] = $iterator->getSubIterator($depth)->key();
          }

          $keyStr = implode(".",$keys);

          $tk = TranslationKey::create([
            "key"   => $keyStr,
            "group" => $group
          ]);

          $tk->translations()->create([
            "locale" => $locale,
            "text"   => $data
          ]);

        }

      });
    }
    
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
	}

}

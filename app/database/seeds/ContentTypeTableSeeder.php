<?php

class ContentTypeTableSeeder extends Seeder {

    public function run(){
        ContentType::truncate();
        DB::transaction(function () {
            ContentType::create(["name" => "Article (mass media)"]);
            ContentType::create(["name" => "Books"]);
            ContentType::create(["name" => "Article (law magazine)"]);
            ContentType::create(["name" => "Judgement"]);
            ContentType::create(["name" => "Appeal board decision"]);
            ContentType::create(["name" => "Responsum"]);
            ContentType::create(["name" => "Dissertation"]);
            ContentType::create(["name" => "Thesis"]);
            ContentType::create(["name" => "International judgement"]);
            ContentType::create(["name" => "Annual report"]);
        });
    }
}
<?php

class StaticPageTableSeeder extends Seeder {

	public function run()
	{
  	DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    StaticPage::truncate();
    StaticPageTranslation::truncate();

    $faker = Faker\Factory::create();
    
    $pages = [];
    $pages[] =  new StaticPage([
        "alias" => "About Ajur",
        "weight" => 1,
        "show" => 1,
        "show_in_top" => 1,
        "show_in_bottom" => 1
    ]);
    $pages[] =  new StaticPage([
        "alias" => "Advertising",
        "weight" => 2,
        "show" => 1,
        "show_in_top" => 0,
        "show_in_bottom" => 1        
    ]);
    $pages[] =  new StaticPage([
        "alias" => "Contact",
        "weight" => 3,
        "show" => 1,
        "show_in_top" => 1,
        "show_in_bottom" => 1        
    ]);
    $pages[] = new StaticPage([
        "alias" => "Change payment method",
        "show" => 1,
        "is_system" => 1
    ]);    
    
  //  $pages = StaticPage::all();//why ?
    
    foreach ($pages as $p) {
      $t = new StaticPageTranslation;
      //$t->createLocale(Config::get("app.fallback_locale"));
      $t->locale = Config::get("app.fallback_locale");
      $t->title = $p->alias;
      $p->alias = preg_replace("/[^a-zA-Z0-9]+/", "-", strtolower($p->alias));
      $t->content = $faker->text(5500);
      $p->save();
      $p->saveOnNew($t);
    }
    
  	DB::statement('SET FOREIGN_KEY_CHECKS=1;');
  }
  
}

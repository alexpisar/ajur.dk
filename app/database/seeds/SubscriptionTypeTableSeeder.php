<?php
class SubscriptionTypeTableSeeder extends Seeder {

    public function run(){
        SubscriptionType::truncate();
        DB::transaction(function (){
            $faker = Faker\Factory::create();
            for($i=0; $i<5; $i++){
                SubscriptionType::create([
                 "name" => $faker->company,
                 "persons" => 5,
                 "price" => 100
                ]);
            }
        });
    }
}

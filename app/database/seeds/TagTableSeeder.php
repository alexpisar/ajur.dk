<?php

class TagTableSeeder extends Seeder {

    public function run(){
        Tag::truncate();
        DB::table('article_tag')->truncate();
        DB::transaction(function () {
            $faker = Faker\Factory::create();

            for($i=0; $i<10; $i++){
                Tag::create([
                    "name"=>$faker->paragraph(1)
                ]);
            }
        });
    }
}
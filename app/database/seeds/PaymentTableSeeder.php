<?php
class PaymentTableSeeder extends Seeder {

    public function run(){
        Payment::truncate();
        DB::transaction(function () {
            $payments = [];
            for($i=0; $i<3; $i++){
               $payments[] = new Payment([
                    "invoice_number" => "001000" . ($i + 1),
                    "billing_date" => date("Y-m-d", strtotime("2015-11-21")),
                   "payment_due" => date("Y-m-d", strtotime("2015-12-02")),
                   "amount_vat" => 249.0,
                   "total_amount_due" => 311.25,
                   "payment_method" => 1,
                   "payment_status" => $i + 1 
                ]);
            }

            //attaching relations
            SubscriberInfo::first()->payments()->saveMany($payments);
           // SubscriberInfo::find(29)->payments()->saveMany($payments);
            
        });
    }
}
<?php

return array(  
    "company_name"        => "Ajur.dk",
    "street"              => "Street name",
    "house"               => "3th",
    "post_number"         => "9000",
    "city"                => "Aalborg",
    "country"             => "Denmark",
    "vat-number"          => "000000001",
    "email"               => "info@ajur.dk",
    "phone"               => "(+45) 3025 0386",
  );
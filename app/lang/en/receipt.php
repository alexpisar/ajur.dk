<?php

return array(  
    "receipt_title"               => "Receipt",
    "sign_up"                     => "Sign up", //different words dk
    "date_sign_up"                => "Date of sing up",
    "order_number"                => "Order number",
    "license_number"              => "License number",
    "name_subscription"           => "Name of subscription",
    "description_title"           => "Product description",
    "description"                 => "This subscription gives :data users access to Ajur.dk",
    "payment_method"              => "Payment method",
//    "method_credit_card"          => "", //in registration
//    "method_online_banking"       => "",
//    "method_payment_slip"         => "",
//    "method_ean"                   => "",
    "credit_card_number"          => "Credit card number",
    "amount_ex_vat"               => "Amount ex. vat",
    "amount_incl_vat"             => "Amount incl. vat",
    "amount_due"                  => "Amount due",
    "OBS_subscription_will_run"   => "OBS: subscription will run until the customer cancel the subscription",
    "customer_info"               => "Customer information",
    "contact_info"               => "Contact information",
    //"company_name"                => "Company name",
    //"vat_number"                => "VAT-number",
    "send_message_to_admin"       => "This receipt will be send to the administrators e-mail. If the information above is not correct please contact Ajur.dk",
    "continue_front_page"         => "Continue to the front page",
    
);


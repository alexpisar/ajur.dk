<?php

return array(

  "delete_user"               => "Delete user hint",
  "send_link"               => "Send link hint",
  "type_name"               => "Type in name hint",
  "type_email"               => "Type in e-mail hint",
);
<?php

return array(

  "company_name"               => "Company name hint",
  "company"                     => "company",
  "vat_number"                  => "VAT-number",
  "street_name"                  => "Street",
  "post_box"                     => "Post box",
  "house_nr"                     => "House nr. / Letter / Floor / Side",
  "city"                         => "Post number / City / Town",
  "nameAdmin"                    => "Administrator / contact person",
  "phone"                        => "Administrators phonenumber",
  "emailAdmin"                   => "Administrators e-mail",
  "password"                     => "Type in password",
  "password_confirmation"        => "Type in password again",  
  "country"                      => "Country hint",
  "occupation"                   => "Occupation hint",    
);

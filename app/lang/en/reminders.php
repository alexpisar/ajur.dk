<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Passwords must be at least six characters and match the confirmation.",

	"user" => "We can't find a user with that e-mail address.",

	"token" => "This password reset token is invalid.",

	"sent" => "Password reminder sent!",

	"reset" => "Password has been reset!",
    
	"lost_password" => "Lost your password?",
	"restore_password" => "Restore password",
	"type_in_email" => 'Type in your e-mail and click "send"',
    "send"        => "send",
  "invalid_email" => "Invalid email address",
  "invalid_token" => "Invalid link",
  "invalid_password" => "Invalid password",
    "thank_you"   => "Thank you",
    "thank_you_message"   => "Thank you. In order to create a new password you must follow instructions in the e-mail you will receive from us.",
    "type_in_new_password" => 'Type in email, new password and click "send"',
    "lost_password_text"          => "consectetur adipiseing elit. Sed enim magna, ultrices suscipit sem vitae, commodo interdum tortor. Donee faucibus justo nulla, at hendrerit justo ultricies non. Ut in pharetra sem. In hac habitasse platea dictumst. Pellentesque rhoncus tristique diam. Donee elit quam, efficitur eu elit sit amet, semper rhoneus metus.",    
    "restore_password_text"          => "consectetur adipiseing elit. Sed enim magna, ultrices suscipit sem vitae, commodo interdum tortor. Donee faucibus justo nulla, at hendrerit justo ultricies non. Ut in pharetra sem. In hac habitasse platea dictumst. Pellentesque rhoncus tristique diam. Donee elit quam, efficitur eu elit sit amet, semper rhoneus metus.",    
    "thank_you_text"          => "consectetur adipiseing elit. Sed enim magna, ultrices suscipit sem vitae, commodo interdum tortor. Donee faucibus justo nulla, at hendrerit justo ultricies non. Ut in pharetra sem. In hac habitasse platea dictumst. Pellentesque rhoncus tristique diam. Donee elit quam, efficitur eu elit sit amet, semper rhoneus metus.",    
);

<?php

return array(

  "credit_card"             => "Credit card",
  "online_banking"          => "Pay through online banking",
  'payment_slip'            => 'Electronic payment slip',
  'ean_payment'             => 'EAN payment',
  'credit_card_info'        => 'Credit card information',
  'card_number'             => 'Card information hint',
  "date_expiration"         => "Date of expiration hint",
  "validation_number"       => "Validation number hint",
);

<?php

return array(

  "advanced_search_title"            => "Advanced search",
  "search"                           => "Search",
  "reset_form"                       => "Reset form",

  "any"                              => "Any",
  "title"                            => "Title",
  "author"                           => "Author",
  "subject"                          => "Subject name",
  "year"                             => "Year of publication",
  "isbn"                             => "ISBN number",
  "ecli"                             => "ECLI number",

  "in_a_row"                         => "Words in a row",
  "contains"                         => "Containing this word",

  "publication_date"                 => "Date of publication",
  "content_type"                     => "Content type",
  "locale"                           => "Language",
  "start_date"                       => "Start date",
  "end_date"                         => "End date",


  "add_new"                         => "Add new",
  "remove_filter"                   => "Remove filter",

  "no_results"                      => "Nothing has been found",

);
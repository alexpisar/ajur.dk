<?php

return array(
    "main_topic_page"   => "Main topic",
    "topic_page"   => "Topic",
    "content_page"   => "Content",
    "authors"   => "af",
    "error_load_tab"    => "Couldn't load this tab",
    "error_find_tag"    => "Couldn't find this tag",
    "error_find_content"    => "Couldn't find this content",
    "no_content"    => "There is no content",
    "tags"    => "Tags",
    );


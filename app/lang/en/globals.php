<?php

return array(
"support_sent"                              => "Inquiry has been sent",
"support_error"                              => "Sorry, inquiry has not been sent. Try again later.",
//  "login"                                 => "Login",
//  "name"                                  => "Name",
//  "email"                                 => "Email",
//  "password"                              => "Password",
  "sign_up"                                 => "Sign up",
  "remember_me"                             => "Remember me",
//    "go_back"                             => "Go back",
//    "next"                                => "Next",
  "type_in"                                 => "Type in",
  "forgot_password"                         => "Forgot your password?",
		
  "menu" => [
    "about"                               => "About Ajur",
    "pricing"                             => "Pricing",
    "contact"                             => "Contact",
    "database"                             => "Database",
    "courses"                             => "Courses",
    "jobs"                                => "Jobs",
    "sign_in"                             => "Sign in",
    "sign_out"                             => "Sign out",
    "profile"                               => "Profile",
  ],

  "week" => [
      "Monday"       =>  "Monday",
      "Tuesday"      =>  "Tuesday",
      "Wednesday"    =>  "Wednesday",
      "Thursday"     =>  "Thursday",
      "Friday"       =>  "Friday",
      "Saturday"     =>  "Saturday",
      "Sunday"       =>  "Sunday",
  ]

);
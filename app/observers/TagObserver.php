<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.04.15
 * Time: 17:17
 */

class TagObserver extends BaseObserver{

  public function saved(Tag $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->updateModel($model);
  }

  public function deleted(Tag $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->deleteModel($model);
  }
}
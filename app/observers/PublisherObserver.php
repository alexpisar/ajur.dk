<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.04.15
 * Time: 17:17
 */

class PublisherObserver extends BaseObserver{

  public function saved(Publisher $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->updateModel($model);
  }

  public function deleted(Publisher $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->deleteModel($model);
  }
}
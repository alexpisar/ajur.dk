<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.04.15
 * Time: 17:17
 */

class ArticleObserver extends BaseObserver{

  public function saved(Article $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $model->load(array_keys($search->getRelations()));
    $search->addBaseModel($model);
  }

  public function deleted(Article $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->deleteModel($model);
  }

  public function created(Article $model){
    if(is_null($model->duplicate_id)){
      $model->duplicate_id = $model->id;
      $model->save();
    }
  }
}
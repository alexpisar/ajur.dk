<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.04.15
 * Time: 17:17
 */

class AuthorObserver extends BaseObserver{

  public function saved(Author $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->updateModel($model);
  }

  public function deleted(Author $model){
    /**@var \CustomSearch\SearchProviders\ArticleSearch $search*/
    $search = new $this->defaultSearchProvider;
    $search->deleteModel($model);
  }
}
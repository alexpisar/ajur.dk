<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 08.04.15
 * Time: 13:34
 */

namespace CustomSearch\SearchProviders;


use CustomSearch\Relations\Relation;
use Elasticsearch\ClientBuilder;
use Elasticsearch\Common\Exceptions\Missing404Exception;

abstract class Search {

  protected $defaultHosts = ["localhost:9200"];
  protected $defaultRetries = 2;
  protected $defaultNumberOfShards = 5;
  protected $defaultNumberOfReplicas = 1;

  protected $indexName;
  protected $typeName ;
  /**@var Relation[] $baseRelation*/
  protected $relations;
  /**@var Relation $baseRelation*/
  protected $baseRelation;

  protected $client;
  protected $cache;


  abstract protected function indexNameSetter();
  abstract protected function typeNameSetter();
  abstract protected function relationsSetter();



  public function __construct(){

    $this->indexName = $this->indexNameSetter();
    $this->typeName  = $this->typeNameSetter();
    $this->relations = $this->relationsSetter();
    $this->baseRelation = $this->relations["base"];
    unset($this->relations["base"]);

    $this->client = ClientBuilder::create()
      ->setHosts( (array)\Config::get("elasticsearch.host", $this->defaultHosts) )
      ->setRetries( \Config::get("elasticsearch.retries", $this->defaultRetries) )
      ->build();
  }


  public function getRelations(){
    return $this->relations;
  }


  public function reindex(){

    $this->clearIndex();

    $query = [
      "index"=> $this->indexName,
      "type" => $this->typeName,
      "body" => []
    ];

    $eloquentData = $this
      ->baseRelation
      ->getAllModels(array_keys($this->relations));

    if($eloquentData->count() === 0) return;

    foreach($eloquentData as $model){
      /**@var \Eloquent $model*/
      $baseModel = $this->getBaseModelData($model);

      $query["body"][] = [
        "index" => [
          "_id" => $model[$this->baseRelation->getIdField()]
        ]
      ];

      $query["body"][] = $baseModel;
    }


    return $this->client->bulk($query);
  }


  public function clearIndex(){
    $this->dropIndex();
    $this->createIndex();
  }


  protected function dropIndex(){
    try{
      $this->client->indices()->delete([
        "index"=> $this->indexName
      ]);
    }catch (Missing404Exception $e){}
  }


  protected function createIndex(){
    try{
      $this->client->indices()->create([
        "index" => $this->indexName,
        "body"  => [
          "settings" => [
            "number_of_shards" => \Config::get("elasticsearch.number_of_shards", $this->defaultNumberOfShards),
            "number_of_replicas" => \Config::get("elasticsearch.number_of_replicas", $this->defaultNumberOfReplicas)
          ],
          'mappings' => [
            $this->typeName => $this->makeMap()
          ]
        ]
      ]);
    }catch (Missing404Exception $e){}
  }


  protected function makeMap(){
//    return new \StdClass();
    $map = $this->baseRelation->getFieldsMapping(true);
    foreach($this->relations as $name=>$relation){
      $map[$name] = $relation->getFieldsMapping();
    }

    return [
      "_source" => ["enabled" => true],
      "properties" => $map
    ];
  }


  protected function getBaseModelData(\Eloquent $baseModel){

    $result = $this->baseRelation->getEloquentFields($baseModel);

    // get relations data
    foreach($this->relations as $relationName => $relation){
      $result[$relationName] = $this->relations[$relationName]
        ->getModelFields($baseModel->$relationName);
    }

    return $result;
  }

  public function addBaseModel(\Eloquent $model){
    $baseModel = $this->getBaseModelData($model);
    $query = [
      "index"=> $this->indexName,
      "type" => $this->typeName,
      "id"   => $baseModel[$this->baseRelation->getIdField()],
      "body" => $baseModel
    ];

    $this->client->index($query);
  }


  public function updateModel(\Eloquent $model){
    $relation = $this->getRelationByModel($model);

    $isBaseModel = $this->baseRelation === $relation->instance;

    $query = [
      "index"=> $this->indexName,
      "type" => $this->typeName,
      "size" => 1000,
      "body" => []
    ];

    // let's find out here to search
    if($isBaseModel){
      $idField = $relation->instance->getIdField();
    }else{
      $idField = $relation->name.".".$relation->instance->getIdField();
    }

    $query['body']['query']['filtered']['filter']['term'][$idField] = $model->id;

    $this->processAll(
      $query,
      function($response, $oldQuery) use ($relation, $isBaseModel, $model){

        $body = [];
        $response = $response["hits"]["hits"];

        if($isBaseModel){
          foreach($response as $document){

            $body[] = ["update"=>["_id" => $document["_id"]]];
            $body[] = [
              "doc_as_upsert" => true,
              "doc" => $relation->instance->updateFields($document["_source"], $model)
            ];
          }
        }else{
          foreach($response as $document){

            $body[] = ["update"=>["_id" => $document["_id"]]];
            $body[] = [
              "doc_as_upsert" => true,
              "doc" =>[
                $relation->name => $relation->instance->updateFields($document["_source"][$relation->name], $model)
              ]
            ];
          }
        }

        if(count($response)){
          $this->client->bulk([
            "index"=> $this->indexName,
            "type" => $this->typeName,
            "body" => $body
          ]);
        }
      }
    );

  }

  protected function processAll($query, callable $callback){
    $resultCnt = 0;
    $offset = 0;

    do{
      $query["from"] = $offset;
      $response = $this->client->search($query);

      $callback($response, $query);
      $resultCnt += count($response["hits"]["hits"]);
      $offset += $query["size"];

    }while($resultCnt < $response["hits"]["total"] );

  }

  protected function getRelationByModel($model){
    if(is_string($model)){
      $model = (new \ReflectionClass($model))->getShortName();
    }

    $rel = $this->relations;
    $rel["base"] = $this->baseRelation;

    foreach($rel as $relationName => $relation){
      if(!$relation->hasModel($model)) continue;
      return (object)[
        "name"     => $relationName,
        "instance" => $relation];
    }

    throw new \Exception("Model $model is not supported");
  }


  public function deleteModel(\Eloquent $model){

    $relation = $this->getRelationByModel($model);
    $isBaseModel = $this->baseRelation === $relation->instance;

    if($isBaseModel){
      return $this->client->delete([
        "index" => $this->indexName,
        "type"  => $this->typeName,
        "id"   => $model->id
      ]);
    }


    $query = [
      "index"=> $this->indexName,
      "type" => $this->typeName,
      "size" => 1000,
      "body" => []
    ];

    $idField = $relation->name.".".$relation->instance->getIdField();
    $query['body']['query']['filtered']['filter']['term'][$idField] = $model->id;

    $this->processAll(
      $query,
      function($response, $oldQuery) use ($relation, $isBaseModel, $model){

        $body = [];
        $response = $response["hits"]["hits"];

        foreach($response as $document){

          $body[] = ["update"=>["_id" => $document["_id"]]];
          $body[] = [
            "doc_as_upsert" => true,
            "doc" =>[
              $relation->name => $relation->instance->removeFields($document["_source"][$relation->name], $model)
            ]
          ];
        }

        if(count($response)) {
          $this->client->bulk([
            "index" => $this->indexName,
            "type" => $this->typeName,
            "body" => $body
          ]);
        }
      }
    );

  }




  public function find( $queryData, $size = 10, $page = 1 ){

    $builder = new \CustomSearch\SearchQueryBuilder\Builder(array_merge(
      ["base" => $this->baseRelation],
      $this->relations
    ));

    return $this->client->search([
      "index" => $this->indexName,
      "type"  => $this->typeName,
      "size"  => $size,
      "from"  => $size * ($page-1),
      "body" => $builder->make($queryData)
    ]);

  }



  public function getFieldsList($flatten = false){
    $relations = $this->relations;
    $relations["base"] = $this->baseRelation;
    $fields = [];

    foreach($relations as $rName => $relation){
      /**@var Relation $relation*/
      $fields[$rName] = $relation->fields(false);
    }

    if(!$flatten) return $fields;

    $iterator = new \RecursiveIteratorIterator(new \RecursiveArrayIterator($fields));
    $array = [];
    foreach($iterator as $key => $data){
      $keys = [];
      foreach(range(0,$iterator->getDepth()) as $depth){
        $key = $iterator->getSubIterator($depth)->key();
        if(is_numeric($key)) continue;
        $keys[] = $key;
      }
      $array[] = implode(".",$keys) .".". $data;
    }

    return $array;

  }


  public function validateFieldValue($fieldName, $value){
    $fieldArray = explode(".", $fieldName);
    if(count($fieldArray) > 1){
      $relation = array_splice($fieldArray, 0, 1)[0];
      $fieldName = implode(".", $fieldArray);
      return $this->relations[$relation]->validateFieldValue($fieldName, $value);
    }else{
      return $this->baseRelation->validateFieldValue($fieldName, $value);
    }
  }
}
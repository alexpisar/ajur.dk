<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.04.15
 * Time: 16:55
 */

namespace CustomSearch\SearchProviders;

use CustomSearch\Relations\ArticleRelation;
use CustomSearch\Relations\AuthorRelation;
use CustomSearch\Relations\ContentTypeRelation;
use CustomSearch\Relations\PublisherRelation;
use CustomSearch\Relations\TagRelation;

class ArticleSearch extends Search{

  protected function indexNameSetter(){
    return \Config::get("elasticsearch.index_name", "ajur_dk");
  }

  protected function typeNameSetter(){
    return \Config::get("elasticsearch.type_name", "article_search");
  }

  protected function relationsSetter(){
    return [
      "base" => new ArticleRelation(),
      "authors" => new AuthorRelation(),
      "contentType" => new ContentTypeRelation(),
      "publishers" => new PublisherRelation(),
      "tags" => new TagRelation()
    ];
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 18.04.15
 * Time: 15:51
 */

namespace CustomSearch\Collections;

class SearchResult extends \Illuminate\Support\Collection{

  public $_index;
  public $_type;
  public $_id;
  public $_score;
  /**@var \Article $model*/
  public $model;
  public $modelName;

  public function __construct(array $item = [], $baseModel = null){
    if(!isset($item["_id"])) throw new \Exception("Invalid data format.");

    $this->items = $item["_source"];

    $this->_index = $item["_index"];
    $this->_type  = $item["_type"];
    $this->_id    = $item["_id"];
    $this->_score = $item["_score"];

    if($baseModel){
      $this->model = $baseModel::find($item["_id"]);
      $this->modelName = $baseModel;
    }
  }


  public function toArray(){
    $res = array_map(function($value){
      return $value instanceof \Illuminate\Support\Contracts\ArrayableInterface ? $value->toArray() : $value;
    }, $this->items);

    $res["_index"] = $this->_index;
    $res["_type"]  = $this->_type;
    $res["_id"]    = $this->_id;
    $res["_score"] = $this->_score;

    return $res;
  }


  public function isNested($fieldName){
    return isset($this->items[$fieldName][0]);
  }


  public function iterate($fieldName, callable $callback){
    if($this->isNested($fieldName)){
      foreach($this->items[$fieldName] as $row){
        $callback($row);
      }
    }else{
      $callback($this->items[$fieldName]);
    }
  }

}
<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 18.04.15
 * Time: 15:51
 */

namespace CustomSearch\Collections;

class ArticleSearchResult extends \Article{

  public $_index;
  public $_type;
  public $_id;
  public $_score;
  public $_source;

  public function __construct(array $item = []){
    if(!isset($item["_id"])) throw new \Exception("Invalid data format.");

    $this->_source = $item["_source"];
    $this->_index = $item["_index"];
    $this->_type  = $item["_type"];
    $this->_id    = $item["_id"];
    $this->_score = $item["_score"];

    $this->model = \Article::find($item["_id"]);
  }


  public function toArray(){
    $res = parent::toArray();

    $res["_index"] = $this->_index;
    $res["_type"]  = $this->_type;
    $res["_id"]    = $this->_id;
    $res["_score"] = $this->_score;
    $res["_source"]= $this->_source;

    return $res;
  }


  public function isNested($fieldName){
    return isset($this->items[$fieldName][0]);
  }


  public function iterate($fieldName, callable $callback){
    if($this->isNested($fieldName)){
      foreach($this->items[$fieldName] as $row){
        $callback($row);
      }
    }else{
      $callback($this->items[$fieldName]);
    }
  }

}
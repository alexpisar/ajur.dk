<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 18.04.15
 * Time: 15:51
 */

namespace CustomSearch\Collections;

class ResponseCollection extends \Illuminate\Support\Collection{

  public $_took;
  public $_timeout;
  public $_shards;
  public $_totalHits;
  public $_maxScore;

  /**@var SearchResult[] $items*/
  protected $items = [];

  public function __construct(array $responseData = [], $baseModel = null){
    if(!isset($responseData["hits"])) throw new \Exception("Invalid data format.");

    $this->_took      = $responseData["took"];
    $this->_timeout   = $responseData["timed_out"];
    $this->_shards    = (object)$responseData["_shards"];
    $this->_totalHits = $responseData["hits"]["total"];
    $this->_maxScore  = $responseData["hits"]["max_score"];


    $this->items = [];

    \DB::transaction(function() use($responseData, $baseModel){
      foreach($responseData["hits"]["hits"] as $hit){
        $this->items[] = new SearchResult($hit, $baseModel);
      }
    });

  }


  public function toArray(){
    $res = array_map(function($value){
      return $value instanceof \Illuminate\Support\Contracts\ArrayableInterface ? $value->toArray() : $value;
    }, $this->items);

    $res["_took"]      = $this->_took;
    $res["_timeout"]   = $this->_timeout;
    $res["_shards"]    = $this->_shards;
    $res["_totalHits"] = $this->_totalHits;
    $res["_maxScore"]  = $this->_maxScore;

    return $res;
  }

}
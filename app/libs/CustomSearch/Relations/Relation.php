<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.04.15
 * Time: 16:52
 */

namespace CustomSearch\Relations;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

abstract class Relation {

  protected $modelName;
  protected $_fields = [];
  protected $fieldsMap = [];
  protected $fieldsQueryBuilderSchema = [];
  protected $idField = "id";
  protected $idFieldMapping = ["type" => "long"];
  protected $idFieldQueryBuilderSchema = "term";


  abstract protected function modelNameSetter();
  abstract protected function fieldsSetter();
  abstract protected function relationMapTypeSetter();


  public function __construct(){
    $this->modelName = $this->modelNameSetter();

    foreach($this->fieldsSetter() as $fieldName => $fieldData){
      $this->_fields[] = $fieldName;
      $this->fieldsMap[$fieldName] = $fieldData["mapping"];
      $this->fieldsQueryBuilderSchema[$fieldName] = $fieldData["queryBuilderSchema"];
    }
    $this->fieldsMap[$this->idField] = $this->idFieldMapping;
    $this->fieldsQueryBuilderSchema[$this->idField] =
      $this->idFieldQueryBuilderSchema;
  }

  public function fields($withId = true){
    if($withId){
      return array_merge(
        $this->_fields,
        (array)$this->idField
      );
    }else{
      return $this->_fields;
    }
  }

  public function getIdField(){ return $this->idField; }

  public function getFieldsMapping($isBaseModel = false){
    if($isBaseModel) return $this->fieldsMap;

    $type = $this->relationMapTypeSetter();

    $map = ["properties" => $this->fieldsMap];

    if($type === "object"){
      $map["type"] = $type;
    }

    return $map;
  }

  public function getFieldsQueryBuilderSchema(){
    return $this->fieldsQueryBuilderSchema;
  }

  public function getAllModels($with = []){
    $modelName = $this->modelName;
    return $modelName::with($with)->get();
  }


  public function getModelFields($models){

    if(is_null($models)){
      return new \StdClass();

    }elseif($models instanceof \Eloquent){     // single element
      return $this->getEloquentFields($models);

    }else{                                      // multiple elements
      return $this->getEloquentCollectionFields($models);

    }

  }


  public function getEloquentFields(\Eloquent $model){
    $result = [];
    foreach($this->fields() as $field){
      $preprocessMethodName = "preprocess".\Str::camel($field);
      if( method_exists($this, $preprocessMethodName)){
        $result[$field] = $this->$preprocessMethodName($model->$field);
      }else{
//        $result[$field] = is_null($model->$field)? new \StdClass(): $model->$field;
        $result[$field] = $model->$field;
      }

      if($result[$field] instanceof \StdClass){
        $result[$field] = $result[$field]->__toString();
      }

    }
    return $result;
  }


  public function getEloquentCollectionFields(Collection $collection){
    $res = [];
    foreach($collection as $model){
      $res[] = $this->getEloquentFields($model);
    }
    return $res;
  }


  public function hasModel($model){
    if(!is_string($model)){
      $model = (new \ReflectionClass($model))->getShortName();
    }
    return $this->modelName === $model;
  }


  public function updateFields(array $oldFields, \Eloquent $newFields){
    $new = $this->getEloquentFields($newFields);

    if(isset($oldFields[$this->idField])){  // one dimension
      return array_merge( $oldFields, $new );

    }else{                                  // two dimensions
      foreach($oldFields as &$dimension){
        if($dimension[$this->idField] !== $new[$this->idField]) continue;
        $dimension = array_merge($dimension, $new);

        return $oldFields;
      }
    }
  }


  public function removeFields(array $oldFields, \Eloquent $newFields){
    $toRemove = $this->getEloquentFields($newFields);

    if(isset($oldFields[$this->idField])){  // one dimension
      $res = array_diff_key(
        $oldFields,
        array_keys($toRemove)
      );

    }else{                                  // two dimensions
      foreach($oldFields as $key => &$dimension){
        if($dimension[$this->idField] !== $toRemove[$this->idField]) continue;

        $dimension = array_diff_key(
          $oldFields,
          array_keys($toRemove)
        );

        if(empty($dimension)) array_splice($oldFields, $key, 1);

        $res = $oldFields;
        break;
      }
    }

    return empty($res)? new \StdClass(): $res;
  }


  public function validateFieldValue($fieldName, $value){

    $map = array_get($this->fieldsMap, $fieldName);

    return $this->{"validate_".$map["type"]}($value);

  }


  protected function validate_string($data){  return true; }
  protected function validate_integer($data){  return is_numeric($data); }
  protected function validate_long($data){  return is_numeric($data); }
  protected function validate_float($data){  return is_numeric($data); }
  protected function validate_double($data){  return is_numeric($data); }
  protected function validate_boolean($data){  return true; }

}
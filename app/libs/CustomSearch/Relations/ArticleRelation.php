<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.04.15
 * Time: 17:02
 */

namespace CustomSearch\Relations;


use Carbon\Carbon;

class ArticleRelation extends Relation{

  protected function modelNameSetter(){ return "Article"; }

  protected function fieldsSetter(){
    return [
      "title" => [
        "mapping" => ["type" => "string"],
        "queryBuilderSchema" => "text"
      ],
      "summary_full" => [
        "mapping" => ["type" => "string"],
        "queryBuilderSchema" => "text"
      ],
      "year" => [
        "mapping" => ["type" => "integer"],
        "queryBuilderSchema" => "integer"
//        "mapping" => ["type" => "date"]
      ],
      "isbn" => [
//        "mapping" => ["type" => "long"]
        "mapping" => ["type" => "string", "index" => "not_analyzed"],
        "queryBuilderSchema" => "isbn_ecli"
      ],
      "ecli" => [
        "mapping" => ["type" => "string", "index" => "not_analyzed"],
        "queryBuilderSchema" => "isbn_ecli"
      ],
      "locale"   => [
        "mapping" => ["type" => "string", "index" => "not_analyzed"],
        "queryBuilderSchema" => "term"
      ],
      "created_at"  => [
//        "mapping" => ["type" => "string", "format" => "dateOptionalTime"]
        "mapping" => ["type" => "date", "format" => "dateOptionalTime"],
        "queryBuilderSchema" => "between"
      ]
    ];
  }


  protected function relationMapTypeSetter(){ return "object"; }


  protected function preprocessYear($data){
    if($data instanceof Carbon) return $data->year;
    else                        return substr($data, 0, 4);
  }

  protected function preprocessCreatedAt($data){
    if($data instanceof Carbon) return $data->toDateString();
    else                        return (new Carbon($data))->toDateString();
  }


}
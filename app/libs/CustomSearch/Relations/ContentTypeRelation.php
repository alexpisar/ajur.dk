<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 09.04.15
 * Time: 17:02
 */

namespace CustomSearch\Relations;


class ContentTypeRelation extends Relation{

  protected function modelNameSetter(){ return "ContentType"; }

  protected function fieldsSetter(){
    return [
      "name"=> [
        "mapping" => ["type" => "string"],
        "queryBuilderSchema" => "text"
      ]
    ];
  }

  protected function relationMapTypeSetter(){ return "object"; }
}
<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.04.15
 * Time: 20:00
 */

namespace CustomSearch\SearchQueryBuilder;


use CustomSearch\Relations\Relation;

class Builder {
  /**@var Relation[] $relation*/
  protected $relations;

  protected $query = ["query" => ["filtered" => [] ] ];

  public function __construct(array $relations){
    $this->relations = $relations;
  }

  public function make(array $data){
    $arr = [];
    $baseArr = [];
    foreach($this->relations as $name => $relation){
      if($name === "base"){
        $baseArr = $relation->getFieldsQueryBuilderSchema();
      }else {
        $arr[$name] = $relation->getFieldsQueryBuilderSchema();
      }
    }

    $fieldsSchema = array_merge(array_dot($arr), $baseArr);

    foreach($data as $row){
      $this->{"schema_".$fieldsSchema[ $row["field"] ]}(
        $row["field"],
        $row["value"],
        $row["condition"],
        $row["type"]
      );

//      $this->query["query"]["highlight"]["fields"][$row["field"]] = new \StdClass();
    }
//    $this->query["query"]["highlight"]["fields"]["title"] = new \StdClass();
//    $this->query["highlight"] = [
//      "pre_tags"  => ["<tag>"],
//      "post_tags" => ["</tag>"],
//      "fields" => [
//        "*" => [ "fragment_size" => 300]
//        "*" => ["index_options" => "offsets"]
//        "*" => new \StdClass()
//      ]
//    ];

    $this->query["query"]["filtered"]["query"]["bool"]["minimum_should_match"] = 1;

    return $this->query;
  }

  protected function schema_text($field, $value, $condition, $type){
    $condition     = ($condition === "contains")? "match": "match_phrase";
    $boolCondition = ($type === "filter")? "must": "should";
    $this->query["query"]["filtered"][$type]["bool"][$boolCondition][][$condition][$field] = ["query" => $value, "fuzziness" => "AUTO"];
  }

  protected function schema_integer($field, $value, $condition, $type){
    $condition     = ($condition === "contains")? "prefix": "term";
    $boolCondition = ($type === "filter")? "must": "should";
    $this->query["query"]["filtered"][$type]["bool"][$boolCondition][][$condition][$field] = $value;
  }

  protected function schema_textPrefix($field, $value, $condition, $type){
    $condition     = "prefix";
    $boolCondition = ($type === "filter")? "must": "should";
    $this->query["query"]["filtered"][$type]["bool"][$boolCondition][][$condition][$field] = $value;
  }

  protected function schema_term($field, $value, $condition, $type){
    $condition     = "term";
    $boolCondition = ($type === "filter")? "must": "should";
    $this->query["query"]["filtered"][$type]["bool"][$boolCondition][][$condition][$field] = $value;
  }

  protected function schema_between($field, $value, $condition, $type){
    $condition     = "range";
    $boolCondition = ($type === "filter")? "must": "should";
    $this->query["query"]["filtered"][$type]["bool"][$boolCondition][][$condition][$field] = [
      "lte" => $value["lte"],
      "gte" => $value["gte"]
    ];
  }


  protected function schema_isbn_ecli($field, $value, $condition, $type){
    $boolCondition = ($type === "filter")? "must": "should";
    $this->query["query"]["filtered"][$type]["bool"][$boolCondition][]["regexp"][$field] = [
      "value" => '[0-9\-]*'.$value.'[0-9\-]*'
    ];
  }
}
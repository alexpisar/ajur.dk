<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.12.14
 * Time: 15:08
 */

namespace UiStarterKit\Builders;


use UiStarterKit\Helpers\AlertsRenderer;
use Illuminate\View\View;

/**
 *
 * show off @method
 *
 * @method null error(string $message)
 * @method null notice(string $message)
 * @method null success(string $message)
 * @method null confirm(string $message)
 *
 * @method array getError()
 * @method array getNotice()
 * @method array getSuccess()
 * @method array getConfirm()
 *
 * @method View renderError()
 * @method View renderNotice()
 * @method View renderSuccess()
 * @method View renderConfirm()
*/
class Alerts {

  /**
   * @var \UiStarterKit\Helpers\MessagesKeeper
   */
  private $alertInstance;
  private $alertsRenderer;

  public function __construct(){
    $this->alertInstance = \App::make('\UiStarterKit\Helpers\MessagesKeeper');
    $this->alertsRenderer = new AlertsRenderer();
  }

  public function __call($method, $params){
    if(strpos($method, "get") === 0){         // get certain messages array
      return $this->getMessages(preg_replace('/get/', '', $method, 1));

    }elseif(strpos($method, "render") === 0){ // render certain messages
      return $this->renderMessages(preg_replace('/render/', '', $method, 1));

    }else{                                // add messages
      $this->addMessage($method, $params[0]);
    }
  }


  /**
   * @param $status string
   * @return array
   */
  public function getMessages($status){
    return $this->alertInstance->get()->get($status);
  }


  /**
   * @param $status string
   * @return View
   */
  public function renderMessages($status){
    return $this->alertsRenderer->render($status);
  }

  /**
   * @param $status string
   * @param $message string
  */
  public function addMessage($status, $message){
    $this->alertInstance->add($status, $message);
  }

  public function translate($enable = true){
    $this->alertInstance->translate = $enable;
  }

  public function keep($andOldRequestData = false){
    $this->alertInstance->storeInSession = true;
    $this->alertInstance->keepOldData = $andOldRequestData;
  }
  public function flush(){ $this->alertInstance->clearMsgCache = true; }
  public function flushOld(){ $this->alertInstance->keepOldData = false; }

  public function clear($group = null){ $this->alertInstance->remove($group); }

  public function getAll(){
    return clone $this->alertInstance->get();
  }

  public function renderAll(){
    return $this->alertsRenderer->renderAll();
  }

  public function selectGroup($group){
    $this->alertInstance->group($group);
  }


} 
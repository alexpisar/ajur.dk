<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 26.12.14
 * Time: 17:36
 */

namespace UiStarterKit\Helpers;


abstract class Widget {

  protected $viewName;
  protected $namespace;
  protected $data = [];

  public function __construct($viewName = null, array $data = []){
    $this->namespace = "widget";
    if($data)     $this->data = $data;
    if($viewName) $this->viewName = $viewName;
  }

  public function setViewData(array $input, $merge = true){
    if($merge) $this->data = array_merge($this->data, $input);
    else       $this->data = $input;
  }

  public function setViewName($name){
    $this->viewName = $name;
  }

  public function render(){
    return \View::make("{$this->namespace}::{$this->viewName}", $this->data);
  }

  public function __toString(){
    return $this->render()->__toString();
  }
} 
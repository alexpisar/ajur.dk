<?php namespace UiStarterKit\Helpers;

use ArrayIterator;
use Illuminate\Pagination\Factory;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;

class CollectionPaginator extends Paginator {

  /**
   * Create a new Paginator instance.
   *
   * @param  \Illuminate\Pagination\Factory  $factory
   * @param  array     $items
   * @param  int       $total
   * @param  int|null  $perPage
   * @return void
   */
  public function __construct(Factory $factory, Collection $items, $total, $perPage = null)
  {
    $this->factory = $factory;

    if (is_null($perPage))
    {
      $this->perPage = (int) $total;
      $this->hasMore = count($items) > $this->perPage;
      $this->items = array_slice($items, 0, $this->perPage);
    }
    else
    {
      $this->items = $items;
      $this->total = (int) $total;
      $this->perPage = (int) $perPage;
    }
  }


  public static function make(Collection $items, $total, $perPage = null){
    $paginator = new self( \App::make("paginator") ,$items, $total, $perPage);
    return $paginator->setupPaginationContext();
  }


  /**
   * Get a collection instance containing the items.
   *
   * @return \Illuminate\Support\Collection
   */
  public function getCollection()
  {
    return $this->items;
  }

  /**
   * Get the items being paginated.
   *
   * @return array
   */
  public function getItems()
  {
    return $this->items;
  }

  /**
   * Set the items being paginated.
   *
   * @param  mixed  $items
   * @return void
   */
  public function setItems($items)
  {
    $this->items = $items;
  }

  /**
   * Get an iterator for the items.
   *
   * @return \ArrayIterator
   */
  public function getIterator()
  {
    return $this->items->getIterator();
  }

  /**
   * Determine if the list of items is empty or not.
   *
   * @return bool
   */
  public function isEmpty()
  {
    return $this->items->isEmpty();
  }

  /**
   * Get the number of items for the current page.
   *
   * @return int
   */
  public function count()
  {
    return $this->items->count();
  }

  /**
   * Determine if the given item exists.
   *
   * @param  mixed  $key
   * @return bool
   */
  public function offsetExists($key)
  {
    return $this->items->has($key);
  }

  /**
   * Get the item at the given offset.
   *
   * @param  mixed  $key
   * @return mixed
   */
  public function offsetGet($key)
  {
    return $this->items->get($key);
  }

  /**
   * Set the item at the given offset.
   *
   * @param  mixed  $key
   * @param  mixed  $value
   * @return void
   */
  public function offsetSet($key, $value)
  {
    $this->items->put($key, $value);
  }

  /**
   * Unset the item at the given key.
   *
   * @param  mixed  $key
   * @return void
   */
  public function offsetUnset($key)
  {
    $this->items->forget($key);
  }
}

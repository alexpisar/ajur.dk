<?php
namespace UiStarterKit\Helpers;

class Assets {

  protected $jsFolder;
  protected $cssFolder;
  protected $pluginsFolder;

  public function __construct(){
    $this->jsFolder       = public_path("js");
    $this->cssFolder      = public_path("styles");
    $this->pluginsFolder  = public_path("plugins");
  }

} 
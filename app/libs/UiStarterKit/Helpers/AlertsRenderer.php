<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 22.12.14
 * Time: 11:52
 */

namespace UiStarterKit\Helpers;


class AlertsRenderer {

  public $tplsFodler = "partials.messages";
  /**
   * @var MessagesKeeper
   */
  protected $messages;

  public function __construct(){
    $this->messages = \App::make('\UiStarterKit\Helpers\MessagesKeeper');
  }

  public function render($type){
    $type = strtolower($type);
    return \View::make($this->tplsFodler.".$type", ["data" => $this->messages->get()->get($type)]);
  }

  public function renderAll(){
    if(!$this->messages->get()->has()) return "";
    $out = "";
    $keys = array_keys($this->messages->get()->toArray());
    foreach($keys as $key){
      $out .= $this->render($key);
    }
    return $out;
  }
} 
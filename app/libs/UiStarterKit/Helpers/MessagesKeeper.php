<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19.12.14
 * Time: 22:52
 */

namespace UiStarterKit\Helpers;


use Illuminate\Support\Arr;
use Illuminate\Support\MessageBag;

class MessagesKeeper {

  public $storeInSession = false;
  public $keepOldData = false;
  public $translate = false;

  protected $currentRequestData;
  protected $prevRequestData;
  protected $bunchOfData;
  protected $selectedGroup;

  protected $sessionSlug = "__messagesKeeper";
  protected $defaultGroup = "default";



  public function __construct(){

    if( $session = \Session::get($this->sessionSlug) ){
      $this->group( array_get($session, "selectedGroup", $this->defaultGroup) );
      $this->bunchOfData = $this->prevRequestData = array_get($session, "storage", []);
    }else{
      $this->group($this->defaultGroup);
    }

  }


  /**
   *
  */
  public function group($groupName){
    $this->selectedGroup = $groupName;
    if(!isset($this->currentRequestData[$groupName])) $this->currentRequestData[$groupName] = new MessageBag();
    if(!isset($this->bunchOfData[$groupName]))        $this->bunchOfData[$groupName] = new MessageBag();
    return $this;
  }


  /**
   * @param $type string
   * @param $message string|array
  */
  public function add($type, $message){
    if(!is_array($message)) $message = [$message];

    foreach($message as $msg){
      if($this->translate){
        $msg = \Lang::get($msg);
      }
      $this->currentRequestData[$this->selectedGroup]->add($type, $msg);
      $this->bunchOfData[$this->selectedGroup]->add($type, $msg);
    }
  }


  /**
   * @return MessageBag
  */
  public function get(){
    return $this->bunchOfData[$this->selectedGroup];
  }

  public function remove($group = null){
    if($group){
      Arr::forget($this->currentRequestData, $group);
      Arr::forget($this->bunchOfData, $group);
    }else{
      $this->currentRequestData = $this->bunchOfData = [];
      $this->group($this->defaultGroup);
    }
  }


  /**
   *
  */
  public function saveMessages(){
    if(!$this->storeInSession){
      \Session::remove($this->sessionSlug);
    }else{

      \Session::set($this->sessionSlug, [
        "selectedGroup" => $this->selectedGroup,
        "storage"       => ($this->keepOldData) ? $this->bunchOfData : $this->currentRequestData
      ]);

    }
  }

} 
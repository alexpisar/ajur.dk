<?php

function t($key, array $replace = array(), $locale = null){
  return Lang::get($key, $replace, $locale);
}
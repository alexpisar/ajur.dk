<?php namespace UiStarterKit\Translation;

use Illuminate\Support\Collection;
use Illuminate\Support\NamespacedItemResolver;
use Illuminate\Translation\LoaderInterface;
use Symfony\Component\Translation\MessageSelector;
use Symfony\Component\Translation\TranslatorInterface;

class Translator extends \Illuminate\Translation\Translator{

  private static $cache;

  /**
   * Get the translation for the given key.
   *
   * @param  string $key
   * @param  array $replace
   * @param  string $locale
   * @return string
   */
  public function get($key, array $replace = array(), $locale = null){
    $locale = $locale?$locale:\App::getLocale();

    if(isset(self::$cache[$locale][$key])){                     // if in cache
      $translation = self::$cache[$locale][$key];
    }elseif($translation = $this->getFromDb($key, $locale)){    // if in db
      $this->fillCache($key, $locale);
    }else{                                                      // get from app/lang
      $translation = parent::get($key, $replace, $locale);
    }
    return e($translation);
  }

  private function getFromDb($key, $locale){
    if( !$transKey = \TranslationKey::where("key","=",$key)->first()){
      return null;
    }

    $currentTrans = $transKey->translations()
      ->where("locale","=",$locale)
      ->first();

    return $currentTrans? $currentTrans->text: null;
  }

  private function fillCache($key, $locale){
    $group = explode(".",$key);
    $group = $group[0];

    $translations = \DB::table("translation_keys")
      ->leftJoin("translations","translations.translation_key_id","=","translation_keys.id")
      ->where("translation_keys.group","=",$group)
      ->where("translations.locale","=",$locale)
      ->lists("text","key");

    if(!isset(self::$cache[$locale])) self::$cache[$locale] = [];

    self::$cache[$locale] = array_merge(self::$cache[$locale], $translations);
  }

}

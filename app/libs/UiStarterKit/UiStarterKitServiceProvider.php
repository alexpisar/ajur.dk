<?php namespace UiStarterKit;

use UiStarterKit\Alerts\Alerts;
use Illuminate\Support\ServiceProvider;

class UiStarterKitServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
//    $this->package('alex-atr/ui-starter-kit');


    \App::singleton('\UiStarterKit\Helpers\MessagesKeeper');

    \App::after(function(){
      $mk = \App::make('\UiStarterKit\Helpers\MessagesKeeper');
      $mk->saveMessages();
    });


    // add widgets
    \View::addNamespace("widget", app_path("widgets/views"));
    \ClassLoader::addDirectories( app_path("widgets") );
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array();
	}

}


require app_path("libs/UiStarterKit/helperFuncs.php");
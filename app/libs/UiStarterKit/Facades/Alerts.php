<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 16.12.14
 * Time: 15:07
 */
namespace UiStarterKit\Facades;

use Illuminate\Support\Facades\Facade;

class Alerts extends Facade{

  protected static function getFacadeAccessor() {
    return new \UiStarterKit\Builders\Alerts();
  }

}

<?php

return [
  "host"               => $_ENV["elasticsearch.host"],
  "retries"            => 2,

  "number_of_shards"   => 5,
  "number_of_replicas" => 1,

  "index_name"         => $_ENV["elasticsearch.index_name"],
  "type_name"          => $_ENV["elasticsearch.type_name"],
];
<?php

class HomeController extends BaseFrontPageController {


	public function showWelcome()
	{
		return View::make('hello');
	}

	public function getIndex(){
		$this->title = "Front page";

    // = Article::with(["contentType", "tab"])->take(6)->get(["summary_short", "date_publication"]);
    $this->viewData["frontNews"] = Article::with(["contentType", "tab", "publishers"])
            ->where("publish_on_page_news", "=", "1")->where("published", "=", "1")
            ->orderBy("date_publication", "desc")->take(6)->get();
		return $this->render(__FUNCTION__);
	}

	public function postSupport(){
		//$code = Input::get('CaptchaCode');
		$inputs = Input::only(["name", "email", "phone", "inquiry"]);
	//	$captcha = App::make("Captcha");

  //		$isHuman = $captcha->Validate($code);
  	//	Alert::keep();
  		$validator = Validator::make($inputs, Support::getRules());
      if($validator->fails()){
          //Alert::error($validator->messages()->all());
          //return Redirect::back()->withInput();
          return Response::json([
                "msg" =>   implode("\n" ,$validator->messages()->all()),
                "success" => false,
              ]);
      }
//      if (!$isHuman) {
//        Alert::error('Captcha validation failed, try again.');
//        return Redirect::back()->withInput();
//	    }
	    Support::create($inputs);
	    $email = Setting::find("support");
      if($email){
        Mail::send("emails.supportInbox", $inputs, 
                function(\Illuminate\Mail\Message $message) use ($email){
               $message->to($email->value);
           });
        }
       //Alert::success(t("globals.support_sent"));
     //  return Redirect::back();
      return Response::json([
          "msg" => t("globals.support_sent"),
          "success" => true
          ]);
  }
  
  public function getStatic($alias){
    $page = StaticPage::with("trans")->find($alias);
    $this->title = $page->title;
    $this->viewData["page"] = $page;
    return $this->render(__FUNCTION__);
  }
}

<?php

class SearchController extends BaseFrontPageController {


  const RESULTS_PRE_PAGE = 15;


  public function __construct(){
    parent::__construct();
    $this->setCaptcha();
  }

  public function getExtendedSearchPage(){
    $this->title = "Search page";

//    $search = new \CustomSearch\SearchProviders\ArticleSearch();
//    dd($search->reindex());
//    $filters = $search->getFieldsList(true);
//    dd($filters);

    $this->fillFilters();

    return $this->render(__FUNCTION__);
  }



  public function postExtendedSearch(){

    $sp = new \CustomSearch\SearchProviders\ArticleSearch();
    $query = [];
    $typeFilter = Input::get("filters.type_filter");

    // type filter, make query
    for($i=0; $i<count($typeFilter["type"]); $i++){
      $typeFilter["text"][$i] = trim($typeFilter["text"][$i]);
      if(!$typeFilter["text"][$i]) continue;

      if( $typeFilter["type"][$i] === "Any"){
        foreach($this->getTypeFilterFields(false) as $field=>$fieldTranslation){

          if( !$sp->validateFieldValue($field, $typeFilter["text"][$i]) ) continue;

          $query[] = [
            "field"     => $field,
            "condition" => $typeFilter["condition"][$i],
            "value"     => trim($typeFilter["text"][$i]),
            "type"      => "query"
          ];
        }
      }else{
        if( !$sp->validateFieldValue($typeFilter["type"][$i], $typeFilter["text"][$i]) ) continue;

        $query[] = [
          "field"     => $typeFilter["type"][$i],
          "condition" => $typeFilter["condition"][$i],
          "value"     => trim($typeFilter["text"][$i]),
          "type"      => "query"
        ];
      }

    }

    if(Input::get("filters.publication_date") != "Any"){
      $query[] = [
        "field"     => "year",
        "condition" => "contains",
        "type"      => "filter",
        "value"     => Input::get("filters.publication_date"),
      ];
    }

    if(Input::get("filters.locale") != "Any"){
      $query[] = [
        "field"     => "locale",
        "condition" => "contains",
        "type"      => "filter",
        "value"     => Input::get("filters.locale"),
      ];
    }

    if((int)Input::get("filters.content_type") > 0){
      $query[] = [
        "field"     => "contentType.id",
        "condition" => "contains",
        "type"      => "filter",
        "value"     => Input::get("filters.content_type"),
      ];
    }


    $sd = Input::get("filters.start_date");
    $ed = Input::get("filters.end_date");
    if(!empty($sd["year"]) || !empty($ed["year"])){

      try{
        $sd = \Carbon\Carbon::create($sd["year"], $sd["month"], $sd["day"]);
      }catch(Exception $e){
        $sd = \Carbon\Carbon::create(1977, 1, 1);
      }

      try{
        $ed = \Carbon\Carbon::create($ed["year"], $ed["month"], $ed["day"]);
      }catch(Exception $e){
        $ed = new \Carbon\Carbon();
      }

      $query[] = [
        "field"     => "created_at",
        "condition" => "contains",
        "type"      => "filter",
        "value"     => [
          "gte" => $sd->toDateString(),
          "lte" => $ed->toDateString()
        ],
      ];
    }

    Session::set("extendedSearchData.query", $query);
    Session::set("extendedSearchData.filters", Input::get("filters"));

    return Redirect::route("search.result")->withInput();
  }



  public function getSearchResult(){
    $this->title = "Search page";
    if(
      !Session::has("extendedSearchData.query") ||
      !Session::has("extendedSearchData.filters")
    ) return Redirect::route("search.extended");

    $this->fillFilters();

    $this->viewData["searchResults"] =
      $this->paginateSearchResults(Session::get("extendedSearchData.query"));


    return $this->render("getExtendedSearchPage");

  }



  public function getSimpleSearchResults(){
    $this->title = "Simple search";
    $s = new \CustomSearch\SearchProviders\ArticleSearch();

    $searchString = trim(Input::get("query", ""));
    $searchQuery = [];

    if($searchString){
      foreach($this->getTypeFilterFields(false) as $field=>$fieldTranslation){
        if( !$s->validateFieldValue($field, $searchString )) continue;

        $searchQuery[] = [
          "field"     => $field,
          "condition" => "contains",
          "value"     => $searchString,
          "type"      => "query"
        ];
      }
    }

    $this->viewData["searchResults"] =
      $this->paginateSearchResults($searchQuery, ["query" => $searchString]);

    return $this->render(__FUNCTION__);
  }




  protected function getTypeFilterFields($withAnyField = true){
    $fields = [];
    if($withAnyField ){
      $fields["Any"] = t("search.any");
    }

    $fields = array_merge($fields, [
      "title"          => t("search.title"),
      "authors.name"   => t("search.author"),
      "summary_full"   => t("search.subject"),
      "year"           => t("search.year"),
      "isbn"           => t("search.isbn"),
      "ecli"           => t("search.ecli"),
    ]);

    return $fields;
  }



  protected function fillFilters(){

    $this->viewData["queryTypes"] = $this->getTypeFilterFields();
    $this->viewData["wordConditions"] = [
      "contains" => t("search.contains"),
      "inARow"   => t("search.in_a_row")
    ];

    $this->viewData["monthList"] = array_combine(
      range(1,12),[
      t("calendar.january"),
      t("calendar.february"),
      t("calendar.march"),
      t("calendar.april"),
      t("calendar.may"),
      t("calendar.june"),
      t("calendar.july"),
      t("calendar.august"),
      t("calendar.september"),
      t("calendar.october"),
      t("calendar.november"),
      t("calendar.december"),
    ]);


    $availableYears = array_reverse(
      range(
        substr(Article::min("year"), 0, 4),
        \Carbon\Carbon::create()->year
      )
    );

    array_unshift($availableYears, t("search.any"));
    $this->viewData["availableYears"] = array_combine($availableYears, $availableYears);

    $this->viewData["minArticleYear"] = substr(Article::min("year"), 0, 4);
    $this->viewData["contentTypes"] = array_merge([0 => "Any"], ContentType::lists("name", "id"));

    $this->viewData["languages"] = array_merge(["Any"=>"Any"], Language::whereEnabled(1)->lists("name", "locale"));


    if(Session::has("_old_input")){
      $typeFilterData = Input::old("filters.type_filter", []);
      $this->viewData["typeFilterData"] = [];
      foreach($typeFilterData as $name=>$data){
        for($i=0; $i<count($data); $i++){
          $this->viewData["typeFilterData"][$i][$name] = $data[$i];
        }
      }
      Session::forget("_old_input.filters.type_filter");
    }

  }


  protected function paginateSearchResults(array $query, array $appends = []){

    if(empty($query)){
      $p = Paginator::make([], 0, self::RESULTS_PRE_PAGE);
      foreach($appends as $key=>$val) $p->appends($key, $val);
      return $p;
    }

    $search = new \CustomSearch\SearchProviders\ArticleSearch();
    $response = $search->find(
      $query,
      self::RESULTS_PRE_PAGE,
      Input::get("page", 1)
    );

    $result = new \CustomSearch\Collections\ResponseCollection($response, "Article");
    $p = \UiStarterKit\Helpers\CollectionPaginator::make(
      $result,
      $response["hits"]["total"],
      self::RESULTS_PRE_PAGE
    );

    foreach($appends as $key=>$val) $p->appends($key, $val);

    return $p;
  }
}

<?php

class BaseController extends Controller {

  protected $viewData = [];
  protected $title;
  protected $description;
  protected $keywords;
  protected $bodyClass;
  protected $bodyId;
  protected $styles = [];
  protected $scripts = [];
  protected $viewPrefix = '';
  protected $beforeRender = [];
  protected $formData = [];
  protected $oldInputHasPriority = true;
  protected $keyOldData = '';


  protected $uploadDir ="";
  protected $aliasMenu = [
    'dashboard' => ["dashboard"],
    'database' => ["database", "database.build", "database.articles", 'database.authors', 'database.authors.add',
        'database.authors.edit', 'database.publishers', 'database.publisher.add', 'database.publisher.edit',
        'database.tags', 'database.tag.add', 'database.tag.edit', 'content.article.new', 'content.article.edit' ],
    'content' => ['content.manage', 'content.browse', 'content.add'],
    'advertisements' => ['advertisements'],
    'staff' => ["users.staff", 'users.addStaff'],
    'pricing' => ["pricing", "pricing.new"],
    'subscriptions' => ['subscriptions', 'subscribers.browse', 'subscribers.add', 'subscribers.edit', 'payments'],
    'settings' => ['settings', 'settings.edit', 'settings.translations.edit', 'settings.translations'],
    'static' => ['static.list', 'static.edit', 'static.add'],
  ];



  public function __construct(){
    Alert::translate();
    App::bind("Captcha", function(){
      $captchaConfig = array(
        'CaptchaId' => 'ExampleCaptcha', // an unique Id for the Captcha instance
        'UserInputId' => 'CaptchaCode' // the Id of the Captcha code input textbox
      );
      return BotDetectCaptcha\LaravelCaptcha\BotDetectLaravelCaptcha::GetCaptchaInstance($captchaConfig);
    });

  }


  /**
   * Render view
   *
   * @param string $view
   * @param $data
   * @return \Illuminate\View\View
   */
  protected function render($view, array $data = []){
    foreach($this->beforeRender as $method) $this->$method();

    $this->viewData['_siteTitle'] = $this->title;
    $this->viewData['_siteDescription'] = $this->description;
    $this->viewData['_siteKeywords'] = $this->keywords;
    $this->viewData['_activeMenu'] = $this->getActiveMenu();

    $className = explode('\\', get_class($this));
    $newClassName = "";
    foreach ((array)$className as $key => $name) {
      if($key+1 !== count($className) ){
        $newClassName .= strtolower($name) ."-";
      }else{
        $newClassName .= strtolower(str_replace("Controller", "", end($className)));
      }
    }

    $this->viewData['_bodyClass'] = $newClassName . ' '. $this->bodyClass;
    $this->viewData['_bodyId'] = $this->bodyId;
    $this->viewData['_styles'] = $this->styles;
    $this->viewData['_scripts'] = $this->scripts;


    if($this->viewPrefix){
      $nbView = $this->viewPrefix.'.'.$view;
    }else{
      $data = array_merge($this->viewData,$data);
      $nbView = 'controllers.' . str_replace("\\", ".", get_class($this)). '.' . $view;
    }

    if(View::exists($nbView)){
      $view = $nbView;
    }

    if($this->formData){
      if($this->oldInputHasPriority){
        Session::flashInput( array_merge(Session::get($this->keyOldData), Input::old(null, [])) );
      }else{
        Session::flashInput( array_merge(Input::old(null, []), $this->formData ));
      }
    }

    return View::make($view,$this->viewData, $data);
  }


  protected function getActiveMenu(){
    $activeRoute = Route::currentRouteName();
    foreach ($this->aliasMenu as $alias => $routes) {
      if(in_array($activeRoute, $routes))
        return $alias;
    }
    return null;
  }

  /**
   * Gives unique file name, for uploaded file and target directory
   * @param Symfony\Component\HttpFoundation\File\UploadedFile $fileObj
   * @param string $dirPath
   * @param string $prefix
   * @return string
   */
  protected function getUniqueFileName(Symfony\Component\HttpFoundation\File\UploadedFile $fileObj, $dirPath, $prefix=''){
    $img_name = $prefix . $fileObj->getFilename();
    $img_ext = $fileObj->getClientOriginalExtension();
    $filecounter = 1;
    $img_duplicate = $img_name.'.'.$img_ext;

    while(file_exists($dirPath.'/'.$img_duplicate)){
      $img_duplicate = $img_name . '_' . $filecounter++ . '.'. $img_ext;
    }
    return $img_duplicate;
  }


  protected function addViewData($data){
    $this->viewData = array_merge($this->viewData, $data);
  }


  /**
   * Saves loaded file
   * @param string|Symfony\Component\HttpFoundation\File\UploadedFile $name
   * @param string $saveDir
   * @param null|array $validatorRules
   * @param string $namePrefix
   * @return object
   */
  protected function fileLoader($name, $saveDir, $validatorRules = null, $namePrefix = '', $newName = ''){
    $imgName = $dirPath = $errorMsg ='';

    if($name instanceof Symfony\Component\HttpFoundation\File\UploadedFile){
      $file = $name;
      $name = 'image';
    }else{
      $file = Input::file($name);
    }

    if(!$validatorRules){
      $validatorRules = [$name => 'mimes:jpeg,jpg,gif,png,bmp'];
    }

    if($file){
      $fileOriginal = $file->getClientOriginalName();
      $validator = Validator::make([$name => $file],$validatorRules);

      if(!$validator->fails()){

        $saveDir .='/';
        if($newName){
          $imgName = $newName.'.'.$file->getClientOriginalExtension();
        }else{
          $imgName = $this->getUniqueFileName($file,$saveDir, $namePrefix);
        }

        $file->move($saveDir, $imgName);
      }else{
        $errorMsg = "Validation error";
      }
    }else{
      $fileOriginal = '';
    }
//TODO:: make normal error return
    return (object)[
      'originalName'=> $fileOriginal,
      'imgName'     => $imgName,
      'pathName'    => $dirPath . $imgName,
      'hasFile'     => !is_null($file),
      'error'       => !empty($errorMsg),
      'errorMsg'    => $errorMsg
    ];
  }

  protected function deleteFileIfExists($file){
    if(file_exists($file) && !is_dir($file)){
      unlink($file);
    }
  }

  protected function createFolderIfNotExist($path){
    if(!file_exists($path) || !is_dir($path)){
      mkdir($path,0777,true);
    }
  }

  protected function addScript($script){
    $this->scripts[] = $script;
  }
  protected function addStyle($style){
    $this->styles[] = $style;
  }

  protected function updateArticle($article, array $inputs){
    if(isset($inputs['author_ids']))
      $article->updateAuthors($inputs["author_ids"]);
    else
      $article->updateAuthors();//detach all

    if(isset($inputs['publisher_ids']))
      $article->updatePublishers($inputs["publisher_ids"]);
    else
      $article->updatePublishers();
    
    if(isset($inputs['tag_ids']))
      $article->updateTags($inputs["tag_ids"]);
    else
      $article->updateTags();
      
    //table_contents_file
    if(!empty($inputs['table_contents_file'])) {
      $fileObj = $this->fileLoader('table_contents_file', Article::getPathImage(true), ['table_contents_file' => 'mimes:jpeg,jpg,png,bmp']);
      if (!$fileObj->error) {
        if (!empty($article->table_contents_file)) {
          $article->deleteFileContents();
        }
        $article->table_contents_file = $fileObj->imgName;
        //
      } else {
        Alert::error('Something wrong happened while saving file contents');
        return Redirect::back()->withInput();
      }
    }
    //file cover
    if(!empty($inputs['view_cover_file'])) {
      unset($fileObj);
      $fileObj = $this->fileLoader('view_cover_file', Article::getPathImage(true), ['view_cover_file' => 'mimes:jpeg,jpg,png,bmp']);
      if (!$fileObj->error) {
        if (!empty($article->view_cover_file)) {
          $article->deleteFileCover();
        }
        $article->view_cover_file = $fileObj->imgName;
      } else {
        Alert::error('Something wrong happened while saving file cover');
        return Redirect::back()->withInput();
      }
    }
    //Destinations at the end
    $idsArticle = [];
    $idsTabs = [];
    $years = [];
    //article_id - exist duplicates
    //tab_id - new duplicates, need to copy
    for($i = 0; $i < $inputs['countDestinations']; $i++){
      if(isset($inputs['tab_ids'][$i]) && $inputs['tab_ids'][$i] != "0"){
        $idsTabs[] = $inputs['tab_ids'][$i];
      }
      if(isset($inputs['years'][$i]) && $inputs['years'][$i] != "0"){
        $years[] = date("Y-m-d", strtotime($inputs['years'][$i].'-01-01'));
      }
      if(isset($inputs['article_ids'][$i]) && $inputs['article_ids'][$i] != "0"){
        $idsArticle[] = $inputs['article_ids'][$i];
      }
    }

    $article->updateDuplicates($idsArticle, $idsTabs, $years);

    $article->save();
  }
  
}

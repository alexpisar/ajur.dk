<?php

abstract class BaseFrontController extends BaseController {

  public function __construct(){
    parent::__construct();
    if (Request::isMethod('get')){
      //$this->setCaptcha();
      $this->setFrontMenu();
    }
  }

	protected function setCaptcha(){
		$captcha = App::make("Captcha");
		$this->viewData["captchaHtml"] = $captcha->Html();
	}
  
  protected function setFrontMenu(){
     $menu = StaticPage::with("trans")->where("show_in_top", "=", 1)
            ->orWhere("show_in_bottom", "=", 1)->orderBy("weight")->get();
     $topMenu = [];
     $bottomMenu = [];
     //$posPricing = 1;
     foreach($menu as $pos => $item){
       if($pos == 1){ // pos of pricing
         $topMenu[] = ["url" => "#", "title" => t("globals.menu.pricing")];//(not yet designed)
         $bottomMenu[] = ["url" => "#", "title" => t("globals.menu.pricing")];
       }
       if((bool)$item->show_in_top)
         $topMenu[] = ["url" => route("static", ["alias"=> $item->alias]), 
             "title" => $item->trans->title];
       if((bool)$item->show_in_bottom)
         $bottomMenu[] = ["url" => route("static", ["alias"=> $item->alias]), 
             "title" => $item->trans->title];       
     }
     $this->viewData["topMenu"] = $topMenu;
     $this->viewData["bottomMenu"] = $bottomMenu;
    //$this->viewData["activeMenu"]
  }

}

<?php

class DatabaseController extends BaseFrontPageController {
  
	public function getIndex(){
    $this->title = t("database.main_topic_page");
    $tree = Topic::getRoots()->with(["children"])->first();
    $this->viewData["topics"] = $tree->children()->orderBy("weight")->get();
    return $this->render(__FUNCTION__);
	}  
  
  public function getTopic($id){
    $this->title = t("database.topic_page");
    $node = Topic::with('children', 'tabs')->find($id);
    $this->viewData["breadcrumbs"] = $node->findAncestors()->where("level", ">", "0")->get();
    $this->viewData["topics"] = $node->children()->orderBy("weight")->get();
    $this->viewData["tabs"] = $node->tabs()->orderBy("name")->get();
//   $this->viewData["articles"] = $node->tabs()->first()->articles()
//        ->with(['authors', 'publishers', 'tags'])->get(); 
    return $this->render(__FUNCTION__);
	}
  
  public function getAjaxTab($id){
      //ajax
    $view = "partials.front." . __FUNCTION__;
    //$tab = Tab::with(["articles", 'articles.authors', 'articles.publishers', 'articles.tags'])
     $tab = Tab::find($id);
    if(!$tab || !View::exists($view))
      return t("database.error_load_tab");
    
    $articles = $tab->articles()->with(['authors', 'publishers', 'tags', 'contentType'])->paginate(5);
    
    if($articles->count() > 0)
      return View::make($view, ["articles" => $articles]);
    else
      return t("database.no_content");
  }
  
  public function getTag($id){
    $this->title = t("database.tags");
    $tag = Tag::find($id);
    if(!$tag){
      Alert::keep();
      Alert::error(t("database.error_find_tag"));
      return Redirect::back();
    }
    $this->viewData["tag"] = $tag;
    $articles = $tag->articles()->with("contentType")->whereHas("tags", 
            function($q) use($id){
              $q->where("tag_id", "=", $id);
            })->orderBy("content_type_id")->paginate(30);
    $contents = [];
    //groupping articles
    foreach($articles as $a){
      $contents[$a->getTypeName()][] = $a;
    }
    $this->viewData["contents"] = $contents;
    $this->viewData["links"] = $articles->links();
    return $this->render(__FUNCTION__);
  }
  
  public function getArticle($id){
    $this->title = t("database.content_page");
    $article = Article::with(['authors', 'publishers', 'tags', 'contentType'])->find($id);
    if(!$article){
      Alert::keep();
      Alert::error(t("database.error_find_content"));
      return Redirect::back();
    }
    $this->viewData["article"]= $article;
    return $this->render(__FUNCTION__);
  }
  
  
}



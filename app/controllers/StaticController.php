<?php

//namespace Admin;

class StaticController extends BaseFrontController{
  
  public function getStaticList(){
    $this->title = "Static pages";
    $this->viewData["pages"] = StaticPage::with(["translations"])->get();
            //orderBy("is_system", "DESC")->orderBy("created_at", "DESC")->get();
            //dataTables sorts by title;
    return $this->render(__FUNCTION__);
  }
  
  public function getStaticNew(){
    $this->title = "Static page create";   
    $this->viewData["languages"] = Language::where("enabled", "=", 1)
              ->orderBy("name")->lists("name", "locale");
    return $this->render(__FUNCTION__);     
  }
  
  public function getStaticDelete($alias){
    $page = StaticPage::find($alias);
    if(!is_null($page)){
      $page->delete();
      Alert::keep();
      Alert::success("Page has been deleted");
    }
    return Redirect::route("static.list");
  }
  
  public function getStaticEdit($alias = null){
    $locale = Input::get("locale");
    if(is_null($alias)){
//      $page = new StaticPage;
//      $this->title = "Static page create";
      return Redirect::route("static.list");
    } else {
      $this->title = "Static page edit";
      $page = StaticPage::with(["translations"])->find($alias);
      if(!$page)
        return Redirect::route("static.list");
    }
    $this->viewData["page"] = $page;
    $this->viewData["languages"] = Language::where("enabled", "=", 1)->orderBy("name")->lists("name", "locale");
    if(!$locale)
      $locale = Config::get("app.fallback_locale");
    $this->viewData["translation"] = $page->translation($locale);
    
    if(is_null($alias)){
      return $this->render("getStaticNew");
    }
    return $this->render(__FUNCTION__);
  }
  
  public function postStaticEdit($alias = null){
    $locale = Input::get("locale");
    if(!$locale){
      Alert::error("Language is not selected!");
    }
    $fields = ["content", "title"];
    $inputsTrans = Input::only($fields);
     if(!is_null($alias)){
      $page = StaticPage::with("translations")->find($alias);
     } else {
       $page = new StaticPage;
     }
//     $is_system = Input::get("is_system", false);
     $isEditAlias = false;
     if(!(bool)$page->is_system){
        $fields[] = "locale";
        //$inputsPage = Input::except($fields);
        $inputsPage["weight"] = Input::get("weight");
        $inputsPage["show"] = Input::get("show", 0);
        $inputsPage["show_in_top"] = Input::get("show_in_top", 0);
        $inputsPage["show_in_bottom"] = Input::get("show_in_bottom", 0);
        $inputsPage["alias"] = preg_replace("/[^a-zA-Z0-9]+/", "-", strtolower(trim(Input::get("alias"))));
        $isEditAlias = $alias != $inputsPage["alias"];
     } else{
       $inputsPage = [];
     }
     
    $validator = Validator::make(array_merge($inputsPage, $inputsTrans), 
            array_merge($page->getRules($isEditAlias), StaticPageTranslation::$rules));
    if($validator->fails() || !$locale){
        Alert::keep();
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
    }
    
    if(!is_null($alias)){
      if(!(bool)$page->is_system && !$page->update($inputsPage) || !$page->translation($locale)->update($inputsTrans)) {
        Alert::error('Something wrong happened while saving page');
        return Redirect::back()->withInput();
      }
    }else{
      //$page = StaticPage::create($inputsPage);
      $page->fill($inputsPage);
      //$alias = $page->alias;
      $page->save();
      $t = new StaticPageTranslation($inputsTrans); 
      //$page = StaticPage::find($alias); //
      $page->saveOnNew($t); //and locale
    }
    

   // Alert::success("Settings has been saved.");
    
    return Redirect::route("static.list");
  }

}
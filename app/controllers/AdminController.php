<?php

//namespace Admin;

class AdminController extends BaseController{
    

	public function getDashboard() {
        $this->title = "Dashboard";
        $this->viewData["news"] = Setting::find("news");//Setting::where('key','=','news')->first();
	   return $this->render(__FUNCTION__);
	}

    public function getStaff(){
    	$this->title = "Staff management";
        //$this->viewData["usersList"] = User::orderBy("name")->get();//paginate(5);
        $this->viewData["usersList"] = User::whereHas('roles',function($q){
            $q->where( function ($q) { //subquery for orWhere
                $q->where('name','LIKE', "employee")
                    ->orWhere('name','LIKE',"admin");
            });
        })->orderBy("name")->get();//paginate(5);
        return $this->render(__FUNCTION__);
    } 

    public function getAddStaff(){
    	$this->title = "Add Staff";
        //$this->viewData["roles"] = Role::getSelectArray(Role::orderBy('id')->lists('id', 'name'));
        $this->viewData["roles"] = Role::where("name", "=", "admin")->orWhere("name", "=", "employee")->orderBy('id')->lists('name', 'id');
        return $this->render(__FUNCTION__);
    }

    public function postAddStaff(){
     	$validator = Validator::make(Input::all(), User::getRules());
      	Alert::keep();
     	if($validator->fails()){
      			Alert::error($validator->messages()->all());
           return Redirect::back()->withInput();
        }
        $last_log = Input::get('last_login');
		if(empty($last_log))
			$last_log = date("Y-m-d H:i:s");
        else
            $last_log = date("Y-m-d", strtotime($last_log));

        $password = substr(md5(time()), 0, 4);
        $user = User::create([
         "name"=>Input::get('name'),
         "initials"=>Input::get('initials'),
         "password"=> Hash::make($password),
         "email" => Input::get('email'),
          "last_login" => $last_log
            ]);


        $user->attachRole(Input::get('role'));

         Mail::send("emails.createUser", [
            // "accountType"=>
             "login" => $user->email,
             "password" => $password,
         ],function(\Illuminate\Mail\Message $message) use ($user){
             $message->to($user->email);
         });


        Alert::success('New user has been added successfully');
        return Redirect::route('users.staff');
       // return back
    }

    public function getDeleteStaff(){
        $id = Input::get("id");
        User::destroy($id);
        Alert::keep();
        Alert::success('User has been deleted successfully');
        return Redirect::back();
    }

    public function getDatabase(){
        $this->title = "Database";
        return $this->render(__FUNCTION__);
    }

    public function getBuildDatabase(){
        $this->title = "Build Database";
        return $this->render(__FUNCTION__);
    }

    public function getArticlesDatabase(){
        $this->title = "Content overview";
        return $this->render(__FUNCTION__);
    }

    // <editor-fold desc="Tree Database">
    //plugin tree
    public function getTreeJson(){
        $id = Input::get('id', 0);//GET request

        if(substr($id, 0, 1) !== 't') { //it isn't tab
            if ($id == 0) {
                $tree = Topic::getRoots()->first();//->toArray();
                if ($tree->children()->count() > 0) {
                    $tree['children'] = true;
                }
                $tree['type'] = 'root';
               // Clockwork::info($tree);
                return $tree;
            } else { //may be Topic::findByID($id); all of this
                $node = Topic::with('children', 'tabs', 'children.tabs', 'tabs.articles')->find($id);
                $topics = $node->children()->get();
                /*if ($node->id == 1)
                    $type = "topic";
                else*/
                $type = "subtopic";

                foreach ($topics as $child) {
                    $child['type'] = $type;
                    if ($child->children()->count() > 0 ||
                        $child->tabs()->count() > 0)
                          $child['children'] = true;
                    $child['text'] = "(".$child['weight'].") " . $child['text'];
                }

                $tabs = $node->tabs()->get();
                foreach ($tabs as $tab) {
                    $topics->add($tab->toTreeArray());
                }
                return $topics;
            }
        }
        else { //it's tab
            return Tab::getArticlesTreeByID($id);
        }
    }

    public function postCreateTreeObj(){
        if(Request::isJson()){
            $newNode = Input::all();
            if(strpos($newNode["id"], 'j') !== false){
                //it's create new object
                switch($newNode["type"]){
                    case "topic":
                    case "subtopic":
                         Topic::createTopic($newNode);
                        break;
                    case "tab":
                        Tab::createTab($newNode);
                        break;
                    default:
                        return Response::json(["success" => false]);
                }
            }
            else{ //it's rename
                switch($newNode["type"]){
                    case "topic":
                    case "subtopic":
                        Topic::renameTopic($newNode);
                        break;
                    case "tab":
                        Tab::renameTab($newNode);
                        break;
                    default:
                        return Response::json(["success" => false]);
                }
            }

            return Response::json(["success" => true]);
        }

        return Response::json(["success" => false]);
    }

    public function getDeleteTreeObj(){
        $id = Input::get('id');
        $type = substr($id, 0, 1);

        switch($type){
            case "t":
                Tab::deleteTab($id);
                break;
            case "a":
                Article::deleteArticleById($id);
                break;
            default:
                if(is_numeric($id)){
                    Topic::deleteTopic($id);
                }
                else
                    return Response::json(["success" => false]);
        }
        return Response::json(["success" => true]);
    }

    public function postMoveTreeObj(){
        if(Request::isJson()){
            $node = Input::all();
                switch($node["type"]){
                    case "topic":
                    case "subtopic":
                        Topic::move($node);
                        break;
                    case "tab":
                        Tab::move($node);
                       // Clockwork::info($node);
                        break;
                    case "article":
                        Article::moveOnTree($node);
                        break;
                    default:
                        return Response::json(["success" => false]);
                }

            return Response::json(["success" => true]);
        }

        return Response::json(["success" => false]);
    }

    public function postCopyTreeObj(){
        if(Request::isJson()){
            $node = Input::only(["type", "parent", "original_id"]);
            if($node["type"] == "article") {
                Article::copyOnTree($node);
                return Response::json(["success" => true]);
            }
            else if($node["type"] == "tab"){
                //Clockwork::info($node);
                Tab::copyOnTree($node);
                return Response::json(["success" => true]);
            }
            else if($node["type"] == "subtopic"){
                Topic::copyOnTree($node);
                return Response::json(["success" => true]);
            }
        }
        return Response::json(["success" => false]);
    }

    public function postUploadTabIcon(){
            $tabID = (integer)substr(Input::get("tabID"), 1);
            $tab = Tab::find($tabID);
            $fileObj = $this->fileLoader('fileIcon', Tab::getPathIcon(true));
            if (!$fileObj->error) {
                if(!empty($fileObj->imgName)) {
                    if (!empty($tab->icon_name)) {
                        //$this->deleteFileIfExists(Tab::getFilePathIcon($tab->icon_name, true));
                        $tab->deleteFileIcon();
                    }
                    $tab->icon_name = $fileObj->imgName;
                    $tab->save();

                    //resize image
                    $imagine = new Imagine\Gd\Imagine();
                    $filePath = Tab::getFilePathIcon($tab->icon_name, true);
                    $imagine->open($filePath)
                        ->resize(new \Imagine\Image\Box(64, 64))
                        ->save($filePath);
                }
            } else {
                //$tab->icon_name = null;
                return Response::json(["success" => false]);
            }

        return Response::json(["success" => true]);

    }

    public function postSetTopicWeight(){
        $id = (integer)Input::get("topicID");
        $weight = Input::get("inputWeight");
        $topic = Topic::find($id);
        if(is_numeric($weight))
            $topic->weight = (integer)$weight;
        else
            $topic->weight = 0;

        $topic->save();

        return Response::json(["success" => true]);
    }

    // </editor-fold>

    // <editor-fold desc="Authors crud">
    public function getAuthors(){
        $this->title = "AUTHORS";
        // $this->viewData["items"] = Author::where("name","like","%{$search}%")->orderBy('name')->paginate($itemsPerPage);
        // $this->viewData["itemsPerPage"] = $itemsPerPage;
        // $this->viewData["search"] = $search;
        $this->viewData["routeAdd"] = "database.authors.add";
        $this->viewData["routeEdit"] = "database.authors.edit";
        $this->viewData["routeDelete"] = "database.authors.delete";
        return $this->render("getItems");
    }

    public function getItemsJson(){
        $inputs = Input::only("length", "start", "draw", "search", "order", "title");
         switch (strtolower($inputs["title"])) {
             case "authors":
                // $query = Authors::where("name", "like", "%{$inputs["search"]["value"]}%");
                 $className = "Author";
                // $itemsTotal = Authors::all()->count();
                 break;
             case "publishers":
                $className = "Publisher";
                break;
             case "tags":
                $className = "Tag";
                 break;
         }
         $query = $className::where("name", "like", "%{$inputs["search"]["value"]}%");
        $countFiltered = $query->count();
        $items = $query->orderBy("name", $inputs["order"][0]["dir"])->skip($inputs["start"])->take($inputs["length"])->get();

       return array(
            "draw"            => intval($inputs['draw']),
            "recordsTotal"    => $className::all()->count(),
            "recordsFiltered" => $countFiltered, //without paging
            "data"            => Author::toDataTable($items)
        );        
    }

    public function getAuthorAdd(){
        $this->title = "New Author";
        return $this->render("getItemAdd");
    }

    public function postAuthorAdd(){
        $validator = Validator::make(Input::all(), 
            ['name'=> "required|max:255|unique:authors,name"]);
        Alert::keep();
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }

        Author::create(["name" => Input::get("name")]);

        Alert::success('Adding author success');
        return Redirect::route('database.authors');
    }

    public function getAuthorsDelete(){
        //Author::findOrFail($id);
        $id = Input::get("id");
        Author::destroy($id);
        Alert::keep();
        Alert::success("Author has been deleted.");
        return Redirect::back();
    }

    public function getAuthorEdit(){
        $id = Input::get("id");
        $this->title = "Edit Author";
        $this->viewData["label"] = "Rename author:";
        $this->viewData["route"] = "database.authors.edit";
        $this->viewData["input"] = Author::findOrFail($id);
        return $this->render("getItemEdit");
    }

    public function postAuthorEdit(){
        $id = Input::get("id");
        $validator = Validator::make(Input::only(["name"]), 
                ['name'=> "required|max:255|unique:authors,name,$id,id"]);
        Alert::keep();
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        //$author = Author::findOrFail(Input::get("id"));
        $author = Author::findOrFail($id);
        $author->name = Input::get("name");
        $author->save();

        Alert::success('Rename author success');
        return Redirect::route('database.authors');
    }
    // </editor-fold>

    // <editor-fold desc="Publishers crud">
    public function getPublishers(){
        /*$search = Input::get('search');
        $itemsPerPage = Input::get('selectItemsPage');
        if(empty($itemsPerPage))
            $itemsPerPage = 5;*/

        $this->title = "PUBLISHERS";
        // $this->viewData["items"] = Publisher::where("name","like","%{$search}%")->orderBy('name')->paginate($itemsPerPage);
        // $this->viewData["itemsPerPage"] = $itemsPerPage;
        // $this->viewData["search"] = $search;
        $this->viewData["routeAdd"] = "database.publisher.add";
        $this->viewData["routeEdit"] = "database.publisher.edit";
        $this->viewData["routeDelete"] = "database.publisher.delete";
        return $this->render("getItems");
    }

    public function getPublisherAdd(){
        $this->title = "New Publisher";
        return $this->render("getItemAdd");
    }

    public function postPublisherAdd(){
        $validator = Validator::make(Input::all(),
                ['name'=> "required|max:255|unique:publishers,name"]);
        Alert::keep();
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }

        Publisher::create(["name" => Input::get("name")]);

        Alert::success('Adding publisher success');
        return Redirect::route('database.publishers');
    }

    public function getPublisherDelete(){
        $id = Input::get("id");
        Publisher::destroy($id);
        Alert::keep();
        Alert::success("Publisher has been deleted.");
        return Redirect::back();
    }

    public function getPublisherEdit(){
        $id = Input::get("id");
        $this->title = "Edit Publisher";
        $this->viewData["label"] = "Rename publisher:";
        $this->viewData["route"] = "database.publisher.edit";
        $this->viewData["input"] = Publisher::findOrFail($id);
        return $this->render("getItemEdit");
    }

    public function postPublisherEdit(){
        $id = Input::get("id");
        $validator = Validator::make(Input::only(["name"]), 
                ['name'=> "required|max:255|unique:publishers,name,$id,id"]);
        Alert::keep();
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        //$author = Author::findOrFail(Input::get("id"));
        $publisher = Publisher::findOrFail($id);
        $publisher->name = Input::get("name");
        $publisher->save();

        Alert::success('Rename publisher success');
        return Redirect::route('database.publishers');
    }

    // </editor-fold>

    // <editor-fold desc="Tags crud">
    public function getTags(){
       /* $search = Input::get('search');
        $itemsPerPage = Input::get('selectItemsPage');

        if(empty($itemsPerPage))
            $itemsPerPage = 5;*/

        $this->title = "TAGS";
        // $this->viewData["items"] = Tag::where("name","like","%{$search}%")->orderBy('name')->paginate($itemsPerPage);
        // $this->viewData["itemsPerPage"] = $itemsPerPage;
        // $this->viewData["search"] = $search;
        $this->viewData["routeAdd"] = "database.tag.add";
        $this->viewData["routeEdit"] = "database.tag.edit";
        $this->viewData["routeDelete"] = "database.tag.delete";
        return $this->render("getItems");
    }

    public function getTagAdd(){
        $this->title = "New Tag";
        return $this->render("getItemAdd");
    }

    public function postTagAdd(){
        $validator = Validator::make(Input::all(), 
                ['name'=> "required|between:2,255|unique:tags,name"]);
        Alert::keep();
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }

        Tag::create(["name" => Input::get("name")]);

        Alert::success('Adding tag success');
        return Redirect::route('database.tags');
    }

    public function getTagDelete(){
        $id = Input::get("id");
        Tag::destroy($id);
        Alert::keep();
        Alert::success("Tag has been deleted.");
        return Redirect::back();
    }

    public function getTagEdit(){
        $id = Input::get("id");
        $this->title = "Edit Tag";
        $this->viewData["label"] = "Rename tag:";
        $this->viewData["route"] = "database.tag.edit";
        $this->viewData["input"] = Tag::findOrFail($id);
        return $this->render("getItemEdit");
    }

    public function postTagEdit(){
        $id = Input::get("id");
        $validator = Validator::make(Input::only(["name"]), 
            ['name'=> "required|between:2,255|unique:tags,name,$id,id"]);
        Alert::keep();
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        $tag = Tag::findOrFail($id);
        $tag->name = Input::get("name");
        $tag->save();

        Alert::success('Rename tag success');
        return Redirect::route('database.tags');
    }
    // </editor-fold>
    //<editor-fold desc="Manage Content">
    public function getManageContent(){
        $this->title = "Content Management";
        return $this->render(__FUNCTION__);
    }

    public function getAddContent(){
        $this->title = "Add Content";
        $this->viewData["contentTypes"] = ContentType::orderBy("name")->get();
        return $this->render(__FUNCTION__);
    }

    public function postAddContent(){
        Alert::keep();
        $validator = Validator::make(Input::only("name"), 
         ['name'=> "required|max:255|unique:content_types,name"]);
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        
        $content = new ContentType( Input::only("name"));
        $fileObj = $this->fileLoader('icon_name', ContentType::getPathIcon(true));
        if (!$fileObj->error){
            if(!empty($fileObj->imgName)) {
                $content->icon_name = $fileObj->imgName;
                //$content->name = "news";
                $content->save();

                //resize image
                $imagine = new Imagine\Gd\Imagine();
                $filePath = ContentType::getFilePathIcon($content->icon_name, true);
                $imagine->open($filePath)
                    ->resize(new \Imagine\Image\Box(64, 64))
                    ->save($filePath);
            }else{
              $content->save();
            }
        } else {
            //$tab->icon_name = null;
           // return Response::json(["success" => false]);
            Alert::error("Something went wrong try again later.");
            return Redirect::back()->withInput();
        }
        Alert::success("Content type has been created");
        //return Response::json(["success" => true, "id" => $content->id]);
        return Redirect::back();
    }

    public function postEditContent(){//$id){
        $id = Input::get("id");
        $content = ContentType::find($id);
        
        Alert::keep();
        $validator = Validator::make(Input::only("name"), 
         ['name'=> "required|max:255|unique:content_types,name,$content->id,id"]);
        if ($validator->fails()) {
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        $content->name = Input::get("name");
        $fileObj = $this->fileLoader('icon_name', ContentType::getPathIcon(true));
        if (!$fileObj->error) {
            if (!empty($fileObj->imgName)) {
                if (!empty($content->icon_name)) {
                    //$this->deleteFileIfExists(Tab::getFilePathIcon($tab->icon_name, true));
                    $content->deleteFileIcon();
                }
                $content->icon_name = $fileObj->imgName;

                //resize image
                $imagine = new Imagine\Gd\Imagine();
                $filePath = ContentType::getFilePathIcon($content->icon_name, true);
                $imagine->open($filePath)
                    ->resize(new \Imagine\Image\Box(64, 64))
                    ->save($filePath);
            }
        }
         else {
            Alert::error("Something went wrong try again later.");
            return Redirect::back()->withInput();
        }
        $content->save();
        Alert::success("Content type has been edited");
        return Redirect::back();
    }

    public function getContentTypeDelete(){
        $id = Input::get("id");
        Alert::keep();
        if(empty($id)){
            Alert::error("Something went wrong try again later.");
            return Redirect::back();
        }
        ContentType::find($id)->delete();
        Alert::success("Content type has been deleted.");
        return Redirect::back();
    }

    public function getNewArticle(){
        $this->title = "Add new content";
        $tabID = Input::get('tab_id');
        if(!empty($tabID)){
            if(!is_numeric(substr($tabID, 0, 1))) {
                //  $tabID = (integer) substr($tabID, 1);
                //$tabID = (integer) substr($tabID, 1); //if from js tree
                $this->viewData["destination"] = Tab::getDestinationById((integer) substr($tabID, 1));
            }
            else {
                $this->viewData["destination"] = Tab::getDestinationById($tabID);
                $tabID = 't' . $tabID; //for one type data article
            }
        }
        else{
            $tabID = null;
        }

        $this->viewData["tab_id"] = $tabID;
        $this->viewData["content_type_id"] =  Input::get('content_type_id'); //if null then nothing
        $this->viewData["authors"] = Author::orderBy("name")->get();
        $this->viewData["publishers"] = Publisher::orderBy("name")->get();
        $this->viewData["tags"] = Tag::orderBy("name")->get();
        $this->viewData["contentTypes"] = ContentType::orderBy("name")->lists("name", "id");
        $years = [];
        $yearToday = date("Y");
        for($i=1995; $i <= $yearToday; $i++){
            $years[$i] = $i;
        }
        $this->viewData["years"] = $years;
        
        $this->viewData["languages"] = Language::where("enabled", "=", 1)
                ->orderBy("name")->lists("name", "locale");
        
        return $this->render(__FUNCTION__);
    }

    public function postNewArticle(){
        $inputs = Input::all();
        Alert::keep();
        $i = 0;
        if($inputs['countDestinations'] == 0){
            Alert::error("Destination is required");
            return Redirect::back()->withInput();
        }
        else{ //get first tab_id
            for(; $i < $inputs['countDestinations']; $i++){
                if(isset($inputs['tab_ids'][$i]) && $inputs['tab_ids'][$i] != "0"){
                    $inputs['tab_id'] = substr($inputs['tab_ids'][$i], 1);
                    $inputs['year'] = date("Y-m-d", strtotime($inputs['years'][$i].'-01-01'));
                    unset($inputs['tab_ids'][$i]); // +
                    unset($inputs['years'][$i]);
                    break;
                }
            }
        }

        $validator = Validator::make($inputs, Article::getRules());

        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }

        if(empty($inputs["date_publication"]))
            $inputs["date_publication"] = date("Y-m-d H:i:s");
        else
           $inputs["date_publication"] =  date("Y-m-d", strtotime($inputs["date_publication"]));

        //  Alert::error("test error");
        // return Redirect::back()->withInput();       
        //$article = Article::create($inputs);
        $article = new Article($inputs);
        $article->content_type_id = $inputs["content_type_id"];
        $article->tab_id = $inputs['tab_id'];
        $article->year = $inputs['year'];
        $article->user_id = Auth::user()->id;
        //for groupping by duplicate_id
        $article->save(); //for get id article
       // $article->duplicate_id = $article->id; //original
        $inputs['article_ids'][$i] = $article->id; //to not delete on update

        $this->updateArticle($article, $inputs);
        Alert::success('Content has been created');
        //return $article;
        $backUrl = $inputs["backUrl"];
        if($backUrl == URL::previous())
            return Redirect::route('content.browse');
        else
            return Redirect::to($backUrl);
    }

    public function getEditArticle(){
        $this->title = "Edit content";
        if(!is_numeric(Input::get('id')))
            $id = (integer) substr(Input::get('id'), 1);
        else
            $id = Input::get('id');
        $article = Article::with("authors", "tags", "publishers")->findOrFail($id);
        if(empty($article->locale))
          $article->locale = Config::get("app.fallback_locale");
        
        $this->viewData["article"] = $article;

        $this->viewData["destinations"] = $article->getDestinations();
        $this->viewData["thisAuthors"] = $article->authors()->get();
        $this->viewData["thisPublishers"] = $article->publishers()->get();
        $this->viewData["thisTags"] = $article->tags()->get();

        $this->viewData["contentTypes"] = ContentType::orderBy("name")->lists("name", "id");
        $years = [];
        for($i=1995; $i <= date("Y"); $i++){
            $years[$i] = $i;
        }
        $this->viewData["years"] = $years;

        // $excludeAuthors = $article->authors()->lists('author_id');
        // $this->viewData["authors"] = Author::whereNotIn('id', $excludeAuthors)->orderBy("name")->get();
         $this->viewData["authors"] = Author::orderBy("name")->get(); //all . and diffing on the blade. Because of Input::old ...


        // $excludePublishers =  $article->publishers()->lists('publisher_id');
        // $this->viewData["publishers"] = Publisher::whereNotIn('id', $excludePublishers)->orderBy("name")->get();
        $this->viewData["publishers"] = Publisher::orderBy("name")->get();

        // $excludeTags =  $article->tags()->lists('tag_id');
        // $this->viewData["tags"] = Tag::whereNotIn('id', $excludeTags)->orderBy("name")->get();
        $this->viewData["tags"] = Tag::orderBy("name")->get();

        $this->viewData["languages"] = Language::where("enabled", "=", 1)
                ->orderBy("name")->lists("name", "locale");
       // Clockwork::info($this->viewData["thisAuthors"]);
        return $this->render(__FUNCTION__);
    }

    public function postEditArticle(){
        $inputs = Input::all();
        //return $inputs;
        $id = $inputs['id'];
       // $inputs['print_button'] = $inputs['print_button'];

        $article = Article::find($id);

        $article->content_type_id = $inputs["content_type_id"];

        if(empty($inputs["date_publication"]))
            $inputs["date_publication"] = date("Y-m-d H:i:s");
        else
            $inputs["date_publication"] =  date("Y-m-d", strtotime($inputs["date_publication"]));

        Alert::keep();
        $validator = Validator::make($inputs, Article::getRules());

        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        
        // Alert::error("test error");
        // return Redirect::back()->withInput();

        if (!$article->update($inputs)) {
            Alert::error('Something wrong happened while saving article');
            return Redirect::back()->withInput();
        }

        $this->updateArticle($article, $inputs);

        Alert::success('Content has been updated');
        $backUrl = $inputs["backUrl"];
        if($backUrl == URL::previous())
            return Redirect::route('content.browse');
        else
            return Redirect::to($backUrl);
    }


    public function getBrowseContent(){
        $this->title = "Content overview";
        $firstFilter = [0 => "all types", "added" => "newly added", "edited" => "newly edited"];
       // $firstFilter2 = ContentType::lists("name", "id");
        $contentTypes =  ContentType::orderBy("name")->lists("name", "id");
        foreach ($contentTypes as $id => $type) {
            $firstFilter[$id] = $type;
        } 
        // array_merge($firstFilter) - lost ids
        $this->viewData["filterTypes"] = $firstFilter;
        return $this->render(__FUNCTION__);
    }

    public function getContentDelete(){ //will be ajax
        Article::deleteArticleById(Input::get("id"));
       // Alert::keep();
       // Alert::success(" has been deleted.");
       // return Redirect::back();
        return Response::json(["success" => true]);
    }


    public function getContentJson(){
        $inputs = Input::only("length", "start", "draw", "published", "filterTypes", "filterTitles", "search");
        $query = DB::table("articles")->leftJoin('content_types','articles.content_type_id', '=', 'content_types.id')
            ->select('articles.id')
            //->select('content_types.name', 'articles.title', 'articles.id', 'articles.date_publication', 'articles.duplicate_id')
            ->where("articles.published","=",$inputs["published"]);

        $orderBy = 'title'; $dir="asc";
        //$orderBy = 'date_publication'; $dir="asc";
        if($inputs["search"]["value"] != "") {
            if($inputs["filterTitles"] == "titles"){
                $query->where("articles.title", "like", "%{$inputs["search"]["value"]}%");
            }
            elseif($inputs["filterTitles"] == "authors"){
                $query->join('article_author', 'articles.id', '=', 'article_author.article_id')
                    ->join('authors', 'authors.id', '=', 'article_author.author_id')
                    ->where("authors.name", "LIKE", "%{$inputs["search"]["value"]}%");
                $orderBy = "date_publication"; $dir="desc";
            }
            elseif($inputs["filterTitles"] == "publishers"){
                $query->join('article_publisher', 'articles.id', '=', 'article_publisher.article_id')
                    ->join('publishers', 'publishers.id', '=', 'article_publisher.publisher_id')
                    ->where("publishers.name", "LIKE", "%{$inputs["search"]["value"]}%");
                $orderBy = "date_publication"; $dir="desc";
            }
        }

        if($inputs["filterTypes"] != '0'){
            if(is_numeric($inputs["filterTypes"])){
                $query->where('content_types.id', '=', $inputs["filterTypes"]);
            }
            elseif($inputs["filterTypes"] == "added"){
               $query->where('articles.created_at', '>', \Carbon\Carbon::now()->subDay()->toDateTimeString());
                $orderBy = "date_publication"; $dir="desc";

            }
            elseif($inputs["filterTypes"] == "edited"){
                $query->where('articles.updated_at', '>', \Carbon\Carbon::now()->subDay()->toDateTimeString());
                 $orderBy = "date_publication"; $dir="desc";
            }
        }

        $countFiltered = $query->distinct()->count();
        $ids = $query->orderBy($orderBy, $dir)->skip($inputs["start"])->take($inputs["length"])->lists('articles.id');

        $articles = Article::with(["contentType", "authors", "publishers", "user"])->orderBy($orderBy, $dir)->find($ids);
       return array(
            "draw"            => intval($inputs['draw']),
            "recordsTotal"    => Article::all()->count(),
            "recordsFiltered" => $countFiltered, //without paging
            "data"            => Article::toDataTable($articles, $inputs["filterTitles"])
        );
    }
    //</editor-fold>

    // <editor-fold desc="Advertisements">
    public function getAdvertisements(){
        $this->title = "Advertisements";
        $items = Advertisement::orderBy('original_name')->get();
        $images = [0 => "Select file from list"];
        foreach ($items as $item) {
            $images[$item->id] = $item->original_name;
        }
        $this->viewData["images"] = $images;//array_merge([0 => 'Select file from list'], $items->lists('original_name', 'id'));
        $imgPaths = [];
        foreach ($items as $item) {
            $imgPaths[$item->id] = $item->getFile();
        }
        $this->viewData["imagePaths"] = $imgPaths;
        $active = Advertisement::where('is_active', '=', '1')->first();
        $this->viewData["activeID"] = is_null($active) ? 0 : $active->id;
        return $this->render(__FUNCTION__);
    }

    public function postAdvertisements(){
        Alert::keep();
        if(Input::hasFile('inputFile')){
            $advert = Advertisement::create(["original_name" => "newFile"]);
            $fileObj = $this->fileLoader('inputFile', Advertisement::getPathImage(true));
            if (!$fileObj->error) {
                if(!is_null($active = Advertisement::where('is_active', '=', '1')->first())) {
                    $active->is_active = 0;
                    $active->save();
                }

                //basename(originalName, pathinfo($path, PATHINFO_EXTENSION)); - without extension
                $advert->is_active = 1;
                $advert->original_name = $fileObj->originalName;
                $advert->file_name = $fileObj->imgName;
                $advert->save();
            } else {
                $advert->delete();
                Alert::error('Something went wrong, try again later.');
                return Redirect::back()->withInput();
            }
        }
        else{
            $id = Input::get('selectFile');
            if($id != '0'){
                if(!is_null($active = Advertisement::where('is_active', '=', '1')->first())) {
                    $active->is_active = 0;
                    $active->save();
                }
                $advert = Advertisement::find($id);
                $advert->is_active = 1;
                $advert->save();
            }
            else
                return Redirect::back();
        }

        Alert::success('Advertisement has been saved.');
        return Redirect::back();
       // return $fileObj->imgName;
    }

    // </editor-fold>

// <editor-fold desc="Pricing">

    public function getPricingNew(){
        $this->title = "New pricing";
        return $this->render(__FUNCTION__);
    }

     public function postPricingNew(){
        $inputs = Input::only("name", "persons", "price");
        $inputs["institutional_access"] = Input::get("institutional_access", 0);
        Alert::keep(); 

        $validator = Validator::make($inputs, SubscriptionType::getRules());
        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }

        SubscriptionType::create($inputs);
/*
        if (!$success) {
            Alert::error('Something wrong happened while saving pricing');
            return Redirect::back()->withInput();
        }*/
 
        Alert::success("Pricing has been created");
        return Redirect::route("pricing");
    }

    //Edit SubscriptionType
    public function getPricingEdit(){
        $this->title = "Edit pricing";
        $this->viewData["pricing"] = SubscriptionType::all();
        return $this->render(__FUNCTION__);
        
    }
    //Edit SubscriptionType
    public function postPricingEdit(){
        $inputs = Input::all();
        Alert::keep(); 
        $validModels = [];       
        for ($i=0; $i < $inputs["count"]; $i++) { 
            if(isset($inputs["isDelete" . $i]) && $inputs["isDelete" . $i] == 1){
                if(!is_null($subType = SubscriptionType::find($inputs["id" . $i])))
                    $subType->delete();
            }
            else{
                //$model = new SubscriptionType();
                $model = [
                    //"id"    => $inputs["id" . $i],
                    "name"  => $inputs["name" . $i],
                    "persons" => $inputs["persons" . $i],
                    "price" => $inputs["price" . $i],
                    "institutional_access" => isset($inputs["institutional_access" . $i]) 
                            ? $inputs["institutional_access" . $i] : 0
                ];
                        
                $validator = Validator::make($model, SubscriptionType::getRules());
                if($validator->fails()){
                    Alert::error($validator->messages()->all());
                    return Redirect::back()->withInput();
                }

                $validModels[$i] = $model;
            }
        }

        $success = false;
        DB::transaction(function() use(&$success, $validModels, $inputs){
            $success = true;
            foreach ($validModels as $key => $model) {
                $subscrType = SubscriptionType::find($inputs["id" . $key]);
                if($subscrType->persons > $model["persons"]){
                    SubscriberInfo::cuttingPersons($model["persons"], $subscrType);
                }
                else if($subscrType->persons < $model["persons"]){
                    $subscrType->load("subscriberInfos");
                    foreach ($subscrType->subscriberInfos as $info) {
                        $info->persons = $model["persons"];
                        $info->save();
                    }
                }
                if (!$subscrType->update($model)) {
                    $success = false;
                }
            }
         });
        if (!$success) {
            Alert::error('Something wrong happened while saving pricing');
            return Redirect::back()->withInput();
        }
 
        Alert::success("Pricing has been saved");
        return Redirect::back();
    }
    // </editor-fold> 
//Subscriptions    
    public function getSubscriptions(){
        $this->title = "Subscriptions";
        return $this->render(__FUNCTION__);
    }

    public function getNewSubscriber(){
        $this->title = "Add new subscriber";
        $this->viewData["subscTypes"] = SubscriptionType::all();
        return $this->render(__FUNCTION__);
    }

     public function postNewSubscriber(){
        $fieldsSubscriber = SubscriberInfo::getFieldsInput();
//for without white spaces Input::merge(array_map('trim', Input::all()));
        $inputsSubscriber = array_map('trim', Input::only($fieldsSubscriber));
                            //SubscriberInfo::trimUsers
        $inputsUsers = array_map('trim', Input::except($fieldsSubscriber));
        //Validation
        Alert::keep();
        if($inputsSubscriber['subscription_type_id'] == 0){
            Alert::error('The subscription type field is required.');
            return Redirect::back()->withInput();
        }
        // Alert::error("Test error");
        // return Redirect::back()->withInput();
        $validator = Validator::make($inputsSubscriber, SubscriberInfo::getRules());

        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }

        $last_login = date("Y-m-d H:i:s");
        $users = [];
        $users[0] = new User;
        $users[0]->name = $users[0]->initials = $inputsUsers["nameAdmin"];
        $users[0]->email = $inputsUsers["emailAdmin"];
        $users[0]->last_login = $last_login;
        $validator =  Validator::make($users[0]->toArray(), User::getRules());
        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        for ($i= $inputsUsers["countCustomers"] - 1, $j = 0; $i > 0 ; $i--) { 
            if(!empty($inputsUsers["tdNameUser" . $i]) && !empty($inputsUsers["tdNameUser" . $i])){
                $users[++$j] = new User;
                $users[$j]->name = $users[$j]->initials = $inputsUsers["tdNameUser" . $i];
                $users[$j]->email = $inputsUsers["tdEmailUser" . $i];
                $users[$j]->last_login = $last_login;
                $validator =  Validator::make($users[$j]->toArray(), User::getRules());
                if($validator->fails()){
                    Alert::error($validator->messages()->all());
                    return Redirect::back()->withInput();
                }
            }
        }
        
        //Save to db
        DB::transaction(function() use($inputsSubscriber, $users){
            $subscrInfo = new SubscriberInfo ($inputsSubscriber);
            $subscrType = SubscriptionType::find($inputsSubscriber['subscription_type_id']);
            //duplicate inform
            $subscrInfo->subscription_name = $subscrType->name;
            $subscrInfo->persons = $subscrType->persons;
            $subscrInfo->price = $subscrType->price;
            $subscrType->subscriberInfos()->save($subscrInfo);
            $subscrInfo->licens_number = str_pad($subscrInfo->id, 9, '0', STR_PAD_LEFT);
            $subscrInfo->save();
            //$users[0]->attachRole(Role::where('name', '=', 'company_owner')->first());
           //  $ownerRole = Role::where('name', '=', 'company_owner')->first();
          //  $customerRole = Role::where('name', '=', 'customer')->first();
            for ($i=0, $count=count($users);  $i < $count; $i++) { 
                if($i == 0){
                   $users[$i]->saveUser($subscrInfo, $i, true);
                }
                else{
                   $users[$i]->saveUser($subscrInfo, $i);
                }
            }
        });

        Alert::success('Subscriber information has been created');
        return Redirect::route('subscriptions');
    }

    public function getSubscribers(){
        $this->title = "Subscribers (overview)";
        // $this->viewData["pagetitle"] = "SUBSCRIBERS (OVERVIEW)";
        // $this->viewData["tableTitle"] = "Subscribers";
        // $this->viewData["thArr"] = ["Company name", "Joined", "Delete", "Edit"];
        $this->viewData["items"] = SubscriberInfo::orderBy("name")->get();
        return $this->render(__FUNCTION__);
    }

    public function getSubscribersJson(){
        //"length" - paging_items/per/page, "start" - start item on page,
        // order[0][column]=0
        // order[0][dir]=asc
        $inputs = Input::only("length", "start", "draw", "search", "order");
        if($inputs["order"][0]["column"] == 0)
            $orderBy = "name";
        else
           $orderBy = "created_at";

        //if(!empty($inputs["search"]["value"]))
            $query = SubscriberInfo::where("name", "like", "%{$inputs["search"]["value"]}%");
        //else
        $countFiltered = $query->count();    //  $inputs["order"][0]["dir"]  ->skip($inputs["start"])->take($inputs["length"])->get()
        $items = $query->orderBy($orderBy, $inputs["order"][0]["dir"])->skip($inputs["start"])->take($inputs["length"])->get();

       return array(
            "draw"            => intval($inputs['draw']),
            "recordsTotal"    => SubscriberInfo::all()->count(),
            "recordsFiltered" => $countFiltered, //without paging
            "data"            => SubscriberInfo::toDataTable($items)
        );
    }


    public function getDeleteSubscr(){
        $id = Input::get("id");
        SubscriberInfo::findOrFail($id)->delete();
        Alert::keep();
        Alert::success('Subscriber information has been deleted successfully');
        return Redirect::back();
    }

    public function getEditSubscriber(){
        $id = Input::get("id");
        $this->title = "Subscriber Information";
        $this->viewData["subscTypes"] = SubscriptionType::all();
        $this->viewData["subscriber"] = SubscriberInfo::with(["owner", "customers", "subscriptionType"])->find($id);
        return $this->render(__FUNCTION__);
    }

    public function postEditSubscriber(){
        $fieldsSubscriber = SubscriberInfo::getFieldsInput();
        $inputsSubscriber = array_map('trim', Input::only($fieldsSubscriber));
        $inputsUsers = array_map('SubscriberInfo::trimUsers', Input::except($fieldsSubscriber));
       // $inputsUsers = Input::except($fieldsSubscriber);
        //Validation
        Alert::keep();
        /*if($inputsSubscriber['subscription_type_id'] == 0){ //if 0 that working type
            Alert::error('The subscription type field is required.');
            return Redirect::back()->withInput();
        }*/
        // Alert::error("Test error");
        // return Redirect::back()->withInput();

        $validator = Validator::make($inputsSubscriber, SubscriberInfo::getRules());

        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        
        $subscrInfo = SubscriberInfo::with(["owner", "customers", "subscriptionType"])->find($inputsSubscriber["id"]);
        //duplicate info when change type
        if($subscrInfo->subscription_type_id != $inputsSubscriber["subscription_type_id"]){
            $subscrType = SubscriptionType::find($inputsSubscriber["subscription_type_id"]);
            $subscrInfo->subscription_name = $subscrType->name;
            $subscrInfo->persons = $subscrType->persons;
            $subscrInfo->price = $subscrType->price;
            $subscrInfo->subscription_type_id = $subscrType->id;//associate. Can't update when id = null, tyring to set 0
        }
        if(!$subscrInfo->update($inputsSubscriber)){
            Alert::error('Something wrong happened while saving Subscriber Info');
            return Redirect::back()->withInput();
        }

        $subscrInfo->owner->is_blocked = isset($inputsUsers["adminBlocked"]);
        $subscrInfo->owner->name = 
                 $subscrInfo->owner->initials =  $inputsUsers["nameAdmin"];
        //for validate unique email
        if($isChangedEmail = ($subscrInfo->owner->email != $inputsUsers["emailAdmin"])) {        
            $subscrInfo->owner->email = $inputsUsers["emailAdmin"];
        }
        
        $validator =  Validator::make($subscrInfo->owner->toArray(), User::getRules($isChangedEmail));
        if($validator->fails()){
            Alert::error($validator->messages()->all());
            return Redirect::back()->withInput();
        }
        $subscrInfo->owner->save();
        $newUsers = [];

        //delete users
        DB::transaction(function() use(&$subscrInfo, &$inputsUsers){
            if(isset($inputsUsers["delete_user_id"])){
                foreach ($inputsUsers["delete_user_id"] as $user_id) {
                    if($user = $subscrInfo->customers->find($user_id)){
                        $user->delete();
                        if(isset($inputsUsers["user_id"])){
                            $key = array_search($user_id, $inputsUsers["user_id"]);
                            unset($inputsUsers["user_id"][$key]);
                        }
                    }
                }
            }
        });

        //validate users
        $last_login = date("Y-m-d H:i:s");
        if(isset($inputsUsers["user_id"])){
             foreach ($inputsUsers["user_id"] as $i => $user_id) { 
                   //new User
                 if($user_id == 0){ 
                    if(empty($inputsUsers["tdEmailUser"][$i]) && empty($inputsUsers["tdNameUser"][$i]))
                        continue;
                    $user = new User;
                    $user->name = $user->initials = $inputsUsers["tdNameUser"][$i];
                    $user->email = $inputsUsers["tdEmailUser"][$i];
                    $user->is_blocked = isset($inputsUsers["is_blocked"][$i]);
                    $user->last_login = $last_login;
                    $validator =  Validator::make($user->toArray(), User::getRules());
                    if($validator->fails()){
                        Alert::error($validator->messages()->all());
                        return Redirect::back()->withInput();
                    }
                    $newUsers[] = $user;
                } //Edit User
                else if ($user = $subscrInfo->customers->find($user_id)){
                    $user->is_blocked = isset($inputsUsers["is_blocked"][$i]);
                    $user->name = $user->initials = $inputsUsers["tdNameUser"][$i];
                    if($isChangedEmail = ($user->email != $inputsUsers["tdEmailUser"][$i])) {        
                        $user->email = $inputsUsers["tdEmailUser"][$i];
                    }
    
                    $validator =  Validator::make($user->toArray(), User::getRules($isChangedEmail));
                    if($validator->fails()){
                         Alert::error($validator->messages()->all());
                         return Redirect::back()->withInput();
                    }
                }           
             }
             }
        // return $inputsUsers;
        //update Users
        //$owner = Role::where('name', '=', 'company_owner')->first();
        DB::transaction(function() use($subscrInfo, $newUsers){
            //$customerRole = Role::where('name', '=', 'customer')->first();
            //save changes (exist users)
            $subscrInfo->customers->each(function($item){$item->save();}); //customers

            //save new users
            $i = 1;
            foreach ($newUsers as $user) {
              $user->saveUser($subscrInfo, $i++);
            }
       });

        Alert::success('Subscriber information has been updated');
        return Redirect::route('subscribers.browse');     
        //return $inputsUsers;
    }


    public function getSubscriberRemind(){
        if($user = User::find(Input::get("id"))){
           // Clockwork::info($user->email);
           switch (Password::remind(["email" => $user->email])){
             case Password::INVALID_USER:
                $msg = ["alert" => "Invalid email address",
                        "success" => false ];
               break;
             case Password::REMINDER_SENT:
                 $msg = ["alert" => "Email has been sent",
                        "success" => true ];          
               break;
           }
        }
        else
            $msg = ["alert" => "Invalid user id",
                        "success" => false ];

       return Response::json($msg);
   } 

   public function getPayments(){
        $this->title = "Payments";
        $id = Input::get("id");
        if($id)
            $this->viewData["subscriber"] = SubscriberInfo::with("payments")->find($id);
         else
             return Redirect::route("subscriptions");
        return $this->render(__FUNCTION__);
   }
   
   public function postPayments(){
      $id = Input::get("id");
      if($id){
        $subscriber = SubscriberInfo::find($id);
        $subscriber->note = Input::get("note");
        $subscriber->save();
      }
      else
        return Redirect::route("subscriptions");
      
      return Redirect::route("subscribers.edit", ["id" => $id]);
   }
   
   public function getSubscriptionDelete(){
        $id = Input::get("id");
        $subscriber = SubscriberInfo::find($id);
       // $subscriber->subscription_type_id = null;
        if($subscriber->subscriptionType)
            $subscriber->subscriptionType()->dissociate();
        $subscriber->price = 0;
        $subscriber->subscription_name = "";
        $subscriber->save();
        Alert::keep();
        Alert::success("Subscription has been deleted.");
        return Redirect::back();//->withInput(); is_blocked losting
        //return Redirect::back();
   }

   public function getSettings(){
        $this->title = "Settings";
        $this->viewData["settings"] = Setting::all();
        return $this->render(__FUNCTION__);
   }

    public function getEditSettings(){
        $id = Input::get("key");
        $this->title = "Settings edit";
        //$this->viewData["item"] = Setting::where("key", "=", $key)->first();
        $this->viewData["item"] = Setting::find($id);
        return $this->render(__FUNCTION__);
    } 

    public function postEditSettings(){
        $inputs = Input::only(['description', 'value']);
        Alert::keep();
	$key = Input::get("key");
	$rules =  Setting::getRules();
	if($key == "support"){
		$rules["value"] .= "|email";
	}	
        $validator = Validator::make($inputs, $rules);

        if($validator->fails()){
                Alert::error($validator->messages()->all());
           return Redirect::back()->withInput();
        }
        //$item = Setting::where("key","=", Input::get("key"))->first();
        $item = Setting::find($key);
        if (!$item->update($inputs)) {
            Alert::error('Something wrong happened while saving settings.');
            return Redirect::back()->withInput();
        }
        Alert::success("Settings has been saved.");
        return Redirect::route("settings");
    }

    public function getTranslationsList(){
        $this->title = "Edit translations";
        $this->viewData["languages"] = Language::getEnabled();
        return $this->render(__FUNCTION__);
    }

    public function getTranslationEdit(){
        $locale = Input::get("locale");
        $this->viewData["sections"] = Translation::getSectionsLocale($locale);
        $currentLocale = [];
        if($locale != "null"){
            $currentLocale = Language::where("locale", "=", $locale)->lists("name", "locale");
            $this->title = "Edit translation";
        }
        else {
            $currentLocale["null"] =  "Select language";
            $this->title = "New translation";
        }
        $this->viewData["languages"] = array_merge($currentLocale,
            Language::where("enabled", "=", 0)->orderBy("name")->lists("name", "locale"));
        $this->viewData["locale"] = $locale;
        return $this->render(__FUNCTION__);
        //return $this->viewData["sections"];
    }

    public function postTranslationEdit(){
        Alert::keep();
        $locale = Input::get("locale");
        $localeSelect = Input::get("locale-select");
        if($localeSelect == "null"){
          Alert::error("You must chose language");
          return Redirect::back()->withInput();
        }

        $translations = Input::get("translation",[]);
        //validation
        foreach ($translations as $tGroup) {
          foreach ($tGroup as $text) {
            $validator = Validator::make(["name" => $text], Translation::getRules());
            if($validator->fails()){
                Alert::error($validator->messages()->all());
                return Redirect::back()->withInput();
            }
          }
        }
        
        if(!empty($localeSelect)){  //not disabled select lang
            Language::where("locale", "=", $localeSelect)->first()->chose();
        
            if($locale != "null"){// edit form
              if($locale != $localeSelect){
                $lang = Language::where("locale", "=", $locale)->first();
                $lang->chose(false); //choose = false;
               // Translation::changeTranslation($lang->locale, $localeSelect);
                Translation::createNewLocale($localeSelect);
              }
            }else{  //new form
              Translation::createNewLocale($localeSelect);
            }
        }
        else
            $localeSelect = Config::get("app.fallback_locale");

        foreach($translations as $groupName => $tGroup){
          Translation::updateRecords($localeSelect, $groupName, $tGroup);
        }

        Alert::success("Translation has been saved");
        return Redirect::route("settings.translations"); 
    }

    public function getTranslationDelete(){
        Alert::keep();
        $locale = Input::get("locale");
        $lang = Language::where("locale", "=", $locale)->first();
        if(!$lang){
          Alert::error("Invalid locale");
        }else{
          $lang->chose(false);
          Alert::error("Translation has been deleted.");
        }
        return Redirect::back();
    }



  public function getStatistics(){
    $this->title = "Statistics";
    return $this->render(__FUNCTION__);
  }
}

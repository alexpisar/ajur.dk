<?php

class LoginController extends BaseFrontController {

	public function getLogin(){
    //if(Auth::check()) return Redirect::route("database");
    $this->title = 'Login';
    return $this->render(__FUNCTION__);
	}
      
 	public function postLogin(){
     if (Auth::attempt(Input::only(['email','password']), true)){
         $user = Auth::user();
         $user->last_login = date("Y-m-d H:i:s");
         $user->save();
         return Redirect::route('dashboard');//('dashboard');
     }
      Alert::keep();
      Alert::error("Invalid email/password pair");
     return Redirect::route('login')->withInput(Input::except('password'));
	}
        
  public function getLogout(){
      Auth::logout();
      if(Input::get("redirect")) return Redirect::to(Input::get("redirect"));
      else                       return Redirect::to("/");
	}
        
        
  public function getRemind(){
      $this->title = "Remind Password";
      return $this->render(__FUNCTION__);
  }
  
  public function postRemind(){
      Alert::keep();
   switch (Password::remind(Input::only('email'))){
     case Password::INVALID_USER:
       Alert::error("Invalid email address");
       return Redirect::back()->withInput();

     case Password::REMINDER_SENT:
       Alert::success("Email has been sent");
       return Redirect::back();
       /*default :
           Alert::error("Error");
           return Redirect::back();*/
   }

 }

    public function getReset($token){
      //  if (is_null($token)) App::abort(404);
        $this->title = t("reminders.restore_password");
      //  $this->viewData["token"] = $token;
        return $this->render(__FUNCTION__);
    }

    public function postReset($token){
        $credentials = Input::only(
            'email', 'password', 'password_confirmation', 'token'
        );
        $credentials["token"] = $token;
        $response = Password::reset($credentials, function($user, $password){
            $user->password = Hash::make($password);
            $user->save();
        });

        if($response == Password::PASSWORD_RESET) return Redirect::to('/');

        Alert::keep();
        switch ($response){
            case Password::INVALID_PASSWORD:
                Alert::error("Invalid password");
            case Password::INVALID_TOKEN:
                Alert::error("Invalid token");
            case Password::INVALID_USER:
                Alert::error("Invalid email");
        }
        return Redirect::back()->withInput(Input::except(['password', 'password_confirmation']));
    }
		
//Registration	
    public function getTeaser(){
		$this->title = t("registration.teaser_page");
		//$this->setCaptcha();
		return $this->render(__FUNCTION__);
     }

	public function getSubscriptions(){
		$this->title = t("registration.choose_subscription");//"Choose subscription";
		//$this->setCaptcha();
    $this->viewData["subscription_type_id"] = Input::get("subscription_type_id", 0);
		$this->viewData["pricing"] = SubscriptionType::where("institutional_access", "=", "0")->get();
		return $this->render(__FUNCTION__);
	}
  
  public function getYourInfo(){
    $id = Input::get("subscription_type_id");
    if(!$id){
      Alert::keep();
      Alert::error("Please, choose subscription.");
      return Redirect::route("choose.subscriptions");
    }
    $this->title = t("registration.your_information");
    $this->viewData["msgAccept"] = str_replace(":linkText","<a href='#'>" 
            . t('registration.link_text_conditions') . "</a>", 
            t('registration.accepted_terms_conditions'));
    //$this->setCaptcha();
    $data = Session::get("registrationData", []);
    $data["subscription_type_id"] = $id; //update subscr_type_id
    Session::put("registrationData", $data);
    $this->formData = $data;
    $this->keyOldData = "registrationData";
    $this->viewData["subscription"] = SubscriptionType::find($id);
    return $this->render(__FUNCTION__);
  }
  
  public function postYourInfo(){
    $exceptFields = ["nameAdmin", "emailAdmin", "password", "password_confirmation"];
    $inputsUser = array_map('trim', Input::only($exceptFields));
   // $exceptFields[] = "accepted_terms_conditions";
    $inputsSubscriber = array_map('trim', Input::except($exceptFields));
    $inputsSubscriber["name"] = $inputsSubscriber["company_name"];
    //Validation
    Alert::keep();
    if(!Input::get("accepted_terms_conditions")){
        Alert::error(t('registration.please_accepted_conditions'));
        return Redirect::back()->withInput();
    }
    if(empty($inputsSubscriber['subscription_type_id'])){
        Alert::error(t("registration.subscription_type_required"));
        return Redirect::back()->withInput();
    }
    // Alert::error("Test error");
    //return $inputsSubscriber;
    $validator = Validator::make($inputsSubscriber, SubscriberInfo::getFrontRules());
    if($validator->fails()){
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
    }
    $validator =  Validator::make($inputsUser, User::getOwnerRules());
    if($validator->fails()){
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
    }  
    
    /*
        

    Alert::success(t('registration.subscriber_created'));
     * */
    Session::put("registrationData", array_merge($inputsSubscriber, $inputsUser));
    return Redirect::route('choose.payment');//, 
      //["subscription_type_id" => $inputsSubscriber['subscription_type_id']]);
  }
  
  public function getPaymentMethod(){
    //$id = Input::get("subscription_type_id");
    $data = Session::get("registrationData", []);
    $id = null;
    if(isset($data["subscription_type_id"]))
      $id = $data["subscription_type_id"];
    else
      $id = Input::get("subscription_type_id", null);
    
    if(!$id){
      if(Auth::check())
        return Redirect::route("profile.subscriptions.change");
      else
        return Redirect::route("choose.subscriptions");
    }elseif(Auth::check()){
      Session::put("registrationData", ["subscription_type_id" => $id]);
    }
    
    $this->title = t('registration.choose_payment');
    $this->viewData["subscription"] = SubscriptionType::find($id);
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  public function getCreditCardInfo(){
    $payment_method = Input::get("payment_method");
    if(!$payment_method)
      return Redirect::route("choose.payment");
      //Rdirect::back();
    $data = Session::get("registrationData", []);
    $subscrTypeID = null;
    if(isset($data["subscription_type_id"]))
      $subscrTypeID = $data["subscription_type_id"];
    if(!$subscrTypeID){
      if(Auth::check())
        return Redirect::route("profile.subscriptions.change");
      else
        return Redirect::route("choose.subscriptions");
    }
    Session::put("payment", ["payment_method" => $payment_method]);
    //if == Payment::METHOD_CREDITCARD
    //Redirect
    $this->title = t("registration.credit_card_info");
    $this->viewData["subscription"] = SubscriptionType::find($subscrTypeID);
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  public function postCreditCardInfo(){
      //Redirect::route("login.yourinfo");
    $inputs = Session::get("registrationData");
    $paymentMethod = Session::get("payment");
    if($paymentMethod)
      $paymentMethod = $paymentMethod["payment_method"];
    else
      $paymentMethod = Payment::METHOD_CREDITCARD;
    
    if(empty($inputs)){
       if(Auth::check())
        return Redirect::route("profile.subscriptions.change");
      else     
       return Redirect::route("teaser");
    }
    //Redirect::to("/");
    $subscrInfo = null;
    $subscrType = SubscriptionType::find($inputs['subscription_type_id']);
    if(!Auth::check()){
        $last_login = date("Y-m-d H:i:s");
        $admin = new User;
        $admin->name = $admin->initials = $inputs["nameAdmin"];
        $admin->email = $inputs["emailAdmin"];
        $admin->last_login = $last_login;
        $admin->password = Hash::make($inputs["password"]);
        Mail::send("emails.createUser", [
            "login" => $admin->email,
            "password" => $inputs["password"]
        ],function(\Illuminate\Mail\Message $message) use ($admin){
            $message->to($admin->email);
        });
        //Save to db
        $subscrInfo = new SubscriberInfo ($inputs);
        
        //duplicate inform
        $subscrInfo->subscription_name = $subscrType->name;
        $subscrInfo->persons = $subscrType->persons;
        $subscrInfo->price = $subscrType->price;
        //the missing fields
        $subscrInfo->is_paid = 1;
        $subscrInfo->licens_number = "";
        //$subscrInfo->to_recive_letters = $inputsSubscriber["to_recive_letters"];
        //save
        $subscrType->subscriberInfos()->save($subscrInfo);
        $ownerRole = Role::where('name', '=', 'company_owner')->first();
        $ownerRole->users()->save($admin);
        $subscrInfo->owner()->associate($admin);
        $subscrInfo->save();
        $subscrInfo->licens_number = str_pad($subscrInfo->id, 9, '0', STR_PAD_LEFT);
        $subscrInfo->save();
    }else{
      if(!$subscrInfo = Auth::user()->subscriber_info){
        return Redirect::route("profile.subscriptions.change");
      }
      $subscrInfo->subscription_name = $subscrType->name;
      //cutting persons
      if($subscrInfo->customers->count() + 1 > $subscrType->persons)
        $subscrInfo->customers->take($subscrInfo->customers->count() + 1 - $subscrType->persons)
                         ->each(function($c) {$c->delete();});
                         
      $subscrInfo->persons = $subscrType->persons;
      $subscrInfo->price = $subscrType->price;
      $subscrInfo->subscription_type_id = $subscrType->id;
      $subscrInfo->save();     
    }
    
    $dataPayment =  Input::all();
    //save payment
    $payment = new Payment ([
        "invoice_number"    => str_pad($subscrInfo->id, 8, '0', STR_PAD_LEFT),
        "billing_date"      => date("Y-m-d"),
        "payment_due"       => date("Y-m-d"),
        "amount_vat"        => $subscrInfo->price,
        "total_amount_due"  => $subscrInfo->price,
        "payment_method"    => $paymentMethod,
        "payment_status"    => Payment::STATUS_PAID,
        "card_number"       => $dataPayment["card_number"]
    ]);
    $subscrInfo->payments()->save($payment);
    $dataPayment["subscriber_info_id"] = $subscrInfo->id;
    Session::put("payment", $dataPayment);
    //Session::forget("payment");
    Session::forget("registrationData");
    return Redirect::route("receipt");
    //      Redirect::route("login.yourinfo"); //doesn't work
    //      Redirect::to("/subscriptions");
  }
  
  public function  getReceipt(){
    $this->title = t("registration.receipt");
    $data = Session::pull("payment");
    if(!$data)
      return Redirect::route("teaser");
    $subscriber = SubscriberInfo::with(['owner', 'payments'])//->find(16);
            ->find($data["subscriber_info_id"]);  
    $this->viewData["subscriber"] = $subscriber;
    if($subscriber->payments->count())
      $payment = $subscriber->payments[0];
    else
      $payment = new Payment; //empty payment
    
   // $this->viewData["dataPayment"] = $data;
    $this->viewData["payment"] = $payment;
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }

  public function postUserLogin(){
    //$remeber = false;
    $user = null;
    $success = false;
     if (Auth::attempt(Input::only(['email','password']), Input::get("remember_me", false))){
         $user = Auth::user();
        if(Entrust::hasRole("company_owner") || Entrust::hasRole("customer")){
          if($user->subscriber_info && $user->subscriber_info->is_paid){
            $success = true;
          }
        } else{ //role admin || employee
          $success = true;
        }
     }
     if($success){
        $user->last_login = date("Y-m-d H:i:s");
        $user->save();
        return Redirect::route('index');
     }
     Auth::logout();
     // Alert::keep();
     // Alert::error("Invalid email/password pair");
     return Redirect::route('access.denied')->withInput(Input::except('password'));
	}
  
  public function getAccessDenied(){
    $this->title = "Access denied";
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  public function getLostPassword(){
    $this->title = t("reminders.lost_password");
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);    
  }
 
  public function postLostPassword(){
    $email = Input::only('email');
    $user = User::with(["subscriberInfoOwner", "subscriberInfoCustomer"])
            ->where("email", "=", $email["email"])->first();
    $result = "";
    if(!$user){
      $result = Password::INVALID_USER;
    } elseif($user->subscriber_info && $user->subscriber_info->is_paid){
      Config::set('auth.reminder.email', 'emails.auth.reminderFront');
       $result = Password::remind($email);
    } else{ //not paid
      return Redirect::route('access.denied');
    }

    switch ($result){
     case Password::INVALID_USER:
       Alert::keep();
       Alert::error(t("reminders.invalid_email"));
       return Redirect::back()->withInput();

     case Password::REMINDER_SENT:
       //Alert::success("Email has been sent");
       return Redirect::route("password.thanks");
   }
  }  
  
  public function getThanks(){
    $this->title = t("reminders.thank_you");
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  public function getResetUser($token){
    $this->title =  t("reminders.lost_password");
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }

    public function postResetUser($token){
        $credentials = Input::only(
            'email', 'password', 'password_confirmation'
        );
        $credentials["token"] = $token;
        $response = Password::reset($credentials, function($user, $password){
            $user->password = Hash::make($password);
            $user->save();
        });

        if($response == Password::PASSWORD_RESET) return Redirect::to('/');

        Alert::keep();
        switch ($response){
          case Password::INVALID_PASSWORD:
              Alert::error(t("reminders.invalid_password"));
          case Password::INVALID_TOKEN:
              Alert::error(t("reminders.invalid_token"));
          case Password::INVALID_USER:
              Alert::error(t("reminders.invalid_email"));
        }
        return Redirect::back()->withInput(Input::except(['password', 'password_confirmation']));
    }
}


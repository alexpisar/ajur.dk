<?php

abstract class BaseFrontPageController extends BaseFrontController {

  public function __construct(){
    parent::__construct();

//    $this->setCaptcha(); //set in BaseFrontController
    $this->getFeedNews();
  }

  protected function getFeedNews(){
    $this->viewData["feedNews"] = Article::with(["contentType", "tab"])
      ->where("show_in_news_feed", "=", "1")
      ->where("published", "=", "1")
      ->orderBy("date_publication", "desc")
      ->take(6)
      ->get();
  }

}

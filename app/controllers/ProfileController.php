<?php

class ProfileController extends BaseFrontController{
  
  public function getCustomer(){
    $this->title = t("profile.colleague_profile");
    //$this->setCaptcha();
    $this->viewData["user"] = User::with(["subscriberInfoOwner", "subscriberInfoCustomer"])->find(Auth::id());
    return $this->render(__FUNCTION__);
  }
  
  public function getPayingOwner(){
    $this->title = t("profile.paying_profile");
    //$this->setCaptcha();
    $this->viewData["user"] = User::with(["subscriberInfoOwner"])->find(Auth::id());
    return $this->render(__FUNCTION__);    
  }
  
  public function postPayingOwner(){
    $id = Input::get("id");
     if(!$id)
      return Redirect::route("index");
    $exceptFields = ["nameAdmin", "emailAdmin"];
    $inputsUser = array_map('trim', Input::only($exceptFields));
    $inputsSubscriber = array_map('trim', Input::except($exceptFields));
    $subscrInfo = SubscriberInfo::with(["owner"])->find($id);
    
    $validator = Validator::make($inputsSubscriber, SubscriberInfo::getFrontRules());
    Alert::keep();
    if($validator->fails()){
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
    }
    $validator =  Validator::make($inputsUser, User::getOwnerRules(false, 
            $subscrInfo->owner->email != $inputsUser["emailAdmin"]));
    if($validator->fails()){
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
    }  
    
    //Save to db
    $subscrInfo->owner->name = $inputsUser["nameAdmin"];
    $subscrInfo->owner->email = $inputsUser["emailAdmin"];
    $subscrInfo->owner->save();
    //save
    if(!$subscrInfo->update($inputsSubscriber)){
        Alert::error(t("profile.error_save"));
        return Redirect::back()->withInput();
    }
    Alert::success(t("profile.success_save"));
    return Redirect::route("profile.owner");
  }
  
  public function getSubscriptions(){
    $user = User::with(["subscriberInfoOwner", 
        "subscriberInfoOwner.subscriptionType"]) //"subscriberInfoOwner.payments"
            ->find(Auth::id());
    if(!$user){
      return Redirect::route("profile");
    }
    $this->title = t("profile.my_subscriptions");
    $this->viewData["user"] = $user;
    $this->viewData["lastPayment"] = Payment::where("subscriber_info_id", "=", 
            $user->subscriber_info->id)->orderBy("billing_date", "desc")->take(1)->first();
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  public function getPayments(){
    $user = User::with(["subscriberInfoOwner"]) //"subscriberInfoOwner.payments"
            ->find(Auth::id());
    if(!$user){
      return Redirect::route("profile");
    }
    $this->title = t("profile.my_payments");
    $this->viewData["payments"] = Payment::where("subscriber_info_id", "=", 
            $user->subscriber_info->id)->orderBy("billing_date", "desc")->get();
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  public function getChangePassword(){
    $this->title = t("profile.change_password");
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
  
  
  public function postChangePassword(){
    $inputs = Input::all();
    $validator = Validator::make($inputs, User::changePasswordRules());
    Alert::keep();
    if($validator->fails()){
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
    }
    //validate old password
    if(!Auth::validate(["email"=>Auth::user()->email, 
            "password" => $inputs["old_password"]])){
        Alert::error(t("profile.invalid_old_password"));
        return Redirect::back()->withInput();   
    }
    $user = Auth::user();
    $user->password = Hash::make($inputs["password"]);
    $user->save();
    Alert::success(t("profile.change_password_success"));
    return Redirect::route("profile");
  }
  
  public function getAddUsers(){
    $this->title = t("profile.add_del_users");
    //$this->setCaptcha();
    $id = Auth::user()->subscriber_info->id;
    $this->viewData["subscriber"] = SubscriberInfo::
            with(["owner", "customers", "subscriptionType"])->find($id);
    $this->viewData["lastPayment"] = Payment::where("subscriber_info_id", "=", $id)
            ->orderBy("billing_date", "desc")->take(1)->first();    
    return $this->render(__FUNCTION__);
  }
  
  public function getDeleteUser($id){
    $user = User::find($id);
    if($user)
      $user->delete();
    Alert::keep();
    Alert::success(t("profile.customer_deleted"));
    return Redirect::route("profile.add_users");
  }
  
  public function postAddUser(){
    $user = new User(array_map('trim', Input::all()));
    $user->initials = $user->name;
     $validator =  Validator::make($user->toArray(), User::getRules());
      if($validator->fails()){
        Alert::keep();
        Alert::error($validator->messages()->all());
        return Redirect::back()->withInput();
      }
      $user->last_login = date("Y-m-d H:i:s");
      $owner = Auth::user();
      if($owner->subscriber_info)
        $user->saveUser($owner->subscriber_info);
      return Redirect::route("profile.add_users");
  }
  
  public function getChangeSubscrip(){
    $this->title = t("profile.change_subscription");
    $user = User::with(["subscriberInfoOwner", 
        "subscriberInfoOwner.subscriptionType"]) //"subscriberInfoOwner.payments"
            ->find(Auth::id());
    if(!$user){
      return Redirect::route("profile");
    }
    $this->viewData["user"] = $user;
    $this->viewData["lastPayment"] = Payment::where("subscriber_info_id", "=", 
            $user->subscriber_info->id)->orderBy("billing_date", "desc")->take(1)->first();
    $this->viewData["pricing"] = SubscriptionType::where("institutional_access", "=", "0")->get();
    //$this->setCaptcha();
    return $this->render(__FUNCTION__);
  }
}


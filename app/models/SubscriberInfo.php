<?php

class SubscriberInfo extends BaseModel {

	//protected $table = 'subsriber_infos';
        public $timestamps = true;
    //protected $guarded = ["id", "subsription_type_id", "created_at","updated_at"];// update() doesn't work
    protected $fillable = ["name", 'vat_number', 'street_name', 
                    'post_number', 'city', 'country', 'note', 'phone', 
                    'note_about_admin', 'occupation', 'optional', 'post_box',
                    'house_nr', 'letter', 'floor', 'side', 'to_recive_letters'];

	public static function getRules(){
        return array(
            'name'     	    => 'required|max:255',
            "vat_number"    => 'required|max:255', 
            //"licens_number" => 'required|max:255', //
            "street_name"   => 'required|max:255', 
            "post_number"   => 'required|max:255', 
            "city"          => 'required|max:255', 
            "country"       => 'required|max:255', //
            "phone"         => 'required|max:255', 
            "note_about_admin" => 'max:255', 
            "occupation"    => 'required|max:255' //
        );
    }
    
  	public static function getFrontRules(){
        return array(
            'name'     	    => 'required|max:255',
            "vat_number"    => 'required|max:255', 
            "street_name"   => 'required|max:255', 
            "post_box"      => 'max:255', 
            "house_nr"      => 'required|max:255', 
            "letter"        => 'max:255', 
            "floor"         => 'max:255', 
            "side"          => 'max:255', 
            "post_number"   => 'required|max:255', 
            "city"          => 'required|max:255', 
            "phone"         => 'required|max:255',
            "occupation"    => 'required|max:255',
            "country"       => 'required|max:255',
        );
    }  
//    nameAdmin
//    emailAdmin
//    password
//    password_confirmation
    
    public function owner(){
        return $this->belongsTo("User", "user_id");
    }

    public function customers(){
        return $this->belongsToMany("User", "subscriber_customer", "subscriber_info_id", "user_id");
    }

    public function payments(){
        return $this->hasMany("Payment");
    }

    public static function getFieldsInput(){
            return ["id", "name", "vat_number", "licens_number", "subscription_type_id",
                            "street_name", "post_number", "city", "country", "note", 
                            "phone", "note_about_admin", "occupation", "optional"];
    }
    
     public function getPersonsAttribute(){
        if($this->subscriptionType){
          return $this->subscriptionType->persons;
        } else {
          return $this->attributes['persons'];
        }
    }

    /*public function getPriceAttribute(){
        if($this->subscriptionType){
          return $this->subscriptionType->price;
        } else {
          return $this->attributes['price'];
        }
    }*/
     /*public function getSubcriptionTypeIdAttribute(){
        if($this->subscription_type_id){
          return 0;
        } else {
          return $this->subscription_type_id;
        }
    } */  
    
    
    public function getSubscriptionNameAttribute(){
        if($this->subscriptionType){
          return $this->subscriptionType->name;
        } else {
         return $this->attributes["subscription_name"];
        }
    }

    public function setSubscriptionNameAttribute($value){
        $this->attributes['subscription_name'] = $value;
    }

    public function subscriptionType(){
        return $this->belongsTo("SubscriptionType");
    }

    public function delete(){
        //with delete users
        $this->owner->delete();
        DB::transaction(function(){$this->customers->each(function($item){$item->delete();});});
        parent::delete();
    }
    
    public function getMaxPersons($subscTypes){
        $max = $subscTypes->max('persons');
        if($this->persons > $max) //becouse current may be bigger, if type == null (did delete)
            return $this->persons;
        return $max;
    }

    public static function trimUsers($item){
        if(is_array($item))
            return $item;
        else
            return trim($item);
    }

    public static function toDataTable($items){
        $out = [];
            foreach ($items as $item) {
                $row = [];
                $row[] = $item->name;
                $row[] = $item->created_at->format("d.m.Y");
                $row[] = $item->id;
                $row[] = $item->id;
                $out[] = $row;
            }
        return $out;
    }

    public static function cuttingPersons($persons, $subscrType){
        //DB::transaction(function() use($persons, $subscrType){
            $items = $subscrType->subscriberInfos;
            foreach ($items as $item) {
                if($item->customers->count() + 1 > $persons){
                    $item->customers->sortByDesc("id")->take($item->customers->count() + 1 - $persons)
                         ->each(function($c) {$c->delete();});
                }
                $item->persons = $persons;
                $item->save();
            }
      //  });
    }
}

<?php

/**
 * Topic
 *
 * @property-read \get_class($this $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\get_class($this[] $children
 */
class Topic extends \Gzero\EloquentTree\Model\Tree {

	//protected $table = 'topics';
	public $timestamps = true;
	protected $fillable = ['text'];
	protected $visible = array('id','text', 'children', 'type', 'weight');

	public function tabs(){
		return $this->hasMany("Tab");
	}

	public static function createTopic(array $newTopic){
		$parent_id = (integer) $newTopic["parent"];
		$parent = self::find($parent_id);
		with(self::create(["text" => $newTopic["text"]]))
			->setChildOf($parent);
	}

	public static function renameTopic(array $newTopic){
		$topic = self::find($newTopic["id"]);
		$topic->text = $newTopic["text"];
		$topic->save();
	}

	public static function move(array $node){
		$newParent = Topic::find((integer) $node["parent"]);
		Topic::find((integer) $node["id"])->setChildOf($newParent);
	}

	public static function deleteTopic($id){
		DB::transaction(function() use($id){
			$topic = self::findOrFail($id);
			//delete file icon on descedants
			$descedants = $topic->findDescendants()->get();
			foreach($descedants as $descedant){
				$descedant->tabs()->get()->each(function($item){$item->delete();});
//				foreach($tabs as $tab)
//					$tab->delete();
			}
			$topic->delete();
		});
	}

	public static function copyOnTree(array $node){
        $topicID  = $node["original_id"]; //who
        $parentTopicID = $node["parent"]; //parent to here
        DB::transaction(function() use($topicID, $parentTopicID) {
         	$parentTopic = self::find($parentTopicID);
        	self::find($topicID)->copyToTopic($parentTopic);
        });
    }

    public function copyToTopic($topic){
        $duplicate = $this->replicate()->setChildOf($topic);
        //copy children
        $this->load("children", "tabs");
        foreach ($this->children as $child) {
            $child->copyToTopic($duplicate); // here is recursion
        }
        foreach ($this->tabs as $tab) {
        	$tab->copyToTopic($duplicate);
        }

    }
    //set link if has children
/*    public function getUrl(){
      if($this->children->count() > 0 || $this->tabs->count() > 0)
        return route("db.topic", ["id"=>$this->id]);
      else
        return "#";
    }
 */
}

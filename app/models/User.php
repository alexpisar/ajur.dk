<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use Zizaco\Entrust\HasRole;

/**
 * User
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust::role[] $roles
 */
class User extends BaseModel implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait, HasRole;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	//protected $hidden = array('password', 'remember_token');
	protected $visible = array('name', 'email', 'initials');

	protected $fillable = array('name', 'initials', 'email', 'password', 'last_login', 'phone', 'occupation', 'optional');

	public static function getRules($isEmail = true)
	{
		$rules = array(
               'name'     	=> 'required|between:2,255', //|unique:users,name', //alpha_spaces|
               'initials' 	=> 'required|between:2,255', //alpha_spaces|
               //'email'    	=> 'required|email|max:255|unique:users,email',
               //'role'		=> 'required|numeric|min:1',
               'last_login' => 'date_format:d-m-Y H:i:s'
			);
		//'last_login' => 'date_format:d-m-Y H:i:s'
		if($isEmail)
			$rules["email"] = 'required|email|max:255|unique:users,email';
		return $rules;
	}
  
	public static function getOwnerRules($isPassword = true, $isEmail = true)
	{
		$rules =  array(
               'nameAdmin'     	=> 'required|between:2,255', 
               //'emailAdmin'     => 'required|email|max:255|unique:users,email',
           //  'password'       => 'required|confirmed',
			);
    if($isPassword)
      $rules["password"] = 'required|confirmed';
    if($isEmail)
      $rules["emailAdmin"] = 'required|email|max:255|unique:users,email';//$this->id,id
		return $rules;
	}
  public static function changePasswordRules(){
    return array(
        "old_password"  => "required",
        "password"  => "required|confirmed",
    );
  }
  
	public function customers(){
        return $this->belongsToMany(get_class($this), 'subscriber_customer', 'parent_user_id', 'user_id');
  }
  // id -> user_id - is this owner
  public function subscriberInfoOwner(){
      return $this->hasOne("SubscriberInfo");
  }
  
  // subscriber_info_id -> id - is this customer
  public function subscriberInfoCustomer(){
    return $this->belongsTo("SubscriberInfo", "subscriber_info_id");
  }
  
  public function getSubscriberInfoAttribute(){
    if($this->subscriberInfoOwner){
      return $this->subscriberInfoOwner;
    } else {
     return $this->subscriberInfoCustomer;
    }
  }
  
  public function saveUser($subscriber, $i = 1, $isOwner = false){
    $password = substr(md5(time() + $i), 0, 4);
    $this->password = Hash::make($password);
    $mail = $this->email;
    Mail::send("emails.createUser", [
         "login" => $this->email,
         "password" => $password
     ], function(\Illuminate\Mail\Message $message) use ($mail){
         $message->to($mail);
     });
     if($isOwner){
        $ownerRole = Role::where('name', '=', 'company_owner')->first();
        $ownerRole->users()->save($this);
        $subscriber->owner()->associate($this);
        $subscriber->save();
     } else{ 
        $customerRole = Role::where('name', '=', 'customer')->first();
        $customerRole->users()->save($this);
        $this->subscriberInfoCustomer()->associate($subscriber);
        $subscriber->customers()->save($this);
        $subscriber->owner->customers()->attach($this->id);
     }
  }
}

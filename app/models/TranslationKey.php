<?php

class TranslationKey extends BaseModel{
  
  protected $fillable = array('key', 'group');
  public $timestamps = false;

  public function translations(){
    return $this->hasMany("Translation");
  }

}
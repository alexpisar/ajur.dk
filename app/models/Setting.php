<?php

class Setting extends BaseModel {

	//protected $table = 'settings';
	protected $primaryKey = "key";
	public $incrementing = false;

	public $timestamps = true; 

	protected $fillable = ['description', 'value'];

    public static function getRules()
    {
        return array(
           // 'key'     		=> 'required|max:255|unique:settings,key',
            'description'     	=> 'required|max:512',
            'value'     		=> 'required|max:1024'
        );
    }
}

<?php

class StaticPage extends BaseModel{

  protected $primaryKey = "alias";
  public $incrementing = false;
  
  protected $guarded = ["created_at", "updated_at"];

//  public static $rules = [
//    "alias"   => "required|unique:static_pages,alias",
//    "weight"  => "numeric",
//  ];
  public function getRules($isEdit = false){
    if((bool)$this->is_system)
      return [];
    $rules = [
       "weight"  => "numeric",
       "alias"  => "required|unique:static_pages,alias,$this->alias,alias"
    ];
//    if($isEdit)
//      $rules["alias"] = "required|unique:static_pages,alias";
    return $rules;
  }

  public $locale = null;


  public function translations(){
    return $this->hasMany("StaticPageTranslation", "static_page_alias");
  }
  

  public function trans(){
    $locale = $this->locale? $this->locale: Config::get("app.fallback_locale");//App::getLocale();
    return $this->hasOne("StaticPageTranslation", "static_page_alias")->whereLocale($locale);
     //->select(["title"]);
  }

  public function translation($locale = null){
    if(!$locale)
      $locale = Config::get("app.fallback_locale");
    if(!$this->translations->isEmpty())
      return $this->translations->filter(function($item) use($locale) {
          return $item->locale === $locale;
      })->first();
      
    return null;
  }

  /**
   * Return translation model for current locale and static page
   * or null
   *
   * @return StaticPageTranslation|null
  */
//  public function translation(){
//    $locale = $this->locale? $this->locale: Config::get("app.fallback_locale");//App::getLocale();
//
//    return $this->throughCache("translation-$locale", function() use ($locale){
//      if($this->trans){
//        return $this->trans;
//      }else{
//        return $this->translations()->whereLocale(Config::get("app.fallback_locale"))->first();
//      }
//    });
//  }


  public function scopeEnabled($query){
    return $query->where(function($q){
      $q->orWhere("show", "=",1)
        ->orWhere("is_system","=",1);
    });
  }


  public function scopeNotSystem($query){
    return $query->where("is_system", "=",0);
  }


//  public static function getByAlias($alias, $cols = ["*"]){
//    return StaticPage::enabled()->where("alias","=",$alias)->first($cols);
//  }


  public function getTitleAttribute(){
    return $this->translation()? $this->translation()->title : null;
  }


  public function setTitleAttribute($data){
    $this->translation()->title = $data;
  }


  public function getContentAttribute(){
    return $this->translation()? $this->translation()->content: null;
  }

  public function setContentAttribute($data){
    $this->translation()->content = $data;
  }
 
  public static function createLocale($locale){
    DB::transaction(function() use ($locale){
        $pages = self::all();
        foreach($pages as $page){
          $t = $page->translation()->replicate();
          $t->locale = $locale;
          $page->translations()->save($t);
        }
    });
  }
  
  
  
//  public function getShowAttribute($value){
//      if(!$this->is_system)
//        return $value ? "yes" : "no";
//      else
//        return "n/a";
//  }


//  public function hasLocale($locale){
//    return $this->translations()->whereLocale($locale)->exists();
//  }


  /**
   * Save the model to the database.
   *
   * @param  array  $options
   * @return bool
   */
  /*
  public function save(array $options = []){
    if(parent::save($options) && $this->translation()){
      $this->translation()->save();
    }
  }
   * */
  
  //Save translation with copy as default
  public function saveOnNew($translation){
    DB::transaction(function() use ($translation){    
        $locals = Language::getEnabled();
       // $p = StaticPage::find($this->alias);//can't save relations
        foreach($locals as $local){
          $t = $translation->replicate();
          $t->locale = $local->locale;
          //$t->static_page_alias = $this->alias;
          $this->translations()->save($t);
          //$t->save(); foreign is 0
        }
    });
  }
}

<?php

class Tag extends BaseModel {
    //public $timestamps = true;
    protected $fillable = ['name'];
    
    public function articles()
    {
        return $this->belongsToMany('Article');
    }
}
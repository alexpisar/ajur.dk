<?php

use Zizaco\Entrust\EntrustRole;

/**
 * Role
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('auth.model[] $users
 * @property-read \Illuminate\Database\Eloquent\Collection|\Config::get('entrust::permission[] $perms
 * @property mixed $permissions
 */
class Role extends EntrustRole
{

    public static function getSelectArray(array $roles){
        $list = [];
        foreach($roles as $id => $role){
            $list[$role] = $id;
        }
        return $list;
    }

   /* public function toSelectObject(){

    }*/

}

<?php

class StaticPageTranslation extends BaseModel {

  protected $fillable = ['title', 'content', 'locale'];
  
    public static $rules = [
    "title"   => "required|max:255",
    "content" => "required",
  ];
  
//  public function createLocale($locale){
//    $this->language = $locale;
//    if(!$this->translations()->whereLocale($locale)->exists()){
//      $this->translations()->create(["locale" => $locale]);
//    }
//  }
} 

<?php

class ContentType extends BaseModel {

   // protected $table = 'content_types';
    public $timestamps = true;
    protected $fillable = ['name', 'icon_name'];

    public static function getPathIcon($isAbsolute = false){
        if($isAbsolute)
            return public_path()."/uploads/contentTypes";
        else
            return "/uploads/contentTypes";
    }

    public static function getFilePathIcon($fileName, $isAbsolute = false){
        $fileName = trim($fileName);
        if(!empty($fileName))
            return self::getPathIcon($isAbsolute) . '/' . $fileName;
        else
            return null;
    }

    public function deleteFileIcon(){
        $file = self::getFilePathIcon($this->icon_name, true);
        if(file_exists($file) && !is_dir($file)){
            unlink($file);
        }
        return $this;
    }

    public function delete(){
        $this->deleteFileIcon();
        parent::delete();
        return $this;
    }
    
    public function getIconView(){
      if($this->icon_name){
        return self::getFilePathIcon($this->icon_name);
      }
      else
        return null;
    }
    
    public function articles(){
        return $this->hasMany('Article');
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10.04.15
 * Time: 17:10
 */

abstract class BaseModel extends Eloquent{

  protected static function boot(){
    parent::boot();

    //lets try to register our observer
    $className = get_called_class();
    $obClass = $className."Observer";
    if(class_exists($obClass)){
      $className::observe(new $obClass );
    }
  }

}
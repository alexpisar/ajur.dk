<?php
class Article extends BaseModel {

    public $timestamps = true;

    protected $fillable = ['title', 'summary_short', 'summary_full', 'date_publication',
                         'print_button', 'send_as_mail_button', 'borrow_button',
                        'borrow_link', 'buy_button', 'buy_link', 'reviews_button',
                        'reviews_link', 'table_contents_button',
                        'publish_on_page_news', 'online_edition', 'view_cover_button',
                        'view_case_num_button', 'case_num', 'save_button',
                        'content_link_button', 'content_link', 'show_in_news_feed', 'published',
                        'locale', 'key_words', 'isbn', 'ecli'
        ];
    //if guarded - sending all field of request to SQL when do update(),  and make error
//    protected $guarded = ['id', 'created_at', 'updated_at', 'content_type_id',
//                        'year', 'view_cover_file', 'table_contents_file', 'duplicate_id', 'tab_id'];
    //protected $visible = array('id', 'text', 'type');

    public static function getRules(){
        return array(
            'title'     	    => 'required|between:2,255',
            'summary_short'	    => 'required|max:500',
            'summary_full'	    => 'required',
            'date_publication'  =>  'date',//'date_format:d-m-Y',
            'borrow_link'	    => 'max:500',
            'buy_link'	        => 'max:500',
            'reviews_link'	    => 'max:500',
            'content_link'	    => 'max:500'
        );
    }

    public function contentType()
    {
        return $this->belongsTo('ContentType');
    }

    public function tab()
    {
        return $this->belongsTo('Tab');
    }

    public function publishers()
    {
        return $this->belongsToMany('Publisher');
    }

    public function tags()
    {
        return $this->belongsToMany('Tag');
    }

    public function authors()
    {
        return $this->belongsToMany('Author');
    }

    /*public function duplicates(){
        return $this->hasMany(get_class($this), 'duplicate_id');
    }*/

    public function original(){
        return $this->belongsTo(get_class($this), 'duplicate_id');
    }

    public function user(){
        return $this->belongsTo('User');
    }

    public function toTreeArray(){
        /*if(is_null($this->duplicate_id))
            $duplicate = "";
        else
            $duplicate = " (duplicate)";*/
        return ["id" => "a".$this->id,
            "text" => $this->title,// . $duplicate,
            "type" => "article"];
    }

    public static function moveOnTree(array $node){
        $articleID  = (integer) substr($node["id"], 1);
        $tabID = (integer) substr($node["parent"], 1);
        self::findOrFail($articleID)->tab()->associate(Tab::findOrFail($tabID))->save();
    }

    public static function copyOnTree(array $node){
        $articleID  = (integer) substr($node["original_id"], 1); //who is copeid
        $tabID = (integer) substr($node["parent"], 1); //parent to here
        DB::transaction(function() use($articleID, $tabID) {
            self::find($articleID)->copyToTab($tabID);
        });
//        $authors = $original->authors()->lists('id');
//        $publishers = $original->publishers()->lists('id');
//        $tags = $original->tags()->lists('id');

//        $original->duplicates()->save($duplicate) //return $duplicate
//                ->tab()->associate(Tab::findOrFail($tabID))->save();

//        $duplicate->authors()->sync($authors);
//        $duplicate->publishers()->sync($publishers);
//        $duplicate->tags()->sync($tags);
    }

    public static function deleteArticleById($id){
        if(!is_numeric(substr($id, 0, 1)))
            $id = (integer) substr($id, 1);
        $article = self::findOrFail( $id);
        $article->delete();
        //$article->delete();
        //self::destroy($id);
    }

    public function delete(){
        $this->deleteFileContents()->deleteFileCover();
        parent::delete();
    }

 //for relation by foreign
   /* public function getDuplicates(){
        $duplicates = $this->duplicates()->get();
        $original = $this->original()->first();
        if(!is_null($original)){
            $siblings = $original->duplicates()->get();
            $siblings->add($original);
        }
        else
            return $duplicates;

        if(!is_null($duplicates)){ //if exist duplicates of duplicate
            //$siblings = $siblings->except($this);
            $siblings = $duplicates->merge($siblings);
        }

        return $siblings;
    }*/

    //for gorupping by field
    public function getDuplicates(){
        return Article::where("duplicate_id", "=", $this->duplicate_id)->get();
        //return $duplicates;
    }


    public function getDestination(){
        $tab = $this->tab()->first();
        $ancestors = $tab->topic()->first()->findAncestors()->get();
        $path = "";

        if($ancestors->count() > 1) {
            for ($i = 1; $i < $ancestors->count(); $i++) {
                if($i > 1)
                    $path .= " - ";
                $path .= $ancestors[$i]->text;
            }
            $path .= " - ";
        }
        $path .= $tab->name . " - " . date('Y', strtotime($this->year));
        //if(is_null($this->duplicate_id))
          //  $path .= " (Original. If you delete this, will lost all destinations)"; //if delete original - will delete all destinations (paths)

        return $path;
    }

    public function getDestinations(){
        $articles = $this->getDuplicates();
        $destinations = [];
        $destinations[] = ["id" => $this->id, "destination" => $this->getDestination()];
        foreach($articles as $article){
            if($article->id == $this->id)
                continue;
            $destinations[] = ["id" => $article->id, "destination" => $article->getDestination()];
        }

        return $destinations;
    }

    public function updateAuthors(array $idArray = []){
        $oldRelation = $this->authors()->lists('author_id');
        $detachArr = array_diff($oldRelation, $idArray);
        if(count($detachArr) > 0)
            $this->authors()->detach($detachArr);

        $attachArr = array_diff($idArray, $oldRelation);

        //$this->authors()->sync([$attachArr => ['expires' => true]]);
        if(count($attachArr) > 0)
            $this->authors()->attach($attachArr);
    }

    public function updatePublishers(array $idArray = []){
        $oldRelation = $this->publishers()->lists('publisher_id');
        $detachArr = array_diff($oldRelation, $idArray);
        if(count($detachArr) > 0)
             $this->publishers()->detach($detachArr);
        $attachArr = array_diff($idArray, $oldRelation);
       // $this->publishers()->sync($idArray);
        if(count($attachArr) > 0)
            $this->publishers()->attach($attachArr);
    }

    public function updateTags(array $idArray = []){
        $oldRelation = $this->tags()->lists('tag_id');
        $detachArr = array_diff($oldRelation, $idArray);
        if(count($detachArr) > 0)
             $this->tags()->detach($detachArr);

        $attachArr = array_diff($idArray, $oldRelation);
        if(count($attachArr) > 0)
            $this->tags()->attach($attachArr);
    }

    //$idsArray - exist copies,  $idsTabs - tabs for new copies
    public function updateDuplicates(array $idsArticle, array $idsTabs, $years){
        DB::transaction(function() use($idsArticle, $idsTabs, $years){
            $idsOld = $this->getDuplicates()->lists('id');
            //$idsOld[] = $this->id;
            $detachArr = array_diff($idsOld, $idsArticle);            
            if(count($detachArr) > 0) {
                //Article::destroy($detachArr);
                foreach ($detachArr as $id) {
                    self::deleteArticleById($id);
                }
            }
            for($i=0; $i < count($idsTabs); $i++) {
                $this->copyToTab((integer) substr($idsTabs[$i], 1), $years[$i]);
            }
        });
    }

    public function copyToTab($tabID, $year = null){
        $duplicate = $this->replicate(); //here is duplicate_id copied
        if(!is_null($year))
            $duplicate->year = $year;

        $copiedFile = self::copyFile($duplicate->table_contents_file);
//        if(empty($copiedFile))
//            App::abort(404);
//        else
            $duplicate->table_contents_file = $copiedFile;

        $copiedFile = self::copyFile($duplicate->view_cover_file);
//        if(empty($copiedFile))
//            App::abort(404);
//        else
            $duplicate->view_cover_file = $copiedFile;

        //$this->duplicates()->save($duplicate)
        Tab::findOrFail($tabID)->articles()->save($duplicate);
        //+copy authors ...
        $authors = $this->authors()->lists('author_id');
       if(count($authors) > 0)
           $duplicate->authors()->attach($authors);
        $publishers = $this->publishers()->lists('publisher_id');
        if(count($publishers) > 0)
            $duplicate->publishers()->attach($publishers);
        $tags = $this->tags()->lists('tag_id');
        if(count($tags))
            $duplicate->tags()->attach($tags);
    }

    public static function getPathImage($isAbsolute = false){
        if($isAbsolute)
            return public_path()."/uploads/articles";
        else
            return "/uploads/articles";
    }

    public static function getFilePathImage($fileName, $isAbsolute = false){
        $fileName = trim($fileName);
        if(!empty($fileName))
            return self::getPathImage($isAbsolute) . '/' . $fileName;
        else
            return null;
    }

    public function deleteFileCover(){
        if(!empty($this->view_cover_file)) {
            $file = self::getFilePathImage($this->view_cover_file, true);
            if (file_exists($file) && !is_dir($file)) {
                unlink($file);
            }
        }
        return $this;
    }

    public function deleteFileContents(){
        if(!empty($this->table_contents_file)) {
            $file = self::getFilePathImage($this->table_contents_file, true);
            if (file_exists($file) && !is_dir($file)) {
                unlink($file);
            }
        }
        return $this;
    }

    public static function copyFile($fileName, $saveDir = ''){
        if(empty($fileName))
            return '';
        if(empty($saveDir))
            $saveDir = self::getPathImage(true);
        $saveDir .='/';
        if(file_exists($saveDir . $fileName)) {
            $newFile = self::getUniqueFileName($fileName, $saveDir);
            if (copy($saveDir . $fileName, $saveDir . $newFile)) {
                return $newFile;
            }
        }

        return ''; //false
    }

    public static function getUniqueFileName($fileName, $dirPath, $prefix='php'){
        $img_name = $prefix . substr(md5($fileName), 0, 6);
        $img_ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $filecounter = 1;
        $img_duplicate = $img_name.'.'.$img_ext;

        while(file_exists($dirPath.'/'.$img_duplicate)){
            $img_duplicate = $img_name . '_' . $filecounter++ . '.'. $img_ext;
        }
        return $img_duplicate;
    }

    public static function toDataTable($articles, $filterTitles){
        $out = [];
        //DB::transaction(function() use($articles, $filterTitles, &$out) {
            foreach ($articles as $article) {
                //Title cell
                $titleCell = "";
                switch($filterTitles){
                    case "titles":
                        $titleCell = $article->title;
                        break;
                    case "authors":
                        if($article->authors->count() > 0)
                            $titleCell = substr(implode(", ", $article->authors->lists('name')), 0, 100);
                        break;
                    case "publishers":
                        if($article->publishers->count() > 0)
                            $titleCell = substr(implode(", ", $article->publishers->lists('name')), 0, 100);
                        break;
                }
               // $titleCell = $article->title . (is_null($article->duplicate_id) ? '' : ' (duplicate)');

                $row = [];
                $row[] = isset($article->contentType) ? $article->contentType->name : '';
                $row[] =  $titleCell;//is_null($article->duplicate_id) ? '' :
                $row[] = isset($article->user) ? $article->user->initials : ''; //By
                $row[] = date("d.m.y", strtotime($article->date_publication));
                $row[] = $article->id;
                $row[] = $article->id;
                $out[] = $row;
            }
       // });

        return $out;
    }

    public function truncateSummary($limit, $break=" ", $pad="...") { 
    // return with no change if string is shorter than $limit 
      $string = $this->summary_full;
        if(strlen($string) <= $limit) return $string; 
    // is $break present between $limit and the end of the string? 
        if(false !== ($breakpoint = strpos($string, $break, $limit))) { 
          if($breakpoint < strlen($string) - 1) { 
            $string = substr($string, 0, $breakpoint) . $pad; 
          } 
        } 
        return $string; 
    }
    
  public function onlineEditionPresenter(){
    if((bool)$this->online_edition)
      return t("home.published_online");
    else 
      return t("home.published_offline");
  }
  
  public function getIconType(){
    if(is_null($this->content_type_id))
      return false;
    else 
      return $this->contentType->getIconView();
  }
  
  public function getTypeName(){
    if(is_null($this->content_type_id))
      return "";
    else 
      return $this->contentType->name;
  }
}
<?php

class Author extends BaseModel {
    //protected $table = 'authors';
    public $timestamps = true;
    protected $fillable = ['name'];

    public static function getRules()
    {
        return array(
            'name'     	=> 'required|between:2,255'
        );
    }

    public static function toDataTable($items){
    	$out = [];
        foreach ($items as $item) {
            $row = [];
            $row[] = $item->name;
            $row[] = $item->id;
            $row[] = $item->id;
            $out[] = $row;
        }
        return $out;
    }
}
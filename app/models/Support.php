<?php

class Support extends BaseModel {
    protected $fillable = ["name", "email", "phone", "inquiry"];

	public static function getRules(){
        return array(
            'name'     	    => 'required|max:255',
            "email"         => 'required|email|max:255', 
            "phone"         => 'required|max:255', 
            "inquiry"       => 'required'
        );
    }
}
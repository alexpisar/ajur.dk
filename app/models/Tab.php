<?php

class Tab extends BaseModel {

    public $timestamps = true;
    //protected $table = 'tabs';
    protected $fillable = ['name'];

    public function toTreeArray(){
       $tab = ["id" =>"t".$this->id, "text"=>$this->name,
           "type"=>"tab", "iconName" => self::getFilePathIcon($this->icon_name)];
        if($this->articles()->count() > 0)
            $tab['children'] = true;
        return $tab;
    }

    public function articles(){
        return $this->hasMany("Article");
        //return $this->belongsToMany("Article", "article_destination");
    }

    public function topic(){
        return $this->belongsTo("Topic");
    }

    public static function createTab(array $newTab){
        $parent = Topic::find((integer) $newTab["parent"]);
        $parent->tabs()->save(new self(["name" => $newTab["text"]]));
        //self::create(["name" => $newTab["text"], "topic_id" => $parent_id]);
    }

    public static function renameTab(array $newTab){
        $id  = (integer) substr($newTab["id"], 1);
        $tab = self::find($id);
        $tab->name = $newTab["text"];
        $tab->save();
    }

    public static function move(array $node){
        $id  = (integer) substr($node["id"], 1);
        $tab = self::find($id);
        Topic::find((integer) $node["parent"])->tabs()->save($tab);
    }

    public static function deleteTab($id){
        $id = (integer) substr($id,1);
        $tab = self::with('articles')->findOrFail($id);
        $tab->delete();
    }

    public function delete(){
        $this->deleteFileIcon();
        DB::transaction(function(){$this->articles->each(function($item){$item->delete();});});
        parent::delete();
    }


    public static function getArticlesTreeByID($tab_id){
        $id = (integer) substr($tab_id, 1);
        $articles = self::with('articles')->find($id)->articles()->where("published", "=", "1")->get();
        //mapping
        $articlesTree = [];
        foreach($articles as $article){
            $articlesTree[] = $article->toTreeArray();
        }

        return $articlesTree;
    }

    public static function getPathIcon($isAbsolute = false){
        if($isAbsolute)
            return public_path()."/uploads/tabs";
        else
            return "/uploads/tabs";
    }

    public static function getFilePathIcon($fileName, $isAbsolute = false){
        $fileName = trim($fileName);
        if(!empty($fileName))
            return self::getPathIcon($isAbsolute) . '/' . $fileName;
        else
            return null;
    }

    public function deleteFileIcon(){
        $file = self::getFilePathIcon($this->icon_name, true);
        if(file_exists($file) && !is_dir($file)){
            unlink($file);
        }
        return $this;
    }
    
    public function getIconView(){
      if($this->icon_name){
        return self::getFilePathIcon($this->icon_name);
      }
      else
        return null;
    }
    
    public static function getDestinationById($id){
        $tab = Tab::with("topic")->findOrFail($id);
        $ancestors = $tab->topic()->first()->findAncestors()->get();
        $path = "";

        if($ancestors->count() > 1) {
            for ($i = 1; $i < $ancestors->count(); $i++) {
                if($i > 1)
                    $path .= " - ";
                $path .= $ancestors[$i]->text;
            }
            $path .= " - ";
        }
        $path .= $tab->name;// . " - " . date('Y', strtotime($this->year));
       // if(is_null($this->duplicate_id))
          //  $path .= " (Original)"; //if delete original - will delete all destinations (paths)

        return $path;
    }

    public static function copyOnTree(array $node){
        $tabID  = (integer) substr($node["original_id"], 1); //who
        $topicID = $node["parent"]; //parent to here

        DB::transaction(function() use($topicID, $tabID) {
           $topic = Topic::find($topicID);
           self::find($tabID)->copyToTopic($topic);
        });
    }

    public function copyToTopic($topic){
        $duplicate = $this->replicate();

        //copy file icon
        if(!empty($duplicate->icon_name)){
            $copiedFile = Article::copyFile($duplicate->icon_name, self::getPathIcon(true));
            $duplicate->icon_name = $copiedFile;
        }
        //DB::transaction(function() use($topic, $duplicate) {
            //set to parent (associate)
            $topic->tabs()->save($duplicate);
            //copy children
            $this->load("articles");
            foreach ($this->articles as $article) {
                $article->copyToTab($duplicate->id);
            }
      //  });
    }
}
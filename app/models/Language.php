<?php

class Language extends BaseModel{
  
  public $timestamps = false;


  public function chose($choose = true, $save = true){
  	if(!$choose && $this->locale == Config::get("app.fallback_locale"))
  		return $this;

    $this->enabled = $choose;
    if($save) $this->save();

  // delete translations
    if(!$choose){
    	//DB::transaction(function(){  
      		Translation::where("locale", "=", $this->locale)->delete();
      			//->get()->each(function($item){$item->delete());
  	   // });
          
    }

    return $this;
  }
  
  public static function getEnabled(){
    return Language::where("enabled", "=", "1")
             ->orderBy("name")->get();
  }
}
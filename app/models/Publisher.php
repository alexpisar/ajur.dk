<?php

class Publisher extends BaseModel {
    public $timestamps = true;
    protected $fillable = ['name'];
}
<?php

class Payment extends BaseModel {

	public $timestamps = true;
 	//payment_status
    const STATUS_PAID = 'paid';
    const STATUS_PENDING = 'pending';
    const STATUS_EXCEEDED = 'exceeded';
    //payment_method
    const METHOD_CREDITCARD = 'creditcard';
    const METHOD_ONLINE_BANKING = 'online_banking';
    const METHOD_PAYMENT_SLIP = 'payment_slip';
    const METHOD_EAN = 'ean';
 	protected $fillable = ['invoice_number', 'billing_date', 'payment_due',
      'amount_vat', 'total_amount_due', 'card_number'];
    

    public static function getRules(){
        return array(
            'invoice_number'    => 'required|max:255',
            'billing_date'      =>  'date',//'date_format:d-m-Y',
            'payment_due'       =>  'date',
            'amount_vat'	    => 'required|numeric|min:0',
            'total_amount_due'	=> 'required|numeric|min:0'
        );
    }


	public function subscriberInfo()
	{
		return $this->belongsTo('SubscriberInfo');
	}
//  public static function getEmptyPayment(){
//    return new Payment;
//  }
  
  public function getStatus(){
    switch($this->payment_status){
      case Payment::STATUS_PAID:
        return t("profile.paid");
        break;
      case Payment::STATUS_PENDING:
//        return t("profile.pending");
//        break;                      
      case Payment::STATUS_EXCEEDED:
//        return t("profile.exceeded");
        return t("profile.not_paid");
        break;
    }
    return "";
  }
  
  public function getMethodPayment(){
    switch($this->payment_method){
      case Payment::METHOD_CREDITCARD:
        return t("registration.credit_card");
        break;
      case Payment::METHOD_ONLINE_BANKING:
        return t("registration.online_banking");
        break;                      
      case Payment::METHOD_PAYMENT_SLIP:
        return t("registration.payment_slip");
        break;
      case Payment::METHOD_EAN:
        return t("registration.ean_payment");
        break;
    }
    return "";
  }
  
  public function formatedCardNumber(){
    if(!$this->card_number)
      return "";
    $length = strlen($this->card_number);
    $formated = substr($this->card_number, -4);
    for($i=$length-5; $i>=0; $i--){
      if((($i+1) - $length) % 4 == 0){
        $formated = ' ' . $formated;
      }
      //put character to the begin
      $formated = $this->card_number[$i] . $formated;
    }
    return self::maskCreditCard($formated);
  }
  
  public static function maskCreditCard($cc){
    $length = strlen($cc);
    for($i=0; $i<$length-4; $i++){
      if($cc[$i] == ' '){
        continue;
      }
      $cc[$i] = 'X';
    }
    return $cc;
  }
}
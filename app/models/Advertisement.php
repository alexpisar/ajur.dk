<?php
class Advertisement extends BaseModel{

    protected $fillable = ['original_name'];

    public $timestamps = true;

    public static function getPathImage($isAbsolute = false){
        if($isAbsolute)
            return public_path()."/uploads/advertisements";
        else
            return "/uploads/advertisements";
    }

    public static function getFilePathImage($fileName, $isAbsolute = false){
        $fileName = trim($fileName);
        if(!empty($fileName))
            return self::getPathImage($isAbsolute) . '/' . $fileName;
        else
            return null;
    }

    public function getFileName(){
       // return 'img' . $this->id;
        return $this->file_name;
    }

    public function getFile($isAbsolute = false){
        $fileName = $this->getFileName();
        if(!empty($fileName))
            return self::getPathImage($isAbsolute) . '/' . $fileName;
        else
            return null;
    }

    public function delete(){
        if(!empty($this->getFileName())) {
            $file = self::getFilePathImage($this->getFileName(), true);
            if (file_exists($file) && !is_dir($file)) {
                unlink($file);
            }
        }
        return parent::delete();
    }
}
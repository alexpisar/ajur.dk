<?php

class Translation extends BaseModel{

  protected $fillable = array('locale', 'text');

  public static function getRules()
  {
    $rules = array(
               'name'       => 'required|max:1024'
      );
    return $rules;
  }


  public function translationKey(){
    return $this->belongsTo("TranslationKey");
  }


  public static function getGroup($group, $locale){
    $translations = \DB::table("translation_keys")
      ->leftJoin("translations", "translation_keys.id", "=", "translations.translation_key_id")
      ->where("translations.locale","=",$locale)
      ->where("translation_keys.group","=",$group)
      ->select(["translation_keys.key AS key", "translations.text AS text"]);
    return $translations->lists("text", "key");
  }


  public static function getGroupName($key){
    $key = explode(".", $key);
    return (string)reset($key);
  }

  public static function createNewLocale($locale){
    $keys = TranslationKey::with([
      "translations"=>function($q){
        $q->where("locale","=", Config::get("app.fallback_locale"));
      }
    ])->get();

    DB::transaction(function() use ($keys, $locale){
      foreach($keys as $key){
        $key->translations()->create([
          "locale" => $locale,
          "text" => $key->translations->first()->text
        ]);
      }
    });
    
    StaticPage::createLocale($locale);
  }

  public static function updateRecords($locale, $group, $data){
    $translations = self::with("translationKey")->whereHas("translationKey", function($q) use ($group){
      $q->where("group","=", $group);
    })
      ->where("locale","=", $locale)
      ->get();

    DB::transaction(function() use ($translations, $data){
      foreach($translations as $trans){
        $trans->text = $data[$trans->translationKey->key];
        $trans->save();
      }
    });

  }

  public static function getSectionsLocale($locale){
      if($locale == "null")
        $locale = Config::get("app.fallback_locale");
      $groupList= TranslationKey::groupBy("group")->lists("group");
      $transList = [];
      DB::transaction(function() use (&$transList, $locale, $groupList){
          foreach($groupList as $group){
              $transList[$group] =  self::getGroup($group, $locale);
          }
      });
      return $transList;
  }

  public static function changeTranslation($oldLocale, $newLocale){
    self::where("locale", "=", $oldLocale)->update(["locale" => $newLocale]);
  }
}
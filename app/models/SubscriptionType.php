<?php

class SubscriptionType extends BaseModel {

	//protected $table = 'subsription_types';
	public $timestamps = true;
    protected $fillable = array('name', 'persons', 'price', 'institutional_access');

    public static function getRules(){
	    return array(
            'name'     	    => 'required|max:255',
            'persons'	    => 'required|integer|min:0',
            'price'		    => 'required|numeric|min:0'
        );
    }

    public function subscriberInfos(){
    	return $this->hasMany("SubscriberInfo");
    }

}

(function($){
  "use strict";


  $(".sp-advanced-search").on("click", ".sp-long-filter-add-new", function (e) {
    e.preventDefault();
    var $container = $(this).parent().after( $(this).parent().clone() );
    $container.find(".sp-long-filter-add-new").hide();
    $container.find(".sp-long-filter-remove").removeClass("hidden");
    $container.next().find("select").prop("selectedIndex", 0);
    $container.next().find("input").val("");
  });

  $(".sp-advanced-search").on("click", ".sp-long-filter-remove", function (e) {
    e.preventDefault();
    $(this).parent().remove();
  });

  $(".reset-spform-button").on("click", function () {
    $(this)
      .parents("form .sp-long-filter-row")
      .slice(0,-1)
      .remove();
  });

})(jQuery);
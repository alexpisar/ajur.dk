(function(w,d,s,g,js,fs){
  g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
  js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
  js.src='https://apis.google.com/js/platform.js';
  fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
}(window,document,'script'));


(function($){
  "use strict";
  /** google analytics dropdowns work improperly when #wrap z-index:1 */
  $("#wrap").css({"z-index":0});

  var viewSelector, chartsList;

  gapi.analytics.ready(function() {

    gapi.analytics.auth.authorize({
      container: 'embed-api-auth-container',
      clientid: '426010625786-kqouf2qvt6opjdmhkfqin3l6tosuf2pk.apps.googleusercontent.com'
    });


    viewSelector = new gapi.analytics.ViewSelector({
      container: 'view-selector-container'
    });
    viewSelector.execute();

    chartsList = {
      activeUsers : new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:date',
          'start-date': $(".from-date").val(),
          'end-date': $(".to-date").val()
        },
        chart: {
          container: 'active-users-container',
          type: 'LINE',
          options: {
            width: '100%',
            title: "Users activity"
          }
        }
      }),


      newUsers : new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:newUsers',
          dimensions: 'ga:userType',
          'start-date': $(".from-date").val(),
          'end-date': $(".to-date").val()
        },
        chart: {
          container: 'new-users-container',
          type: 'LINE',
          options: {
            width: '100%',
            title: "New users"
          }
        }
      }),


      topCountries : new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:country',
          'start-date': $(".from-date").val(),
          'end-date': $(".to-date").val(),
          'max-results': 10,
          sort: '-ga:sessions'
        },
        chart: {
          container: 'top-countries-container',
          type: 'PIE',
          options: {
            width: '100%',
            title: "Top countries"
          }
        }
      }),


      topBrowsers : new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:browser',
          'start-date': $(".from-date").val(),
          'end-date': $(".to-date").val(),
          'max-results': 10,
          sort: '-ga:sessions'
        },
        chart: {
          container: 'top-browsers-container',
          type: 'PIE',
          options: {
            width: '100%',
            title: "Top browsers"
          }
        }
      })



    };


    viewSelector.on('change', refreshAllCharts);

  });


  function refreshAllCharts(ids){

    var sd = $(".from-date").val();
    var ed = $(".to-date").val();

    if( (new Date(sd)) > (new Date(ed)) ){
      sd = $(".to-date").val();
      ed = $(".from-date").val();
    }

    var query = {query: {
      'start-date': sd,
      'end-date':   ed
    }};

    if(ids) query.query.ids = ids;

    $.each(chartsList, function(){
      this.set(query).execute();
      $("#"+this.get("chart").chart.container).css({
        padding: "5px",
        background: "#FFFFFF"
      })
    });
  }


  $(document).ready(function(){
    $(".datepicker").datepicker({
      dateFormat: "yy-mm-dd",
      onSelect: function(){refreshAllCharts();}
    });
  });

}(jQuery));



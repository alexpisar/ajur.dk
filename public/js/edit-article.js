
//author handler
$("#add-author").on("click", function(e){
    e.preventDefault();
    var count = $("#countAuthors").val();
   // console.log(count);
    var itemID = $("#author-select option:selected").val();
    var itemName = $("#author-select option:selected").text();
    if(!itemID || !$.trim(itemName))
        return;

    $("#adding-authors").append('<div class="form-group">' +
    '<label class="col-sm-2 control-label">Author</label>' +
    '<div class="col-sm-9">' +
    '<input name="author_ids[]" type="hidden" value="' + itemID +'">' +
    '<input class="form-control" readonly="readonly" name="authors[]" type="text" value="'+ itemName +'"> </div>' +
    '<div class="col-sm-1">' +
    '<a href="#" role="button" class="btn btn-default width-100 delete-author">Delete</a>' +
    '</div> </div>');

    $("#countAuthors").val(++count);
    $("#author-select option:selected").remove();
    $('#author-select').selectpicker('refresh'); //refresh select$('.remove-example').selectpicker('refresh');
    //$("#author-select+div li.selected").remove();//.attr("data-hide-disabled", "true");
  //  $("#author-select+div button span").text('');
    //console.log("OK");
    //$("#selectBox option[value='option1']").remove();
});
//delete author
$("#adding-authors").on("click", ".delete-author", function(e){
    e.preventDefault();
    //  console.log("deletePublisher");
    var parent = $(this).parent().parent();
    var count = $("#countAuthors").val();
    var itemID = parent.find("input[type='hidden']").val();
    var itemName = parent.find("input[name^='authors']").val();
    $("#author-select").append($("<option></option>")
        .attr("value", itemID)
        .text(itemName));
    parent.remove();
    $("#countAuthors").val(--count);
    $('#author-select').selectpicker('refresh');
    //console.log('remove author ok');
});

//publisher handler
$("#add-publisher").on("click", function(e){
    e.preventDefault();
    var count = $("#countPublishers").val();

    var itemID = $("#publisher-select option:selected").val();
    var itemName = $("#publisher-select option:selected").text();
    if(!itemID || !$.trim(itemName))
        return;
    $("#adding-publishers").append('<div class="form-group">' +
    '<label class="col-sm-2 control-label">Publisher</label>' +
    '<div class="col-sm-9">' +
    '<input name="publisher_ids[]" type="hidden" value="' + itemID +'">' +
    '<input class="form-control" readonly="readonly" name="publishers[]" type="text" value="'+ itemName +'"> </div>' +
    '<div class="col-sm-1">' +
    '<a href="#" class="btn btn-default width-100 delete-publisher" role="button">Delete</a>' +
    '</div> </div>');

    $("#countPublishers").val(++count);
    $("#publisher-select option:selected").remove();
    $('#publisher-select').selectpicker('refresh');
    //console.log("OK");
    //$("#selectBox option[value='option1']").remove();
});

$("#adding-publishers").on("click", ".delete-publisher", function(){
    // e.preventDefault();
   // console.log("deletePublisher");
    var parent = $(this).parent().parent();
    var count = $("#countPublishers").val();
    var itemID = parent.find("input[type='hidden']").val();
    var itemName = parent.find("input[name^='publishers']").val();
    $("#publisher-select").append($("<option></option>")
        .attr("value", itemID)
        .text(itemName));
    parent.remove();
    $("#countPublishers").val(--count);
    $('#publisher-select').selectpicker('refresh');
//        console.log(itemID);
//        console.log(itemName);
});

//tag handler
$("#add-tag").on("click", function(e){
    e.preventDefault();
    var count = $("#countTags").val();

    var itemID = $("#tag-select option:selected").val();
    var itemName = $("#tag-select option:selected").text();
    if(!itemID || !$.trim(itemName))
        return;
    $("#adding-tags").append('<div class="form-group">' +
    '<label class="col-sm-2 control-label">Tag</label>' +
    '<div class="col-sm-9">' +
    '<input name="tag_ids[]" type="hidden" value="' + itemID +'">' +
    '<input class="form-control" readonly="readonly" name="tags[]" type="text" value="'+ itemName +'"> </div>' +
    '<div class="col-sm-1">' +
    '<a href="#" class="btn btn-default width-100 delete-tag" role="button">Delete</a>' +
    '</div> </div>');

    $("#countTags").val(++count);
    $("#tag-select option:selected").remove();
    $('#tag-select').selectpicker('refresh');
});

$("#adding-tags").on("click", ".delete-tag", function(){
    // e.preventDefault();
    var parent = $(this).parent().parent();
    var count = $("#countTags").val();
    var itemID = parent.find("input[type='hidden']").val();
    var itemName = parent.find("input[name^='tags']").val();
    $("#tag-select").append($("<option></option>")
        .attr("value", itemID)
        .text(itemName));
    parent.remove();
    $("#countTags").val(--count);
    $('#tag-select').selectpicker('refresh');
//        console.log(itemID);
//        console.log(itemName);
});

//destination handler
$("#add-destination").on("click", function(e){
    e.preventDefault();
    var count = $("#countDestinations").val();

    var itemID = $("#sel-tab_id").val();
    var itemName = $("#selected_tab").val();
    var tempYear = $("#year_temp").val();
    
    if(!itemID || !$.trim(itemName))
        return;
    if(!tempYear) {
        tempYear = (new Date).getFullYear();
    }

    var html = '<div class="form-group">' +
        '<label class="col-sm-2 control-label">Destination</label>' +
        '<div class="col-sm-9">' +
            '<input name="tab_ids[' + count +  ']" type="hidden" value="' + itemID + '" >' +
            '<input name="years[' + count +  ']" type="hidden" value="' + tempYear + '" >' +
            '<input name="destinations['+ count +']" class="form-control" readonly type="text" value="'+ itemName + " - " + tempYear +'" >' +
            '</div> <div class="col-sm-1">' +
            '<a href="#" role="button" class="btn btn-default width-100 delete-destination">Delete</a></div></div>';

    $("#adding-destinations").append(html);
    $("#countDestinations").val(++count);
    /*if(typeof(disableAdd) == "function") // if edit - then undefined
        disableAdd($(this));*/

    clearDestination(e);
});

$("#adding-destinations").on("click", ".delete-destination", function(e){
     e.preventDefault();
    var parent = $(this).parent().parent();
    parent.remove();
    /*if(typeof(enableAdd) == "function")
        enableAdd();*/
});

$("#clear-destination").on("click", clearDestination);

function clearDestination (e){
    e.preventDefault();
    $("#sel-tab_id").val('');
    $("#selected_tab").val('');
   // $("#year_temp").val('');
}


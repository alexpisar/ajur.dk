<?php

return array(

  /*
  |--------------------------------------------------------------------------
  | Database Connections
  |--------------------------------------------------------------------------
  |
  | Here are each of the database connections setup for your application.
  | Of course, examples of configuring each database platform that is
  | supported by Laravel is shown below to make development simple.
  |
  |
  | All database work in Laravel is done through the PHP PDO facilities
  | so make sure you have the driver for your particular database of
  | choice installed on your machine before you begin development.
  |
  */

  'mysql.driver'    => 'mysql',
  'mysql.host'      => 'localhost',
  'mysql.database'  => 'database',
  'mysql.username'  => '',
  'mysql.password'  => '',
  'mysql.charset'   => 'utf8',
  'mysql.collation' => 'utf8_unicode_ci',
  'mysql.prefix'    => '',

  /*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

  'redis.host'     => '127.0.0.1',
  'redis.port'     => 6379,
  'redis.database' => 0,

  /*
	|--------------------------------------------------------------------------
	| SMTP Server Username
	|--------------------------------------------------------------------------
	|
	| If your SMTP server requires a username for authentication, you should
	| set it here. This will get used to authenticate with your server on
	| connection. You may also set the "password" value below this one.
	|
	*/

  'mail.username' => 'alexeyDSLR@gmail.com',
  'mail.from'     => 'testuser@testbox.com',

  /*
  |--------------------------------------------------------------------------
  | SMTP Server Password
  |--------------------------------------------------------------------------
  |
  | Here you may set the password required by your SMTP server to send out
  | messages from your application. This will be given to the server on
  | connection so that the application will be able to send messages.
  |
  */

  'mail.password' => 'fooeTF7edCRIorPK5bH0Pg',

  'mail.protocol'            => 'smtp',
  'mail.host'                => 'smtp.mandrillapp.com',
  'mail.port'                => 587,
  'mail.driver'              => 'smtp',


  /**
   * Elastisearch connection data
  */

  "elasticsearch.host"       => "localhost:2915",
  "elasticsearch.index_name" => "ajur_dk",
  "elasticsearch.type_name"  => "article"

);